#!/bin/bash

#########################################################################################
#                                                                                       #
# Title         :   recette-technique-launcher.sh                                       #
# Description   :   Script used to schedule tests execution                             #
# Date          :   2013-03-18                                                          #
# Usage         :   bash recette-technique-launcher.sh <rectech|recfonc|tmc|all>        #
# Author        :   Hugo GUERINEAU                                                      #
# Contributors  :   Damien PELLISSON                                                    #
# Maintainer    :   contact@wwsight.com                                                 #
# Copyright(C)  :   WWSIGHT SARL                                                        #
#                                                                                       #
#########################################################################################

source ./LIBS/manage-options.fct

CHECK_SCRIPT_LIST=
CHECK_SCRIPT_PARAMS=
NOW=$(date +%Y%m%d-%H%M%S)

# Manage command line parameters
retrieve_options "$@"
if [[ $? -ne 0 ]]
then
    print_usage
    exit 1
fi

# Retrieve the selected method to display the check results
source ./LIBS/display-$DISPLAY.fct
# Print the check results on the screen as well as in a log file
exec &> >(tee -a LOGS/check-external-${ENVIRONNEMENT}-${TEST_TYPE}_$NOW.log)

# Retrieve the list of scripts to launch
# This list contains the selected $TEST_TYPE checks as well as the ones of all the previous steps
for test_type in $KNOWN_TEST_TYPES
do
    CHECK_SCRIPT_LIST="$CHECK_SCRIPT_LIST $(ls ./CHECKS/EXTERNAL/$test_type/$SCRIPT-check.sh 2> /dev/null)"
    [[ "$test_type" == "$TEST_TYPE" ]] && break
done

for environnement in $ENVIRONNEMENT
do
    for check_script in $CHECK_SCRIPT_LIST
    do
        check_script_directory=$(dirname $check_script)
        check_script_file=$(basename $check_script)
        check_script_name=${check_script_file:0:-9}
        check_script_config_file="CONFIGS/$check_script_name/$environnement"

        CHECK_NAME="$check_script_name"

        # Launch the script only if the server name exists in the configuration
        if ! [[ -f $check_script_config_file ]]
        then
            continue
        fi

        display_lowtitle "$check_script_name"

        cat $check_script_config_file | while read check_script_params
        do
            if $VERBOSE
            then
                (cd $check_script_directory && ./$check_script_file $check_script_params)
            else
                LOG_FILE="LOGS/check-external-${ENVIRONNEMENT}-${TEST_TYPE}-${CHECK_NAME}_${NOW}.log"
                (cd $check_script_directory && ./$check_script_file $check_script_params) >> $LOG_FILE
                case $? in
                    0 )
                        display_success "Checks end without error"
                        ;;
                    1 )
                        display_warning "Checks end with warning(s). See $LOG_FILE for further details"
                        ;;
                    * )
                        display_error "Checks end with error(s). See $LOG_FILE for further details"
                esac
            fi
        done
        echo
    done
done
