#!/bin/bash

#########################################################################################
#                                                                                       #
# Title         :   recette-technique-launcher.sh                                       #
# Description   :   Script used to schedule tests execution                             #
# Date          :   2013-03-18                                                          #
# Usage         :   bash recette-technique-launcher.sh <rectech|recfonc|tmc|all>        #
# Author        :   Hugo GUERINEAU                                                      #
# Contributors  :   Damien PELLISSON                                                    #
# Maintainer    :   contact@wwsight.com                                                 #
# Copyright(C)  :   WWSIGHT SARL                                                        #
#                                                                                       #
#########################################################################################

source ./LIBS/manage-options.fct

CHECK_SCRIPT_LIST=
CHECK_SCRIPT_PARAMS=
NOW=$(date +%Y%m%d-%H%M%S)

# Manage command line parameters
retrieve_options "$@"
if [[ $? -ne 0 ]]
then
    print_usage
    exit 1
fi

# Ask the DB passwords only if required scripts are used
if [ "${SCRIPT}" = "*" ]; then
	retrieve_passwords $ENVIRONNEMENT
else
	echo "${SCRIPT}" | grep -E "mysql" > /dev/null 2>&1
	if [ ${?} -eq 0 ]; then
		retrieve_passwords $ENVIRONNEMENT
	else
		if [[ "$TEST_TYPE" = "dinst" ]] || [[ "$TEST_TYPE" = "mco" ]] || [[ "$TEST_TYPE" = "dex" ]]; then
			echo "${SCRIPT}" | grep -E "inno-db" > /dev/null 2>&1
			if [ ${?} -eq 0 ]; then retrieve_passwords $ENVIRONNEMENT; fi
		fi
	fi
fi

# Retrieve the selected method to display the check results
source ./LIBS/display-$DISPLAY.fct
# Print the check results on the screen as well as in a log file
exec &> >(tee -a LOGS/check-${ENVIRONNEMENT}-${TEST_TYPE}_$NOW.log)

# Retrieve the list of scripts to launch
# This list contains the selected $TEST_TYPE checks as well as the ones of all the previous steps
CHECK_SCRIPT_LIST=
for test_type in $KNOWN_TEST_TYPES
do
    CHECK_SCRIPT_LIST="$CHECK_SCRIPT_LIST $(ls ./CHECKS/$test_type/$SCRIPT-check.sh 2> /dev/null)"
    [[ "$test_type" == "$TEST_TYPE" ]] && break
done

# Generate scripts by replacing "source" instructions by the related file contents
GENERATED_CHECK_SCRIPT_LIST=
trap '[[ "$GENERATED_CHECK_SCRIPT_LIST" != "" ]] && rm $GENERATED_CHECK_SCRIPT_LIST; exit 130' SIGINT
for check_script in $CHECK_SCRIPT_LIST
do
    tmp_script="/tmp/$NOW-$(basename $check_script)"
    if ! echo "$GENERATED_CHECK_SCRIPT_LIST" | grep "$tmp_script" > /dev/null
    then
        GENERATED_CHECK_SCRIPT_LIST="$GENERATED_CHECK_SCRIPT_LIST $tmp_script"
    fi
    cp $check_script $tmp_script
    sed -i "/^source ..\/..\/LIBS\/display.fct/{s|.*|cat ./LIBS/display-$DISPLAY.fct|e}" $tmp_script
    sed -i "/^source ..\/..\/LIBS\/check.fct/{s|.*|cat ./LIBS/check.fct|e}" $tmp_script
done

for environnement in $ENVIRONNEMENT
do
    # Launch the checks on each server corresponding to the environment
    for server in $(cat SERVERS_LIST/$environnement)
    do
        display_uptitle "$server configuration"
        for check_script in $GENERATED_CHECK_SCRIPT_LIST
        do
            check_script_file=$(basename $check_script)
            check_script_name=${check_script_file:16:-9}
            check_script_config_file="CONFIGS/$check_script_name/$environnement"
            check_script_launchable=1
            check_script_params=

            CHECK_NAME="$check_script_name"

            # Launch the script only if the server name exists in the configuration
            if ! [[ -f $check_script_config_file ]] || \
                ! cat $check_script_config_file | grep -E "^$server" > /dev/null
            then
                continue
            fi

            check_script_params=$(cat ${check_script_config_file} | grep -E "^$server" | cut -d ',' -f 2-)
            # Replace password variable by their corresponding value
            for pswd in ${LISTE_PASS}
            do
                check_script_params=$(echo "$check_script_params" | sed -e "s/<$pswd>/${!pswd}/g")
            done

            echo
            display_lowtitle "$check_script_name"

            if $VERBOSE
            then
                ssh $server 'bash -s' < $check_script $check_script_params
                if [[ $? -eq 255 ]]
                then
                    display_error "SSH connection refused"
                fi
            else
                LOG_FILE="LOGS/check-${ENVIRONNEMENT}-${TEST_TYPE}-${CHECK_NAME}_${NOW}.log"
                display_uptitle "$server configuration" >> $LOG_FILE
                ssh $server 'bash -s' < $check_script $check_script_params >> $LOG_FILE
                case $? in
                    0 )
                        display_success "Checks end without error"
                        ;;
                    1 )
                        display_warning "Checks end with warning(s). See $LOG_FILE for further details"
                        ;;
                    255 )
                        display_error "SSH connection refused"
                        ;;
                    * )
                        display_error "Checks end with error(s). See $LOG_FILE for further details"
                esac
            fi
        done

        echo
        echo
    done
done

# Remove generated files
[[ "$GENERATED_CHECK_SCRIPT_LIST" != "" ]] && rm $GENERATED_CHECK_SCRIPT_LIST
