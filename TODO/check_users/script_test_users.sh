#!/bin/sh

# Usage : $0 <environnement>

exec 2>$HOME/test-users/log-test-user.log

if [ -z $1 ];then
        echo "Usage : $0 <environnement_a_tester>"
        exit
fi

for i in `cat serverlist.txt`
        do
            scp $i-$1:/etc/passwd $HOME/test-users/$i-$1-passwd
            if [ $? != 0 ];then
                echo "Machine inexistante : $i-$1"
            else
                echo
                echo Machine : $i-$1
                echo Users present :
                if [ -f $HOME/test-users/$i-$1-passwd ];then
                    for j in `cat users.txt`
                        do
                            res=`cat $HOME/test-users/$i-$1-passwd | grep $j`
							if [ -z $res ];then
								echo "$j : User absent"
							else 
								echo $res
							fi
                        done
                fi
            fi
			
        done



