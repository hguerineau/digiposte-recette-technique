#! /bin/bash

#====== DEBUT DE LA CONFIGURATION ======#

#### VARIABLES DRBD
ressource="r0"				  # le nom de la ressource DRBD
volume="/dev/drbd1"			# le chemin du volume DRBD
montage="/data"				  # point de montage du volume DRBD

#### VARIABLES SYSTEME
monipv="x.x.x.x/x"			# l adresse ip qui doit suivre le PRIMARY

#### MYSQL
mysql="0"					      # prendre en charge le service mysql
mycnf="/data/my.cnf"		# chemin drbd du fichier my.cnf

#======= FIN DE LA CONFIGURATION ======#

clear
error_file=/tmp/err
script_file=/tmp/script
error() {
	RED=`tput setaf 1`;echo "${RED}[ KO ]";tput sgr0
	echo "*** ERROR ***"
	echo "- Code retour = $1"
	echo "- Messages de sortie inscrits dans $error_file"
	read -p "Press enter to exit ..."
	exit 1
}

cmd() {
	__txt="$1"
	shift
	echo -n "$__txt ... "
	echo "COMMAND : '$@'" >$error_file
	echo "---------------------------------------------------------------------------" >>$error_file
	echo "$*" >$script_file
	bash $script_file >>$error_file 2>&1
	__rt=$?
	if [ $__rt -ne 0 ]; then
		error $__rt
	fi
	GREEN=`tput setaf 2`;echo "${GREEN}[ OK ]";tput sgr0
}

CL=`tput setaf 4`;echo "${CL}DOCAPOST-DPS Administration DRBD";tput sgr0
drbd_disk_statut=`drbd-overview | awk '{print $4}'`
JN=`tput setaf 3`;echo "Statut Actuel : ${JN}" `drbdadm role $ressource |cut -d / -f1` " " $drbd_disk_statut;tput sgr0
echo ""

#on affiche les choix
PS3='> '   # le prompt
LISTE=("[1] pour passer PRIMARY" "[2] pour passer SECONDARY" "[3] pour quitter")  # liste de choix disponibles
select CHOIX in "${LISTE[@]}" ; do
	case $REPLY in
	
	1)
		# les commandes pour passer en mode PRIMARY
		JN=`tput setaf 3`;echo "Bascule en mode ${JN}Primary";tput sgr0
		cmd "Passage de $ressource en primary"  "drbdadm primary $ressource"
		sleep 1
		cmd "Montage de $volume dans $montage"  "mount -o acl $volume $montage"
		sleep 1
		cmd "Ajout IP"  "ip addr add $monipv dev eth0"
		sleep 1
		if [ $mysql == "1" ];then
			cmd "Creation lien symbolique my.cnf"   "mv /etc/my.cnf /etc/my.cnf.old && ln -sf $mycnf /etc/my.cnf"
			sleep 1
			cmd "Lancement de MYSQL"        "/etc/init.d/mysqld start"
			sleep 1
		fi
		
		echo "DRBD statut : " `drbdadm cstate $ressource` `drbdadm role $ressource` `drbdadm dstate $ressource`
		read -p "Press enter to exit ..."
		exit 1
	break
	;;
	
	2)
  
		# les commandes pour passer en mode SECONDARY
		JN=`tput setaf 3`;echo "Bascule en mode ${JN}Secondary";tput sgr0
		if [ $mysql == "1" ];then  
			cmd "Arret de MYSQL"            "/etc/init.d/mysqld stop"
			sleep 1
		fi
		cmd "Demontage de $volume monte dans $montage"  "umount $montage"
		sleep 1
		cmd "Passage de $ressource en secondary"        "drbdadm secondary $ressource"
		sleep 1
		cmd "Suppression IP"    "ip addr del $monipv dev eth0"
		echo "DRBD statut : " `drbdadm cstate $ressource` `drbdadm role $ressource` `drbdadm dstate $ressource`
		read -p "Press enter to exit ..."
		exit 1
	break
	;;
	
	3)
	exit 1
	break
	;;
	esac
done
