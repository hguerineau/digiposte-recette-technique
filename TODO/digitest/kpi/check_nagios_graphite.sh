#!/bin/bash
# test du bon fonctionnement de la sonde check_graphite
# ./check_nagios_graphite.sh -a
# ssh support-recfonc 'bash -s' <./check_nagios_graphite.sh
# ssh kpi-rectech 'bash -s' <./check_nagios_graphite.sh
# ssh kpi-tmc 'bash -s' <./check_nagios_graphite.sh


if [[ $1 == "-a" ]] ; 
then
	
	listserver="support-recfonc" 
	listserver=$listserver" kpi-rectech"
	listserver=$listserver" kpi-tmc"
	listserver=$listserver" kpi-preprod-a kpi-preprod-b"
	listserver=$listserver" kpi-prod-a kpi-prod-b"

	

	for servertmp in $listserver; do
		# grep -v ATTENTION : enleve le message d'avertissement à chaque connexion machine
		ssh $servertmp 'bash -s ' 2>$1 <check_nagios_graphite.sh | grep -v ATTENTION
		echo ""
	done
	exit
fi



#affiche le détail des tests et des erreurs
if [[ $1 == "-v" ]] || [[ $2 == "-v" ]] || [[ $3 == "-v" ]]; 
then
    VERBOSE=1
    NAGIOSSTYLEEXIT=0
else 
    VERBOSE=0
    NAGIOSSTYLEEXIT=1
fi


############## DESACTIVE LES OPTIONS CI-DESSUS qui ne passent pas lors des apples distants ##########
#@scancouleur
VERBOSE=1
NAGIOSSTYLEEXIT=0


    

#initialisation
if [[ $NAGIOSSTYLEEXIT == 0 ]] ; 
    then
    ECHOCOLORGREEN="\\033[1;32m"
    ECHOCOLORRED="\\033[1;31m"
    ECHOCOLORNORMAL="\\033[0;39m"
fi

nbResultsOK=0
nbResultsKO=0
notExpected=""
expected=""

ECHOCOLORGREEN="\\033[1;32m"
ECHOCOLORRED="\\033[1;31m"
ECHOCOLORNORMAL="\\033[0;39m"

echo $HOSTNAME | egrep 'rectech' > /dev/null 2>&1
if [ $? -eq 0 ] ;
    then 
    ENV="rectech"
fi


echo $HOSTNAME | egrep 'recfonc' > /dev/null 2>&1
if [ $? -eq 0 ] ;
    then 
	ENV="recfonc"
fi

echo $HOSTNAME | egrep 'prod' > /dev/null 2>&1
if [ $? -eq 0 ] ;
    then 
	ENV="prod"
fi

echo $HOSTNAME | egrep 'preprod' > /dev/null 2>&1
if [ $? -eq 0 ] ;
    then 
	ENV="preprod"
fi


# fonction réalisant un test netcat et renvoyant en sortie standard OK ou KO coloré
# check <nomTest    <warning    critical <code exit attendu    <message attendu   
# ex : check "test1" 80 90 0 "OK"
function check {


resultTest=$(sudo /home/nagios/check_graphite.rb --url "http://kpi-web.digiposte.local" --metric "maxSeries(*.*.cpu.*.cpu.idle)" --duration 10 --warning $2 --critical $3 )

    ncResult=$?
   #test du code d'exit 
    if [[ $ncResult -eq $4 ]] 
    then
        if  [[ $VERBOSE -eq 1 ]]
        then
            echo -e $ECHOCOLORGREEN "OK" $1 ": warning:"$2 "critical:"$3  "expectedExitCode:"$4  $ECHOCOLORNORMAL
        fi
          nbResutsOK=$(($nbResutsOK+1))
    else
        if  [[ $VERBOSE -eq 1 ]]
        then
            echo -e $ECHOCOLORRED "KO" $1 ": warning:"$2 "critical:"$3 "expectedExitCode:"$4  $ECHOCOLORNORMAL
        fi
        nbResutsKO=$(($nbResutsKO+1))
    fi
    
    

    #test du message de sortie
    echo $resultTest | egrep $5 > /dev/null 2>&1
    if [ $? -eq 0 ] ;

    then
        if  [[ $VERBOSE -eq 1 ]]
        then
            echo -e $ECHOCOLORGREEN "OK" $1 ": warning:"$2 "critical:"$3  "expectedMessage:"$5  $ECHOCOLORNORMAL
        fi
          nbResutsOK=$(($nbResutsOK+1))
    else
        if  [[ $VERBOSE -eq 1 ]]
        then
            echo -e $ECHOCOLORRED "KO" $1 ": warning:"$2 "critical:"$3 "expectedMessage:"$5   $ECHOCOLORNORMAL
        fi
        nbResutsKO=$(($nbResutsKO+1))
    fi

}






# test des seuils : x < warning < critical
test=$HOSTNAME"_check_graphite.x>warning"
check $test 1 1000 1 "WARNING" 

test=$HOSTNAME"_check_graphite.x>critical"
check $test 1 2 2 "CRITICAL" 

test=$HOSTNAME"_check_graphite.x<warning"
check $test 1000 10000 0 "OK" 


# test des seuils inversés : x > warning > critical
test=$HOSTNAME"_check_graphite.x>warning"
check $test 2 1 0 "OK" 

test=$HOSTNAME"_check_graphite.x<warning"
check $test 1000 1 1 "WARNING" 

test=$HOSTNAME"_check_graphite.x<critical"
check $test 2000 1000 2 "CRITICAL" 








if  [[ $nbResutsKO -eq 0 ]] && [[ $nbResutsOK -gt 0 ]]
    then 
    echo -e $ECHOCOLORGREEN "OK - tests success:"$nbResutsOK " | tests error:0"$nbResutsKO  $ECHOCOLORNORMAL
    exit 0;
elif [[ $nbResutsKO -gt 0 ]] && [[ $nbResutsOK -eq 0 ]]
    then 
    echo -e $ECHOCOLORRED "UNKNOWN - tests success:"$nbResutsOK " | tests error:0" $ECHOCOLORNORMAL
    exit 3;
else 
    echo -e $ECHOCOLORRED "CRITICAL - tests success:"$nbResutsOK " | tests error:0"$nbResutsKO $ECHOCOLORNORMAL
    exit 2;
fi



