#!/bin/bash

# outils de test du monitoring kpi
# SFTD : https://wiki.collaboratif-courrier.fr/confluence/display/DGPV2/SFTD+-+Monitoring+d%27Exploitation+V6
# le scipt appelle des métriques kpi au format csv et inspecte le contenu

# ./check_metric.sh >check_metric-$(date +%Y-%m-%d).txt

# usage : pour chaque test il y a 3 lignes à renseigner
# urlToCheck : url à checker (ex : https://admin.recfonc.u-post.fr:8080/render?from=06:50_20130212&until=06:51_20130212&target=*.db-recfonc.mysql.graphite.mysql_locks.immediate&format=csv)
# expected : chaine de valeur devant être présente dans la réponse à l'url
# checkKPI : fonction prenant en paramètre 2 entrées qui sont ajoutés dans le retour du test et qui permette une meilleure lisibilité

# 3 tests expected sont possibles :
#    expected=$checkMoreThan0 : vérifie qu'une valeur n'est pas nulle ET supérieure à zéro
#    expected=chainedecaractère : vérifie que la chaine de caratère est présente en sortie
#    notExpected=chainedecaractere : vérifiie que la chaine n'est pas présente

#@todo : corriger car ne marche pas sur processes
#@todo : completer les tests

checkOK="recfonc" # chaine recherchée à chaque appel de métrique
site="https://admin.recfonc.u-post.fr:8080/" # url du site

#liste des serveurs
serverList="dmz-recfonc web1-recfonc web2-recfonc worker-recfonc db-recfonc support-recfonc"


# récupéré de https://docs.google.com/spreadsheet/ccc?key=0AmFCStZdp0gSdHJ4c0sxaDYyUTF5TXNoYllEZTcwdUE#gid=2
# serveurs où sont déployés les composants
# à voir dans node puppet?
serverRPList="dmz-recfonc"
serverLBList="web1-recfonc"
serverMQList="worker-recfonc" #je n'ai trouve rabbitmq sur aucun serveur
serverApacheList="web1-recfonc web2-recfonc support-recfonc worker-recfonc dmz-recfonc"
serverJavaList="web1-recfonc web2-recfonc db1-recfonc support-recfonc"
serverTomcatList="web1-recfonc web2-recfonc db1-recfonc support-recfonc"
serverJavaBatchList="worker-recfonc" #@todo non indiqué dans document reference
serverJavaAPIList="web1-recfonc web2-recfonc worker-recfonc"
serverImapList="web1-recfonc web2-recfonc worker-recfonc" #@todo non indiqué dans document reference
serverIdxlist="db-recfonc"
serverDBList="db-recfonc"
serverWebList="web1-recfonc web2-recfonc"

#https://wiki.collaboratif-courrier.fr/confluence/pages/viewpage.action?pageId=95326721 #liste à revoir
listQueues="digiposte.exchange.dead.letters digiposte.queue.batch.routage.notif digiposte.queue.lot.index digiposte.queue.batch.routage.notif.mail digiposte.queue.batch.routage.notif.mobile pasdedoclistantlesqueuesetleurexplication" 

listJvmNameWorker="batch_indexer_document_worker batch_indexer_lot_worker batch_mail_worker batch_mobile_worker batch_routage_worker" # pas de doc listant les jvm (rechercher dans graphitei *.*.java.apps.)
listJvmNameBack="tomcat_backoffice_api tomcat_backoffice_batch" # pas de doc listant les jvm (rechercher dans graphitei *.*.java.apps.)
listJvmNameWeb="tomcat_api" # pas de doc listant les jvm (rechercher dans graphitei *.*.java.apps.)
listJvmNameidx="tomcat_solr"
listNomJobBatch="deltaIndexerJob fullIndexerJob lotIndexerJob routageJob singleDocIndexerJob singleDocDeleteJob userDocDeleteJob routageMailNotificationJob routageMobileNotificationJob" #@todo : vérifier si la liste pas dans specification

listDB="digiposte graphite" # liste des bases de données digiposte
dbdigiposte="digiposte"  # base de donnée digiposte applicative




# des dates sont calculées pour utiliser des from et untill fixes :permet re retrouver ce qui a été testé
# en fonctions du contexte de la métriques les valeurs des dates sont changées
currentDate=$(date "+%H:%M_%Y%m%d")
currentDate60min=$(date -d "60 minutes ago" "+%H:%M_%Y%m%d")
currentDate10min=$(date -d "10 minutes ago" "+%H:%M_%Y%m%d")
currentDate1min=$(date -d "1 minutes ago" "+%H:%M_%Y%m%d")
currentDate1day=$(date -d "1 days ago" "+%H:%M_%Y%m%d")
currentDate7days=$(date -d "7 days ago" "+%H:%M_%Y%m%d")

fromUntill60min="from="$currentDate60min"&until="$currentDate"&"
fromUntill10min="from="$currentDate10min"&until="$currentDate"&"
fromUntill1min="from="$currentDate1min"&until="$currentDate"&"
fromUntill1day="from="$currentDate1day"&until="$currentDate"&"
fromUntill7days="from="$currentDate7days"&until="$currentDate"&"

# proxy pandore en cas de test depuis le réseau de la poste, changer laposte avec le mot de passe pandore et pwyh295 avec l'id RH
#proxyPandore=" -x http://web.pandore.log.intra.laposte.fr:8080 -U pwyh295:laposte -u pwyh295:laposte "

# exemple d'url testée
#urlToCheck="https://admin.recfonc.u-post.fr:8080/render?from=-60minutes&until=now&target=drawAsInfinite(*.db-recfonc.business.solr.ok)&format=csv"


nbResultsOK=0
nbResultsKO=0
notExpected=""
expected=""

ECHOCOLORGREEN="\\033[1;32m"
ECHOCOLORRED="\\033[1;31m"
ECHOCOLORNORMAL="\\033[0;39m"
checkMoreThan0="ValeurSuperieureA0"


function checkKPI {

    if [ $notExpected ]
    then
        # test d'excusion de valeur
        status=`curl $proxyPandore -4 --insecure --connect-timeout 3 --max-time 3 -s -E certificat-acces-admin.pem:digiposte $urlToCheck 2>&1 /dev/null | grep $checkOK | grep $expected | grep -v $notExpected`
        return=$?
    else
        if [ "$expected" = "$checkMoreThan0" ]
        then
           # le awk renvoi ValeurSuperieurA0 si au moins une valeur est supérieur a 0 ET non vide
           # le test regarde si une des valeur est supérieure à 0, si c'est le cas c'est qu'il y a eu une prise de mesure
           
           status=`curl $proxyPandore  -4 --insecure --connect-timeout 3 --max-time 3 -s -E certificat-acces-admin.pem:digiposte $urlToCheck 2>&1 /dev/null | grep $checkOK  | sed 's/0\.0.$/0/g' |  awk -F ',' '{ if ($3>0) { key=1;} } END { if (key>0) { print "ValeurSuperieureA0"; } else  { print "0.0";}  }' | grep $checkMoreThan0;`           
            return=$?

        else
            status=`curl $proxyPandore -4 --insecure --connect-timeout 3 --max-time 3 -s -E certificat-acces-admin.pem:digiposte $urlToCheck 2>&1 /dev/null | grep $checkOK | grep $expected`
            return=$?

        fi
    fi
    if  [ $return -eq 0 ] 
    then 
            echo -e $ECHOCOLORGREEN "OK - "$1 $2" - "$urlToCheck "- EXPECTED:" $expected  $ECHOCOLORNORMAL;
            #@todo mieux gérer le not expected not expected désactivé en sortie
            #echo -e $ECHOCOLORGREEN "OK - "$1$2" - "$urlToCheck ":EXPECTED:" $expected "NOTEXPECTED " $notExpected  $ECHOCOLORNORMAL; 
            nbResutsOK=$(($nbResutsOK+1))
    else 
           echo -e $ECHOCOLORRED "KO - "$1 $2" - "$urlToCheck "- EXPECTED:" $expected  $ECHOCOLORNORMAL;  
           #echo -e $ECHOCOLORRED "KO - "$1$2" - "$urlToCheck "- EXPECTED:" $expected "NOTEXPECTED " $notExpected  $ECHOCOLORNORMAL; 
           nbResutsKO=$(($nbResutsKO+1))
    fi

    notExpected=""
    expected=""
}







# test de présence de métrique : le test des valeurs est difficile
for server in $serverList; do


    urlToCheck=$site"render?"$fromUntill60min"&target=drawAsInfinite(averageSeries(*."$server".cpu.*.cpu.system))&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "cpu.<numerocpu>.cpu.system"

    urlToCheck=$site"render?"$fromUntill60min"&target=drawAsInfinite(averageSeries(*."$server".cpu.*.cpu.nice))&format=csv"
    expected="cpu.nice" # n'a pas souvent de valeur non nulle dans les tests donc on test juste la présence
    checkKPI $server "presenc ecpu.<numerocpu>.cpu.nice"

    urlToCheck=$site"render?"$fromUntill60min"&target=drawAsInfinite(averageSeries(*."$server".cpu.*.cpu.user))&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "cpu.<numerocpu>.cpu.user"

    urlToCheck=$site"render?"$fromUntill60min"&target=drawAsInfinite(averageSeries(*."$server".cpu.*.cpu.steal))&format=csv"
    expected="cpu.steal" # n'a pas souvent de valeur non nulle dans les tests donc on test juste la présence
    checkKPI $server " presence cpu.<numerocpu>.cpu.steal"

    urlToCheck=$site"render?"$fromUntill60min"&target=drawAsInfinite(averageSeries(*."$server".cpu.*.cpu.softirq))&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "cpu.<numerocpu>.cpu.softirq"

    urlToCheck=$site"render?"$fromUntill60min"&target=drawAsInfinite(averageSeries(*."$server".cpu.*.cpu.wait))&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "cpu.<numerocpu>.cpu.wait"

    urlToCheck=$site"render?"$fromUntill60min"&target=drawAsInfinite(averageSeries(*."$server".cpu.*.cpu.interrupt))&format=csv"
    expected="cpu.interrupt" # n'a pas souvent de valeur non nulle dans les tests donc on test juste la présence
    checkKPI $server "cpu.<numerocpu>.cpu.interrupt"

    urlToCheck=$site"render?"$fromUntill60min"&target=drawAsInfinite(averageSeries(*."$server".cpu.*.cpu.idle))&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "cpu.<numerocpu>.cpu.idle"

done




# test de présence de valeur de métrique : le test des valeurs est difficile car il y a de nombreuses partitions
for server in $serverList; do

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".df.*.df_complex.used&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "df.<nompartition>.df_complex.used"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".df.*.df_complex.free&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "df.<nompartition>.df_complex.free"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".df.*.df_complex.reserved&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "df.<nompartition>.df_complex.reserved"

done 


# test de présence de métrique : le test des valeurs est difficile
for server in $serverList; do

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".disk.*.disk_octets.write&format=csv"
    expected="disk_octets.write"
    checkKPI $server "disk.<nomdisk>.disk_octets.write"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".disk.*.disk_octets.read&format=csv"
    expected="disk_octets.read"
    checkKPI $server "disk.<nomdisk>.disk_octets.read"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".disk.*.disk_merged.write&format=csv"
    expected="disk_merged.write"
    checkKPI $server "disk.<nomdisk>.disk_merged.write"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".disk.*.disk_merged.read&format=csv"
    expected="disk_merged.read"
    checkKPI $server "disk.<nomdisk>.disk_merged.read"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".disk.*.disk_time.write&format=csv"
    expected="disk_time.write"
    checkKPI $server "disk.<nomdisk>.disk_time.write"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".disk.*.disk_time.read&format=csv"
    expected="disk_time.read"
    checkKPI $server "disk.<nomdisk>.disk_time.read"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".disk.*.disk_ops.write&format=csv"
    expected="disk_ops.write"
    checkKPI $server "disk.<nomdisk>.disk_ops.write"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".disk.*.disk_ops.read&format=csv"
    expected="disk_ops.read"
    checkKPI $server "disk.<nomdisk>.disk_ops.read"

done 

# test de présence de métrique :  le test des valeurs est difficile
for server in $serverList; do

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".interface.*.if_errors.rx&format=csv"
    expected="if_errors.rx"
    checkKPI $server "interface.<nominterface>.if_errors.rx"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".interface.*.if_errors.tx&format=csv"
    expected="if_errors.tx"
    checkKPI $server "interface.<nominterfac>.if_errors.tx"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".interface.*.if_packets.rx&format=csv"
    expected="if_packets.rx"
    checkKPI $server "interface.<nominterface>.if_packets.rx"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".interface.*.if_packets.tx&format=csv"
    expected="if_packets.tx"
    checkKPI $server "interface.<nominterfac>.if_packets.tx"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".interface.*.if_octets.rx&format=csv"
    expected="if_octets.rx"
    checkKPI $server "interface.<nominterface>.if_octets.rx"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".interface.*.if_octets.tx&format=csv"
    expected="if_octets.tx"
    checkKPI $server "interface.<nominterfac>.if_octets.tx"

done


# test de présence de métrique : load des serveurs
for server in $serverList; do

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".load.load.shortterm&format=csv"
    expected="shortterm"
    checkKPI $server "présence .load.load.shortterm"

    urlToCheck=$site"render?"$fromUntill60min"&target=averageSeries(*."$server".load.load.shortterm)&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "value .load.load.shortterm"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".load.load.midterm&format=csv"
    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".load.load.midterm&format=csv"
    expected="midterm"
    checkKPI $server "presence .load.load.midtterm"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".load.load.longterm&format=csv"
    expected="longterm"
    checkKPI $server "presence .load.load.longtterm"

done



# test de présence de métrique : memoire des serveurs
for server in $serverList; do

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".memory.memory.used&format=csv"
    expected=$checkMoreThan0
    checkKPI $server " .memory.memory.used"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".memory.memory.buffered&format=csv"
    expected="memory.memory.buffered"
    checkKPI $server "presence .memory.memory.buffered"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".memory.memory.free&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "memory.memory.free"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".memory.memory.cached&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "memory.memory.cached"

done


# test de présence de métrique : ntp des serveurs
for server in $serverList; do
    
    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".ntpd.frequency_offset.loop&format=csv"
    expected="frequency_offset.loop"
    checkKPI $server "presence .ntpd.frequency_offset.loop"

    urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".ntpd.time_dispersion.*)&format=csv"
    expected=".ntpd.time_dispersion."
    checkKPI $server "presence .ntpd.time_dispersion."

    urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".ntpd.time_offset.*)&format=csv"
    expected=".ntpd.time_offset."
    checkKPI $server "presence .ntpd.time_offset."

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".ntpd.time_dispersion.loop&format=csv"
    expected="ntpd.time_dispersion.loop"
    checkKPI $server "presence ntpd.time_dispersion.loop"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".ntpd.time_dispersion.error&format=csv"
    expected="ntpd.time_dispersion.error"
    checkKPI $server "presence ntpd.time_dispersion.error"

done

# test de présence de métrique : process des serveurs
for server in $serverList; do

    urlToCheck=$site"render?"$fromUntill10min"&target=*."$server".processes.ps_state.paging&format=csv"
    expected="processes.ps_state.paging"
    checkKPI $server "presence processes.ps_state.paging"

    urlToCheck=$site"render?"$fromUntill10min"&target=*."$server".processes.ps_state.running&format=csv"
    expected="processes.ps_state.running"
    checkKPI $server "presence processes.ps_state.running"
 
    urlToCheck=$site"render?"$fromUntill10min"&target=*."$server".processes.ps_state.stopped&format=csv"
    expected="processes.ps_state.stopped"
    checkKPI $server "presence processes.ps_state.stopped"

    urlToCheck=$site"render?"$fromUntill10min"&target=*."$server".processes.ps_state.zombies&format=csv"
    expected="processes.ps_state.zombies"
    checkKPI $server "presence processes.ps_state.zombies"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".processes.ps_state.sleeping&format=csv"
    expected="processes.ps_state.sleeping"
    checkKPI $server "presence processes.ps_state.sleeping"

    urlToCheck=$site"render?"$fromUntill10min"&target=*."$server".processes.ps_state.blocked&format=csv"
    expected="processes.ps_state.blocked"
    checkKPI $server "presence processes.ps_state.blocked"
    
    urlToCheck=$site"render?"$fromUntill10min"&target=*."$server".processes.fork_rate&format=csv"
    expected="processes.fork_rate"
    checkKPI $server "presence processes.fork_rate"

done


# test de présence de métrique :  swap 
for server in $serverList; do

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".swap.swap.cached&format=csv"
    expected="swap.swap.cached"
    checkKPI $server "presence swap.swap.cached"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".swap.swap.free&format=csv"
    expected="swap.swap.free"
    checkKPI $server "presence swap.swap.free"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".swap.swap.used&format=csv"
    expected="swap.swap.used"
    checkKPI $server "presence swap.swap.used"
 
    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".swap.swap_io.in&format=csv"
    expected="swap.swap_io.in"
    checkKPI $server "presence swap.swap_io.in"

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".swap.swap_io.out&format=csv"
    expected="swap.swap_io.out"
    checkKPI $server "presence swap.swap_io.out"

done


# test de présence de métrique :  uptime 
for server in $serverList; do

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".uptime.uptime&format=csv"
    expected="uptime.uptime"
    checkKPI $server "presence uptime.uptime"

done

# test de présence de métrique :  users au sens ssh 
for server in $serverList; do

    urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".users.users&format=csv"
    expected="users.users"
    checkKPI $server "presence users.users"

done



# test de présence de métrique :  HAProxy
for server in $serverLBList; do

	# teste les haproxy nominativement sur une métrique seulenment
	metric="haproxy.counter.backend_haproxy_adherer_downtime"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="adherer"
	checkKPI $server "presence "$metric
	
	metric="haproxy.counter.backend_haproxy_admin_downtime"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="admin"
	checkKPI $server "presence "$metric
	
	metric="haproxy.counter.backend_haproxy_api_downtime"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="api"
	checkKPI $server "presence "$metric
	
	metric="haproxy.counter.backend_haproxy_emetteur_downtime"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="emetteur"
	checkKPI $server "presence "$metric
	
	metric="haproxy.counter.backend_haproxy_graphite_downtime"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="graphite"
	checkKPI $server "presence "$metric
	
	metric="haproxy.counter.backend_haproxy_mq_downtime"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="haproxy_mq"
	checkKPI $server "presence "$metric
	
	metric="haproxy.counter.backend_haproxy_mq_web_downtime"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="mq_web"
	checkKPI $server "presence "$metric
	
	metric="haproxy.counter.backend_haproxy_secure_downtime"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="secure"
	checkKPI $server "presence "$metric
	
	metric="haproxy.counter.backend_haproxy_ws_downtime"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="ws"
	checkKPI $server "presence "$metric
	
	

	#test sur les métriques générales
	
	metric="haproxy.counter.*_downtime"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="downtime"
	checkKPI $server "presence "$metric
	
	
	metric="haproxy.counter.*_retries"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="retries"
	checkKPI $server "presence "$metric
	
	metric="haproxy.counter.*_session_total"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="session_total"
	checkKPI $server "presence "$metric
	
	metric="haproxy.counter.backend_stats_downtime"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected="backend_stats_downtime"
	checkKPI $server "presence "$metric
	
	metric="haproxy.counter.backend_stats_retries"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.counter.backend_stats_session_total"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.counter.*_session_total"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="session_total"
	checkKPI $server "presence "$metric
	
	metric="haproxy.counter.frontend_stats_session_total"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
    #@todo corriger
	metric="haproxy.derive.*_bytes_in"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_bytes_in"
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_haproxy_*_bytes_out"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_bytes_out"
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_haproxy_*_denied_request"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_denied_request"
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_haproxy_*_denied_response"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_denied_response"
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_haproxy_*_error_connection"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_error_connection"
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_haproxy_*_error_response"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_error_response"
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_haproxy_*_redistributed"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_redistributed"
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_stats_bytes_in"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_stats_bytes_out"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_stats_denied_request"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_stats_denied_response"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_stats_error_connection"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_stats_error_response"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_stats_redistributed"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_stats_response_1xx"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_stats_response_2xx"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_stats_response_3xx"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_stats_response_4xx"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_stats_response_5xx"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.backend_stats_response_other"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_haproxy_*_bytes_in"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_bytes_in"
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_haproxy_*_bytes_out"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_bytes_out"
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_haproxy_*_denied_request"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_denied_request"
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_haproxy_*_denied_response"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_denied_response"
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_haproxy_*_error_request"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_error_request"
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_stats_bytes_in"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_stats_bytes_out"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_stats_denied_request"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_stats_denied_response"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_stats_error_request"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_stats_response_1xx"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_stats_response_2xx"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_stats_response_3xx"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_stats_response_4xx"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_stats_response_5xx"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.derive.frontend_stats_response_other"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.gauge.backend_haproxy_*_queue_current"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_queue_current"
	checkKPI $server "presence "$metric
	
	metric="haproxy.gauge.backend_haproxy_*_session_current"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_session_current"
	checkKPI $server "presence "$metric
	
	metric="haproxy.gauge.backend_haproxy_*_session_rate"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_session_rate"
	checkKPI $server "presence "$metric
	
	metric="haproxy.gauge.backend_stats_queue_current"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.gauge.backend_stats_session_current"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.gauge.backend_stats_session_rate"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.gauge.frontend_haproxy_*_request_rate"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_request_rate"
	checkKPI $server "presence "$metric
	
	metric="haproxy.gauge.frontend_haproxy_*_session_current"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_session_current"
	checkKPI $server "presence "$metric
	
	metric="haproxy.gauge.frontend_haproxy_*_session_rate"
	urlToCheck=$site"render?"$fromUntill1min"&target=averageSeries(*."$server".$metric)&format=csv"
	expected="_session_rate"
	checkKPI $server "presence "$metric
	
	metric="haproxy.gauge.frontend_stats_request_rate"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.gauge.frontend_stats_session_current"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="haproxy.gauge.frontend_stats_session_rate"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric

done





# test de présence de métrique :  Mysql
for server in $serverDBList; do

	for dbtmp in $listDB; do

			# test de présence des métriques
		
			metric="mysql."$dbtmp".cache_result.qcache-hits"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			
			metric="mysql."$dbtmp".cache_result.qcache-hits"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".cache_result.qcache-inserts"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".cache_result.qcache-not_cached"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".cache_result.qcache-prunes"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".cache_size.qcache"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.admin_commands"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.alter_db"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.alter_table"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.begin"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.change_db"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.commit"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.create_db"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.create_table"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.create_user"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.delete"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.drop_db"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.drop_table"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.drop_user"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.flush"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.grant"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.insert"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.lock_tables"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.rollback"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.select"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.set_option"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_collations"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_create_db"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_create_table"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_databases"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_events"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_fields"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_function_status"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_grants"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_keys"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_privileges"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_procedure_status"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_status"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_storage_engines"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_tables"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_table_status"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_triggers"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_variables"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.show_warnings"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.truncate"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.unlock_tables"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_commands.update"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_handler.commit"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_handler.delete"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_handler.read_first"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_handler.read_key"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_handler.read_last"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_handler.read_next"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_handler.read_prev"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_handler.read_rnd"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_handler.read_rnd_next"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_handler.rollback"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_handler.update"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_handler.write"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.buffer_pool_pages_data"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.buffer_pool_pages_flushed"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.buffer_pool_pages_misc"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.buffer_pool_pages_total"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.buffer_pool_read_ahead"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.buffer_pool_read_ahead_evicted"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.buffer_pool_read_requests"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.buffer_pool_reads"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.buffer_pool_write_requests"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.data_fsyncs"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.data_read"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.data_reads"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.data_writes"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.data_written"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.dblwr_pages_written"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.dblwr_writes"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.log_write_requests"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.log_writes"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.os_log_fsyncs"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.os_log_written"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.pages_created"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.page_size"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.pages_read"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.pages_written"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.rows_deleted"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.rows_inserted"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.rows_read"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_innodb.rows_updated"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_locks.immediate"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_locks.waited"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_max_used_connections"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_locks.immediate"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_locks.waited"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_octets.rx"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_octets.tx"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_opened.files"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_opened.table_definitions"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_opened.tables"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_open.files"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_open.table_definitions"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_open.tables"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_slow.queries"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".mysql_table.locks_immediate"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".threads.cached"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".threads.connected"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".threads.running"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="mysql."$dbtmp".total_threads.created"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
	done
done





# test de présence de métrique :  Métier DB
for server in $serverDBList; do
    #@todo : clarifier l'emaplecement de la métrique : pourquoi sur  un serveur?

    for dbdigipostetmp in $dbdigiposte; do

        metric="business.sql.mysql_info_tables."$dbdigipostetmp".*.data"
        urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
        expected="data"
        checkKPI $server "presence "$metric

        metric="business.sql.mysql_info_tables."$dbdigipostetmp".*.indexes"
        urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
        expected="indexes"
        checkKPI $server "presence "$metric

        metric="business.sql.mysql_info_tables."$dbdigipostetmp".*.rows"
        urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
        expected="rows"
        checkKPI $server "presence "$metric

        metric="business.sql.mysql_info_tables."$dbdigipostetmp".*.total"
        urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
        expected="total"
        checkKPI $server "presence "$metric

    
      # check d'une table, on considère ensuite qu'elles sont toutes ok si celle là est ok
        metric="business.sql.mysql_info_tables."$dbdigipostetmp".document.data"
        urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
        expected="data"
        checkKPI $server "presence "$metric
        
        metric="business.sql.mysql_info_tables."$dbdigipostetmp".document.indexes"
        urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
        expected="indexes"
        checkKPI $server "presence "$metric
        
        metric="business.sql.mysql_info_tables."$dbdigipostetmp".document.rows"
        urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
        expected="rows"
        checkKPI $server "presence "$metric
        
        metric="business.sql.mysql_info_tables."$dbdigipostetmp".document.total"
        urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
        expected="total"
        checkKPI $server "presence "$metric
        
        # check d'une table, on considère ensuite qu'elles sont toutes ok si celle là est ok : testt de valeur > 0
        metric="business.sql.mysql_info_tables."$dbdigipostetmp".document.data"
        urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
        expected=$checkMoreThan0
        checkKPI $server "valeur sup 0 "$metric
        
        metric="business.sql.mysql_info_tables."$dbdigipostetmp".document.indexes"
        urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
        expected=$checkMoreThan0
        checkKPI $server "valeur sup 0 "$metric
        
        metric="business.sql.mysql_info_tables."$dbdigipostetmp".document.rows"
        urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
        expected=$checkMoreThan0
        checkKPI $server "valeur sup 0 "$metric
        
        metric="business.sql.mysql_info_tables."$dbdigipostetmp".document.total"
        urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
        expected=$checkMoreThan0
        checkKPI $server "valeur sup 0 "$metric

    done

    #test de présence de métrique
    metric="business.sql.documents.by_size.size_00000"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_size.size_00010_00100"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_size.size_00100_00300"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_size.size_00300_00600"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_size.size_00600_01000"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_size.size_01000_02000"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_size.size_02000_05000"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_size.size_05000_07000"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_size.size_07000_10000"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_size.size_10000_more"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_size.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric


    # test de présence de valeur
    metric="business.sql.documents.by_size.size_00000"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_size.size_00010_00100"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_size.size_00100_00300"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_size.size_00300_00600"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_size.size_00600_01000"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_size.size_01000_02000"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_size.size_02000_05000"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_size.size_05000_07000"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_size.size_07000_10000"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_size.size_10000_more"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_size.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric



    # test de presence de metrique
    metric="business.sql.documents.by_user.range_0001_0002"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_user.range_0003_0005"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_user.range_0006_0010"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_user.range_0011_0020"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_user.range_0021_0030"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_user.range_0031_0050"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_user.range_0051_0080"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_user.range_0081_0150"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_user.range_0151_0300"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_user.range_0301_0600"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_user.range_0601_more"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_user.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric





    # test de  métrique non nulle
    metric="business.sql.documents.by_user.range_0001_0002"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_user.range_0003_0005"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_user.range_0006_0010"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_user.range_0011_0020"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_user.range_0021_0030"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_user.range_0031_0050"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_user.range_0051_0080"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_user.range_0081_0150"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_user.range_0151_0300"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_user.range_0301_0600"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_user.range_0601_more"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.documents.by_user.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric




    metric="business.sql.membership.lbp.status_2.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_2.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_3.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_3.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_3_notif0.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_3_notif0.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_3_notif1_inf10mn.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_3_notif1_inf10mn.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_3_notif1_sup10mn.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_3_notif1_sup10mn.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_4.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_4.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_5.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_5.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_6.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_6.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_7.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.membership.lbp.status_7.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric


    # test de metrique non nulles
    metric="business.sql.membership.lbp.status_2.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_2.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_3.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_3.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_3_notif0.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_3_notif0.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_3_notif1_inf10mn.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_3_notif1_inf10mn.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_3_notif1_sup10mn.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_3_notif1_sup10mn.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_4.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_4.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_5.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_5.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_6.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_6.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_7.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.membership.lbp.status_7.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric





    metric="business.sql.accounts.active.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.active.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.closed.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.closed.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.closing.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.closing.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.created.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.created.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.to_purge.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.to_purge.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.valid_temporary.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.valid_temporary.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.with_new_auth.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.with_new_auth.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.contribution_lapostenet.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.accounts.contribution_lapostenet.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric


    #test valeur non nulles
		metric="business.sql.accounts.active.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.active.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.closed.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.closed.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.closing.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.closing.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.created.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.created.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.to_purge.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.to_purge.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.valid_temporary.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.valid_temporary.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.with_new_auth.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.with_new_auth.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.contribution_lapostenet.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric

    metric="business.sql.accounts.contribution_lapostenet.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric




    metric="business.sql.documents.dispatch.ok.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.dispatch.ok.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.dispatch.ko.count"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="business.sql.documents.dispatch.ko.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric


    # il n'y a pas de chech de valeur non nulles pour les autres métriques car c'est le cas non nominale en recette et cela peut conduire à des faux positif
    # pour que cette valeur soit ok il faut qu'il y ait eu un routage ok depuis 7 jours
    metric="business.sql.documents.dispatch.ok.count"
    urlToCheck=$site"render?"$fromUntill7day"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "valeur sup 0 "$metric





    #@todo faire des check pour un emetteur particulier

    metric="business.sql.documents.by_sender.count.*"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected="business.sql.documents.by_sender.count"
    checkKPI $server "presence "$metric

    metric="business.sql.documents.by_sender.failed"
    urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

done


# test de présence de métrique :  apache
for server in $serverApacheList; do

	metric="apache.server_status.apache_idle_workers"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="apache.server_status.apache_requests"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="apache.server_status.apache_bytes"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="apache.server_status.apache_scoreboard.finishing"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="apache.server_status.apache_scoreboard.keepalive"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="apache.server_status.apache_scoreboard.closing"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="apache.server_status.apache_scoreboard.idle_cleanup"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="apache.server_status.apache_scoreboard.dnslookup"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="apache.server_status.apache_scoreboard.waiting"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="apache.server_status.apache_scoreboard.logging"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="apache.server_status.apache_scoreboard.sending"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="apache.server_status.apache_scoreboard.starting"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="apache.server_status.apache_scoreboard.open"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="apache.server_status.apache_scoreboard.reading"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric

done


# test de présence de métrique :  RP métier
for server in $serverRPList; do

	metric="business.apache.secure_access_log.request_time_final.average"
	urlToCheck=$site"render?"$fromUntill60min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.request_time_final.rate"
	urlToCheck=$site"render?"$fromUntill60min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.identification_302.count"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.identification_302.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.inscription_ok.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.inscription_ok.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.archive2coffre_ok.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.archive2coffre_ok.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.archive2coffre_ko.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.archive2coffre_ko.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.account_status_ok.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.account_status_ok.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.account_status_ko.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.account_status_ko.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.documents_upload.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.documents_upload.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.validations_email.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.validations_email.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.docs_affiches.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.docs_affiches.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.nb_connexion_mobile.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.nb_connexion_mobile.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.sso_lapostenet.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.sso_lapostenet.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.sso_creditmutuel.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.sso_creditmutuel.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.ip_max.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.ip_max.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.acces_partage.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.acces_partage.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.docs_telech.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.docs_telech.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.conn_success.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.conn_success.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.conn_echec.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.conn_echec.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.sso_ko.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.sso_ko.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric

done



# test de présence de métrique :  RP métier
for server in $serverRPList; do

	metric="business.apache.secure_access_log.request_time_final.average"
	urlToCheck=$site"render?"$fromUntill60min"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.request_time_final.rate"
	urlToCheck=$site"render?"$fromUntill60min"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.identification_302.count"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.identification_302.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.inscription_ok.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.inscription_ok.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.archive2coffre_ok.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.archive2coffre_ok.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.archive2coffre_ko.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.archive2coffre_ko.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.account_status_ok.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.account_status_ok.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.account_status_ko.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.account_status_ko.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.documents_upload.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.documents_upload.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.validations_email.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.validations_email.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.docs_affiches.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.docs_affiches.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.nb_connexion_mobile.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.nb_connexion_mobile.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.sso_lapostenet.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.sso_lapostenet.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.sso_creditmutuel.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.sso_creditmutuel.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.ip_max.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.ip_max.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.acces_partage.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.acces_partage.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.docs_telech.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.docs_telech.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.conn_success.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.conn_success.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.conn_echec.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.conn_echec.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.sso_ko.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.sso_ko.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric

done




# test de présence de métrique :  Métier RP secure
for server in $serverRPList; do

	metric="business.apache.secure_access_log.http_status_code.http_1xx.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_1xx.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_2xx.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_2xx.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_3xx.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_3xx.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_4xx.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_4xx.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_5xx.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_5xx.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.http_status_code.hit.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.apache.secure_access_log.http_status_code.hit.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric

done


# test de valeur de métrique :  Métier RP secure
for server in $serverRPList; do

	#non testé car n'arrive quasiment jamais
    #metric="business.apache.secure_access_log.http_status_code.http_1xx.count"
	#urlToCheck=$site"render?"$fromUntill7days"&target=sumSeries(*."$server".$metric)&format=csv"
	#expected=$checkMoreThan0
	#checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_1xx.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=sumSeries(*."$server".$metric)&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_2xx.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=sumSeries(*."$server".$metric)&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_2xx.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=sumSeries(*."$server".$metric)&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_3xx.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=sumSeries(*."$server".$metric)&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_3xx.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=sumSeries(*."$server".$metric)&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_4xx.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=sumSeries(*."$server".$metric)&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_4xx.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=sumSeries(*."$server".$metric)&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_5xx.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=sumSeries(*."$server".$metric)&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.http_status_code.http_5xx.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=sumSeries(*."$server".$metric)&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.http_status_code.hit.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=sumSeries(*."$server".$metric)&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric
	
	metric="business.apache.secure_access_log.http_status_code.hit.rate"
	urlToCheck=$site"render?"$fromUntill7days"&target=sumSeries(*."$server".$metric)&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "valeur sup 0 "$metric

done






# test de présence de métrique :  Mq 
for server in $serverMQList; do
	
	metric="rabbitmq.disk_free"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.disk_free_limit"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.fd_total"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.fd_used"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.mem_atom"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.mem_atom_used"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.mem_binary"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.mem_code"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.mem_ets"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.mem_limit"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.mem_proc"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.mem_proc_used"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.mem_used"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.messages.count"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.messages.rate"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.messages_ready.count"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.messages_ready.rate"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.messages_unacknowledged.count"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.messages_unacknowledged.rate"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.processors"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.proc_total"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.proc_used"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.run_queue"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.sockets_total"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.sockets_used"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="rabbitmq.uptime"
	urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	
	# test des queues
	for queue in $listQueues; do
			metric="rabbitmq.queues."$queue".consumers"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="rabbitmq.queues."$queue".memory"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="rabbitmq.queues."$queue".messages.count"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="rabbitmq.queues."$queue".messages.rate"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="rabbitmq.queues."$queue".messages_ready.count"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="rabbitmq.queues."$queue".messages_ready.rate"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="rabbitmq.queues."$queue".messages_unacknowledged.count"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="rabbitmq.queues."$queue".messages_unacknowledged.rate"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="rabbitmq.queues."$queue".slave_nodes.count"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
	done
done






# test de présence de métrique :  Java
for server in $serverList; do

    listJvmName=""
    
    
    #tout pourri
    echo $server | grep "worker" > /dev/null 2>&1
    if [[ $? -eq 0 ]] ;
    then
        listJvmName=$listJvmNameWorker
    fi
    echo $server | grep "web" > /dev/null 2>&1
    if [[ $? -eq 0 ]] ;
    then
        listJvmName=$listJvmNameWeb
    fi
    echo $server | grep "back" > /dev/null 2>&1
    if [[ $? -eq 0 ]] ;
    then
        listJvmName=$listJvmNameBack
    fi
    echo $server | grep "idx" > /dev/null 2>&1
    if [[ $? -eq 0 ]] ;
    then
        listJvmName=$listJvmNameIdx
    fi

    #exceptions pour recfonc
    echo $server | grep "db-recfonc" > /dev/null 2>&1
    if [[ $? -eq 0 ]] ;
    then
        listJvmName=$listJvmNameIdx
    fi
    echo $server | grep "worker-recfonc" > /dev/null 2>&1
    if [[ $? -eq 0 ]] ;
    then
        listJvmName=$listJvmNameBack" "$listJvmNameWorker
    fi

 


	
		# test des jvm
	for jvmname in $listJvmName; do
	
			metric="java.apps."$jvmname".jvm.memory.HeapMemoryUsage_used"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory.HeapMemoryUsage_committed"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory.HeapMemoryUsage_max"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory.NonHeapMemoryUsage_init"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory.NonHeapMemoryUsage_used"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory.NonHeapMemoryUsage_committed"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory.NonHeapMemoryUsage_max"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_cms_old_gem.Usage_init"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_cms_old_gem.Usage_used"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_cms_old_gem.Usage_committed"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_cms_old_gem.Usage_max"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_par_eden_space.Usage_init"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_par_eden_space.Usage_used"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_par_eden_space.Usage_committed"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_par_eden_space.Usage_max"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_par_survivor_space.Usage_init"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_par_survivor_space.Usage_used"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_par_survivor_space.Usage_committed"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_par_survivor_space.Usage_max"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_code_cache.Usage_init"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_code_cache.Usage_used"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_code_cache.Usage_committed"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_code_cache.Usage_max"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.operating_system.ProcessCpuTime"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.threading.ThreadCount"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_cms_perm_gen.Usage_init"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.memory_pool_cms_perm_gen.Usage_used"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".memory_pool_cms_perm_gen.Usage_committed"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".memory_pool_cms_perm_gen.Usage_max"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.garbage_collector_concurrent_mark_sweep.CollectionCount"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.garbage_collector_concurrent_mark_sweep.CollectionTime"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.garbage_collector_par_new.CollectionCount"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric
			
			metric="java.apps."$jvmname".jvm.garbage_collector_par_new.CollectionTime"
			urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
			expected=$metric
			checkKPI $server "presence "$metric

            # test des tomcat
            echo $jvmname | grep "tomcat" > /dev/null 2>&1
            if [[ $? -eq 0 ]] ;
            then
                
                metric="java.apps."$jvmname".catalina.data_source.numActive"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.data_source.numIdle"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.data_source.maxActive"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.executor.activeCount"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.executor.queueSize"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.executor.completedTaskCount"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.executor.maxThreads"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.global_request_processor_ajp.bytesReceived"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.global_request_processor_ajp.requestCount"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.global_request_processor_ajp.maxTime"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.global_request_processor_ajp.errorCount"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.global_request_processor_ajp.processingTime"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.global_request_processor_ajp.bytesSent"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.manager.activeSessions"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.global_request_processor_http.bytesReceived"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.global_request_processor_http.requestCount"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.global_request_processor_http.maxTime"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.global_request_processor_http.errorCount"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.global_request_processor_http.processingTime"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.global_request_processor_http.bytesSent"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.threadPool.currentThreadsBusy"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric
                
                metric="java.apps."$jvmname".catalina.threadPool.currentThreadCount"
                urlToCheck=$site"render?"$fromUntill1min"&target=*."$server".$metric&format=csv"
                expected=$metric
                checkKPI $server "presence "$metric

            fi



	done
done


# test de présence de métrique :  api
echo "il faut qu'il y ait eu des tests pour avoir des valeurs sup 0"
for server in $serverWebList; do

    #@todo : augmenter la couverture
    #@todo : tester si les valeurs sont >0
    #@todo : ajouter les métriques de connexion Oauth (non documenté mais présent dans graphite)
    apiVersion="1_0"
    apiRessourceName="document"
    apiPartenaire="recettef6"
    apiVerbehttp="GET"

    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".error_metered.count"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".timed.count"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".timed.max"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".timed.min"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric




    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".error_metered.count"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "presence "$metric

    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".timed.count"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "presence "$metric

    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".timed.max"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "presence "$metric

    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".timed.min"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "presence "$metric

done

# test de présence de métrique :  api
echo "il faut qu il y ait eu des tests pour avoir des valeurs sup 0"
for server in $serverWebList; do

    #@todo : augmenter la couverture
    #@todo : tester si les valeurs sont >0
    #@todo : ajouter les métriques de connexion Oauth (non documenté mais présent dans graphite)
    apiVersion="1_0"
    apiRessourceName="document"
    apiPartenaire="recettef6"
    apiVerbehttp="GET"

    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".error_metered.count"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".timed.count"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".timed.max"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric

    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".timed.min"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$metric
    checkKPI $server "presence "$metric




    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".error_metered.count"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "presence "$metric

    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".timed.count"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "presence "$metric

    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".timed.max"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "presence "$metric

    metric="java.api.subscriber."$apiVersion"."$apiRessourceName"."$apiPartenaire"."$apiVerbehttp".timed.min"
    urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
    expected=$checkMoreThan0
    checkKPI $server "presence "$metric



done









if  [[ $nbResutsKO -eq 0 ]] && [[ $nbResutsOK -gt 0 ]]
    then 
    echo -e $ECHOCOLORGREEN "OK - tests success:"$nbResutsOK " | tests error:"$nbResutsKO  $ECHOCOLORNORMAL
    exit 0;
elif [[ $nbResutsKO -gt 0 ]] && [[ $nbResutsOK -eq 0 ]]
    then 
    echo -e $ECHOCOLORRED "UNKNOWN - tests success:"$nbResutsOK " | tests error:"$nbResutsKO $ECHOCOLORNORMAL
    exit 3;
else 
    echo -e $ECHOCOLORRED "CRITICAL - tests success:"$nbResutsOK " | tests error:"$nbResutsKO $ECHOCOLORNORMAL
    exit 2;
fi

exit




################################################
############ test à implementer et tester
################################################


# test de présence de métrique :  java API stongbox
for server in $serverJavaAPIList; do
	
	operation="storeDocument"
	#@todo : augmenter couverture test
    #@todo ajoutere des tests de valeurs >0
	
	metric="java.api.strongbox."$operation".timed.count"
	urlToCheck=$site"render?"$fromUntill7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="java.api.strongbox."$operation".timed.max"
	urlToCheck=$site"render?"$fromUntil7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="java.api.strongbox."$operation".timed.min"
	urlToCheck=$site"render?"$fromUntil7days"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric

done







# test de présence de métrique
for server in $serverImapList; do
	
	metric="business.imap.imap_failed.count"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.imap.imap_failed.failed"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.imap.imap_inbox.count"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.imap.imap_inbox.failed"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.imap.imap_import.count"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.imap.imap_import.failed"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.imap.imap_mail2digiposte.count"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.imap.imap_mail2digiposte.failed"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.imap.imap_mail2digiposte_failed.count"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.imap.imap_mail2digiposte_failed.failed"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.imap.imap_spam.count"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.imap.imap_spam.failed"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.imap.imap_junk.count"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.imap.imap_junk.failed"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric

done

# test de présence de métrique :  indexeur
for server in $serverIdxlist; do
	
	metric="business.solr.ok"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric

	metric="business.solr.ok"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "presence "$metric
	
	metric="business.solr.critical"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.solr.warning"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.solr.failed"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric
	
	metric="business.solr.time"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$metric
	checkKPI $server "presence "$metric

	metric="business.solr.time"
	urlToCheck=$site"render?"$fromUntill1day"&target=*."$server".$metric&format=csv"
	expected=$checkMoreThan0
	checkKPI $server "presence "$metric
	
done

