#!/bin/bash

# utilitaire de test des connexions entre les machines
# usage 
# -a all scanne plusieurs machines , -v mode verbeux avec coloration syntaxique
# .check_network.sh  -v 
# .check_network.sh  -a -v 
# ssh web1-recfonc 'bash -s ' 2>1 <check_network_internal.sh | grep -v ATTENTION 

#@todo : gerer les all in one et recfonc



if [[ $1 == "-a" ]] ; 
then
	
	#serverList=$serverList" all-interop"

	serverList=$serverList" db-recfonc web1-recfonc dmz-recfonc support-recfonc worker-recfonc"
	
	
#	serverList=$serverList" dns1-rectech dns2-rectech rp1-rectech rp2-rectech lb1-rectech lb2-rectech web1-rectech web2-rectech deploy-rectech  back1-rectech back2-rectech worker1-rectech worker2-rectech db1-rectech db2-rectech kpi-rectech log-web-rectech log-collector-rectech mq1-rectech mq2-rectech nfs-rectech idx1-rectech idx2-rectech" 
	
#	serverList=$serverList" dns1-tmc dns2-tmc rp1-tmc rp2-tmc lb1-tmc lb2-tmc web1-tmc web2-tmc deploy-tmc  back1-tmc back2-tmc worker1-tmc worker2-tmc db1-tmc db2-tmc kpi-tmc log-web-tmc log-collector-tmc mq1-tmc mq2-tmc nfs-tmc idx1-tmc idx2-tmc" 


#	serverList=$serverList" dns1-prod-a nfs-prod-a mq1-prod-a mq2-prod-a worker1-prod-a worker2-prod-a db1-prod-a db2-prod-a idx1-prod-a kpi-prod-a log-web-prod-a log-collector-prod-a rp1-prod-a lb1-prod-a web11-prod-a web12-prod-a web13-prod-a dns1-prod-b nfs-prod-b mq1-prod-b mq2-prod-b worker1-prod-b worker2-prod-b db1-prod-b db2-prod-b idx1-prod-b kpi-prod-b log-web-prod-b log-collector-prod-b rp1-prod-b lb1-prod-b web21-prod-b web22-prod-b web23-prod-b"

#	serverList=$serverList" dns1-preprod-a nfs-preprod-a mq1-preprod-a mq2-preprod-a worker1-preprod-a worker2-preprod-a db1-preprod-a db2-preprod-a idx1-preprod-a kpi-preprod-a log-web-preprod-a log-collector-preprod-a rp1-preprod-a lb1-preprod-a web11-preprod-a web12-preprod-a web13-preprod-a dns1-preprod-b nfs-preprod-b mq1-preprod-b mq2-preprod-b worker1-preprod-b worker2-preprod-b db1-preprod-b db2-preprod-b idx1-preprod-b kpi-preprod-b log-web-preprod-b log-collector-preprod-b rp1-preprod-b lb1-preprod-b web21-preprod-b web22-preprod-b web23-preprod-b"


	for servertmp in $serverList; do
		# grep -v ATTENTION : enleve le message d'avertissement à chaque connexion machine
		ssh $servertmp 'bash -s ' 2>$1 <check_network_internal.sh | grep -v ATTENTION
		echo ""
	done
	exit
fi



#affiche le détail des tests et des erreurs
if [[ $1 == "-v" ]] || [[ $2 == "-v" ]] || [[ $3 == "-v" ]]; 
then
    VERBOSE=1
    NAGIOSSTYLEEXIT=0
else 
    VERBOSE=0
    NAGIOSSTYLEEXIT=1
fi

############## DESACTIVE LES OPTIONS CI-DESSUS qui ne passent pas lors des apples distants ##########
#@scancouleur
VERBOSE=1
NAGIOSSTYLEEXIT=0



#initialisation
if [[ $NAGIOSSTYLEEXIT == 0 ]] ; 
    then
    ECHOCOLORGREEN="\\033[1;32m"
    ECHOCOLORRED="\\033[1;31m"
    ECHOCOLORNORMAL="\\033[0;39m"
fi
nbResutsOK=0
nbResutsKO=0

echo $HOSTNAME | egrep 'recfonc' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then 
	ENV="recfonc"
fi
echo $HOSTNAME | egrep 'rectech' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then 
	ENV="rectech"
fi
echo $HOSTNAME | egrep 'tmc' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then 
	ENV="tmc"
fi
echo $HOSTNAME | egrep 'preprod-a' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then 
	ENV="preprod"
	SITE="-a"
fi
echo $HOSTNAME | egrep 'preprod-b' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then 
	ENV="preprod"
	SITE="-b"
fi
echo $HOSTNAME | egrep 'prod-a' > /dev/null 2>&1
if [[ $? -eq 0 ]] ; 
    then 
	ENV="prod"
	SITE="-a"
fi
echo $HOSTNAME | egrep 'prod-b' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then 
	ENV="prod"
	SITE="-b"
	
fi

#Analyse le serveur sur lequel est executé le script pour indiquer les tests de connexion à faire #par exemple webx doit se connecter à db, mq,..
echo $HOSTNAME | egrep 'dns' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "logcollector"
    "dns"
    "kpi"
    "ubuntumirror"
    "puppetmaster"
    "nfs"
    )
fi
echo $HOSTNAME | egrep 'rp' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "lb"
    "harp"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'lb' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "web"
    "back"
    "mq"
    "halb"
    "hamq"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'web' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "mq"
    "db"
    "cfe"
    "idx"
    "logcollector12201"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )   
fi
echo $HOSTNAME | egrep 'back' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "mqweb"
    "mq"	
    "db"
    "cfe"
    "idx"
    "kpiweb"
    "logweb"
    "logcollector12201"
    "ubuntumirror"
    "puppetmaster"
    "nfs"
	"centreoncoffre"
    ) 
fi
echo $HOSTNAME | egrep 'mq1' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "hamq1"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'mq2' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "hamq2"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'worker' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "mq"
    "db"
    "cfe"
    "idx"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'log-web' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "halogweb"
    "logcollectordata"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'log-collector' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "halogcollector"
    "ubuntumirror"
    "puppetmaster"
    "logwebdata"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'kpi' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "hakpi"
    "db"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'deploy' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "ssh"
    "ubuntumirror"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'nfs' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "hanfs"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'db1' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
#base master
    then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "hadb"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'db2' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
#base slave
    then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "db"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'idx' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "haidx"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'dmz-recfonc' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
then listcheckok=(
    "logcollector"
    "dns"
    "kpi"
    "ubuntumirror"
    "puppetmaster"
    "ntp"
    "lb"
    "nfs"

    )
fi

echo $HOSTNAME | egrep 'support-recfonc' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "db"
    "ubuntumirror"
    "puppetmaster"
    "logcollector"
    "kpi"
    "logcollectordata"
    "logwebdata"
    "ssh"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'web1-recfonc' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "mq"
    "db"
    "cfe"
    "nfs"
    "idx"
    "logcollector12201"
    "ubuntumirror"
    "puppetmaster"
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "web"
    "back"
    "mq"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'web2-recfonc' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "mq"
    "db"
    "cfe"
    "nfs"
    "idx"
    "logcollector12201"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'db-recfonc' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "haidx"
    "ubuntumirror"
    "puppetmaster"
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "hadb"
    "ubuntumirror"
    "puppetmaster"
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "hanfs"
    "ubuntumirror"
    "puppetmaster"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'worker-recfonc' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
then listcheckok=(
    "logcollector"
    "dns"
    "ntp"
    "kpi"
    "mq"
    "db"
    "cfe"
    "idx"
    "ubuntumirror"
    "puppetmaster"
    "mqweb"	
    "kpiweb"
    "logweb"
    "logcollector12201"
    "nfs"

    )
fi
echo $HOSTNAME | egrep 'all' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
then listcheckok=(
	"puppetmaster"
	"dns"
	"ntp"
	"lb"
	"web"
	"mq"
	"mqweb"
	"back" 
	"cfe" 
	"db" 
	"idx" 
	"kpi" 
	"kpiweb" 
	"logcollector" 
	"logcollectordata" 
	"logweb" 
	"logwebdata" 
	"nfs" 
	"ssh" 
	"ubuntumirroir" 
    	"logcollector12201"

    )
	ENV="all"
fi





# liste des connexions qui doivent être accessible sur la machine cible depuis la machine source
# ex si une machine doit se connecter à web, il faut qu'elle ait accès à tcp:443
function puppetmaster {
	check $1 "deploy.digiposte.local" "8140" "tcp" $2
}
function dns {
	
	#suppression test des dns sur les all-ini-one https://jira.collaboratif-courrier.fr/jira/browse/DGP-5429
	if [  "$ENV" != "all" ] 
	then


		check $1 "dns.digiposte.local" "53" "udp" $2
		check $1 "dns.digiposte.local" "53" "tcp" $2
		if [  "$ENV" != "recfonc" ] && [  "$ENV" != "all" ] 
	    	then 
			check $1 "dns-s.digiposte.local" "53" "udp" $2
			check $1 "dns-s.digiposte.local" "53" "tcp" $2
		fi
	fi
}
function ntp {

	#suppression test des dns sur les all-ini-one https://jira.collaboratif-courrier.fr/jira/browse/DGP-5429
	if [  "$ENV" != "all" ] 
	then
		check $1 "dns.digiposte.local" "123" "udp" $2
		if [  "$ENV" != "recfonc" ] && [  "$ENV" != "all" ] 
	    	then 
			check $1 "dns-s.digiposte.local" "123" "udp" $2
		fi
	fi
}
function lb {
	check $1 "lb.ws.digiposte.local" "443" "tcp" $2
	check $1 "lb.admin.digiposte.local" "443" "tcp" $2
	check $1 "lb.admin.digiposte.local" "8080" "tcp" $2
	check $1 "lb.emetteur.digiposte.local" "443" "tcp" $2
	check $1 "lb.adherer.digiposte.local" "443" "tcp" $2
	check $1 "lb.secure.digiposte.local" "443" "tcp" $2
	check $1 "lb.api.digiposte.local" "443" "tcp" $2
	check $1 "mq.digiposte.local" "55672" "tcp" $2

}
function web {
	
	if [  "$ENV" == "prod" ] 
    	then 
		check $1 "web11.ws.digiposte.local" "443" "tcp" $2
		check $1 "web11.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web11.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web11.secure.digiposte.local" "443" "tcp" $2
		check $1 "web11.api.digiposte.local" "443" "tcp" $2

		check $1 "web12.ws.digiposte.local" "443" "tcp" $2
		check $1 "web12.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web12.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web12.secure.digiposte.local" "443" "tcp" $2
		check $1 "web12.api.digiposte.local" "443" "tcp" $2

		check $1 "web13.ws.digiposte.local" "443" "tcp" $2
		check $1 "web13.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web13.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web13.secure.digiposte.local" "443" "tcp" $2
		check $1 "web13.api.digiposte.local" "443" "tcp" $2

		check $1 "web21.ws.digiposte.local" "443" "tcp" $2
		check $1 "web21.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web21.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web21.secure.digiposte.local" "443" "tcp" $2
		check $1 "web21.api.digiposte.local" "443" "tcp" $2

		check $1 "web22.ws.digiposte.local" "443" "tcp" $2
		check $1 "web22.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web22.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web22.secure.digiposte.local" "443" "tcp" $2
		check $1 "web22.api.digiposte.local" "443" "tcp" $2

		check $1 "web23.ws.digiposte.local" "443" "tcp" $2
		check $1 "web23.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web23.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web23.secure.digiposte.local" "443" "tcp" $2
		check $1 "web23.api.digiposte.local" "443" "tcp" $2
	elif [ "$ENV" == "preprod" ]
	then
		check $1 "web11.ws.digiposte.local" "443" "tcp" $2
		check $1 "web11.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web11.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web11.secure.digiposte.local" "443" "tcp" $2
		check $1 "web11.api.digiposte.local" "443" "tcp" $2

		check $1 "web12.ws.digiposte.local" "443" "tcp" $2
		check $1 "web12.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web12.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web12.secure.digiposte.local" "443" "tcp" $2
		check $1 "web12.api.digiposte.local" "443" "tcp" $2

		check $1 "web13.ws.digiposte.local" "443" "tcp" $2
		check $1 "web13.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web13.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web13.secure.digiposte.local" "443" "tcp" $2
		check $1 "web13.api.digiposte.local" "443" "tcp" $2

		check $1 "web21.ws.digiposte.local" "443" "tcp" $2
		check $1 "web21.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web21.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web21.secure.digiposte.local" "443" "tcp" $2
		check $1 "web21.api.digiposte.local" "443" "tcp" $2

		check $1 "web22.ws.digiposte.local" "443" "tcp" $2
		check $1 "web22.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web22.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web22.secure.digiposte.local" "443" "tcp" $2
		check $1 "web22.api.digiposte.local" "443" "tcp" $2

		check $1 "web23.ws.digiposte.local" "443" "tcp" $2
		check $1 "web23.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web23.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web23.secure.digiposte.local" "443" "tcp" $2
		check $1 "web23.api.digiposte.local" "443" "tcp" $2
	elif [ "$ENV" == "tmc" ]
	then
		check $1 "web1.ws.digiposte.local" "443" "tcp" $2
		check $1 "web1.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web1.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web1.secure.digiposte.local" "443" "tcp" $2
		check $1 "web1.api.digiposte.local" "443" "tcp" $2

		check $1 "web2.ws.digiposte.local" "443" "tcp" $2
		check $1 "web2.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web2.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web2.secure.digiposte.local" "443" "tcp" $2
		check $1 "web2.api.digiposte.local" "443" "tcp" $2

		check $1 "web3.ws.digiposte.local" "443" "tcp" $2
		check $1 "web3.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web3.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web3.secure.digiposte.local" "443" "tcp" $2
		check $1 "web3.api.digiposte.local" "443" "tcp" $2

	elif [ "$ENV" == "rectech" ]
	then
		check $1 "web1.ws.digiposte.local" "443" "tcp" $2
		check $1 "web1.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web1.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web1.secure.digiposte.local" "443" "tcp" $2
		check $1 "web1.api.digiposte.local" "443" "tcp" $2

		check $1 "web2.ws.digiposte.local" "443" "tcp" $2
		check $1 "web2.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web2.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web2.secure.digiposte.local" "443" "tcp" $2
		check $1 "web2.api.digiposte.local" "443" "tcp" $2

	elif [ "$ENV" == "recfonc" ]
	then
		check $1 "web1.ws.digiposte.local" "443" "tcp" $2
		check $1 "web1.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web1.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web1.secure.digiposte.local" "443" "tcp" $2
		check $1 "web1.api.digiposte.local" "443" "tcp" $2

		check $1 "web2.ws.digiposte.local" "443" "tcp" $2
		check $1 "web2.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web2.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web2.secure.digiposte.local" "443" "tcp" $2
		check $1 "web2.api.digiposte.local" "443" "tcp" $2
	else
		check $1 "web1.ws.digiposte.local" "443" "tcp" $2
		check $1 "web1.emetteur.digiposte.local" "443" "tcp" $2
		check $1 "web1.adherer.digiposte.local" "443" "tcp" $2
		check $1 "web1.secure.digiposte.local" "443" "tcp" $2
		check $1 "web1.api.digiposte.local" "443" "tcp" $2
	fi

}

function hadb {

	tmp="les tests drbd ne fonctionnent pas avec netcat"
	#@todo:touver une solution
	#if [  "$ENV" == "prod" ] 
    	#then     
	#    check $1 "db1-prod-a" "7789" "tcp" $2
    	#    check $1 "db1-prod-b" "7789" "tcp" $2
	#elif [ "$ENV" == "preprod" ]
	#then
	#    check $1 "db1-preprod-a" "7789" "tcp" $2
    	#    check $1 "db1-preprod-b" "7789" "tcp" $2	
	#else
	#    tmp="nodrbd"
	#fi
}
function haidx {
	tmp="les tests drbd ne fonctionnent pas avec netcat"
	#@todo:touver une solution
	#if [  "$ENV" == "prod" ] 
    	#then 
	#    check $1 "idx1-prod-a" "7789" "tcp" $2
    	#    check $1 "idx1-prod-b" "7789" "tcp" $2
	#elif [ "$ENV" == "preprod" ]
	#then
	#    check $1 "idx1-preprod-a" "7789" "tcp" $2
    	#    check $1 "idx1-preprod-b" "7789" "tcp" $2	
	#elif [ "$ENV" == "rectech" ]
	#then
	#    check $1 "idx1-rectech" "7789" "tcp" $2
    	#    check $1 "idx2-rectech" "7789" "tcp" $2
	#    check $1 "idx1-rectech" "7789" "udp" $2
    	#    check $1 "idx2-rectech" "7789" "udp" $2
	#	#si udp retenu et pas tcp mettre les memes tests pour tout le monde
	#else
	#    tmp="nodrbd"
	#fi
}
function halb {
	## utilise du braodcat/multicast non testable par netcat
	#@todo:touver une solution pour tester à priori intestable
	tmp=""
	#check $1 "lb1-env-@todo-keep-alived-"$ENV "??" "tcp" $2
	#check $1 "lb2-env-@todo-keep-alived-"$ENV "??" "tcp" $2
}
function hamq {
	check $1 "mq1.digiposte.local" "55672" "tcp" $2
	check $1 "mq2.digiposte.local" "55672" "tcp" $2
}
function mq {
	check $1 "mq.digiposte.local" "55672" "tcp" $2
}
function mqweb {
	check $1 "mq.digiposte.local" "55672" "tcp" $2
}
function hamq1 {
	tmp="les tests drbd ne fonctionnent pas avec netcat"
	#@todo:touver une solution pour tester
	#if [  "$ENV" == "prod" ] 
    	#then 
	#    check $1 "mq1-prod-a" "7789" "tcp" $2
    	#    check $1 "mq1-prod-b" "7789" "tcp" $2
	#elif [ "$ENV" == "preprod" ]
	#then
	#    check $1 "mq1-preprod-a" "7789" "tcp" $2
    	#    check $1 "mq1-preprod-b" "7789" "tcp" $2	
	#else
	#    tmp="nodrbd"
	#fi
}
function hamq2 { 
	tmp="les tests drbd ne fonctionnent pas avec netcat"
	#@todo:touver une solution pour tester
	#if [  "$ENV" == "prod" ] 
    	#then 
	#    check $1 "mq2-prod-a" "7789" "tcp" $2
    	#    check $1 "mq2-prod-b" "7789" "tcp" $2
	#elif [ "$ENV" == "preprod" ]
	#then
	#    check $1 "mq2-preprod-a" "7789" "tcp" $2
    	#    check $1 "mq2-preprod-b" "7789" "tcp" $2	
	#else
	#    tmp="nodrbd"
	#fi
}
function harp {
	tmp=""
	#@todo:touver une solution pour tester
	## gestion par brodcast/multicat non testable 
	#check $1 "rp1-env-@todo-keep-alived-"$ENV "??" "tcp" $2
	#check $1 "rp2-env-@todo-keep-alived-"$ENV "??" "tcp" $2
}
function hanfs {
	tmp="les tests drbd ne fonctionnent pas avec netcat"
	#@todo:touver une solution pour tester
	#if [  "$ENV" == "prod" ] 
    	#then 
	#    check $1 "nfs-prod-a" "7789" "tcp" $2
    	#    check $1 "nfs-prod-b" "7789" "tcp" $2
	#elif [ "$ENV" == "preprod" ]
	#then
	#    check $1 "nfs-preprod-a" "7789" "tcp" $2
    	#    check $1 "nfs-preprod-b" "7789" "tcp" $2	
	#else
	#    tmp="nodrbd"
	#fi
}
function halogweb {
	tmp="les tests drbd ne fonctionnent pas avec netcat"
	#@todo:touver une solution pour tester
	#if [  "$ENV" == "prod" ] 
    	#then 
	#    check $1 "log-web-prod-a" "7789" "tcp" $2
    	#    check $1 "log-web-prod-b" "7789" "tcp" $2
	#elif [ "$ENV" == "preprod" ]
	#then
	#    check $1 "log-web-preprod-a" "7789" "tcp" $2
    	#    check $1 "log-web-preprod-b" "7789" "tcp" $2	
	#else
	#    tmp="nodrbd"
	#fi
}
function halogcollector {
	tmp="les tests drbd ne fonctionnent pas avec netcat"
	#@todo:touver une solution pour tester
	#if [  "$ENV" == "prod" ] 
    	#then 
	#    check $1 "log-collector-prod-a" "7789" "tcp" $2
    	#    check $1 "log-collector-prod-b" "7789" "tcp" $2
	#elif [ "$ENV" == "preprod" ]
	#then
	#    check $1 "log-collector-preprod-a" "7789" "tcp" $2
    	#    check $1 "log-collector-preprod-b" "7789" "tcp" $2	
	#else
	#    tmp="nodrbd"
	#fi
}
function hakpi {
	tmp="les tests drbd ne fonctionnent pas avec netcat"
	#@todo:touver une solution pour tester
	#if [  "$ENV" == "prod" ] 
    	#then 
	#    check $1 "kpi-prod-a" "7789" "tcp" $2
    	#    check $1 "kpi-prod-b" "7789" "tcp" $2
	#elif [ "$ENV" == "preprod" ]
	#then
	#    check $1 "kpi-preprod-a" "7789" "tcp" $2
    	#    check $1 "kpi-preprod-b" "7789" "tcp" $2	
	#else
	#    tmp="nodrbd"
	#fi
}

function back {
	check $1 "back1.admin.digiposte.local" "443" "tcp" $2
	if [  "$ENV" == "prod" ] 
    	then 
	    check $1 "back2.admin.digiposte.local" "443" "tcp" $2
	elif [ "$ENV" == "preprod" ]
	then
	    check $1 "back2.admin.digiposte.local" "443" "tcp" $2	
	elif [ "$ENV" == "rectech" ]
	then
	    check $1 "back2.admin.digiposte.local" "443" "tcp" $2
	else
	    tmp="nodrbd"
	fi
    
}
function cfe {
    check $1 "coffre.digiposte.local" "443" "tcp" $2
}
function db {
    check $1 "db.digiposte.local" "3306" "tcp" $2
    check $1 "db-slave.digiposte.local" "3306" "tcp" $2
}
function idx {
    check $1 "idx.digiposte.local" "8414" "tcp" $2
}
function kpi {
    check $1 "kpi.digiposte.local" "2003" "tcp" $2
    check $1 "kpi.digiposte.local" "2003" "udp" $2
}
function kpiweb {
    check $1 "kpi-web.digiposte.local" "80" "tcp" $2
}
function logcollector {
    check $1 "log-collector.digiposte.local" "514" "udp" $2
    check $1 "es.digiposte.local" "514" "udp" $2
}
function logcollector12201 {
    check $1 "log-collector.digiposte.local" "12201" "udp" $2
}
function logcollectordata {
    check $1 "es.digiposte.local" "9200" "tcp" $2
}
function logweb {
    check $1 "log-web.digiposte.local" "80" "tcp" $2
}
function logwebdata {
    check $1 "mongo.digiposte.local" "27017" "tcp" $2
}

function nfs {
	#tout est ouvert pour nfs, on teste seulement quelques flux
	#http://prefetch.net/blog/index.php/2010/11/02/firewalling-a-linux-nfs-server-with-iptables/
	#@todo les tests par netcat en tcp ne fonctionnent pas
    #check $1 "nfs.digiposte.local" "111" "udp" $2
    #check $1 "nfs.digiposte.local" "111" "tcp" $2
    #check $1 "nfs.digiposte.local" "2049" "udp" $2
    #check $1 "nfs.digiposte.local" "2049" "tcp" $2
    #check $1 "nfs.digiposte.local" "1050" "udp" $2
    #check $1 "nfs.digiposte.local" "1050" "tcp" $2
    #check $1 "nfs.digiposte.local" "1051" "udp" $2
    #check $1 "nfs.digiposte.local" "1051" "tcp" $2
    #check $1 "nfs.digiposte.local" "1053" "udp" $2
    #check $1 "nfs.digiposte.local" "1053" "tcp" $2
	tmp=""
}
function ssh {
	if [  "$ENV" == "prod" ] 
    	then 
		check $1 "rp1-prod-a" "22" "tcp" $2
		check $1 "rp1-prod-b" "22" "tcp" $2
		check $1 "dns1-preprd-a" "22" "tcp" $2
		check $1 "dns1-preprd-b" "22" "tcp" $2
		check $1 "back1-prod-a" "22" "tcp" $2
		check $1 "back1-prod-b" "22" "tcp" $2
		check $1 "web11-prod-a" "22" "tcp" $2
		check $1 "web21-prod-a" "22" "tcp" $2
		check $1 "web31-prod-a" "22" "tcp" $2
		check $1 "web21-prod-b" "22" "tcp" $2
		check $1 "web22-prod-b" "22" "tcp" $2
		check $1 "web22-prod-b" "22" "tcp" $2
		check $1 "idx1-prod-a" "22" "tcp" $2
		check $1 "idx1-prod-b" "22" "tcp" $2
		check $1 "db1-prod-a" "22" "tcp" $2
		check $1 "db2-prod-a" "22" "tcp" $2
		check $1 "db1-prod-b" "22" "tcp" $2
		check $1 "db2-prod-b" "22" "tcp" $2
		check $1 "lb1-prod-a" "22" "tcp" $2
		check $1 "lb1-prod-b" "22" "tcp" $2
		check $1 "kpi-prod-a" "22" "tcp" $2
		check $1 "kpi-prod-b" "22" "tcp" $2
		check $1 "log-collector-prod-a" "22" "tcp" $2
		check $1 "log-collector-prod-b" "22" "tcp" $2
		check $1 "log-web-prod-a" "22" "tcp" $2
		check $1 "log-web-prod-b" "22" "tcp" $2
		check $1 "nfs-prod-a" "22" "tcp" $2
		check $1 "nfs-prod-b" "22" "tcp" $2
		check $1 "mq1-prod-a" "22" "tcp" $2
		check $1 "mq2-prod-a" "22" "tcp" $2
		check $1 "mq1-prod-b" "22" "tcp" $2
		check $1 "mq2-prod-b" "22" "tcp" $2
	   	check $1 "worker1-prod-a" "22" "tcp" $2
	   	check $1 "worker2-prod-a" "22" "tcp" $2   
	   	check $1 "worker1-prod-b" "22" "tcp" $2
	   	check $1 "worker2-prod-b" "22" "tcp" $2 

	elif [ "$ENV" == "preprod" ]
	then
	    	check $1 "rp1-preprod-a" "22" "tcp" $2
	    	check $1 "rp1-preprod-b" "22" "tcp" $2
	    	check $1 "dns1-preprd-a" "22" "tcp" $2
		check $1 "dns1-preprd-b" "22" "tcp" $2
	   	check $1 "back1-preprod-a" "22" "tcp" $2
	   	check $1 "back1-preprod-b" "22" "tcp" $2
	   	check $1 "web11-preprod-a" "22" "tcp" $2
	    	check $1 "web21-preprod-a" "22" "tcp" $2
	    	check $1 "web31-preprod-a" "22" "tcp" $2
	   	check $1 "web21-preprod-b" "22" "tcp" $2
	    	check $1 "web22-preprod-b" "22" "tcp" $2
	    	check $1 "web23-preprod-b" "22" "tcp" $2
		check $1 "idx1-preprod-a" "22" "tcp" $2
		check $1 "idx1-preprod-b" "22" "tcp" $2
	    	check $1 "db1-preprod-a" "22" "tcp" $2
	    	check $1 "db2-preprod-a" "22" "tcp" $2
	    	check $1 "db1-preprod-b" "22" "tcp" $2
	    	check $1 "db2-preprod-b" "22" "tcp" $2	    
		check $1 "lb1-preprod-a" "22" "tcp" $2
		check $1 "lb1-preprod-b" "22" "tcp" $2
		check $1 "kpi-preprod-a" "22" "tcp" $2
		check $1 "kpi-preprod-b" "22" "tcp" $2
		check $1 "log-collector-preprod-a" "22" "tcp" $2
		check $1 "log-collector-preprod-b" "22" "tcp" $2
		check $1 "log-web-preprod-a" "22" "tcp" $2
		check $1 "log-web-preprod-b" "22" "tcp" $2
		check $1 "nfs-preprod-a" "22" "tcp" $2
		check $1 "nfs-preprod-b" "22" "tcp" $2
		check $1 "mq1-preprod-a" "22" "tcp" $2
		check $1 "mq2-preprod-a" "22" "tcp" $2
		check $1 "mq1-preprod-b" "22" "tcp" $2
		check $1 "mq2-preprod-b" "22" "tcp" $2	 
	   	check $1 "worker1-preprod-a" "22" "tcp" $2
	   	check $1 "worker2-preprod-a" "22" "tcp" $2   
	   	check $1 "worker1-preprod-b" "22" "tcp" $2
	   	check $1 "worker2-preprod-b" "22" "tcp" $2 

	elif [ "$ENV" == "rectech" ]
	then
		check $1 "rp1-"$ENV "22" "tcp" $2
		check $1 "rp2-"$ENV "22" "tcp" $2
	   	check $1 "dns1-"$ENV "22" "tcp" $2
	    	check $1 "dns2-"$ENV "22" "tcp" $2
	   	check $1 "web1-"$ENV "22" "tcp" $2
	    	check $1 "web2-"$ENV "22" "tcp" $2
		check $1 "idx1-"$ENV "22" "tcp" $2
		check $1 "idx2-"$ENV "22" "tcp" $2
	    	check $1 "db1-"$ENV "22" "tcp" $2
	    	check $1 "db2-"$ENV "22" "tcp" $2
		check $1 "lb1-"$ENV "22" "tcp" $2
		check $1 "lb2-"$ENV "22" "tcp" $2
		check $1 "kpi-"$ENV "22" "tcp" $2
		check $1 "log-collector-"$ENV "22" "tcp" $2
		check $1 "log-web-"$ENV "22" "tcp" $2
		check $1 "nfs-"$ENV "22" "tcp" $2
		check $1 "mq1-"$ENV "22" "tcp" $2
		check $1 "mq2-"$ENV "22" "tcp" $2	
	   	check $1 "back1-"$ENV "22" "tcp" $2
	   	check $1 "back2-"$ENV "22" "tcp" $2
	   	check $1 "worker1-"$ENV "22" "tcp" $2
	   	check $1 "worker2-"$ENV "22" "tcp" $2

	elif [ "$ENV" == "tmc" ]
	then
		check $1 "rp1-"$ENV "22" "tcp" $2
		check $1 "rp2-"$ENV "22" "tcp" $2
	   	check $1 "dns1-"$ENV "22" "tcp" $2
	    	check $1 "dns2-"$ENV "22" "tcp" $2
	   	check $1 "web1-"$ENV "22" "tcp" $2
	    	check $1 "web2-"$ENV "22" "tcp" $2
	    	check $1 "web3-"$ENV "22" "tcp" $2 #tmc seulement
		check $1 "idx1-"$ENV "22" "tcp" $2
		check $1 "idx2-"$ENV "22" "tcp" $2
	    	check $1 "db1-"$ENV "22" "tcp" $2
	    	check $1 "db2-"$ENV "22" "tcp" $2
		check $1 "lb1-"$ENV "22" "tcp" $2
		check $1 "lb2-"$ENV "22" "tcp" $2
		check $1 "kpi-"$ENV "22" "tcp" $2
		check $1 "log-collector-"$ENV "22" "tcp" $2
		check $1 "log-web-"$ENV "22" "tcp" $2
		check $1 "nfs-"$ENV "22" "tcp" $2
		check $1 "mq1-"$ENV "22" "tcp" $2
		check $1 "mq2-"$ENV "22" "tcp" $2	
	   	check $1 "back1-"$ENV "22" "tcp" $2
	   	check $1 "back2-"$ENV "22" "tcp" $2
	   	check $1 "worker1-"$ENV "22" "tcp" $2
	   	check $1 "worker2-"$ENV "22" "tcp" $2

	elif [ "$ENV" == "recfonc" ]
	then
		check $1 "dmz-"$ENV "22" "tcp" $2
		check $1 "support-"$ENV "22" "tcp" $2
	   	check $1 "web1-"$ENV "22" "tcp" $2
	    	check $1 "web2-"$ENV "22" "tcp" $2
	    	check $1 "db-"$ENV "22" "tcp" $2
		check $1 "worker-"$ENV "22" "tcp" $2
	else
	    tmp="nossh"
	fi
}
function ubuntumirror {
    check $1 "172.14.1.25" "80" "tcp" $2
}

function centreoncoffre {
    	
	if [  "$ENV" == "prod" ] 
	then
		check $1 "172.19.1.231" "443" "tcp" $2
		check $1 "centreon.digiposte.local" "443" "tcp" $2

	elif [ "$ENV" == "tmc" ]
	then
		check $1 "172.17.1.231" "443" "tcp" $2
		check $1 "centreon.digiposte.local" "443" "tcp" $2
	fi

}


function kotest {
    #check $1 "localhost" "22" "tcp" $2
    #check $1 "localhost" "21" "tcp" $2
    #check $1 "localhost" "8033" "tcp" $2
    #check $1 "localhost" "1-79" "tcp" $2
    #check $1 "localhost" "80-80" "tcp" $2
    #check $1 "localhost" "80-81" "tcp" $2
    #check $1 "localhost" "1-81" "udp" $2
	tmp=""
}



# fonction réalisant un test netcat et renvoyant en sortie standard OK ou KO coloré
# check <nomTest    <resultatAttendu    <url/IP    <port    <protocole    <nomService   
# check "open" "localhost" "80" "tcp" "webTest" 
function check {

    if [[ $1 == "open" ]] ; 
        then expectedncResult=0;
        else expectedncResult=1;
    fi
    url=$2;
    port=$3;
    serviceName=$5;

    if [[ $4 == "udp" ]] ; 
        then protocol="-u";
	else protocol="  ";
    fi

	#on ne fait pas le test si on est en mode test de vip seulement
	#if [[ $testVipOnly == "true" ]] ; 
	#then 
	#	echo $url | egrep -v 'digiposte.local' > /dev/null 2>&1
	#	if [[ $? -eq 0 ]] ;
	#	then
	#	 return 1
	#	echo "exit"
	#	fi
	#fi

    
    # lancement du test netcat # 2>/dev/null pour eviter les nc: getaddrinfo: Name or service not known
    ncCommand="nc -z -w 5 "
    $ncCommand $protocol $url $port 2>/dev/null
    ncResult=$?

    
    if [[ $ncResult -eq $expectedncResult ]] 
    then
        if  [[ $VERBOSE -eq 1 ]]
        then
            echo -e $ECHOCOLORGREEN "OK" $1 ": " $ncCommand "-v "$protocol $url $port" : "$4 $HOSTNAME "-->" $serviceName $ECHOCOLORNORMAL
        fi
          nbResutsOK=$(($nbResutsOK+1))
    else
        if  [[ $VERBOSE -eq 1 ]]
        then
            echo -e $ECHOCOLORRED "KO" $1 ": " $ncCommand "-v "$protocol $url $port" : " $4 $HOSTNAME "-->" $serviceName $ECHOCOLORNORMAL
        fi
        nbResutsKO=$(($nbResutsKO+1))
    fi
}




#liste de tous les tests à réaliser
testList=(
	"puppetmaster"
	"dns"
	"ntp"
	"lb"
	"web"
	"hadb"
	"haidx"
	"halb"
	"hamq"
	"mq"
	"mqweb"
	"hamq1"
	"hamq2"
	"harp"
	"hanfs"
	"halobweb" 
	"halogcollector" 
	"hakpi" 
	"back" 
	"cfe" 
	"db" 
	"idx" 
	"kpi" 
	"kpiweb" 
	"logcollector" 
	"logcollectordata" 
	"logweb" 
	"logwebdata" 
	"nfs" 
	"ssh" 
	"ubuntumirroir" 
    	"logcollector12201"
	"centreoncoffre"
)
#@todo ajouterkotest pour vérifier les ports fermés


#lance les checks OK ou KO
for itemTestList in "${testList[@]}"
do
    expectedNetworkTestTesult="close"
    testToDo=$itemTestList

    for j in "${listcheckok[@]}"
    do
        if [[ $itemTestList == $j ]] ; 
            then expectedNetworkTestTesult="open"; 
            break;
        fi
    done
    
    #lance le test (web "open" web)
    if [ $expectedNetworkTestTesult == "open" ]
    then
        $itemTestList $expectedNetworkTestTesult $itemTestList;
    fi
    #@todo : lancer les tests close kotest


done




if  [ $nbResutsKO -eq 0 ] && [ $nbResutsOK -gt 0 ]
    then 
    echo -e $ECHOCOLORGREEN "OK - Connection tests success:"$nbResutsOK " | Connection tests error:"$nbResutsKO  $ECHOCOLORNORMAL
    exit 0;
elif [ $nbResutsKO -gt 0 ] && [ $nbResutsOK -eq 0 ]
    then 
    echo -e $ECHOCOLORRED "UNKNOWN - Connection tests success:"$nbResutsOK " | Connection tests error:"$nbResutsKO $ECHOCOLORNORMAL
    exit 3;
else 
    echo -e $ECHOCOLORRED "CRITICAL - Connection tests success:"$nbResutsOK " | Connection tests error:"$nbResutsKO $ECHOCOLORNORMAL
    exit 2;
fi


exit


