#!/bin/bash

# sonde de test au format de sortie NAGIOS/CENTREON des connexions des serveurs vers l'extérieur
# reférence : DAT diagramme de flux : https://docs.google.com/spreadsheet/ccc?key=0AmFCStZdp0gSdHNqYy1maDlrbjIxcmVjOE4wS21LQ3c#gid=4
# usage
# A) a distance : $> ssh dns1-rectech 'bash -s ' <check_network_dmz-to-backend.sh
# B) local à la machine $> ./check_network_dmz-to-backend.sh <couleur>  <verbose>  (par ex -c -v) 
# C) a distance scan de toutes les machines : $> check_network_dmz-to-backend.sh --scan
# pour faire un scan en couleur à distance il faut editer le fichier et decommanter apres @scancouleur


if [[ $1 == "--scan" ]] ; 
then
	#@todo decommenter les auttres environnements  preprod prod
	listserver="dmz-recfonc" 
	listserver=$listserver" dns1-rectech dns2-rectech rp1-rectech rp2-rectech"
	listserver=$listserver" dns1-tmc dns2-tmc rp1-tmc rp2-tmc"
	#listserver=$listserver" dns1-tmc dns2-tmc web1-tmc web2-tmc web3-tmc deploy-tmc worker1-tmc worker2-tmc back1-tmc back2-tmc rp1-tmc rp2-tmc lb1-recteh lb2-tmc db1-tmc db2-tmc kpi-tmc log-web-tmc log-collector-tmc mq1-tmc mq2-tmc nfs-tmc"

	#listserver=$listserver" dns1-preprod-a rp1-preprod-a dns1-preprod-b rp1-preprod-b"

	#listserver=$listserver" dns1-prod-a rp1-prod-a dns1-prod-b rp1-prod-b"


	for servertmp in $listserver; do
		# grep -v ATTENTION : enleve le message d'avertissement à chaque connexion machine
		ssh $servertmp 'bash -s ' 2>$1 <check_network_dmz-to-backend.sh | grep -v ATTENTION
		echo ""
	done
	exit
fi


#affiche en couleur les résultats
if [[ $1 == "-c" ]] || [[ $2 == "-c" ]] || [[ $3 == "-c" ]]; 
then
    NAGIOSSTYLEEXIT=0
else 
    NAGIOSSTYLEEXIT=1
fi

#affiche le détail des tests et des erreurs
if [[ $1 == "-v" ]] || [[ $2 == "-v" ]] || [[ $3 == "-v" ]]; 
then
    VERBOSE=1
    NAGIOSSTYLEEXIT=0
else 
    VERBOSE=0
fi


############## DESACTIVE LES OPTIONS CI-DESSUS qui ne passent pas lors des apples distants ##########
#@scancouleur
VERBOSE=1
NAGIOSSTYLEEXIT=0


    

#initialisation
if [[ $NAGIOSSTYLEEXIT == 0 ]] ; 
    then
    ECHOCOLORGREEN="\\033[1;32m"
    ECHOCOLORRED="\\033[1;31m"
    ECHOCOLORNORMAL="\\033[0;39m"
fi
nbResutsOK=0
nbResutsKO=0

# recupere l'environnement en fonction du nom de la machine
echo $HOSTNAME | egrep 'rectech' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then 
	ENV="rectech"
	iprange="172.16.5"
fi
echo $HOSTNAME | egrep 'recfonc' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then 
	ENV="recfonc"
	iprange="172.16.4"
fi
echo $HOSTNAME | egrep 'tmc' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then 
	ENV="tmc"
	iprange="172.23.1"
fi
echo $HOSTNAME | egrep 'interop' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then 
	ENV="interop"

fi
echo $HOSTNAME | egrep 'demo' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then 
	ENV="demo"

fi
echo $HOSTNAME | egrep 'tma' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then 
	ENV="tma"

fi
echo $HOSTNAME | egrep 'prod' > /dev/null 2>&1
if [[ $? -eq 0 ]] ; 
    then 
	ENV="prod"
	iprange="172.19.1"
fi

# ATTENTION : l'ordre prod puis preprod est important
echo $HOSTNAME | egrep 'preprod' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then 
	ENV="preprod"
	iprange="172.17.1"
fi



#Analyse le serveur sur lequel est executé le script pour indiquer les tests de connexion à faire 
#par exemple dns doit se connecter à dnsinternet, miroirubuntu,..
#@todo : gerer les allinone et recfonc
echo $HOSTNAME | egrep 'dns' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "kpi"
    "logcollector"
    "deploy"
    )
fi
echo $HOSTNAME | egrep 'rp' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "lb"
    "kpi"
    "logcollector"
    "deploy"
    "mq"
    )
fi
echo $HOSTNAME | egrep 'dmz' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "lb"
    "kpi"
    "logcollector"
    "deploy"
    "mq"
    )
fi



dns=".digiposte.local"

# liste des connexions IP Port Protocole qui doivent être accessible sur la machine cible depuis la machine source
# ex si une machine doit se connecter à un serveur web, il faut qu'elle ait accès à tcp:443
function lb {
	check $1 $iprange".121" "443" "tcp" "lb.ws"$dns
	check $1 $iprange".122" "443" "tcp" "lb.admin"$dns
	check $1 $iprange".122" "8080" "tcp" "lb.admin"$dns
	check $1 $iprange".123" "443" "tcp" "lb.emetteur"$dns
	check $1 $iprange".124" "443" "tcp" "lb.adherer"$dns
	check $1 $iprange".125" "443" "tcp" "lb.secure"$dns
	check $1 $iprange".126" "443" "tcp" "lb.api"$dns

	check $1 "lb.ws"$dns "443" "tcp" "lb.ws"$dns
	check $1 "lb.admin"$dns "443" "tcp" "lb.admin"$dns
	check $1 "lb.admin"$dns "8080" "tcp" "lb.admin"$dns
	check $1 "lb.emetteur"$dns "443" "tcp" "lb.emetteur"$dns
	check $1 "lb.adherer"$dns "443" "tcp" "lb.adherer"$dns
	check $1 "lb.secure"$dns "443" "tcp" "lb.secure"$dns
	check $1 "lb.api"$dns "443" "tcp" "lb.api"$dns

}
function kpi {
    	check $1 $iprange".71" "2003" "tcp" "kpi"$dns
	check $1 "kpi-web.digiposte.local" "2003" "tcp" "kpi-web."$dns
}
function logcollector {
    	check $1 $iprange".81" "514" "udp" "log-collector"$dns
	check $1 "log-collector"$dns "514" "udp" "log-collector"$dns
	check $1 "es.digiposte.local" "514" "udp" "es."$dns
}
function deploy {
	check $1 $iprange".66" "8140" "tcp" "deploy"$dns
	check $1 "deploy"$dns "8140" "tcp" "deploy"$dns
 }
function mq {
    	check $1 $iprange".26" "55672" "tcp" "mq"$dns
	check $1 "mq"$dns "55672" "tcp" "mq"$dns
}







# fonction réalisant un test netcat et renvoyant en sortie standard
# check <resultatAttendu(open|close)    <url/IP    <port    <protocole    <nomService   
# check "open" "localhost" "80" "tcp" "webTest" 
function check {

    if [[ $1 == "open" ]] ; 
        then expectedncResult=0;
        else expectedncResult=1;
    fi
    url=$2;
    port=$3;
    serviceName=$5;

    if [[ $4 == "udp" ]] ; 
        then protocol="-u";
	else protocol="  ";
    fi
    
    # lancement du test netcat # 2   /dev/null pour eviter les nc: getaddrinfo: Name or service not known
    ncCommand="nc -z -w 1 "
    $ncCommand $protocol $url $port 2>/dev/null
    ncResult=$?

    
    if [[ $ncResult -eq $expectedncResult ]] 
    then
        if  [[ $VERBOSE -eq 1 ]]
        then
            echo -e $ECHOCOLORGREEN "OK" $1 ": " $ncCommand "-v "$protocol $url $port" : "$4 $HOSTNAME "-->" $serviceName $ECHOCOLORNORMAL
        fi
          nbResutsOK=$(($nbResutsOK+1))
    else
        if  [[ $VERBOSE -eq 1 ]]
        then
            echo -e $ECHOCOLORRED "KO" $1 ": " $ncCommand "-v "$protocol $url $port" : " $4 $HOSTNAME "-->" $serviceName $ECHOCOLORNORMAL
        fi
        nbResutsKO=$(($nbResutsKO+1))
    fi
}




#liste de tous les tests à réaliser
testList=(
    "lb"
    "kpi"
    "logcollector"
    "deploy"
    "mq"
)






#lance les checks OK ou KO
for itemTestList in "${testList[@]}"
do
    expectedNetworkTestTesult="close"
    testToDo=$itemTestList

    for j in "${listcheckok[@]}"
    do
        if [[ $itemTestList == $j ]] ; 
            then expectedNetworkTestTesult="open"; 
            break;
        fi
    done
    
    #lance le test (web "open" web)
    if [ $expectedNetworkTestTesult == "open" ]
    then
        $itemTestList $expectedNetworkTestTesult $itemTestList;
    fi
done



# affiche les resultats synthetiques finaux
if  [ $nbResutsKO -eq 0 ] && [ $nbResutsOK -gt 0 ]
    then 
    echo -e $ECHOCOLORGREEN "OK - "$HOSTNAME" Connection DMZ to BackEnd | nbtestok="$nbResutsOK"nb nbtestko="$nbResutsKO"nb"  $ECHOCOLORNORMAL
    exit 0;
elif [ $nbResutsKO -gt 0 ] && [ $nbResutsOK -eq 0 ]
    then 
    echo -e $ECHOCOLORRED "UNKNOWN - "$HOSTNAME" Connection DMZ to BackEnd | nbtestok="$nbResutsOK"nb nbtestko="$nbResutsKO"nb"  $ECHOCOLORNORMAL
    exit 3;
else 
    echo -e $ECHOCOLORRED "CRITICAL - "$HOSTNAME" Connection DMZ to BackEnd | nbtestok="$nbResutsOK"nb nbtestko="$nbResutsKO"nb"  $ECHOCOLORNORMAL
    exit 2;
fi


exit


