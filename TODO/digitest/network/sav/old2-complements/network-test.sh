#!/bin/bash

# utilitaire de test des connexions entre les machines
# usage
# .check_network.sh <nagiosplugin> <verbose>
# .check_network.sh -n -v 

HOSTNAME=rp1-rectech

#affiche en couleur les résultats
if [[ $1 == "-n" ]] || [[ $2 == "-n" ]] || [[ $3 == "-n" ]]; 
then
	NAGIOSPLUGIN=1
else 
	NAGIOSPLUGIN=0
fi

#affiche le détail des tests et des erreurs
if [[ $1 == "-v" ]] || [[ $2 == "-v" ]] || [[ $3 == "-v" ]]; 
then
	VERBOSE=1
	NAGIOSPLUGIN=1
else 
	VERBOSE=0
fi



#initialisation
if [[ $NAGIOSPLUGIN == 1 ]] ; 
	then
	ECHOCOLORGREEN="\\033[1;32m"
	ECHOCOLORRED="\\033[1;31m"
	ECHOCOLORNORMAL="\\033[0;39m"
fi
nbResutsOK=0
nbResutsKO=0



#Analyse le serveur sur lequel est executé le script pour indiquer les tests de connexion à faire #par exemple webx doit se connecter à db, mq,..
if [[ $HOSTNAME == dns* ]] ; 
	then listcheckok=(
	"logcollector"	
	"dns"
	"web"
	)
fi
if [[ $HOSTNAME == web* ]] ; 
	then listcheckok=(
	"logcollector"	
	"dns"
	"web"
	) 
fi
if [[ $HOSTNAME == rp* ]] ; 
	then listcheckok=(
	"logcollector"	
	"dns"
	"web"
	)
fi





# liste des connexions qui doivent être accessible sur la machine cible depuis la machine source
# ex si une machine doit se connecter à web, il faut qu'elle ait accès à tcp:443
function web {
	check $1 "localhost" "443" "tcp" $2
}
function dns {
	check $1 "dns.digiposte.local" "123" "udp" $2
	check $1 "dns.digiposte.local" "123" "tcp" $2
	check $1 "dns-s.digiposte.local" "123" "udp" $2
	check $1 "dns-s.digiposte.local" "123" "tcp" $2
}
function ntp {
	check $1 "dns.digiposte.local" "???" "udp" $2
	check $1 "dns-s.digiposte.local" "123" "udp" $2
}
function logcollector {
	check $1 "log-collector.digiposte.local" "514" "udp" $2
	check $1 "log-collector.digiposte.local" "12201" "udp" $2
}
function lb {
	check $1 "lb.digiposte.local" "???" "tcp" $2
	check $1 "dns-s.digiposte.local" "???" "tcp" $2
}
function kpi {
	check $1 "localhost" "44322" "tcp" $2
	check $1 "localhost" "44323" "tcp" $2
	check $1 "localhost" "8033" "tcp" $2
}
function db {
	check $1 "localhost" "44322" "tcp" $2
	check $1 "localhost" "44323" "tcp" $2
	check $1 "localhost" "8033" "tcp" $2
}
function mq {
	check $1 "localhost" "44322" "tcp" $2
	check $1 "localhost" "44323" "tcp" $2
	check $1 "localhost" "8033" "tcp" $2
}
function kotest {
	check $1 "localhost" "22" "tcp" $2
	check $1 "localhost" "21" "tcp" $2
	check $1 "localhost" "8033" "tcp" $2
	check $1 "localhost" "1-79" "tcp" $2
	check $1 "localhost" "80-80" "tcp" $2
	check $1 "localhost" "80-81" "tcp" $2
	check $1 "localhost" "1-81" "udp" $2
}


#liste de tous les tests à réaliser
testList=(
	"db"
	"web"
	"mq"
	"kpi"
	"kotest"
)


# fonction réalisant un test netcat et renvoyant en sortie standard OK ou KO coloré
# check <nomTest> <resultatAttendu> <url/IP> <port> <protocole> <nomService>
# check "open" "localhost" "80" "tcp" "webTest" 
function check {

	if [[ $1 == "open" ]] ; 
		then expectedncResult=0;
		else expectedncResult=1;
	fi
	url=$2;
	port=$3;
	serviceName=$5;
	if [[ $4 == "udp" ]] ; 
		then protocol="-u";
	fi
	
	# lancement du test netcat # 2>/dev/null pour eviter les nc: getaddrinfo: Name or service not known
	ncCommand="nc -z -w 1 "
	$ncCommand $protocol $url $port 2>/dev/null
	ncResult=$?

	
	if [ $ncResult -eq $expectedncResult ] 
	then
		if  [ $VERBOSE -eq 1 ]
		then
	    		echo -e $ECHOCOLORGREEN "OK" $1 ": " $ncCommand " -v " $protocol $url $port " : " $HOSTNAME "-->" $serviceName $ECHOCOLORNORMAL
		fi
	    	nbResutsOK=$(($nbResutsOK+1))
	else
		if  [ $VERBOSE -eq 1 ]
		then
	    		echo -e $ECHOCOLORRED "KO" $1 ": " $ncCommand " -v " $protocol $url $port " : " $HOSTNAME "-->" $serviceName $ECHOCOLORNORMAL
		fi
	    	nbResutsKO=$(($nbResutsKO+1))
	fi

}

#lance les checks OK ou KO
for itemTestList in "${testList[@]}"
do
	expectedNetworkTestTesult="close"
	testToDo=$itemTestList	

	for j in "${listcheckok[@]}"
	do
		if [[ $itemTestList == $j ]] ; 
			then expectedNetworkTestTesult="open"; 
			break;
		fi		
	done	
	
	#lance le test (web "open" web)
	$itemTestList $expectedNetworkTestTesult $itemTestList;
done




if  [ $nbResutsKO -eq 0 ] && [ $nbResutsOK -gt 0 ]
	then 
	echo -e $ECHOCOLORGREEN "OK - Connection tests success:"$nbResutsOK " | Connection tests error:"$nbResutsKO  $ECHOCOLORNORMAL
	exit 0;
elif [ $nbResutsKO -gt 0 ] && [ $nbResutsOK -eq 0 ]
	then 
	echo -e $ECHOCOLORRED "UNKNOWN - Connection tests success:"$nbResutsOK " | Connection tests error:"$nbResutsKO $ECHOCOLORNORMAL
	exit 3;
else 
	echo -e $ECHOCOLORRED "CRITICAL - Connection tests success:"$nbResutsOK " | Connection tests error:"$nbResutsKO $ECHOCOLORNORMAL
	exit 2;
fi


exit


