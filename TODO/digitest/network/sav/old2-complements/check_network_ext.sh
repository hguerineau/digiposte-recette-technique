#!/bin/bash

# utilitaire de test des connexions entre les machines
# usage
# a distance : ssh digi-worker1-rectech 'bash -s ' <check_network_ext.sh
# .check_network_ext.sh <NAGIOSSTYLEEXIT>    <verbose>   
# .check_network_ext.sh -n -v 

#HOSTNAME=rp1-rectech

if [[ $1 == "--scan" ]] ; 
then
	listserver="dns1-rectech dns2-rectech web1-rectech web2-rectech deploy-rectech worker1-rectech worker2-rectech back1-rectech back2-rectech rp1-rectech rp2-rectech lb1-recteh lb2-rectech db1-rectech db2-rectech kpi-rectech log-web-rectech log-collector-rectech mq1-rectech mq2-rectech" 
	for servertmp in $listserver; do

		ssh $servertmp 'bash -s ' <check_network_ext.sh

	done
	exit
fi




#affiche en couleur les résultats
if [[ $1 == "-c" ]] || [[ $2 == "-c" ]] || [[ $3 == "-c" ]]; 
then
    NAGIOSSTYLEEXIT=0
else 
    NAGIOSSTYLEEXIT=1
fi

#affiche le détail des tests et des erreurs
if [[ $1 == "-v" ]] || [[ $2 == "-v" ]] || [[ $3 == "-v" ]]; 
then
    VERBOSE=1
    NAGIOSSTYLEEXIT=0
else 
    VERBOSE=0
fi


########################################## DESACTIVE LES OPTIONS CI-DESSUS qui ne passent pas lors des apples distants ##########
VERBOSE=0
NAGIOSSTYLEEXIT=1
#VERBOSE=1
#NAGIOSSTYLEEXIT=1

    

#initialisation
if [[ $NAGIOSSTYLEEXIT == 0 ]] ; 
    then
    ECHOCOLORGREEN="\\033[1;32m"
    ECHOCOLORRED="\\033[1;31m"
    ECHOCOLORNORMAL="\\033[0;39m"
fi
nbResutsOK=0
nbResutsKO=0


#inutile pour ce script
echo $HOSTNAME | egrep 'rectech' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then ENV="rectech"
fi
echo $HOSTNAME | egrep 'tmc' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then ENV="tmc"
fi
echo $HOSTNAME | egrep 'prod' > /dev/null 2>&1
if [[ $? -eq 0 ]] ; 
    then ENV="prod"
fi

# attention l'ordre prod puis preprod est important
echo $HOSTNAME | egrep 'preprod' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then ENV="preprod"
fi
#@todo : ajouter autres environnements

#Analyse le serveur sur lequel est executé le script pour indiquer les tests de connexion à faire #par exemple dns doit se connecter à dnsinternet, miroirubuntu,..
echo $HOSTNAME | egrep 'dns' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "dnsinternet"
    "ntpinternet"
    "miroirubuntu"
    )
fi
echo $HOSTNAME | egrep 'deploy' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "git"
    "miroirubuntu"
    )
fi
echo $HOSTNAME | egrep 'web' | grep -v "log" > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "miroirubuntu"
    "newrelic"
    "coffre"
    "cabestan-smtp"
    "cabestan-imap"
    "imap"
    "maileva"
    "mascadia"
    "epel-uri"
    "epel-ack"
    )
fi
echo $HOSTNAME | egrep 'worker' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "miroirubuntu"
    "lbp"	
    "newrelic"
    "coffre"
    "cabestan-smtp"
    "cabestan-imap"
    "maileva"
    "mascadia"
    "epel-uri"
    "epel-ack"
    "apns-gw"
    "apns-feedback"
    "arkea"
    "reunica"
    )
fi
echo $HOSTNAME | egrep 'back' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "miroirubuntu"
    "lbp"
    "newrelic"
    "coffre"
    "cabestan-smtp"
    "cabestan-imap"
    "maileva"
    "mascadia"
    "epel-uri"
    "epel-ack"
    "apns-gw"
    "apns-feedback"
    "arkea"
    "reunica"
    )
fi

# pour tous les autres serveurs
if [  "$listcheckok" == "" ] 
then
	listcheckok=(
    		"miroirubuntu"
	)
fi




# liste des connexions qui doivent être accessible sur la machine cible depuis la machine source
# ex si une machine doit se connecter à web, il faut qu'elle ait accès à tcp:443
function dnsinternet {
    check $1 "192.5.5.241" "53" "udp" $2
    check $1 "192.5.5.241" "53" "tcp" $2
}
function ntpinternet {
    check $1 "0.ubuntu.pool.ntp.org" "123" "udp" $2
    check $1 "0.ubuntu.pool.ntp.org" "123" "tcp" $2
    check $1 "1.ubuntu.pool.ntp.org" "123" "udp" $2
    check $1 "1.ubuntu.pool.ntp.org" "123" "tcp" $2
    check $1 "2.ubuntu.pool.ntp.org" "123" "udp" $2
    check $1 "2.ubuntu.pool.ntp.org" "123" "tcp" $2
    check $1 "3.ubuntu.pool.ntp.org" "123" "udp" $2
    check $1 "3.ubuntu.pool.ntp.org" "123" "tcp" $2
    check $1 "server.ntp.ubuntu.com" "123" "udp" $2
    check $1 "server.ntp.ubuntu.com" "123" "tcp" $2
}
function miroirubuntu {
	check $1 "172.14.1.25" "80" "tcp" $2
 }
function git {
    check $1 "84.14.82.12" "22" "udp" $2
}
function lbp {
    check $1 "@todo" "443" "tcp" $2
}
function newrelic {
	check $1 "204.93.223.128" "80" "tcp" $2
	check $1 "204.93.223.128" "443" "tcp" $2
	check $1 "@todo204.93.223.128/27" "443" "tcp" $2
	check $1 "@todo204.93.223.128/27" "80" "tcp" $2
 }
function coffre {
	check $1 "coffre.digiposte.local" "443" "tcp" $2
 }
function cabestan-smtp {

	if [ "$env" = "prod" ]
	then
		check $1 "213.41.75.90" "2526" "tcp" $2
	elsif [ "$env" = "preprod" ]
		check $1 "213.41.75.90" "25" "tcp" $2
	else
		check $1 "213.41.75.90" "25" "tcp" $2
	fi 

 }
function cabestan-imap {
	check $1 "213.41.75.90" "143" "tcp" $2
 }

function maileva {
	check $1 "81.27.21.14" "20" "tcp" $2
	check $1 "81.27.21.14" "21" "tcp" $2
 }
function mascadia {
	if [ "$env" = "prod" ]
	then
		check $1 "94.124.131.207" "80" "tcp" $2
	elsif [ "$env" = "preprod" ]
		check $1 "94.124.131.189" "80" "tcp" $2
	else
		check $1 "94.124.131.189" "80" "tcp" $2
	fi 
 }
function epel-uri {

	if [ "$env" = "prod" ]
	then
		check $1 "160.92.106.219" "443" "tcp" $2
	elsif [ "$env" = "preprod" ]
		check $1 "160.92.145.106" "443" "tcp" $2
	else
		check $1 "160.92.145.106" "443" "tcp" $2
	fi 

 }
function epel-ack {

	if [ "$env" = "prod" ]
	then
		check $1 "160.92.106.221" "443" "tcp" $2
	elsif [ "$env" = "preprod" ]
		check $1 "160.92.145.108" "443" "tcp" $2
	else
		check $1 "160.92.145.108" "443" "tcp" $2
	fi 
 }
function apns-gw {
	check $1 "17.0.0.0" "2195" "tcp" $2
	check $1 "@todo17.0.0.0/8" "2195" "tcp" $2
 }
function apns-feedback {
	check $1 "17.0.0.0" "2196" "tcp" $2
	check $1 "@todo17.0.0.0/8" "2196" "tcp" $2
 }
function arkea {
	if [ "$env" = "prod" ]
	then
		check $1 "62.23.36.142" "443" "tcp" $2
		check $1 "83.206.181.206" "443" "tcp" $2
		check $1 "90.80.8.46" "443" "tcp" $2

	elsif [ "$env" = "preprod" ]
		check $1 "62.23.36.142" "443" "tcp" $2
		check $1 "83.206.181.206" "443" "tcp" $2
		check $1 "90.80.8.46" "443" "tcp" $2

	else
		check $1 "83.206.181.219" "443" "tcp" $2
		check $1 "90.80.8.59" "443" "tcp" $2
		check $1 "90.80.8.59" "443" "tcp" $2

	fi 
 }
function reunica {
	if [ "$env" = "prod" ]
	then
		check $1 "93.20.42.112" "443" "tcp" $2
	elsif [ "$env" = "preprod" ]
	

	else
		check $1 "80.124.164.150" "443" "tcp" $2

	fi 
 }



















# fonction réalisant un test netcat et renvoyant en sortie standard
# check <resultatAttendu(open|close)    <url/IP    <port    <protocole    <nomService   
# check "open" "localhost" "80" "tcp" "webTest" 
function check {

    if [[ $1 == "open" ]] ; 
        then expectedncResult=0;
        else expectedncResult=1;
    fi
    url=$2;
    port=$3;
    serviceName=$5;

    if [[ $4 == "udp" ]] ; 
        then protocol="-u";
	else protocol="  ";
    fi
    
    # lancement du test netcat # 2   /dev/null pour eviter les nc: getaddrinfo: Name or service not known
    ncCommand="nc -z -w 1 "
    $ncCommand $protocol $url $port 2>/dev/null
    ncResult=$?

    
    if [[ $ncResult -eq $expectedncResult ]] 
    then
        if  [[ $VERBOSE -eq 1 ]]
        then
            echo -e $ECHOCOLORGREEN "OK" $1 ": " $ncCommand "-v "$protocol $url $port" : "$4 $HOSTNAME "-->" $serviceName $ECHOCOLORNORMAL
        fi
          nbResutsOK=$(($nbResutsOK+1))
    else
        if  [[ $VERBOSE -eq 1 ]]
        then
            echo -e $ECHOCOLORRED "KO" $1 ": " $ncCommand "-v "$protocol $url $port" : " $4 $HOSTNAME "-->" $serviceName $ECHOCOLORNORMAL
        fi
        nbResutsKO=$(($nbResutsKO+1))
    fi
}




#liste de tous les tests à réaliser
testList=(
    "dnsinternet"
    "ntpinternet"
    "miroirubuntu"
    "git"
    "lbp"
    "newrelic"
    "coffre"
    "cabestan-smtp"
    "cabestan-imap"
    "cabestan-smtp"
    "maileva"
    "mascadia"
    "epel-uri"
    "epel-ack"
    "apns-gw"
    "apns-feedback"
    "arkea"
    "reunica"
)



















#lance les checks OK ou KO
for itemTestList in "${testList[@]}"
do
    expectedNetworkTestTesult="close"
    testToDo=$itemTestList

    for j in "${listcheckok[@]}"
    do
        if [[ $itemTestList == $j ]] ; 
            then expectedNetworkTestTesult="open"; 
            break;
        fi
    done
    
    #lance le test (web "open" web)
    if [ $expectedNetworkTestTesult == "open" ]
    then
        $itemTestList $expectedNetworkTestTesult $itemTestList;
    fi
done




if  [ $nbResutsKO -eq 0 ] && [ $nbResutsOK -gt 0 ]
    then 
    echo -e $ECHOCOLORGREEN "OK - "$HOSTNAME" Connexion Digiposte vers Internet | nbtestok="$nbResutsOK"nb nbtestko="$nbResutsKO"nb"  $ECHOCOLORNORMAL
    exit 0;
elif [ $nbResutsKO -gt 0 ] && [ $nbResutsOK -eq 0 ]
    then 
    echo -e $ECHOCOLORRED "UNKNOWN - "$HOSTNAME" Connexion Digiposte vers Internet | nbtestok="$nbResutsOK"nb nbtestko="$nbResutsKO"nb"  $ECHOCOLORNORMAL
    exit 3;
else 
    echo -e $ECHOCOLORRED "CRITICAL - "$HOSTNAME" Connexion Digiposte vers Internet | nbtestok="$nbResutsOK"nb nbtestko="$nbResutsKO"nb"  $ECHOCOLORNORMAL
    exit 2;
fi


exit


