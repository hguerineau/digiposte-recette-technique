#!/bin/bash

# sonde de test au format de sortie NAGIOS/CENTREON des connexions des serveurs vers l'extérieur
# reférence : Diagramme de flux du DAT : https://docs.google.com/spreadsheet/ccc?key=0AmFCStZdp0gSdHNqYy1maDlrbjIxcmVjOE4wS21LQ3c#gid=2
# usage
# a distance : $> ssh worker1-rectech 'bash -s ' <check_network_digiposte-to-internet.sh
# $> ./check_network_internet-to-digiposte.sh <couleur>  <verbose>  (par ex -c -v) 
# pour faire un scan en couleur à distance il faut editer le fichier et decommanter apres @scancouleur


if [[ $1 == "--scan" ]] ; 
then
	listserver="worker1-rectech dns1-rectech dns2-rectech web1-rectech web2-rectech deploy-rectech  worker2-rectech back1-rectech back2-rectech rp1-rectech rp2-rectech lb1-recteh lb2-rectech db1-rectech db2-rectech kpi-rectech log-web-rectech log-collector-rectech mq1-rectech mq2-rectech" 
	for servertmp in $listserver; do

		ssh $servertmp 'bash -s ' 2>$1 <check_network_digiposte-to-internet.sh | grep -v ATTENTION
		echo ""

	done
	exit
fi


#affiche en couleur les résultats
if [[ $1 == "-c" ]] || [[ $2 == "-c" ]] || [[ $3 == "-c" ]]; 
then
    NAGIOSSTYLEEXIT=0
else 
    NAGIOSSTYLEEXIT=1
fi

#affiche le détail des tests et des erreurs
if [[ $1 == "-v" ]] || [[ $2 == "-v" ]] || [[ $3 == "-v" ]]; 
then
    VERBOSE=1
    NAGIOSSTYLEEXIT=0
else 
    VERBOSE=0
fi


############## DESACTIVE LES OPTIONS CI-DESSUS qui ne passent pas lors des apples distants ##########
#@scancouleur
VERBOSE=1
NAGIOSSTYLEEXIT=0


    

#initialisation
if [[ $NAGIOSSTYLEEXIT == 0 ]] ; 
    then
    ECHOCOLORGREEN="\\033[1;32m"
    ECHOCOLORRED="\\033[1;31m"
    ECHOCOLORNORMAL="\\033[0;39m"
fi
nbResutsOK=0
nbResutsKO=0


#Liste des test à lancer
listcheckok=(
    "prod"
    "preprod"
    "tmc"
    "rectech"
    "recfonc"
    "tma"
    "interop"
    "demo"
    )





# liste des connexions qui doivent être accessible sur la machine cible depuis la machine source
# ex si une machine doit se connecter à web, il faut qu'elle ait accès à tcp:443
function prod {
    check $1 "192.5.5.241" "53" "udp" $2
    check $1 "192.5.5.241" "53" "tcp" $2
}

function recfonc {
	dns=".recfonc.u-post.fr"
	check $1 "46.255.129.83" "80" "tcp" "ws"$dns
	check $1 "46.255.129.84" "80" "tcp" "admin"$dns
	check $1 "46.255.129.85" "80" "tcp" "emetteur"$dns
	check $1 "46.255.129.86" "80" "tcp" "adherer"$dns
	check $1 "46.255.129.87" "80" "tcp" "secure"$dns
	check $1 "46.255.129.88" "80" "tcp" "api"$dns

	check $1 "46.255.129.83" "443" "tcp" "ws"$dns
	check $1 "46.255.129.84" "443" "tcp" "admin"$dns
	check $1 "46.255.129.85" "443" "tcp" "emetteur"$dns
	check $1 "46.255.129.86" "443" "tcp" "adherer"$dns
	check $1 "46.255.129.87" "443" "tcp" "secure"$dns
	check $1 "46.255.129.88" "443" "tcp" "api"$dns

	check $1 "46.255.129.84" "8080" "tcp" "admin"$dns

}
function rectech {
	dns=".rectech.u-post.fr"
	check $1 "46.255.129.89" "80" "tcp" "ws"$dns
	check $1 "46.255.129.90" "80" "tcp" "admin"$dns
	check $1 "46.255.129.91" "80" "tcp" "emetteur"$dns
	check $1 "46.255.129.92" "80" "tcp" "adherer"$dns
	check $1 "46.255.129.93" "80" "tcp" "secure"$dns
	check $1 "46.255.129.94" "80" "tcp" "api"$dns

	check $1 "46.255.129.89" "443" "tcp" "ws"$dns
	check $1 "46.255.129.90" "443" "tcp" "admin"$dns
	check $1 "46.255.129.91" "443" "tcp" "emetteur"$dns
	check $1 "46.255.129.92" "443" "tcp" "adherer"$dns
	check $1 "46.255.129.93" "443" "tcp" "secure"$dns
	check $1 "46.255.129.94" "443" "tcp" "api"$dns

	check $1 "46.255.129.90" "8080" "tcp" "admin"$dns
}
function tmc {
	dns=".tmc.u-post.fr"
	check $1 "46.255.129.17" "80" "tcp" "ws"$dns
	check $1 "46.255.129.18" "80" "tcp" "admin"$dns
	check $1 "46.255.129.19" "80" "tcp" "emetteur"$dns
	check $1 "46.255.129.20" "80" "tcp" "adherer"$dns
	check $1 "46.255.129.21" "80" "tcp" "secure"$dns
	check $1 "46.255.129.22" "80" "tcp" "api"$dns

	check $1 "46.255.129.17" "443" "tcp" "ws"$dns
	check $1 "46.255.129.18" "443" "tcp" "admin"$dns
	check $1 "46.255.129.19" "443" "tcp" "emetteur"$dns
	check $1 "46.255.129.20" "443" "tcp" "adherer"$dns
	check $1 "46.255.129.21" "443" "tcp" "secure"$dns
	check $1 "46.255.129.22" "443" "tcp" "api"$dns

	check $1 "46.255.129.18" "8080" "tcp" "admin"$dns
}
function tma {
	dns=".tma.u-post.fr"
	check $1 "46.255.129.65" "80" "tcp" "ws"$dns
	check $1 "46.255.129.66" "80" "tcp" "admin"$dns
	check $1 "46.255.129.67" "80" "tcp" "emetteur"$dns
	check $1 "46.255.129.68" "80" "tcp" "adherer"$dns
	check $1 "46.255.129.69" "80" "tcp" "secure"$dns
	check $1 "46.255.129.70" "80" "tcp" "api"$dns

	check $1 "46.255.129.65" "443" "tcp" "ws"$dns
	check $1 "46.255.129.66" "443" "tcp" "admin"$dns
	check $1 "46.255.129.67" "443" "tcp" "emetteur"$dns
	check $1 "46.255.129.68" "443" "tcp" "adherer"$dns
	check $1 "46.255.129.69" "443" "tcp" "secure"$dns
	check $1 "46.255.129.70" "443" "tcp" "api"$dns

	check $1 "46.255.129.66" "8080" "tcp" "admin"$dns
}

function demo {
	dns=".demo.u-post.fr"
	check $1 "@matthieuF-la-doc-de-flux-nest-pas-a-jour" "80" "tcp" ""$dns
	check $1 "46.255.129.71" "80" "tcp" "ws"$dns
	check $1 "46.255.129.72" "80" "tcp" "admin"$dns
	check $1 "46.255.129.73" "80" "tcp" "emetteur"$dns
	check $1 "46.255.129.74" "80" "tcp" "adherer"$dns
	check $1 "46.255.129.75" "80" "tcp" "secure"$dns
	check $1 "46.255.129.76" "80" "tcp" "api"$dns

	check $1 "46.255.129.71" "443" "tcp" "ws"$dns
	check $1 "46.255.129.72" "443" "tcp" "admin"$dns
	check $1 "46.255.129.73" "443" "tcp" "emetteur"$dns
	check $1 "46.255.129.74" "443" "tcp" "adherer"$dns
	check $1 "46.255.129.75" "443" "tcp" "secure"$dns
	check $1 "46.255.129.76" "443" "tcp" "api"$dns

	check $1 "46.255.129.72" "8080" "tcp" "admin"$dns
}
function interop {
	dns=".interop.u-post.fr"
	check $1 "46.255.129.101" "80" "tcp" "ws"$dns
	check $1 "46.255.129.102" "80" "tcp" "admin"$dns
	check $1 "46.255.129.103" "80" "tcp" "emetteur"$dns
	check $1 "46.255.129.104" "80" "tcp" "adherer"$dns
	check $1 "46.255.129.105" "80" "tcp" "secure"$dns
	check $1 "46.255.129.106" "80" "tcp" "api"$dns

	check $1 "46.255.129.101" "443" "tcp" "ws"$dns
	check $1 "46.255.129.102" "443" "tcp" "admin"$dns
	check $1 "46.255.129.103" "443" "tcp" "emetteur"$dns
	check $1 "46.255.129.104" "443" "tcp" "adherer"$dns
	check $1 "46.255.129.105" "443" "tcp" "secure"$dns
	check $1 "46.255.129.106" "443" "tcp" "api"$dns

	check $1 "46.255.129.102" "8080" "tcp" "admin"$dns
}

function preprod {
	dns=".preprod.digiposte.fr"
	check $1 "46.255.129.136" "80" "tcp" "ws"$dns
	check $1 "46.255.129.137" "80" "tcp" "admin"$dns
	check $1 "46.255.129.138" "80" "tcp" "emetteur"$dns
	check $1 "46.255.129.139" "80" "tcp" "adherer"$dns
	check $1 "46.255.129.140" "80" "tcp" "secure"$dns
	check $1 "46.255.129.141" "80" "tcp" "api"$dns

	check $1 "46.255.129.136" "443" "tcp" "ws"$dns
	check $1 "46.255.129.137" "443" "tcp" "admin"$dns
	check $1 "46.255.129.138" "443" "tcp" "emetteur"$dns
	check $1 "46.255.129.139" "443" "tcp" "adherer"$dns
	check $1 "46.255.129.140" "443" "tcp" "secure"$dns
	check $1 "46.255.129.141" "443" "tcp" "api"$dns

	check $1 "46.255.129.137" "8080" "tcp" "admin"$dns
}
function prod {
	dns=".prod.u-post.fr"
	check $1 "46.255.129.9" "80" "tcp" "ws"$dns
	check $1 "46.255.129.10" "80" "tcp" "admin"$dns
	check $1 "46.255.129.11" "80" "tcp" "emetteur"$dns
	check $1 "46.255.129.12" "80" "tcp" "adherer"$dns
	check $1 "46.255.129.13" "80" "tcp" "secure"$dns
	check $1 "46.255.129.14" "80" "tcp" "api"$dns

	check $1 "46.255.129.9" "443" "tcp" "ws"$dns
	check $1 "46.255.129.10" "443" "tcp" "admin"$dns
	check $1 "46.255.129.11" "443" "tcp" "emetteur"$dns
	check $1 "46.255.129.12" "443" "tcp" "adherer"$dns
	check $1 "46.255.129.13" "443" "tcp" "secure"$dns
	check $1 "46.255.129.14" "443" "tcp" "api"$dns

	check $1 "46.255.129.10" "8080" "tcp" "admin"$dns
}










# fonction réalisant un test netcat et renvoyant en sortie standard
# check <resultatAttendu(open|close)    <url/IP    <port    <protocole    <nomService   
# check "open" "localhost" "80" "tcp" "webTest" 
function check {

    if [[ $1 == "open" ]] ; 
        then expectedncResult=0;
        else expectedncResult=1;
    fi
    url=$2;
    port=$3;
    serviceName=$5;

    if [[ $4 == "udp" ]] ; 
        then protocol="-u";
	else protocol="  ";
    fi
    
    # lancement du test netcat # 2   /dev/null pour eviter les nc: getaddrinfo: Name or service not known
    ncCommand="nc -z -w 1 "
    $ncCommand $protocol $url $port 2>/dev/null
    ncResult=$?

    
    if [[ $ncResult -eq $expectedncResult ]] 
    then
        if  [[ $VERBOSE -eq 1 ]]
        then
            echo -e $ECHOCOLORGREEN "OK" $1 ": " $ncCommand "-v "$protocol $url $port" : "$4 $HOSTNAME "-->" $serviceName $ECHOCOLORNORMAL
        fi
          nbResutsOK=$(($nbResutsOK+1))
    else
        if  [[ $VERBOSE -eq 1 ]]
        then
            echo -e $ECHOCOLORRED "KO" $1 ": " $ncCommand "-v "$protocol $url $port" : " $4 $HOSTNAME "-->" $serviceName $ECHOCOLORNORMAL
        fi
        nbResutsKO=$(($nbResutsKO+1))
    fi
}




#liste de tous les tests à réaliser
testList=(
    "rectech"
    "recfonc"
    "tmc"
    "preprod"
    "prod"
    "tma"
    "interop"
    "demo"
    )




















#lance les checks OK ou KO
for itemTestList in "${testList[@]}"
do
    expectedNetworkTestTesult="close"
    testToDo=$itemTestList

    for j in "${listcheckok[@]}"
    do
        if [[ $itemTestList == $j ]] ; 
            then expectedNetworkTestTesult="open"; 
            break;
        fi
    done
    
    #lance le test (web "open" web)
    if [ $expectedNetworkTestTesult == "open" ]
    then
        $itemTestList $expectedNetworkTestTesult $itemTestList;
    fi
done




if  [ $nbResutsKO -eq 0 ] && [ $nbResutsOK -gt 0 ]
    then 
    echo -e $ECHOCOLORGREEN "OK - "$HOSTNAME" Connexion Digiposte vers Internet | nbtestok="$nbResutsOK"nb nbtestko="$nbResutsKO"nb"  $ECHOCOLORNORMAL
    exit 0;
elif [ $nbResutsKO -gt 0 ] && [ $nbResutsOK -eq 0 ]
    then 
    echo -e $ECHOCOLORRED "UNKNOWN - "$HOSTNAME" Connexion Digiposte vers Internet | nbtestok="$nbResutsOK"nb nbtestko="$nbResutsKO"nb"  $ECHOCOLORNORMAL
    exit 3;
else 
    echo -e $ECHOCOLORRED "CRITICAL - "$HOSTNAME" Connexion Digiposte vers Internet | nbtestok="$nbResutsOK"nb nbtestko="$nbResutsKO"nb"  $ECHOCOLORNORMAL
    exit 2;
fi


exit


