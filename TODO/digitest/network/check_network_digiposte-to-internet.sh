#!/bin/bash

# sonde de test au format de sortie NAGIOS/CENTREON des connexions des serveurs vers l'extérieur
# reférence : https://docs.google.com/spreadsheet/ccc?key=0AmFCStZdp0gSdHNqYy1maDlrbjIxcmVjOE4wS21LQ3c#gid=1
# usage
# A) a distance : $> ssh worker1-rectech 'bash -s ' <check_network_digiposte-to-internet.sh
# B) local à la machine $> ./check_network_digiposte-to-internet.sh -a 
# C) a distance scan de toutes les machines : $> check_network_digiposte-to-internet.sh --a
# pour faire un scan en couleur à distance il faut editer le fichier et decommanter apres @scancouleur


if [[ $1 == "-a" ]] || [[ $2 == "-a" ]] || [[ $3 == "-a" ]]; 
then
	listserver=""
	listserver=$listserver" dns1-rectech dns2-rectech web1-rectech web2-rectech deploy-rectech worker1-rectech worker2-rectech back1-rectech back2-rectech rp1-rectech rp2-rectech lb1-rectech lb2-rectech db1-rectech db2-rectech kpi-rectech log-web-rectech log-collector-rectech mq1-rectech mq2-rectech nfs-rectech idx1-rectech idx2-rectech" 

	listserver=$listserver" dmz-recfonc support-recfonc web1-recfonc web2-recfonc worker-recfonc db-recfonc" 

	listserver=$listserver" dns1-tmc dns2-tmc web1-tmc web2-tmc web3-tmc deploy-tmc worker1-tmc worker2-tmc back1-tmc back2-tmc rp1-tmc rp2-tmc lb1-recteh lb2-tmc db1-tmc db2-tmc kpi-tmc log-web-tmc log-collector-tmc mq1-tmc mq2-tmc nfs-tmc idx1-tmc"

	listserver=$listserver" dns1-prod-a nfs-prod-a mq1-prod-a mq2-prod-a worker1-prod-a worker2-prod-a db1-prod-a db2-prod-a idx1-prod-a kpi-prod-a log-web-prod-a log-collector-prod-a rp1-prod-a lb1-prod-a web11-prod-a web12-prod-a web13-prod-a dns1-prod-b nfs-prod-b mq1-prod-b mq2-prod-b worker1-prod-b worker2-prod-b db1-prod-b db2-prod-b idx1-prod-b kpi-prod-b log-web-prod-b log-collector-prod-b rp1-prod-b lb1-prod-b web21-prod-b web22-prod-b web23-prod-b"

	listserver=$listserver" dns1-preprod-a nfs-preprod-a mq1-preprod-a mq2-preprod-a worker1-preprod-a worker2-preprod-a db1-preprod-a db2-preprod-a idx1-preprod-a kpi-preprod-a log-web-preprod-a log-collector-preprod-a rp1-preprod-a lb1-preprod-a web11-preprod-a web12-preprod-a web13-preprod-a dns1-preprod-b nfs-preprod-b mq1-preprod-b mq2-preprod-b worker1-preprod-b worker2-preprod-b db1-preprod-b db2-preprod-b idx1-preprod-b kpi-preprod-b log-web-preprod-b log-collector-preprod-b rp1-preprod-b lb1-preprod-b web21-preprod-b web22-preprod-b web23-preprod-b"


	listserver=$listserver" all-interop all-tma all-demo"


	for servertmp in $listserver; do
		# grep -v ATTENTION : enleve le message d'avertissement à chaque connexion machine
		ssh $servertmp 'bash -s ' 2>&1 <check_network_digiposte-to-internet.sh | grep -v ATTENTION
		echo ""
	done
	exit
fi




#affiche le détail des tests et des erreurs
if [[ $1 == "-v" ]] || [[ $2 == "-v" ]] || [[ $3 == "-v" ]]; 
then
    VERBOSE=1
    NAGIOSSTYLEEXIT=0
else 
    VERBOSE=0
    NAGIOSSTYLEEXIT=1
fi


############## DESACTIVE LES OPTIONS CI-DESSUS qui ne passent pas lors des apples distants ##########
#@scancouleur
VERBOSE=1
NAGIOSSTYLEEXIT=0


    

#initialisation
if [[ $NAGIOSSTYLEEXIT == 0 ]] ; 
    then
    ECHOCOLORGREEN="\\033[1;32m"
    ECHOCOLORRED="\\033[1;31m"
    ECHOCOLORNORMAL="\\033[0;39m"
fi
nbResutsOK=0
nbResutsKO=0

# reccupere l'environnement en fonction du nom de l amachine
echo $HOSTNAME | egrep 'rectech' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then ENV="rectech"
fi
echo $HOSTNAME | egrep 'recfonc' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then ENV="recfonc"
fi
echo $HOSTNAME | egrep 'tmc' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then ENV="tmc"
fi
echo $HOSTNAME | egrep 'tma' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then ENV="tma"
fi
echo $HOSTNAME | egrep 'interop' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then ENV="interop"
fi
echo $HOSTNAME | egrep 'demo' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then ENV="demo"
fi
echo $HOSTNAME | egrep 'tma' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then ENV="tma"
fi
echo $HOSTNAME | egrep 'prod' > /dev/null 2>&1
if [[ $? -eq 0 ]] ; 
    then ENV="prod"
fi

# ATTENTION : l'ordre prod puis preprod est important
echo $HOSTNAME | egrep 'preprod' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then ENV="preprod"
fi



#Analyse le serveur sur lequel est executé le script pour indiquer les tests de connexion à faire 
#par exemple dns doit se connecter à dnsinternet, miroirubuntu,..
echo $HOSTNAME | egrep 'dns' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "dnsinternet"
    "ntpinternet"
    "miroirubuntu"
    )
fi
echo $HOSTNAME | egrep 'deploy' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "git"
    "miroirubuntu"
    )
fi
echo $HOSTNAME | egrep 'web' | grep -v "log" > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "miroirubuntu"
    "newrelic"
    "coffre"
    "cabestan-smtp"
    "cabestan-imap"
    "imap"
    "maileva"
    "mascadia"
    "epel-uri"
    "epel-ack"
    )
fi
echo $HOSTNAME | egrep 'worker' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "miroirubuntu"
    "lbp"	
    "newrelic"
    "coffre"
    "cabestan-smtp"
    "cabestan-imap"
    "maileva"
    "mascadia"
    "epel-uri"
    "epel-ack"
    "apns-gw"
    "apns-feedback"
    "creditmutuel"
    "reunica"
    )
fi
echo $HOSTNAME | egrep 'back' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "miroirubuntu"
    "lbp"
    "newrelic"
    "coffre"
    "cabestan-smtp"
    "cabestan-imap"
    "maileva"
    "mascadia"
    "epel-uri"
    "epel-ack"
    "apns-gw"
    "apns-feedback"
    "creditmutuel"
    "reunica"
    "centreoncoffre"
    )
fi

echo $HOSTNAME | egrep 'dmz' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "dnsinternet"
    "ntpinternet"
    "miroirubuntu"
    )
fi

echo $HOSTNAME | egrep 'all' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
    then listcheckok=(
    "dnsinternet"
    "ntpinternet"
    "miroirubuntu"
    "git"
    "lbp"
    "newrelic"
    "coffre"
    "cabestan-smtp"
    "cabestan-imap"
    "cabestan-smtp"
    "maileva"
    "mascadia"
    "epel-uri"
    "epel-ack"
    "apns-gw"
    "apns-feedback"
    "creditmutuel"
    "reunica"
    )
fi




# pour tous les autres serveurs
if [  "$listcheckok" == "" ] 
then
	listcheckok=(
    		"miroirubuntu"
	)
fi




# liste des connexions IP Port Protocole qui doivent être accessible sur la machine cible depuis la machine source
# ex si une machine doit se connecter à un serveur web, il faut qu'elle ait accès à tcp:443
function dnsinternet {
    check $1 "192.5.5.241" "53" "udp" $2
    check $1 "192.5.5.241" "53" "tcp" $2
}
function ntpinternet {
    check $1 "0.ubuntu.pool.ntp.org" "123" "udp" $2
    check $1 "1.ubuntu.pool.ntp.org" "123" "udp" $2
    check $1 "2.ubuntu.pool.ntp.org" "123" "udp" $2
    check $1 "3.ubuntu.pool.ntp.org" "123" "udp" $2
}
function miroirubuntu {
	check $1 "172.14.1.25" "80" "tcp" $2
 }
function git {
    check $1 "84.14.82.12" "22" "udp" $2
}
function lbp {
    	
	if [  "$ENV" == "prod" ] 
	then
		check $1 "172.19.1.200" "443" "tcp" $2

	elif [ "$ENV" == "preprod" ]
	then
		check $1 "172.17.1.201" "443" "tcp" $2

	elif [ "$ENV" == "rectech" ]
	then
		check $1 "172.16.5.202" "443" "tcp" $2

	elif [ "$ENV" == "recfonc" ]
	then
		check $1 "172.16.4.202" "443" "tcp" $2

	elif [ "$ENV" == "tma" ]
	then
		check $1 "172.16.1.202" "443" "tcp" $2

	elif [ "$ENV" == "tmc" ]
	then
		tmp="pas de liason depuis tmc"

	elif [ "$ENV" == "interop" ]
	then
		check $1 "172.16.3.202" "443" "tcp" $2

	elif [ "$ENV" == "demo" ]
	then
		check $1 "172.16.2.202" "443" "tcp" $2

	else
		check $1 "@todoenvinconnu" "443" "tcp" $2
	fi 

}
function centreoncoffre {
    	
	if [  "$ENV" == "prod" ] 
	then
		check $1 "172.19.1.231" "443" "tcp" $2

	elif [ "$ENV" == "tmc" ]
	then
		check $1 "172.17.1.231" "443" "tcp" $2
	fi

}
function newrelic {
	# ne teste qu'une partie des flux qui doivent être autorisés : 204.93.223.128/27
	check $1 "204.93.223.150" "80" "tcp" $2
	check $1 "204.93.223.150" "443" "tcp" $2
	check $1 "www.newrelic.com" "443" "tcp" $2
}
function coffre {
	check $1 "coffre.digiposte.local" "443" "tcp" $2
 }
function cabestan-smtp {

	if [ "$ENV" == "prod" ]
	then
		check $1 "213.41.75.90" "2526" "tcp" $2
	elif [ "$ENV" == "preprod" ]
	then
		check $1 "213.41.75.90" "25" "tcp" $2
	else
		check $1 "213.41.75.90" "25" "tcp" $2
	fi 

 }
function cabestan-imap {
	check $1 "213.41.75.90" "143" "tcp" $2
 }

function maileva {
	# le flux sur port 20 ne ping pas mais cela semble être normal car bloqué
	# check $1 "81.27.21.14" "20" "tcp" $2
	check $1 "81.27.21.14" "21" "tcp" $2
 }
function mascadia {
	if [ "$ENV" == "prod" ]
	then
		check $1 "94.124.131.207" "80" "tcp" $2
	elif [ "$ENV" == "preprod" ]
	then
		check $1 "94.124.131.189" "80" "tcp" $2
	else
		check $1 "94.124.131.189" "80" "tcp" $2
		check $1 "sna-sca-2-prp.cvf.fr" "80" "tcp" $2
	fi 
 }
function epel-uri {

	if [ "$ENV" == "prod" ]
	then
		check $1 "160.92.106.219" "443" "tcp" $2
		check $1 "www.plateformedepaiement.fr" "443" "tcp" $2

	elif [ "$ENV" == "preprod" ]
	then
		check $1 "160.92.145.106" "443" "tcp" $2
	else
		check $1 "160.92.145.106" "443" "tcp" $2
	fi 

 }
function epel-ack {

	if [ "$ENV" == "prod" ]
	then
		check $1 "160.92.106.221" "443" "tcp" $2
		check $1 "digiposte-interface-plateformedepaiement-fr.aw.atosorigin.com" "443" "tcp" $2

	elif [ "$ENV" == "preprod" ]
	then
		check $1 "160.92.145.108" "443" "tcp" $2
	else
		check $1 "160.92.145.108" "443" "tcp" $2
	fi 
 }
function apns-gw {
	# ne teste pas tous les flux, seulment gateway.push.apple.com: 	17.0.0.0/8
	# les tests ne fonctionnenet pas à priori du fait d'appel, ils sont désactivés
	#check $1 "17.0.0.0" "2195" "tcp" $2
	#check $1 "gateway.push.apple.com" "2196" "tcp" $2
	tmp="notest"
 }
function apns-feedback {
	# ne test pas tous les flux, seulement feedback.push.apple.com	17.0.0.0/8
	# les tests ne fonctionnenet pas à priori du fait d'appel, ils sont désactivés
	#check $1 "17.0.0.0" "2196" "tcp" $2
	#check $1 "feedback.push.apple.com" "2196" "tcp" $2
	tmp="notest"
 }

function reunica {
	if [ "$ENV" == "prod" ]
	then
		check $1 "62.23.36.142" "443" "tcp" $2
		check $1 "83.206.181.206" "443" "tcp" $2
		check $1 "90.80.8.46" "443" "tcp" $2
		check $1 "espacemembre.reunica.com" "443" "tcp" $2

	elif [ "$ENV" == "preprod" ]
	then
		check $1 "62.23.36.142" "443" "tcp" $2
		check $1 "83.206.181.206" "443" "tcp" $2
		check $1 "90.80.8.46" "443" "tcp" $2
		
	else
		check $1 "83.206.181.219" "443" "tcp" $2
		check $1 "90.80.8.59" "443" "tcp" $2
		check $1 "62.23.36.155" "443" "tcp" $2
	fi 
 }


function creditmutuel {
	if [ "$ENV" == "prod" ]
	then
		check $1 "93.20.42.112" "443" "tcp" $2
		check $1 "93.20.46.112" "443" "tcp" $2
		check $1 "194.51.217.112" "443" "tcp" $2
		check $1 "services.arkea.com" "443" "tcp" $2

	elif [ "$ENV" == "preprod" ]
	then
		$tmp="pasdepreproduction"
	else
		check $1 "80.124.164.156" "443" "tcp" $2
		check $1 "rec-services.arkea.com" "443" "tcp" $2

	fi 
 }





# fonction réalisant un test netcat et renvoyant en sortie standard
# check <resultatAttendu(open|close)    <url/IP    <port    <protocole    <nomService   
# check "open" "localhost" "80" "tcp" "webTest" 
function check {

    if [[ $1 == "open" ]] ; 
        then expectedncResult=0;
        else expectedncResult=1;
    fi
    url=$2;
    port=$3;
    serviceName=$5;

    if [[ $4 == "udp" ]] ; 
        then protocol="-u";
	else protocol="  ";
    fi
    
    # lancement du test netcat # 2   /dev/null pour eviter les nc: getaddrinfo: Name or service not known
    ncCommand="nc -z -w 5 "
    $ncCommand $protocol $url $port 2>/dev/null
    ncResult=$?

    
    if [[ $ncResult -eq $expectedncResult ]] 
    then
        if  [[ $VERBOSE -eq 1 ]]
        then
            echo -e $ECHOCOLORGREEN "OK" $1 ": " $ncCommand "-v "$protocol $url $port" : "$4 $HOSTNAME "-->" $serviceName $ECHOCOLORNORMAL
        fi
          nbResutsOK=$(($nbResutsOK+1))
    else
        if  [[ $VERBOSE -eq 1 ]]
        then
            echo -e $ECHOCOLORRED "KO" $1 ": " $ncCommand "-v "$protocol $url $port" : " $4 $HOSTNAME "-->" $serviceName $ECHOCOLORNORMAL
        fi
        nbResutsKO=$(($nbResutsKO+1))
    fi
}




#liste de tous les tests à réaliser
testList=(
    "dnsinternet"
    "ntpinternet"
    "miroirubuntu"
    "git"
    "lbp"
    "newrelic"
    "coffre"
    "renuica"
    "creditmutuel"
    "cabestan-smtp"
    "cabestan-imap"
    "cabestan-smtp"
    "maileva"
    "mascadia"
    "epel-uri"
    "epel-ack"
    "apns-gw"
    "apns-feedback"
    "centreoncoffre"
)






#lance les checks OK ou KO
for itemTestList in "${testList[@]}"
do
    expectedNetworkTestTesult="close"
    testToDo=$itemTestList

    for j in "${listcheckok[@]}"
    do
        if [[ $itemTestList == $j ]] ; 
            then expectedNetworkTestTesult="open"; 
            break;
        fi
    done
    
    #lance le test (web "open" web)
    if [ $expectedNetworkTestTesult == "open" ]
    then
        $itemTestList $expectedNetworkTestTesult $itemTestList;
    fi
done



# affiche les resultats synthetiques finaux
if  [ $nbResutsKO -eq 0 ] && [ $nbResutsOK -eq 0 ]
    then 
    echo -e $ECHOCOLORGREEN "OK - "$HOSTNAME" Connexion Digiposte vers Internet | nbtestok="$nbResutsOK"nb nbtestko="$nbResutsKO"nb"  $ECHOCOLORNORMAL
    exit 3
fi



# affiche les resultats synthetiques finaux
if  [ $nbResutsKO -eq 0 ] && [ $nbResutsOK -gt 0 ]
    then 
    echo -e $ECHOCOLORGREEN "OK - "$HOSTNAME" Connexion Digiposte vers Internet | nbtestok="$nbResutsOK"nb nbtestko="$nbResutsKO"nb"  $ECHOCOLORNORMAL
    exit 0;
elif [ $nbResutsKO -gt 0 ] && [ $nbResutsOK -eq 0 ]
    then 
    echo -e $ECHOCOLORRED "UNKNOWN - "$HOSTNAME" Connexion Digiposte vers Internet | nbtestok="$nbResutsOK"nb nbtestko="$nbResutsKO"nb"  $ECHOCOLORNORMAL
    exit 3;
else 
    echo -e $ECHOCOLORRED "CRITICAL - "$HOSTNAME" Connexion Digiposte vers Internet | nbtestok="$nbResutsOK"nb nbtestko="$nbResutsKO"nb"  $ECHOCOLORNORMAL
    exit 2;
fi







