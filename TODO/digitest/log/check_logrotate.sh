#!/bin/bash

# Analyse les logs des serveurs et identifie les problèmes de logrotate
# lancer check_logrotates.sh pour passer sur tous les serveurs
#ssh digi-db-recfonc 'bash -s ' < check_logrotate.sh >db-recfonc-logs-$(date "+%T_%Y%m%d").txt
#ssh digi-web1-recfonc 'bash -s ' < check_logrotate.sh >web1-recfonc-logs-$(date "+%T_%Y%m%d").txt
#ssh digi-web2-recfonc 'bash -s ' < check_logrotate.sh >web2-recfonc-logs-$(date "+%T_%Y%m%d").txt
#ssh digi-dmz-recfonc 'bash -s ' < check_logrotate.sh >dmz-recfonc-logs-$(date "+%T_%Y%m%d").txt
#ssh digi-support-recfonc 'bash -s ' < check_logrotate.sh >support-recfonc-logs-$(date "+%T_%Y%m%d").txt
#ssh digi-worker-recfonc 'bash -s ' < check_logrotate.sh >worker-recfonc-logs-$(date "+%T_%Y%m%d").txt


# directory to analyse ex : dirLogList= "/var/log/ /log/"
dirLogList="/var/log/ /data/var/log/ /data/opt/graphite/storage/log/ /var/www/"
expectedLogFileNumber=10
expectedLogFileNumberTolerance=12


if [[ $1 == "-a" ]] || [[ $2 == "-a" ]] || [[ $3 == "-a" ]]; 
then
	
	listserver="db-recfonc web1-recfonc dmz-recfonc support-recfonc worker-recfonc"
	
    #listserver=$listserver" dns1-rectech dns2-rectech web1-rectech web2-rectech deploy-rectech worker1-rectech worker2-rectech back1-rectech back2-rectech rp1-rectech rp2-rectech lb1-rectech lb2-rectech db1-rectech db2-rectech kpi-rectech log-web-rectech log-collector-rectech mq1-rectech mq2-rectech nfs-rectech idx1-rectech idx2-rectech" 


    #listserver=$listserver" dns1-tmc dns2-tmc web1-tmc web2-tmc web3-tmc deploy-tmc worker1-tmc worker2-tmc back1-tmc back2-tmc rp1-tmc rp2-tmc lb1-tmc lb2-tmc db1-tmc db2-tmc kpi-tmc log-web-tmc log-collector-tmc mq1-tmc mq2-tmc nfs-tmc idx1-tmc"

	#listserver=$listserver" dns1-prod-a nfs-prod-a mq1-prod-a mq2-prod-a worker1-prod-a worker2-prod-a db1-prod-a db2-prod-a idx1-prod-a kpi-prod-a log-web-prod-a log-collector-prod-a rp1-prod-a lb1-prod-a web11-prod-a web12-prod-a web13-prod-a dns1-prod-b nfs-prod-b mq1-prod-b mq2-prod-b worker1-prod-b worker2-prod-b db1-prod-b db2-prod-b idx1-prod-b kpi-prod-b log-web-prod-b log-collector-prod-b rp1-prod-b lb1-prod-b web21-prod-b web22-prod-b web23-prod-b idx1-prod-a idx1-prod-b"

	#listserver=$listserver" dns1-preprod-a nfs-preprod-a mq1-preprod-a mq2-preprod-a worker1-preprod-a worker2-preprod-a db1-preprod-a db2-preprod-a idx1-preprod-a kpi-preprod-a log-web-preprod-a log-collector-preprod-a rp1-preprod-a lb1-preprod-a web11-preprod-a web12-preprod-a web13-preprod-a dns1-preprod-b nfs-preprod-b mq1-preprod-b mq2-preprod-b worker1-preprod-b worker2-preprod-b db1-preprod-b db2-preprod-b idx1-preprod-b kpi-preprod-b log-web-preprod-b log-collector-preprod-b rp1-preprod-b lb1-preprod-b web21-preprod-b web22-preprod-b web23-preprod-b  idx1-preprod-a idx1-preprod-b"


	#implementer tests
	#listserver=$listserver" all-interop all-tma all-demo"

	for servertmp in $listserver; do
		# grep -v ATTENTION : enleve le message d'avertissement à chaque connexion machine
		ssh $servertmp 'bash -s ' 2>$1 <check_logrotate.sh | grep -v ATTENTION
		echo ""
	done
	exit
fi



#affiche le détail des tests et des erreurs
if [[ $1 == "-v" ]] || [[ $2 == "-v" ]] || [[ $3 == "-v" ]]; 
then
    VERBOSE=1
    NAGIOSSTYLEEXIT=0
else 
    VERBOSE=0
    NAGIOSSTYLEEXIT=1
fi


############## DESACTIVE LES OPTIONS CI-DESSUS qui ne passent pas lors des apples distants ##########
#@scancouleur
VERBOSE=1
NAGIOSSTYLEEXIT=0


    

#initialisation
if [[ $NAGIOSSTYLEEXIT == 0 ]] ; 
    then
    ECHOCOLORGREEN="\\033[1;32m"
    ECHOCOLORRED="\\033[1;31m"
    ECHOCOLORORANGE="\e[0;33m" # Yellow
    ECHOCOLORNORMAL="\\033[0;39m"
	#sortie pour html
    #ECHOCOLORGREEN="<font color='green'>"
    #ECHOCOLORRED="<font color='red'>"
    #ECHOCOLORORANGE="<font color='orange'>"
    #ECHOCOLORNORMAL="</font><br />"

fi







#@todo : checker les problèmes de permission sur les répertoires ou fichiers
#@todo : traiter les cas ou le répertoire n'existe pas
for dirLog in $dirLogList; do

    #recherche les fichiers de log non rotate
    for logFile in `sudo find -P $dirLog -type f  -mtime -3 | grep -v gz | grep -v bin | grep -v .lock | grep -v wtmp | grep -v .1 `; do
        logFileName=$(basename $logFile)
        logFileNameWithoutExtension=${logFileName%.*}
        logFileDir=$(dirname $logFile) 
        
        
        # teste si il existe des logrotate
        numberLogFile=`find -P $logFileDir -type f  -mtime -12 | grep  gz  | grep "/"$logFileName |  wc -l `;

        if [[ $numberLogFile -eq 0 ]]; then 
            echo -e $ECHOCOLORRED "KO - $HOSTNAME" $logFileDir"/"$logFileName  ":" $numberLogFile "log rotate files - Expected some" $ECHOCOLORNORMAL; 
        else
            echo -e $ECHOCOLORGREEN "OK - $HOSTNAME" $logFileDir"/"$logFileName  ":" $numberLogFile "log rotate files exist" $ECHOCOLORNORMAL; 
            


            
            #teste si il y a le bon nombre de log rotate (rotate dans logrotate)
            if [[ $numberLogFile -eq $expectedLogFileNumber  ]]; then 
                echo -e $ECHOCOLORGREEN "OK - $HOSTNAME" $logFileDir"/"$logFileName  ":" $numberLogFile "log rotate files : "$expectedLogFileNumber "expected" $ECHOCOLORNORMAL; 
            else
		if [[ $numberLogFile -gt $expectedLogFileNumberTolerance  ]]; then 
                	echo -e $ECHOCOLORRED "KO - $HOSTNAME" $logFileDir"/"$logFileName  ":" $numberLogFile "log rotate files : "$expectedLogFileNumber "expected" $ECHOCOLORNORMAL; 
		else
			echo -e $ECHOCOLORORANGE "WARNING -  $HOSTNAME" $logFileDir"/"$logFileName  ":" $numberLogFile "log rotate files : "$expectedLogFileNumber "expected" $ECHOCOLORNORMAL;
		fi
            fi



            #teste si le logrotate est basé sur les dates (dateext dans logrotate)
            # les dates sont privilégiées car simplifient les sytèmes d'archivages qui par défaut ont tendance à copier tout de ce qui été modifié y compris le nom du fichier)
            logFileNameTmpDate=`find -P $logFileDir -type f  -mtime -12 | grep  gz  | grep "/"$logFileName `;
            echo $logFileNameTmpDate | egrep '2013' > /dev/null 2>&1
            if [[ $? -eq 0 ]] ;
            then
                echo -e $ECHOCOLORGREEN "OK - $HOSTNAME" $logFileDir"/"$logFileName  ": log rotate name based on date" $ECHOCOLORNORMAL; 
            else
                echo -e $ECHOCOLORRED "KO - $HOSTNAME" $logFileDir"/"$logFileName  ": log rotate name NOT based on date" $ECHOCOLORNORMAL; 
            fi

	    #teste si le logrotate utilise des compressions
        numberLogFileNotCompressed=`find -P $logFileDir -type f  -mtime -12 | grep -v gz  | grep "/"$logFileName |  wc -l `;
        numberLogFileCompressed=`find -P $logFileDir -type f  -mtime -12 | grep  gz  | grep "/"$logFileName |  wc -l `;

        let numberLogFileNotCompressed--  
            if [ $numberLogFileNotCompressed -lt 1 ] && [ $numberLogFileCompressed -gt 1 ] ;
            then
                echo -e $ECHOCOLORGREEN "OK - $HOSTNAME" $logFileDir"/"$logFileName " : " $numberLogFileCompressed" log rotate compressed " $ECHOCOLORNORMAL; 
            else
                echo -e $ECHOCOLORRED "KO - $HOSTNAME" $logFileDir"/"$logFileName  " : " $numberLogFileNotCompressed" log rotate NOT compressed - "$numberLogFileCompressed "log compressed " $ECHOCOLORNORMAL; 
            fi

         

         #teste l'horaire du logrotate
         fileNameDate=`find -P $logFileDir -type f  -mtime -5 | grep  gz  | grep $logFileName | head -n1 `
         logRotateDate=`ls -l $fileNameDate | awk -F ' ' '{print $8; }'  `
         expectedlogRotateDate="00:"
         echo $logRotateDate | egrep $expectedlogRotateDate > /dev/null 2>&1
         if [[ $? -eq 0 ]] ;
         then
            echo -e $ECHOCOLORGREEN "OK - $HOSTNAME" $logFileDir"/"$logFileName  ": log rotate executed on time " $ECHOCOLORNORMAL; 
         else
            echo -e $ECHOCOLORORANGE "WARNING - $HOSTNAME" $logFileDir"/"$logFileName  ": log rotate NOT executed on time "$logRotateDate "Expected:"$expectedlogRotateDate"**" $ECHOCOLORNORMAL; 
         fi




        fi

    done
done

