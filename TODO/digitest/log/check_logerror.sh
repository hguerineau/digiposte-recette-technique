#!/bin/bash

# Analyse les logs des serveurs et compte le nb d'erreurs dans les fichiers

#ssh db-recfonc 'bash -s ' < check_logerror.sh >db-recfonc-check_logerror.html
#ssh web1-recfonc 'bash -s ' < check_logerror.sh >web1-recfonc-check_logerror.html
#ssh dmz-recfonc 'bash -s ' < check_logerror.sh >dmz-recfonc-check_logerror.html
#ssh support-recfonc 'bash -s ' < check_logerror.sh >support-recfonc-check_logerror.html
#ssh worker-recfonc 'bash -s ' < check_logerror.sh >worker-recfonc-check_logerror.html
#ssh web2-recfonc 'bash -s ' < check_logerror.sh >web2-recfonc-check_logerror.html

# directory to analyse ex : dirLogList= "/var/log/ /log/"
dirLogList="/var/log/ /data/var/log/"

if [[ $1 == "-a" ]] || [[ $2 == "-a" ]] || [[ $3 == "-a" ]]; 
then
	
	listserver="db-recfonc web1-recfonc dmz-recfonc support-recfonc worker-recfonc"
	
    listserver=$listserver" dns1-rectech dns2-rectech web1-rectech web2-rectech deploy-rectech worker1-rectech worker2-rectech back1-rectech back2-rectech rp1-rectech rp2-rectech lb1-rectech lb2-rectech db1-rectech db2-rectech kpi-rectech log-web-rectech log-collector-rectech mq1-rectech mq2-rectech nfs-rectech idx1-rectech idx2-rectech" 


    listserver=$listserver" dns1-tmc dns2-tmc web1-tmc web2-tmc web3-tmc deploy-tmc worker1-tmc worker2-tmc back1-tmc back2-tmc rp1-tmc rp2-tmc lb1-tmc lb2-tmc db1-tmc db2-tmc kpi-tmc log-web-tmc log-collector-tmc mq1-tmc mq2-tmc nfs-tmc idx1-tmc"

	#listserver=$listserver" dns1-prod-a nfs-prod-a mq1-prod-a mq2-prod-a worker1-prod-a worker2-prod-a db1-prod-a db2-prod-a idx1-prod-a kpi-prod-a log-web-prod-a log-collector-prod-a rp1-prod-a lb1-prod-a web11-prod-a web12-prod-a web13-prod-a dns1-prod-b nfs-prod-b mq1-prod-b mq2-prod-b worker1-prod-b worker2-prod-b db1-prod-b db2-prod-b idx1-prod-b kpi-prod-b log-web-prod-b log-collector-prod-b rp1-prod-b lb1-prod-b web21-prod-b web22-prod-b web23-prod-b idx1-prod-a idx1-prod-b"

	#listserver=$listserver" dns1-preprod-a nfs-preprod-a mq1-preprod-a mq2-preprod-a worker1-preprod-a worker2-preprod-a db1-preprod-a db2-preprod-a idx1-preprod-a kpi-preprod-a log-web-preprod-a log-collector-preprod-a rp1-preprod-a lb1-preprod-a web11-preprod-a web12-preprod-a web13-preprod-a dns1-preprod-b nfs-preprod-b mq1-preprod-b mq2-preprod-b worker1-preprod-b worker2-preprod-b db1-preprod-b db2-preprod-b idx1-preprod-b kpi-preprod-b log-web-preprod-b log-collector-preprod-b rp1-preprod-b lb1-preprod-b web21-preprod-b web22-preprod-b web23-preprod-b  idx1-preprod-a idx1-preprod-b"


	#implementer tests
	#listserver=$listserver" all-interop all-tma all-demo"

	for servertmp in $listserver; do
		# grep -v ATTENTION : enleve le message d'avertissement à chaque connexion machine
		ssh $servertmp 'bash -s ' 2>$1 <check_logerror.sh | grep -v ATTENTION
		echo ""
	done
	exit
fi

# 
#sortie ecran pour cat
ECHOCOLORGREEN="\\033[1;32m"
ECHOCOLORRED="\\033[1;31m"
ECHOCOLORORANGE="\\033[1;33m"
ECHOCOLORNORMAL="\\033[0;39m"




#@todo : checker les problèmes de permission sur les répertoires ou fichiers
#@todo : traiter les cas ou le répertoire n'existe pas
for dirLog in $dirLogList; do

    #recherche les fichiers de log non rotate
    for logFile in `sudo find -P $dirLog -type f -mtime -3 | grep -v gz | grep -v ".1" | grep -v bin `; do
        
        logFileName=$(basename $logFile)
        logFileNameWithoutExtension=${logFileName%.*}
        logFileDir=$(dirname $logFile) 
        
        
        #cat $logFileDir"/"$logFileName | grep "error" | wc -l   
        errorFile=`echo $logFileName | egrep "error|.err" | wc -l`
        if [[ $errorFile -eq 0 ]]; then 
        
            numberLog=`sudo cat $logFileDir"/"$logFileName | egrep "error|ERROR|FATAL|fatal|SEVERE|severe|CRITICAL|critical|1\.1\|500\||1\.1\|501\||1\.1\|502\|" | grep -v "parse_error" | grep -v "-error"| wc -l`
            if [[ $numberLog -gt 0 ]]; then 
                echo -e $ECHOCOLORRED "KO - "$HOSTNAME $logFileDir"/"$logFileName  ":" $numberLog "errors found - Expected 0" $ECHOCOLORNORMAL; 
            else
                echo -e $ECHOCOLORGREEN "OK - "$HOSTNAME $logFileDir"/"$logFileName  ":" $numberLog "errors found - Expected 0" $ECHOCOLORNORMAL; 
            fi  

            numberLog=`sudo cat $logFileDir"/"$logFileName | egrep "warn|WARN|notice|NOTICE" | wc -l`
            if [[ $numberLog -gt 0 ]]; then 
                echo -e $ECHOCOLORRED "KO - "$HOSTNAME $logFileDir"/"$logFileName  ":" $numberLog "warn-notice found - Expected 0" $ECHOCOLORNORMAL; 
            else
                echo -e $ECHOCOLORGREEN "OK - "$HOSTNAME $logFileDir"/"$logFileName  ":" $numberLog "warn-notice found - Expected 0" $ECHOCOLORNORMAL; 
            fi  

            
            numberLog=`sudo cat $logFileDir"/"$logFileName | egrep "info|INFO" | wc -l`
            if [[ $numberLog -gt 0 ]]; then 
                echo -e $ECHOCOLORORANGE "KO - "$HOSTNAME $logFileDir"/"$logFileName  ":" $numberLog "info found" $ECHOCOLORNORMAL; 
            else
                echo -e $ECHOCOLORGREEN "OK - "$HOSTNAME $logFileDir"/"$logFileName  ":" $numberLog "info found" $ECHOCOLORNORMAL; 
            fi  
        else
            numberLog=`sudo cat $logFileDir"/"$logFileName | wc -l`
            if [[ $numberLog -gt 0 ]]; then 
                echo -e $ECHOCOLORRED "KO - "$HOSTNAME $logFileDir"/"$logFileName  ":" $numberLog "errors found - Expected 0" $ECHOCOLORNORMAL; 
            else
                echo -e $ECHOCOLORGREEN "OK - "$HOSTNAME $logFileDir"/"$logFileName  ":" $numberLog "errors found - Expected 0" $ECHOCOLORNORMAL; 
            fi  

        fi
        
        #teste le nb de ligne et renvoie une alerte si trop important
        maxAcceptableLines=300000
        numberLog=`sudo cat $logFileDir"/"$logFileName | wc -l`
        if [[ $numberLog -gt $maxAcceptableLines ]]; then 
                echo -e $ECHOCOLORRED "KO - "$HOSTNAME $logFileDir"/"$logFileName  ":" $numberLog " lines found : expected less than "$maxAcceptableLines $ECHOCOLORNORMAL; 
            fi  



    done
done

