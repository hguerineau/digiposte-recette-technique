#!/bin/bash


madate=$(date '+%Y%m%d-%H%M%S')

serverList="db-recfonc web1-recfonc dmz-recfonc support-recfonc worker-recfonc"
#serverList=$serverList" dns1-rectech dns2-rectech web1-rectech web2-rectech deploy-rectech worker1-rectech worker2-rectech back1-rectech back2-rectech rp1-rectech rp2-rectech lb1-recteh lb2-rectech db1-rectech db2-rectech kpi-rectech log-web-rectech log-collector-rectech mq1-rectech mq2-rectech nfs-rectech" 


for server in $serverList; do

	# Analyse les logs des serveurs et identifie les problèmes de logrotate
	echo "*******************" >>$madate-check_errors.txt
	echo "***" $server "***" >>$madate-check_errors.txt
	echo "*******************" >>$madate-check_errors.txt

	ssh $server 'bash -s ' < check_logerror.sh >>$madate-check_logerrors.txt

done
