
Ensemble de script de tests de la plateforme digiposte

dex : tests du dossier d'exploitation
	� lancer sur chaque serveur, il lance les commandes d�crites dans le dossier d'exploitation et v�rifie que les retours sont conformes � ce qui est d�crit dans le dossier d'exploitation
	il renvoie un r�sultat au format nagios ou en mode couleur pour les humains
	
	STATUS et avancement : 1% de la cible : en attente dossier d'eploitation
	
	
	
	
kpi : permet de tester les m�triques graphite
	
	check_metric.sh : le script font des appels aux m�triques kpi http://admin.recfonc.u-post.fr/graphite/ et v�rifient :
		la pr�sence des m�triques,
		la pr�sence de valeur
		si les valeurs �voluent dans le temps

	les retours sont au format centreon ou en mode humain color�
	
	Statut et avancement : 
		des sondes sont �crites mais pas lanc�es (en fin de fichier)
		les sondes sont � adapter pour rectech, tmc, preprod, prod
		certaines sondes sont � revoir car la SFTD a �volu� et certaines sondes ont �t� supprim�es/renomm�es
		il faut ajouter des tests qui permettent de valider que les valeurs bougent dans le temps (usage de diff dans les requetes graphite
		avancement 40%

	generate_metrics.sh : fait des appels http pour g�n�rer des m�triques business kpi (car en recette fonctionnelle il y a peu d'appels


log
	** check_logrotate.sh : v�rifie les logrotate sur une machine (pr�sence de zip, de date dans les noms, de la bonne p�riode de retenetion
	** check_logrotates.sh : lance check_logrotate.sh sur plusieurs serveurs
	
	les retours sont en mode nagios ou humain color�
		
	** check_logerror.sh : compte le nombre d'erreur/warning dans les logs du serveur
	
	
	les retours sont en mode nagios ou humain color�
	
	 
	 
network : fait les tests r�seaux d'ouverture r�seaux et de validation IP/DNS

		check_network_digiposte-to-internet.sh : 
		check_network_dmz-to-backend.sh 
		check_network_internet-to-dmz.sh 
		check_network_internal.sh         : flux interne � la plateforme
		
		les retours sont en mode nagios ou humain color�














