#!/bin/bash

# test du dossier d exploitation


ECHOCOLORGREEN="\\033[1;32m"
ECHOCOLORRED="\\033[1;31m"
ECHOCOLORNORMAL="\\033[0;39m"

nbResultsOK=0
nbResultsKO=0

function test {
#echo "command : " $command
result=$($command 2>&1)
echo $result | grep  "$expected"
return=$?

if  [ $return -eq 0 ]
    then 
        echo -e $ECHOCOLORGREEN "OK - "$HOSTNAME ":COMMAND " $command " :EXPECT " $expected ": RESULT " $return  $ECHOCOLORNORMAL; 
        nbResutsOK=$(($nbResutsOK+1))
    else 
        echo -e $ECHOCOLORRED "CRITICAL - "$HOSTNAME ":COMMAND " $command " :EXPECT " $expected ": RESULT "  $return  $ECHOCOLORNORMAL; 
        nbResutsKO=$(($nbResutsKO+1))
fi
echo ""
}






echo $HOSTNAME| egrep 'db' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
then
  
command="service mysql status"
expected="mysql start/running, process "
test $command $expected

command="sudo service mysql stop"
expected="mysql stop/waiting"
test $command $expected

command="service mysql status"
expected="mysql stop/waiting"
test $command $expected

command="sudo service mysql stop"
expected="stop: Unknown instance"
test $command $expected

command="sudo service mysql start"
expected="mysql start/running, process "
test $command $expected

command="sudo service mysql start"
expected="start: Job is already running: mysql"
test $command $expected

command="sudo service mysql restart"
expected="mysql start/running, process "
test $command $expected

command="service serviceinconnu start"
expected="serviceinconnu"
test $command $expected

fi




echo $HOSTNAME| egrep 'web' | egrep -v 'log' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
then
  
command="service apache status"
expected="@todo"
test $command $expected

command="sudo service mysql stop"
expected="mysql stop/waiting"
test $command $expected

command="service mysql status"
expected="mysql stop/waiting"
test $command $expected

command="sudo service mysql stop"
expected="stop: Unknown instance"
test $command $expected

command="sudo service mysql start"
expected="mysql start/running, process "
test $command $expected

command="sudo service mysql start"
expected="start: Job is already running: mysql"
test $command $expected

command="sudo service mysql restart"
expected="mysql start/running, process "
test $command $expected

command="service serviceinconnu start"
expected="serviceinconnu"
test $command $expected

fi










if  [[ $nbResutsKO -eq 0 ]] && [[ $nbResutsOK -gt 0 ]]
    then 
    echo -e $ECHOCOLORGREEN "OK - tests success:"$nbResutsOK " | tests error:"$nbResutsKO  $ECHOCOLORNORMAL
    exit 0;
elif [[ $nbResutsKO -gt 0 ]] && [[ $nbResutsOK -eq 0 ]]
    then 
    echo -e $ECHOCOLORRED "UNKNOWN - tests success:"$nbResutsOK " | tests error:"$nbResutsKO $ECHOCOLORNORMAL
    exit 3;
else 
    echo -e $ECHOCOLORRED "CRITICAL - tests success:"$nbResutsOK " | tests error:"$nbResutsKO $ECHOCOLORNORMAL
    exit 2;
fi

exit


