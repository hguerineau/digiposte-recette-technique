#!/bin/sh

# Usage : $0 <environnement>

exec 2>$HOME/test-users/log-test-depots.log

if [ -z $1 ];then
        echo "Usage : $0 <environnement_a_tester>"
        exit
fi

for i in `cat serverlist.txt`
        do
			sum=`ssh $i-$1 -f "md5sum /etc/apt/sources.list"`
			if [ -z $sum ];then
				echo "$i-$1 : Machine inexistante, injoingable ou fichier sources.list absent --> Contreole manuel !"
			else
				echo "MD5 de /etc/apt/sources.list sur $i-$1 : $sum"
			fi
        done



