#!/bin/sh
# DOCAPOST DPS

#ce script :
#ajoute un coffre a une salle 
#ajoute un utilisateur admin dans le coffre avec le meme certificat que l administrateur
#ajoute un document
#telecharge le document

#parametres #ip a tester:84.14.109.190 (depuis mon laptop), 172.16.0.151 (depuis VMEP)
ip_cfec="172.16.0.151" 
peuplement="1"                                           # combien de fois on veut jouer ce scenario
empreinte="620e7c670f69c1c1c7c165353a4f42cf"     # empreinte du fichier a uploader
archive="/home/hguerineau/SAFE_ADMIN/Anos_changelog.txt.zip"                           # fichier a uploader
certificat="/home/hguerineau/SAFE_ADMIN/CERTIFICATS/adm.rectech.coffre.pem"             # certificat admin partie pub+key
certcfe="/home/hguerineau/SAFE_ADMIN/CERTIFICATS/usr.rectech.coffre.pem"                # certificat de l utilisateur a ajouter partie pub  
curlexec="/usr/bin/curl -k -s -i --connect-timeout 20"
pass=""                                                  # le mdp du certificat admin 
numcfec="7"                                              # le numero de la salle 
#script
i=1
while [ $i -le $peuplement ]
do

nom=$(date +"%H%M%S%N")
#session
session=`$curlexec -F cfec=$numcfec -F cfe=1 "https://$ip_cfec/cfec/sess/init.php" -E $certificat:$pass |grep CFEC_SESSION: |awk '{print $2}'`
session=`/bin/echo $session | /usr/bin/tr -dc [:alnum:]`
echo "Session : " $session

#creation coffre
creation_coffre=`$curlexec -F CFEC_SESSION=$session -F nom=$nom "https://$ip_cfec/cfec/safe/create.php" -E $certificat:$pass |grep CFEC_SUBSET_ID |awk '{print $2}'`
id_cfe=`/bin/echo $creation_coffre | /usr/bin/tr -dc [:alnum:]`
echo "Coffre : " $id_cfe


#recherche numero de profil
cfe_prof=`$curlexec -F CFEC_SESSION=$session -F cfe=$id_cfe "https://$ip_cfec/cfec/profil/list.php" -E $certificat:$pass |grep CFEC_LISTPROFIL_0: |awk '{print $2}'`
cfe_prof=`/bin/echo $cfe_prof | /usr/bin/tr -dc [:alnum:]`
cfe_prof=`/bin/echo $(echo $cfe_prof | cut -d"a" -f1)`
echo "Obtention du profil : " $cfe_prof 

#creation utilisateur
create_user=`$curlexec -F CFEC_SESSION=$session -F profilId=$cfe_prof -F prenom=docapost -F nom=$id_cfe -F mail=bench@docapost-dps.com -F civ=docapost -F cfe=$id_cfe -F userfile=@$certcfe "https://$ip_cfec/cfec/user/create.php" -E $certificat:$pass`

#on ouvre une session dans le nouveau coffre
s=`$curlexec -F cfec=$numcfec -F cfe=$id_cfe "https://$ip_cfec/cfec/sess/init.php" -E $certificat:$pass |grep CFEC_SESSION: |awk '{print $2}'`
s=`/bin/echo $s | /usr/bin/tr -dc [:alnum:]`
echo "Session : " $s

#recherche id conteneur
cont_id=`$curlexec -F CFEC_SESSION=$s -F contIdParent=-1 -F contName=$id_cfe "https://$ip_cfec/cfec/fld/create.php" -E $certificat:$pass`
cont_id=`$curlexec -F CFEC_SESSION=$s -F contIdParent=-1 -F contName=$id_cfe "https://$ip_cfec/cfec/fld/create.php" -E $certificat:digiposte |grep CFEC_CONTID: |awk '{print $2}'`
cont_id=`/bin/echo $cont_id | /usr/bin/tr -dc [:alnum:]`
echo "Creation du conteneur : " $cont_id

#on upload un document
upload_doc=`$curlexec -F CFEC_SESSION=$s -F contID=$cont_id -F empreinte=$empreinte -F algoEmpreinte=SHA1 -F userfile=@$archive "https://$ip_cfec/cfec/doc/upldxml.php" -E $certificat:$pass |grep CFEC_ARCHID |awk '{print $2}'`
upload_doc=`/bin/echo $upload_doc | /usr/bin/tr -dc [:alnum:]`; 
echo "Depot : " $upload_doc

#on telecharge le document
download_doc=`$curlexec -F CFEC_SESSION=$s -F archID=$upload_doc -F mode=org "https://$ip_cfec/cfec/doc/dnldxml.php" -E $certificat:$pass |grep CFEC_STATUS |awk '{print $3}'`
download_doc=`/bin/echo $download_doc | /usr/bin/tr -dc [:alnum:]`
download_doc="$upload_doc"
echo "Retrait : " $download_doc

let i=1+$i
done
