# #!/bin/sh

#parametres #ip a tester:84.14.109.190 (depuis mon laptop), 172.16.0.151 (depuis VMEP)
ip_cfec="172.16.0.151"
CFE=$1

certificat="/home/hguerineau/SAFE_ADMIN/CERTIFICATS/adm.rectech.coffre.pem"             # certificat admin partie pub+key
certcfe="/home/hguerineau/SAFE_ADMIN/CERTIFICATS/usr.rectech.coffre.pem"                # certificat de l utilisateur a ajouter partie pub
curlexec="/usr/bin/curl -k -s -i --connect-timeout 20"
pass=""                                                  # le mdp du certificat admin
numcfec="7"                                              # le numero de la salle
IS_ARCHIVE_CONTAINER_PRESENT="NO"
IS_IN_CONTAINER_PRESENT="NO"


#script
nom=$(date +"%H%M%S%N")
#session
session=`$curlexec -F cfec=$numcfec -F cfe=$CFE "https://$ip_cfec/cfec/sess/init.php" -E $certificat:$pass |grep CFEC_SESSION: |awk '{print $2}'`
session=`/bin/echo $session | /usr/bin/tr -dc [:alnum:]`
#echo "Session : $session"

#structure coffre

CONT_NB=`$curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass |grep CFEC_LISTCONT_NB:|awk -F "\r" '{print $2}'

echo "Nombre de container dans le coffre : $CONT_NB"

for (( container =0 ; container < $CONT_NB ; container++ ))
do
        CONT_ID=`$curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass`
        # CONT_ID=`$curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass |grep CFEC_LISTCONT_$container:|a$
        echo "CONTAINER ID : " $CONT_ID
        DIRNAME=`$curlexec -F CFEC_SESSION=$session -F contId=$CONT_ID "https://$ip_cfec/cfec/fld/name.php" -E $certificat:$pass |grep CFEC_CONTNAME:|awk '{print $2}'
        echo "CONTAINER Name : " $DIRNAME
        if [[ $DIRNAME = archive* ]] ; then
                IS_ARCHIVE_CONTAINER_PRESENT="YES"
                echo "dossier archive existe"
        elif [[ $DIRNAME = in* ]] ; then
                IS_IN_CONTAINER_PRESENT="YES"
                echo "dossier in existe"
        fi
done


echo "**********  TEST *************"

#NB_ARCHIVE=`$curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass | grep CFEC_LISTARCH_NB:|awk -F "\r" '$'

#echo "Nombre de archive: $NB_ARCHIVE"

#for (( archive =0 ; archive < $NB_ARCHIVE ; archive++ ))
#do
#        ARCHID=`$curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass | grep CFEC_LISTARCH_$archive:| aw$

#       ARCHNAME=`$curlexec -F CFEC_SESSION=$session -F archId=$ARCHID "https://$ip_cfec/cfec/doc/stat.php" -E $certificat:$pass |grep CFEC_DOCNAME:| awk -F$

#        ARCHNAMETEST=`echo $ARCHNAME | cut -c 1-110`
#        #echo "ARCHNAME:" $ARCHNAMETEST;
#        ARCHNAMEDECODE=`echo $ARCHNAMETEST|base64 -d`
#        echo "N°:" $archive " - ID:" $ARCHID " - NAME:" $ARCHNAMEDECODE
#done

