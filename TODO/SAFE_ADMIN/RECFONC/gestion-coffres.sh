#!/bin/bash
# set -x
export LANG=en_US.UTF-8
# -----------------------
ip_cfec="coffre.digiposte.local"
# -----------------------
CFE=$1
id_cfe=0
SessInit=0
ccd=999
environnement=`hostname |cut -d- -f2`

if [ $environnement = "recfonc" ]; then
    numcfec="6"
elif [ $environnement = "rectech" ]; then
    numcfec="7"
elif [ $environnement = "tmc" ]; then
    numcfec="1"
fi

#-------------------
	DbUser="digiposte"
	DbPass="digiposte"
	DbHost="db.digiposte.local"
#-------------------

CertificatRoot="/data/var/security/certificates/cecurity"

certificat="$CertificatRoot/adm.$environnement.$numcfec.coffre.pem"             # certificat admin partie pub+key
certcfe="$CertificatRoot/adm.$environnement.$numcfec.coffre.pem"                # certificat de l utilisateur a ajouter partie pub
curlexec="/usr/bin/curl -k -s -i --connect-timeout 20"
pass=""                                                  # le mdp du certificat admin


IS_ARCHIVE_CONTAINER_PRESENT="NO"
IS_IN_CONTAINER_PRESENT="NO"


function InitSession		# Ouvre une session sur le coffre passe en parametre. Est appele avant chaque commande
	{
	echo "Initialisation session pour le coffre $1"
	if [ $SessInit -eq 0 ]; then
		session=`$curlexec -F cfec=$numcfec -F cfe=$1 "https://$ip_cfec/cfec/sess/init.php" -E $certificat:$pass |grep CFEC_SESSION: |awk '{print $2}'`
		session=`/bin/echo $session | /usr/bin/tr -dc [:alnum:]`
		echo "Session : " $session
		if [ -z $session ]; then
			echo "Echec de l'initialisation du coffre $1"
			SessInit=0
			return 254
		else
			echo "Session initialisee pour le coffre $1"
			SessInit=1
			return 0
		fi
    else			#session deja initialisee
        echo "Session $1 deja initialisee"
		return 0
	fi
	}

# retourne le nombre de conteneurs
function ConteneurNb
	{
	# Parametres :
	# $1 : Id coffre
	InitSession $1
	CONT_NB=`$curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass |grep CFEC_LISTCONT_NB:|awk '{print $2}'`
	CONT_NB=` echo $CONT_NB| awk 'sub(/\r$/,"") ;'`
	if [ -z $CONT_NB ]; then
		echo "Coffre $1 vide ou inexistant"
	else
		echo "Nombre de container dans le coffre : $CONT_NB "
	fi
	return $CONT_NB
	}

function CoffreCreation
	{
	# Aucun parametres

	nom=$(date +"%H%M%S%N")
	InitSession 1
	echo "Creation du coffre"
	creation_coffre=`$curlexec -F CFEC_SESSION=$session -F nom=$nom "https://$ip_cfec/cfec/safe/create.php" -E $certificat:$pass |grep CFEC_SUBSET_ID |awk '{print $2}' `
	id_cfe=`/bin/echo $creation_coffre`
	id_cfe=`/bin/echo $id_cfe | /usr/bin/tr -dc [:alnum:]`
	echo "Creation dossiers in  et archive..."
	#recherche numero de profil
	cfe_prof=`$curlexec -F CFEC_SESSION=$session -F cfe=$id_cfe "https://$ip_cfec/cfec/profil/list.php" -E $certificat:$pass |grep CFEC_LISTPROFIL_0: |awk '{print $2}'`
	echo "Obtention du profil - avant formatage:  $cfe_prof"
	cfe_prof=`/bin/echo $cfe_prof | /usr/bin/tr -dc [:alnum:]`
	cfe_prof=`/bin/echo $(echo $cfe_prof | cut -d"a" -f1)`
	echo "Obtention du profil :  -- $cfe_prof --"

	#creation utilisateur
	create_user=`$curlexec -F CFEC_SESSION=$session -F profilId=$cfe_prof -F prenom=docapost -F nom=$id_cfe -F mail=bench@docapost-dps.com -F civ=docapost -F cfe=$id_cfe -F userfile=@$certcfe "https://$ip_cfec/cfec/user/create.php" -E $certificat:$pass`
	session=`$curlexec -F CFEC_SESSION=$session "https://$ip_cfec/cfec/sess/delete.php" -E $certificat:$pass |grep CFEC_SESSION: |awk '{print $2}'`
	SessInit=0
	InitSession $id_cfe
	cont_id=`$curlexec -F CFEC_SESSION=$session -F contIdParent=-1 -F contName="in" "https://$ip_cfec/cfec/fld/create.php" -E $certificat:$pass`
	cont_id=`$curlexec -F CFEC_SESSION=$session -F contIdParent=-1 -F contName="archive" "https://$ip_cfec/cfec/fld/create.php" -E $certificat:digiposte |grep CFEC_CONTID: |awk '{print $2}'`
	return $id_cfe
	}

function CoffreDestruction
	 {
	 echo "Destruction du coffre $2"
	 InitSession 1
	 echo "Detruire le coffre $1 ? ENTREE: continuer, <ctrl+c> : interrompre"
	 read
	 retour=`$curlexec -F CFEC_SESSION=$session -F cfeId=$1 "https://$ip_cfec/cfec/safe/close.php" -E $certificat:$pass |grep CFEC_STATUS: |awk '{print $2}'`
	 echo "Operation : $retour"
	 }

function CoffreListe
	{
	InitSession $1
	echo "Analyse du coffre $1 ...."
	structure_coffre=`$curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass `
	echo "Coffre : " $structure_coffre
}

function NombreCoffres				# renvoie le nombre de coffres dans la salle passe en parametre
{
	InitSession 1
	echo "Comptage des coffres..."
	retour=`$curlexec -F CFEC_SESSION=$session -F cfec=$numcfec "https://$ip_cfec/cfec/safe/list.php" -E $certificat:$pass `
	nombre_coffres=`$curlexec -F CFEC_SESSION=$session -F cfec=$numcfec "https://$ip_cfec/cfec/safe/list.php" -E $certificat:$pass |grep CFEC_LISTCFE_NB| /usr/bin/tr -dc [:alnum:][:blank:] |awk '{print $2}' `
	# echo "--- DEBUG --- retour : $retour"
	# echo $retour | awk 'sub(/\r$/,"") ;' > variable-retour.txt
	# echo $retour | sed -e 's/^M//g'  > variable-retour.txt
	# nombre_coffre=`cat variable-retour.txt | grep CFEC_LISTCFE_NB | awk '{print $2}'`
	echo "$nombre_coffres coffre(s) dans la salle $1"
	return $nombre_coffres
	}


function CoffreControle 				# presence et correspondance bdd-coffre
	{
	# Parametres :
	# $1 : Id du coffre
	# $2 : PID

	# echo "--- DEBUG --- CoffreControle($1)"
	# id=`echo "$1"`

	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	# SqlRequete=
	SqlRequete="select strongbox from sender where pid='$1'"
	retour=`$SqlCmd -e "$SqlRequete"`
	echo "--- DEBUG --- retour brut : $retour"
	id=`/bin/echo $retour | /usr/bin/tr -dc [:alnum:][:blank:] | cut -d" " -f10 `
	echo "--- DEBUG --- id coffre depuis la bdd : $id"
	if [ -z $id ];then
		echo "Pas de coffre pour l'utilisateur $1"
		return 254
	fi
	CoffreControleDossiers $id
	if [ $ccd -eq 0 ]; then
		echo "Coffre de l'utilisateur $1 OK"
	else
		echo "Coffre de l'util:isateur $1 non conforme"
	fi

	# echo " --- DEBUG --- \$SqlCmd=$SqlCmd"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	db=`$SqlCmd -e "$SqlRequete"`
	# echo " --- DEBUG --- Variable DB = $db ..FIN-Variable......"
	echo $db > variable-db.txt
	db=`cat variable-db.txt | cut -d" " -f10 | tr -dc [:digit:]`
	# echo "--- DEBUG variable db apres nettoyage : $db ----"
	if [ -z $db ]; then
		echo "Pas de coffre parametre pour l'utilisataur $1"
	else
		echo "Coffre present pour l'utilisateur $1. Numero du coffre : $db "
	fi
	}

function CoffreControleDossiers
	{
	# Parametres :
	# $1 : ID du coffre
	
	echo "Initialisation controle dossiers dans le coffre $1 "
	InitSession $1
	ConteneurNb $1
	echo "Le coffre $1 contient $CONT_NB dossier(s) "
	typeset -i container=0
	while [[ "$container" -lt $CONT_NB  ]]; do
		echo $container
		CONT_ID=`$curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass |grep CFEC_LISTCONT_$container: |awk '{print $2}'`
		CONT_ID=` echo $CONT_ID| awk 'sub(/\r$/,"") ;'`
		echo "CONTAINER ID : " $CONT_ID
		DIRNAME=`$curlexec -F CFEC_SESSION=$session -F contId=$CONT_ID "https://$ip_cfec/cfec/fld/name.php" -E $certificat:$pass |grep CFEC_CONTNAME: |awk '{print $2}'`
		DIRNAME=` echo $DIRNAME| awk 'sub(/\r$/,"") ;'`
		echo "CONTAINER Name : " $DIRNAME
		if [[ $DIRNAME = archive* ]] ; then
			IS_ARCHIVE_CONTAINER_PRESENT="YES"
			echo "dossier archive existe"
		elif [[ $DIRNAME = in* ]] ; then
			IS_IN_CONTAINER_PRESENT="YES"
			echo "dossier in existe"
		fi
		let container=container+1
	done
	if [ -z $IS_ARCHIVE_CONTAINER_PRESENT ] && [ -z $IS_IN_CONTAINER_PRESENT ]; then
		ccd=254
	else
		ccd=0
	fi
	}

function connect
	{
	# Parametres :
	# $2 : ID coffre
	# $3 : PID
	# $4 : chambre forte
	# $5 : sender_id

	echo "Fonction Connect"
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	# echo "--- DEBUG --- SqlCmd : $SqlCmd --//--"
	# SqlRequete="use digiposte" # requete-bouchon pour tests
	SqlRequete="update sender set strongbox='$4' where pid='$3'";
	db=`$SqlCmd -e "$SqlRequete"`
	# echo "--- DEBUG --- requete update strongroom: $SqlRequete retour : $db --FIN_db"
	SqlRequete="select id from sender where pid='$3'"
	db=`$SqlCmd -e "$SqlRequete"`
	SenderId=`/bin/echo $db |  /usr/bin/tr -dc [:alnum:][:blank:] | cut -d" " -f10` # potentiellement piege si la chaine "id" est presente dans l'id --  | /usr/bin/tr -dc [:\n:]
	# echo "--- DEBUG --- requete : $SqlRequete retour : $db -- SenderId : $SenderId --FIN_db"
	SqlRequete="update strongbox set id='$2', strongroom='$4' where sender_id='$SenderId'";
	db=`$SqlCmd -e "$SqlRequete"`
	echo "Connexion terminee, retour : $db"
	}

function AfficheAide
	{
	cat << EOF
	Usage :
	Syntaxe : $0 <commande> <N° de coffre> (optionnel)
	commandes :
	 - creer : cree un nouveau coffre. Cette commande ne demande aucune option
	 - supprimer : Supprimer un coffre
	 - controler_coffre : Verifie la coherance emetteur-coffre.
	 - controler_dossiers : Liste les objets contenus dans le dossier passe en parametre
	 - connect : relie l'emetteur au coffre correspondant
	 - nb_coffres : Compte le nombre de coffres dans la salle passe en parametre.
EOF
	}

# Init -----------------------------------------------------------------------------------------------

if [ -z $1 ];then
	echo "Parametres requis manquant"
	AfficheAide
	exit
fi

case "$1" in
	creer)
	CoffreCreation;;
	supprimer)
	if [ -z $2 ]; then
		echo "Parametre Numero de coffre manquant."
		exit
	fi
	CoffreDestruction $2;;
	compter)
	if [ -z $2 ]; then
		echo "Parametre Numero de coffre manquant."
		exit
	fi
	ConteneurNb $2;;
	lister)
	if [ -z $2 ]; then
		echo "Parametre Numero de coffre manquant."
		exit
	fi
	CoffreListe $2;;
	controler_coffre)
	if [ -z $2 ]; then
		echo "Parametre Numero de coffre manquant."
		exit
	fi
	CoffreControle $2 $3;;
	controler_dossiers)
	if [ -z $2 ]; then
		echo "Parametre Numero de coffre manquant."
		exit
	fi
	CoffreControleDossiers $2
	exit;;
	connect)
	if [ -z $2 ]; then
		echo "Parametres Numero de coffre et/ou emetteur manquant."
		exit
	fi
	connect $1 $2 $3 $4;;

	nb_coffres)
	if [ -z $2 ]; then
		echo "Parametres salle des coffres manquant."
		exit
	fi
	NombreCoffres $2;;

	*)
	echo "Option inconnue";
	AfficheAide;
	exit;;
esac
