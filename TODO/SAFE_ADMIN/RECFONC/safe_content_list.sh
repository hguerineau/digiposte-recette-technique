#!/bin/sh
# DOCAPOST DPS

#ce script :
#ajoute un coffre a une salle 
#ajoute un utilisateur admin dans le coffre avec le meme certificat que l administrateur
#ajoute un document
#telecharge le document

#parametres #ip a tester:84.14.109.190 (depuis mon laptop), 172.16.0.151 (depuis VMEP)
ip_cfec="172.16.0.151" 
peuplement="1"                                           # combien de fois on veut jouer ce scenario
CFE=$1

certificat="/home/hguerineau/SAFE_ADMIN/CERTIFICATS/adm.recfonc.coffre.pem"             # certificat admin partie pub+key
certcfe="/home/hguerineau/SAFE_ADMIN/CERTIFICATS/usr.recfonc.coffre.pem"                # certificat de l utilisateur a ajouter partie pub  
curlexec="/usr/bin/curl -k -s -i --connect-timeout 20"
pass=""                                                  # le mdp du certificat admin 
numcfec="6"                                              # le numero de la salle 
IS_ARCHIVE_CONTAINER_PRESENT="NO"
IS_IN_CONTAINER_PRESENT="NO"


#script
i=1
while [ $i -le $peuplement ]
do

nom=$(date +"%H%M%S%N")
#session
session=`$curlexec -F cfec=$numcfec -F cfe=$CFE "https://$ip_cfec/cfec/sess/init.php" -E $certificat:$pass |grep CFEC_SESSION: |awk '{print $2}'`
session=`/bin/echo $session | /usr/bin/tr -dc [:alnum:]`
#echo "Session : $session"

#structure coffre
#structure_coffre=`$curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass `
#echo "Coffre : " $structure_coffre

CONT_NB=`$curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass |grep CFEC_LISTCONT_NB:|awk -F "\r" '{print $1}' |awk '{print $2}'`
echo "Nombre de container dans le coffre : $CONT_NB"

for (( container =0 ; container < $CONT_NB ; container++ ))
do
	CONT_ID=`$curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass |grep CFEC_LISTCONT_$container:|awk -F "\r" '{print $1}' |awk '{print $2}'`
	echo "CONTAINER ID : " $CONT_ID
	DIRNAME=`$curlexec -F CFEC_SESSION=$session -F contId=$CONT_ID "https://$ip_cfec/cfec/fld/name.php" -E $certificat:$pass |grep CFEC_CONTNAME:|awk '{print $2}'`
	echo "CONTAINER Name : " $DIRNAME
	if [[ $DIRNAME = archive* ]] ; then
		IS_ARCHIVE_CONTAINER_PRESENT="YES"
		echo "dossier archive existe"
	elif [[ $DIRNAME = in* ]] ; then
		IS_IN_CONTAINER_PRESENT="YES"
		echo "dossier in existe"
	fi
done

if [[ $IS_ARCHIVE_CONTAINER_PRESENT = NO ]] ; then
	echo "creation dossier archive"
	cont_id=`$curlexec -F CFEC_SESSION=$session -F contIdParent=-1 -F contName="archive" "https://$ip_cfec/cfec/fld/create.php" -E $certificat:digiposte`
fi

if [[ $IS_IN_CONTAINER_PRESENT = NO ]] ; then
	echo "creation dossier in"
	cont_id=`$curlexec -F CFEC_SESSION=$session -F contIdParent=-1 -F contName="in" "https://$ip_cfec/cfec/fld/create.php" -E $certificat:$pass`
fi

let i=1+$i
done
