#!/bin/bash

#########################################################################################
#                                                                                       #
# Title         :   recette-technique-ws-launcher.sh                                    #
# Description   :   Script used to schedule  web servicestests execution                #
# Date          :   2013-08-06                                                          #
# Usage         :   bash recette-technique-ws-launcher.sh <rectech|recfonc|tmc|all>     #
# Author        :   Ayoub EL MAAROUF                                                    #
# Contributors  :   Mathieu LEMARCHAND                                 					#
# Maintainer    :   contact@wwsight.com                                                 #
# Copyright(C)  :   WWSIGHT SARL                                                        #
#                                                                                       #
#########################################################################################

source ./LIBS/ws.fct

CHECK_SCRIPT_LIST=
CHECK_SCRIPT_PARAMS=
NOW=$(date +%Y%m%d-%H%M%S)
QUERRY_DATE=$(date +%Y-%m-%d' '%H:%M:%S)
FUNCTIONS_LIST="adhesion_post adhesion_put adhesion_get preinscription preinscriptions"

# Manage command line parameters
retrieve_options "$@"
if [[ $? -ne 0 ]]
then
    print_usage
    exit 1
fi

# Retrieve the selected method to display the check results
source ./LIBS/display-$DISPLAY.fct
# Print the check results on the screen as well as in a log file
if [ ! -d LOGS ]; then
	mkdir LOGS
fi

#Création du fichier de log webservices
exec &> >(tee -a LOGS/check-${ENV}-ws_$NOW.log)

#Vérification que la liste des fonctions à exécuter a été renseignée, sinon on le demande à l'utilisateur
if [ "$SCRIPT" = "none" ]; then
	functions_list="$FUNCTIONS_LIST"
	#Pour chaque fonction, demande à l'utilisateur confirmation pour son exécution
	for func in $functions_list; do
		echo "Voulez-vous tester la fonction $func? (o/n)"
		read go
		if [ -z $go ] || [ $go = "o" ]; then
			#Sauvegarde des fonctions à exécuter
			if [ "$SCRIPT" = "none" ]; then
				SCRIPT="$func"
			else
				SCRIPT="$func $SCRIPT"
			fi
		fi
	done
elif [ "$SCRIPT" = "all" ]; then
	SCRIPT="$FUNCTIONS_LIST"
fi

#Si une fonction est à exécuter
if [ "$SCRIPT" != "none" ]; then
	#Vérification que l'environnement a été renseigné, sinon on le demande à l'utilisateur
	while [ -z $ENV ] || [ $ENV = "undefined" ]; do
		echo "Sur quel environnement voulez-vous lancer ce test?"
		read ENV
	done
	#Vérification que le nombre d'adhésions a été renseigné, sinon on le demande à l'utilisateur
	while [ -z $NB_PREADHESION ]; do
		echo "Combien de préadhésions voulez-vous crééer?"
		read NB_PREADHESION
	done
	#Exécution des fonctions
	for func in $SCRIPT; do	
		#Vérification que la fonction renseignée par l'utilisateur existe dans ws.fct
		echo $FUNCTIONS_LIST | grep $func > /dev/null 2>&1
		if [ $? -eq 0 ]; then
			echo "- $func $ENV $NB_PREADHESION -"
			if [ $? -eq 0 ]; then
				display_success "Exécution de la fonction $func OK"
			else
				display_error "Exécution de la fonction $func KO"
			fi
		else
			echo "ERREUR: la fonction $func n'existe pas dans LIBS/ws.fct"; exit 1
		fi
	done
else
	echo "ERREUR: aucune fonction n'est à exécuter."; exit 1
fi