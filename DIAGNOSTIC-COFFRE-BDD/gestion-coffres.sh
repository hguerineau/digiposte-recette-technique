#!/bin/bash
# set -x
export LANG=en_US.UTF-8
# -----------------------
ip_cfec="coffre.digiposte.local"
# -----------------------
CFE=$1
id_cfe=0
SessInit=0
ccd=999
environnement=`hostname |cut -d- -f2`

if [ $environnement = "recfonc" ]; then
    numcfec="6"
elif [ $environnement = "rectech" ]; then
    numcfec="7"
elif [ $environnement = "tmc" ]; then
    numcfec="1"
fi

#-------------------
	DbUser="digiposte"
	DbPass="RJhe0vKqlM"
	DbHost="db.digiposte.local"
#-------------------

CertificatRoot="/data/var/security/certificates/cecurity"

certificat="$CertificatRoot/adm.$environnement.$numcfec.coffre.pem"             # certificat admin partie pub+key
certcfe="$CertificatRoot/adm.$environnement.$numcfec.coffre.pem"                # certificat de l utilisateur a ajouter partie pub
curlexec="/usr/bin/curl -k -s -i --connect-timeout 20"
pass=""                                                  # le mdp du certificat admin


IS_ARCHIVE_CONTAINER_PRESENT="NO"
IS_IN_CONTAINER_PRESENT="NO"

#-------------------- Variables globales --------------------
coffreId=""
emetteurName=""
nbAbonnes=""
nbDocsBdd=""
nbDocsCoffre=""
userLogin=""
cfeList=""
strongboxList=""
strongboxUserIdList=""
tagsDocumentsList=""
tagsUtilisateursList=""
nbPartagesUtilisateursList=""
#------------------------------------------------------------

function InitSession		# Ouvre une session sur le coffre passe en parametre. Est appele avant chaque commande
	{
	#echo "Initialisation session pour le coffre $1"
	if [ $SessInit -eq 0 ]; then
		session=`$curlexec -F cfec=$numcfec -F cfe=$1 "https://$ip_cfec/cfec/sess/init.php" -E $certificat:$pass |grep CFEC_SESSION: |awk '{print $2}'`
		session=`/bin/echo $session | /usr/bin/tr -dc [:alnum:]`
		#echo "Session : " $session
		if [ -z $session ]; then
			#echo "Echec de l'initialisation du coffre $1"
			SessInit=0
			return 254
		else
			#echo "Session initialisee pour le coffre $1"
			SessInit=1
			return 0
		fi
    else			#session deja initialisee
        #echo "Session $1 deja initialisee"
		return 0
	fi
	}

# retourne le nombre de conteneurs
function ConteneurNb
	{
	# Parametres : 
	# $1 : Id coffre
	InitSession $1
	CONT_NB=`$curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass |grep CFEC_LISTCONT_NB:|awk '{print $2}'`
	CONT_NB=` echo $CONT_NB| awk 'sub(/\r$/,"") ;'`
	if [ -z $CONT_NB ]; then
		echo "Coffre $1 vide ou inexistant"
	else
		echo "Nombre de container dans le coffre : $CONT_NB "
	fi
	return $CONT_NB
	}
	
function CoffreCreation
	{
	# Aucun parametres

	nom=$(date +"%H%M%S%N")
	InitSession 1
	echo "Creation du coffre"	
	creation_coffre=`$curlexec -F CFEC_SESSION=$session -F nom=$nom "https://$ip_cfec/cfec/safe/create.php" -E $certificat:$pass |grep CFEC_SUBSET_ID |awk '{print $2}' `
	id_cfe=`/bin/echo $creation_coffre`
	id_cfe=`/bin/echo $id_cfe | /usr/bin/tr -dc [:alnum:]`
	echo "Creation dossiers in  et archive..."
	#recherche numero de profil
	cfe_prof=`$curlexec -F CFEC_SESSION=$session -F cfe=$id_cfe "https://$ip_cfec/cfec/profil/list.php" -E $certificat:$pass |grep CFEC_LISTPROFIL_0: |awk '{print $2}'`
	echo "Obtention du profil - avant formatage:  $cfe_prof"
	cfe_prof=`/bin/echo $cfe_prof | /usr/bin/tr -dc [:alnum:]`
	cfe_prof=`/bin/echo $(echo $cfe_prof | cut -d"a" -f1)`
	echo "Obtention du profil :  -- $cfe_prof --"

	#creation utilisateur
	create_user=`$curlexec -F CFEC_SESSION=$session -F profilId=$cfe_prof -F prenom=docapost -F nom=$id_cfe -F mail=bench@docapost-dps.com -F civ=docapost -F cfe=$id_cfe -F userfile=@$certcfe "https://$ip_cfec/cfec/user/create.php" -E $certificat:$pass`
	session=`$curlexec -F CFEC_SESSION=$session "https://$ip_cfec/cfec/sess/delete.php" -E $certificat:$pass |grep CFEC_SESSION: |awk '{print $2}'`
	SessInit=0
	InitSession $id_cfe
	cont_id=`$curlexec -F CFEC_SESSION=$session -F contIdParent=-1 -F contName="in" "https://$ip_cfec/cfec/fld/create.php" -E $certificat:$pass`
	cont_id=`$curlexec -F CFEC_SESSION=$session -F contIdParent=-1 -F contName="archive" "https://$ip_cfec/cfec/fld/create.php" -E $certificat:digiposte |grep CFEC_CONTID: |awk '{print $2}'`
	return $id_cfe
	}
	
function CoffreDestruction
	 {
	 echo "Fonction destruction"
	 InitSession 1
	 echo "Detruire le coffre $1 ? ENTREE: continuer, <ctrl+c> : interrompre"
	 read
	 retour=`$curlexec -F CFEC_SESSION=$session -F cfeId=$1 "https://$ip_cfec/cfec/safe/close.php" -E $certificat:$pass |grep CFEC_STATUS: |awk '{print $2}'`
	 echo "Operation : $retour"
	 }

function CoffreListe
	{
	InitSession $1
	echo "Analyse du coffre $1 ...."
	structure_coffre=`$curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass `
	echo "Coffre : " $structure_coffre
}

function NombreCoffres				# renvoie le nombre de coffres dans la salle passe en parametre
{
    InitSession 1
	echo "Comptage des coffres..."
	retour=`$curlexec -F CFEC_SESSION=$session -F cfec=$numcfec "https://$ip_cfec/cfec/safe/list.php" -E $certificat:$pass `
	nombre_coffres=`$curlexec -F CFEC_SESSION=$session -F cfec=$numcfec "https://$ip_cfec/cfec/safe/list.php" -E $certificat:$pass |grep CFEC_LISTCFE_NB| /usr/bin/tr -dc [:alnum:][:blank:] |awk '{print $2}' `
	# echo "--- DEBUG --- retour : $retour"
	# echo $retour | awk 'sub(/\r$/,"") ;' > variable-retour.txt
	# echo $retour | sed -e 's/^M//g'  > variable-retour.txt
	# nombre_coffre=`cat variable-retour.txt | grep CFEC_LISTCFE_NB | awk '{print $2}'`
	echo "$nombre_coffres coffre(s) dans la salle $1"
	return $nombre_coffres
	}


function CoffreControle 				# presence et correspondance bdd-coffre
	{
	# Parametres :
	# $1 : Id du coffre
	# $2 : PID
	
	# echo "--- DEBUG --- CoffreControle($1)"
	# id=`echo "$1"`
	
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	# SqlRequete=
	SqlRequete="select strongbox from sender where pid='$1'"
	retour=`$SqlCmd -e "$SqlRequete"`
	echo "--- DEBUG --- retour brut : $retour"
	id=`/bin/echo $retour | /usr/bin/tr -dc [:alnum:][:blank:] | cut -d" " -f10 `
	echo "--- DEBUG --- id coffre depuis la bdd : $id"
	if [ -z $id ];then
		echo "Pas de coffre pour l'utilisateur $1"
		return 254
	fi
	CoffreControleDossiers $id
	if [ $ccd -eq 0 ]; then
		echo "Coffre de l'utilisateur $1 OK"
	else
		echo "Coffre de l'util:isateur $1 non conforme"
	fi

	# echo " --- DEBUG --- \$SqlCmd=$SqlCmd"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	db=`$SqlCmd -e "$SqlRequete"`
	# echo " --- DEBUG --- Variable DB = $db ..FIN-Variable......"
	echo $db > variable-db.txt
	db=`cat variable-db.txt | cut -d" " -f10 | tr -dc [:digit:]`
	# echo "--- DEBUG variable db apres nettoyage : $db ----"
	if [ -z $db ]; then
		echo "Pas de coffre parametre pour l'utilisataur $1"
	else
		echo "Coffre present pour l'utilisateur $1. Numero du coffre : $db "
	fi
	}
	
function CoffreControleDossiers
	{
	# Parametres :
	# $1 : ID du coffre
	
	echo "Initialisation controle dossiers dans le coffre $1 "
	InitSession $1
	ConteneurNb $1
	echo "Le coffre $1 contient $CONT_NB dossier(s) "
	typeset -i container=0
	while [[ "$container" -lt $CONT_NB  ]]; do
		echo $container
		CONT_ID=`$curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass |grep CFEC_LISTCONT_$container: |awk '{print $2}'`
		CONT_ID=` echo $CONT_ID| awk 'sub(/\r$/,"") ;'`
		echo "CONTAINER ID : " $CONT_ID
		DIRNAME=`$curlexec -F CFEC_SESSION=$session -F contId=$CONT_ID "https://$ip_cfec/cfec/fld/name.php" -E $certificat:$pass |grep CFEC_CONTNAME: |awk '{print $2}'`
		DIRNAME=` echo $DIRNAME| awk 'sub(/\r$/,"") ;'`
		echo "CONTAINER Name : " $DIRNAME
		if [[ $DIRNAME = archive* ]] ; then
			IS_ARCHIVE_CONTAINER_PRESENT="YES"
			echo "dossier archive existe"
		elif [[ $DIRNAME = in* ]] ; then
			IS_IN_CONTAINER_PRESENT="YES"
			echo "dossier in existe"
		fi
		let container=container+1
	done
	if [ -z $IS_ARCHIVE_CONTAINER_PRESENT ] && [ -z $IS_IN_CONTAINER_PRESENT ]; then
		ccd=254
	else
		ccd=0
	fi
	}

function connect
	{
	# Parametres :
	# $2 : ID coffre
	# $3 : PID
	# $4 : chambre forte
	# $5 : sender_id

	echo "Fonction Connect"
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	# echo "--- DEBUG --- SqlCmd : $SqlCmd --//--"
	# SqlRequete="use digiposte" # requete-bouchon pour tests
	SqlRequete="update sender set strongbox='$4' where pid='$3'";
	db=`$SqlCmd -e "$SqlRequete"`
	# echo "--- DEBUG --- requete update strongroom: $SqlRequete retour : $db --FIN_db"
	SqlRequete="select id from sender where pid='$3'"
	db=`$SqlCmd -e "$SqlRequete"`
	SenderId=`/bin/echo $db |  /usr/bin/tr -dc [:alnum:][:blank:] | cut -d" " -f10` # potentiellement piege si la chaine "id" est presente dans l'id --  | /usr/bin/tr -dc [:\n:]
	# echo "--- DEBUG --- requete : $SqlRequete retour : $db -- SenderId : $SenderId --FIN_db"
	SqlRequete="update strongbox set id='$2', strongroom='$4' where sender_id='$SenderId'";
	db=`$SqlCmd -e "$SqlRequete"`	
	echo "Connexion terminee, retour : $db"
	}

function id_strongbox_utilisateur
{
	# Parametres :
	# $1 : id de l'utilisateur
	# Description : Retourne l'id d'un coffre en fonction l'id de l'utilisateur
	#				=> Modifie la variable coffreId = id du coffre d'un utilisateur
	
	userId=$1
	
	# MySQL command
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	
	SqlRequete="select id from strongbox where user_id='$userId';"
	result=`$SqlCmd -e "$SqlRequete"`
	coffreId=$(echo $result | awk -F"-------------- id " '{print $2}')
}

function check_docs_by_login
{
	# Parametres :
	# $1 : login de l'utilisateur
	# Description : Affiche les différences entre les documents présents en BDD
	# 				et ceux présents dans le coffre pour un utilisateur donné

	login=$1
	
	echo "Function check_user_docs for user $login"
	
	# MySQL command
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	
	# echo "--- DEBUG --- SqlCmd : $SqlCmd --//--"
	# SqlRequete="use digiposte" # requete-bouchon pour tests
	
	# Recuperation de l'id du coffre (cfe_user)
	SqlRequete="select s.id from strongbox s, user u where u.login='$login' and u.id=s.user_id;"
	result=`$SqlCmd -e "$SqlRequete"`
	cfe_user=$(echo $result | awk -F"-------------- id " '{print $2}')
	
	# Récupération de la liste des documents en BDD
	SqlRequete="select d.orsid_id from document d, user u where u.login='$login' and u.id=d.user_id order by d.orsid_id ASC;"
	result=`$SqlCmd -e "$SqlRequete"`
	docs_list=$(echo $result | awk -F"-------------- orsid_id " '{print $2}')
	
	# Initialisation de la session avec l'id du coffre du user
	InitSession $cfe_user
	
	# Récupération de la liste des archives dans le coffre : dirList
	# contId=-1 pour conteneur HOME
	arch_list=$($curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass | grep -E "CFEC_LISTARCH_[0-9]+: " | cut -d" " -f2 | tr "\r" " ")
	
	echo "+---------------------------------------------+"
	echo "| Document(s) in the BDD                      |"
	echo "+---------------------------------------------+"
	echo $docs_list
	
	echo "+---------------------------------------------+"
	echo "| Document(s) in the CFE                      |"
	echo "+---------------------------------------------+"
	echo $arch_list
	
	# Check des documents qui sont en BDD mais pas dans le coffre
	absents_list=""
	for doc in $docs_list
	do
		res=$(echo $arch_list | grep $doc);
		if [[ -z $res ]]
		then
			absents_list="$absents_list $doc"
		fi
	done
	echo "+---------------------------------------------+"
	echo "| Document(s) in BDD but not in CFE           |"
	echo "+---------------------------------------------+"
	echo $absents_list
	
	# Check des documents qui sont dans le coffre mais pas en BDD
	absents_list=""
	for arch in $arch_list
	do
		res=$(echo $docs_list | grep $arch);
		if [[ -z $res ]]
		then
			absents_list="$absents_list $arch"
		fi
	done
	echo "+---------------------------------------------+"
	echo "| Document(s) in CFE but not in BDD           |"
	echo "+---------------------------------------------+"
	echo $absents_list
}

function nom_emetteur
{
	# Parametres :
	# $1 : id de l'émetteur
	# Description : Retourne le nom d'un émetteur en fonction de son id.
	# 				=> Modifie la variable emetteurName = le nom d'un émetteur.
	
	emetteurId=$1
	
	# MySQL command
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	SqlRequete="select name from sender where id='$emetteurId'"
	result=`$SqlCmd -e "$SqlRequete"`
	emetteurName=$(echo $result | awk -F"-------------- name " '{print $2}')
}

function nb_abonnes
{
	# Parametres :
	# $1 : id de l'émetteur
	# $2 : statut de l'adhésion (champ "status" dans membership)
	# Description : Retourne le nombre d'adhésions d'un émetteur donné
	#				=> Modifie la variable nbAbonnes = le nombre d'abonnés d'un émetteur.
	
	emetteurId=$1
	status=$2
	
	# MySQL command
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	SqlRequete="select count(*) from membership where sender_id='$emetteurId' and status=$status;"
	result=`$SqlCmd -e "$SqlRequete"`
	nbAbonnes=$(echo $result | awk -F"-------------- count[^ ]+ " '{print $2}')
}

function abonnes_emetteurs
{
	# Parametres : aucun
	# Description : Affiche le nombre d'adhésions pour chaque émetteur
	
	# MySQL command
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	SqlRequete="select id from sender;"
	result=`$SqlCmd -e "$SqlRequete"`

	emetteurIdList=$(echo $result | awk -F"-------------- id " '{print $2}')
	
	echo "+---------------------------------------------+"
	echo "| Nombre d'adhésions par émetteur             |"
	echo "+---------------------------------------------+"
	
	printf "%-24s" "Nom de l'émetteur"
	echo " | Statut de l'adhésion (status)"
	printf "%-24s" " "
	echo -n "| "
	echo -e "4\t2\t7\t3\t5\t6\t8\t0"
	echo "----------------------------------------------"
	
	echo -e "#Emetteur\tStatus4\tStatus2\tStatus7\tStatus3\tStatus5\tStatus6\tStatus8\tStatus0" > nombre_adhesions.dat
	
	for emetteurId in $emetteurIdList
	do
		nom_emetteur $emetteurId
		line=$(echo $emetteurName | tr " " "_")
		printf "%-24s" "$emetteurName"
		echo -n "| "
		nb_abonnes $emetteurId 4
		line=$line"\t$nbAbonnes"
		echo -n -e $nbAbonnes"\t"
		nb_abonnes $emetteurId 2
		line=$line"\t$nbAbonnes"
		echo -n -e $nbAbonnes"\t"
		nb_abonnes $emetteurId 7
		line=$line"\t$nbAbonnes"
		echo -n -e $nbAbonnes"\t"
		nb_abonnes $emetteurId 3
		line=$line"\t$nbAbonnes"
		echo -n -e $nbAbonnes"\t"
		nb_abonnes $emetteurId 5
		line=$line"\t$nbAbonnes"
		echo -n -e $nbAbonnes"\t"
		nb_abonnes $emetteurId 6
		line=$line"\t$nbAbonnes"
		echo -n -e $nbAbonnes"\t"
		nb_abonnes $emetteurId 8
		line=$line"\t$nbAbonnes"
		echo -n -e $nbAbonnes"\t"
		nb_abonnes $emetteurId 0
		line=$line"\t$nbAbonnes"
		echo $nbAbonnes
		
		echo -e $line >> nombre_adhesions.dat
	done
}

function nb_documents_bdd
{
	# Parametres :
	# $1 : id de l'utilisateur
	# Description : Retourne le nombre de documents en BDD d'un utilisateur donné.
	#				=> modifie la variable userDocsBdd = le nombre de documents en BDD
	
	userId=$1
	nbDocsBdd=""
	
	# MySQL command
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	SqlRequete="select count(*) from document where user_id='$userId'"
	result=`$SqlCmd -e "$SqlRequete"`
	nbDocsBdd=$(echo $result | awk -F"-------------- count[^ ]+ " '{print $2}')
}

function nb_documents_coffre
{
	# Parametres :
	# $1 : id de l'utilisateur
	# Description : Retourne le nombre de documents en coffre d'un utilisateur donné.
	#				=> Modifie la variable userDocsCoffre = le nombre de documents en coffre
	
	userId=$1
	nbDocsCoffre=""
	
	id_strongbox_utilisateur $userId
	
	# Initialisation de la session avec l'id du coffre du user
	InitSession $coffreId
	
	# Récupération de la liste des archives dans le coffre : dirList
	# contId=-1 pour conteneur HOME
	nbDocsCoffre=$($curlexec -F CFEC_SESSION=$session -F contId=-1 "https://$ip_cfec/cfec/fld/list.php" -E $certificat:$pass | grep -E "CFEC_LISTARCH_NB: " | cut -d" " -f2)
	SessInit=0
}

function login_utilisateur
{
	# Parametres :
	# $1 : id de l'utilisateur
	# Description : Retourne le login d'un utilisateur en fonction de son id.
	#				=> Modifie la variable userLogin = le login d'un utilisateur.
	
	userId=$1
	userLogin=""
	
	# MySQL command
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	SqlRequete="select login from user where id='$userId'"
	result=`$SqlCmd -e "$SqlRequete"`
	userLogin=$(echo $result | awk -F"-------------- login " '{print $2}')
}

function documents_utilisateurs
{
	# Parametres : aucun
	# Description : Affiche le nombre de documents en BDD et en coffre pour chaque utilisateur.
	
	
	# MySQL command
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	SqlRequete="select id from user;"
	result=`$SqlCmd -e "$SqlRequete"`
	
	userIdList=$(echo $result | awk -F"-------------- id " '{print $2}')
	
	echo -e "#User\tNbDocsBdd\tNbDocsCoffre" > documents_utilisateurs.dat
	idx=0
	
	echo "+---------------------------------------------+"
	echo "| Nombre de documents par utilisateur         |"
	echo "+---------------------------------------------+"
	
	printf "%-24s" "Login de l'utilisateur"
	echo -n " | "
	printf "%-24s" "Nombre de documents bdd"
	echo -n " | "
	echo "Nombre de documents coffre"
	echo "------------------------------------------------------------------------------"
	
	for userId in $userIdList
	do
		line="user"$idx
		login_utilisateur $userId
		printf "%-24s" "$userLogin"
		echo -n " | "
		nb_documents_bdd $userId
		printf "%-24s" "$nbDocsBdd"
		echo -n " | "
		line=$line"\t$nbDocsBdd"
		nb_documents_coffre $userId
		echo "$nbDocsCoffre"
		line=$line"\t$nbDocsCoffre"
		echo -e $line >> documents_utilisateurs.dat
		let idx++
	done
}

function cfe_liste
{
	# Parametres : aucun
	# Description : Retourne la liste des id des coffres (cfe) de la salle (cfec) de l'environnement
	#				=> Modifie la variable cfeList = la liste des cfe de la cfec de l'environnement
	
	InitSession 1
	cfeList=`$curlexec -F CFEC_SESSION=$session -F cfec=$numcfec "https://$ip_cfec/cfec/safe/list.php" -E $certificat:$pass | grep -E "CFEC_LISTCFE_[0-9]+: " | cut -d" " -f2 | tr "\r" " "`
}

function strongbox_liste
{
	# Parametres : aucun
	# Description : Retourne la liste des id des coffres (strongbox) présents en BDD
	#				=> Modifie la variable strongboxList = la liste des id de strongbox présents en BDD
	
	# MySQL command
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	SqlRequete="select id from strongbox;"
	result=`$SqlCmd -e "$SqlRequete"`
	
	strongboxList=$(echo $result | awk -F"-------------- id " '{print $2}')
}

function strongbox_coffre
{
	# Parametres : aucun
	# Description : Affiche les différences entre les coffres présents en BDD (strongbox) et en salle (cfe)
	
	cfe_liste
	cfeIdList=$(echo $cfeList | sed -e 's/_[^ ]*//g')
	strongbox_liste
	
	echo "+---------------------------------------------+"
	echo "| Coffres dans la salle mais pas en bdd       |"
	echo "+---------------------------------------------+"
	
	for id in $cfeIdList
	do
		res=$(echo $strongboxList | grep -E " $id |^$id | $id$");
		if [[ -z $res ]]
		then
			absents_list="$absents_list $id"
		fi
	done
	echo $absents_list
}

function strongbox_userId_liste
{
	# Parametres : aucun
	# Description : Retourne la liste des couples (id strongbox,id utilisateur) présents en BDD
	#				=> Modifie la variable strongboxUserIdList = la liste des couples (id strongbox,id utilisateur) présents en BDD
	
	# MySQL command
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	SqlRequete="select id,user_id from strongbox where user_id is not null order by id;"
	result=`$SqlCmd -e "$SqlRequete"`
	
	strongboxUserIdList=$(echo $result | awk -F"-------------- id user_id " '{print $2}')
}

function check_nom_cfe
{
	# Parametres : aucun
	# Description : Affiche les coffres d'utilisateurs présents en BDD (strongbox) dont le nom
	#				n'est pas de la forme "<id coffre>_strongbox_user_<id utilisateur>"
	
	# Récupération de la liste des couples (id strongbox,id utilisateur)
	strongbox_userId_liste		# => variable strongboxUserIdList
	
	# Récupération de la liste des coffres (cfe) dans la salle (cfec)
	cfe_liste 	# => variable cfeList
	
	tmpList=$strongboxUserIdList" stop"

	cfeId=$(echo $tmpList | cut -d" " -f1)
	userId=$(echo $tmpList | cut -d" " -f2)

	echo "+---------------------------------------------+"
	echo "| Coffres inexistants dans la salle $numcfec  |"
	echo "+---------------------------------------------+"

	while [[ $cfeId != "stop" ]] && [[ $userId != "stop" ]]
	do
		# Création du nom du coffre
		nomCFE=$cfeId"_strongbox_user_"$userId
		
		res=$(echo $cfeList | grep $nomCFE);
		if [[ -z $res ]]
		then
			echo $nomCFE
		fi
		
		tmpList=$(echo $tmpList | sed -e "s/$cfeId $userId //")
		cfeId=$(echo $tmpList | cut -d" " -f1)
		userId=$(echo $tmpList | cut -d" " -f2)
	done
}

function tags_documents_liste
{
	# Parametres : aucun
	# Description : Retourne la liste des tags de chaque document
	#				=> Modifie la variable tagsDocumentsList = la liste des tags de chaque document
	
	# MySQL command
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	SqlRequete="select replace(user_tag,\" \",\"_\") from document;"
	result=`$SqlCmd -e "$SqlRequete"`
	#echo $result
	tagsDocumentsList=`echo $result | awk -F"-------------- replace.*) " '{print $2}'`
}

function tags_utilisateurs_liste
{
	# Parametres : aucun
	# Description : Retourne la liste des couples (id utilisateur,tags du document)
	#				=> Modifie la variable tagsUtilisateursList = la liste des couples (id utilisateur,tags du document)
	
	# MySQL command
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	SqlRequete="select user_id,replace(user_tag,\" \",\"_\") from document order by user_id;"
	result=`$SqlCmd -e "$SqlRequete"`
	#echo $result
	tagsUtilisateursList=`echo $result | awk -F"-------------- user_id replace.*) " '{print $2}'`
	#echo $tagsUtilisateursList
}

function diagnostic_tags
{
	# Parametres : aucun
	# Description : Affiche des informations sur les tags
	#				- Moyenne du nombre de tags/document
	#				- Liste du nombre de tags distincts par utilisateur
	#					ex : 1 3 2 0 4
	#						=> signifie qu'un user utilise 1 tag, un autre user utilise 3 tags, etc.
	
	# Récupération de la liste des tags de chaque document
	tags_documents_liste
	#echo $tagsDocumentsList
	
	# Calcul du nombre de tags moyen par document
	cpt=0
	somme=0	
	moyenne=0
	max=0
	nbDocs=$(echo $tagsDocumentsList | wc -w)
	nbTagsDocsList=""
	for tagsList in $tagsDocumentsList
	do
		if [[ $tagsList != "NULL" ]]
		then
			cpt=$(echo $tagsList | tr "|" " " | wc -w)
			let "somme = somme + cpt"
			nbTagsDocsList=$nbTagsDocsList" $cpt"
			if [[ $cpt -gt $max ]]
			then
				max=$cpt
			fi
		fi
	done
	moyenne=$(echo $somme/$nbDocs | bc -l)
	echo -n "Nombre total de documents : " ; echo $nbDocs
	echo -n "Nombre total de tags : " ; echo $somme
	echo -n "Moyenne nombre tags par document : " ; echo $moyenne | bc -l
	echo -n "Max nombre tags par document: " ; echo $max
	echo "Liste du nombre de tags distincts par document :"
	echo $nbTagsDocsList | tee tags_docs.txt
	
	# Récupération de la liste des tags de chaque utilisateur
	tags_utilisateurs_liste
	
	#echo $tagsUtilisateursList
	
	isUserId=1
	currUserId=$(echo $tagsUtilisateursList | cut -d" " -f1)
	prevUserId=$(echo $tagsUtilisateursList | cut -d" " -f1)
	tagsList=""
	nbTagsUsersList=""
	nbTags=0
	
	for i in $tagsUtilisateursList
	do
		if [[ $isUserId -eq 1 ]] 	# Si l'élément "i" est le userId du document
		then
			prevUserId=$currUserId
			currUserId=$i
		else 						# Si l'élément "i" est la liste des tags du document
			if [[ $currUserId != $prevUserId ]]
			then
				nbTags=$(echo $tagsList | tr "|" " " | wc -w)
				tagsList=""
				nbTagsUsersList=$nbTagsUsersList" $nbTags"
			fi
			
			tags=$(echo $i | tr "|" " ")
			for tag in $tags
			do
				if [[ $tag != "NULL" ]]
				then
					echo "|"$tagsList"|" | grep "|$tag|" > /dev/null
					if [[ $? -ne 0 ]] # Si le tag n'existe pas, on l'ajoute à la liste
					then
						tagsList=$tagsList"|"$tag
					fi
				fi
			done
		fi
		isUserId=$(( $isUserId ? 0 : 1 ))
	done
	echo "Liste du nombre de tags distincts par utilisateur :"
	echo $nbTagsUsersList | tee tags_users.txt
	
	tar czvf tags.tar.gz tags_docs.txt tags_users.txt > /dev/null
	rm tags_docs.txt tags_users.txt
}


function diagnostic_partages
{
	# Parametres : aucun
	# Description : Affiche des informations sur les partages
	#				- Liste du nombre de partages par utilisateurs
	#				- Liste du nombre de documents par partages
	#				- Liste du nombre de destinataires par partages
	
	# MySQL command
	SqlCmd="mysql -v -u $DbUser -p$DbPass -h$DbHost digiposte"
	SqlCmd=`/bin/echo $SqlCmd | /usr/bin/tr -dc [:alnum:][:blank:][===][=-=][=.=] `
	SqlRequete="select count(*) as nbPartages from share group by user_id;"
	result=`$SqlCmd -e "$SqlRequete"`
	nbPartagesUtilisateursList=`echo $result | awk -F"-------------- nbPartages " '{print $2}'`
	echo "Liste du nombre de partages par utilisateurs :"
	echo $nbPartagesUtilisateursList | tee partages_users.txt
	
	
	SqlRequete="select count(*) as nbDocs from document__share group by share_id;"
	result=`$SqlCmd -e "$SqlRequete"`
	nbDocsPartagesList=`echo $result | awk -F"-------------- nbDocs " '{print $2}'`
	echo "Liste du nombre de documents par partages :"
	echo $nbDocsPartagesList | tee partages_docs.txt
	
	
	SqlRequete="select count(*) as nbRecipients from share_recipient group by share_id;"
	result=`$SqlCmd -e "$SqlRequete"`
	nbRecipientsPartagesList=`echo $result | awk -F"-------------- nbRecipients " '{print $2}'`
	echo "Liste du nombre de destinataires par partages :"
	echo $nbRecipientsPartagesList | tee partages_recipients.txt
	
	tar czvf partages.tar.gz partages_users.txt partages_docs.txt partages_recipients.txt > /dev/null
	rm partages_users.txt partages_docs.txt partages_recipients.txt
}

function AfficheAide
	{
	cat << EOF
	Usage : 
	Syntaxe : $0 <commande> <N° de coffre> (optionnel)
	commandes :
	 - creer : cree un nouveau coffre. Cette commande ne demande aucune option
	 - supprimer : Supprimer un coffre
	 - controler_coffre : Verifie la coherance emetteur-coffre.
	 - controler_dossiers : Liste les objets contenus dans le dossier passe en parametre
	 - connect : relie l'emetteur au coffre correspondant
	 - nb_coffres : Compte le nombre de coffres dans la salle passe en parametre.
	 - check_docs_by_login : Affiche les différences entre les documents présents en BDD et ceux présents dans le coffre pour un utilisateur donné. Paramètre : login_user
	 - abonnes_emetteurs : Affiche le nombre d'abonnés pour tous les émetteurs. Paramètre : aucun
	 - documents_utilisateurs : Affiche le nombre de documents en bdd et en coffre pour chaque utilisateur. Paramètre : aucun
	 - strongbox_coffre : Affiche les différences entre les coffres présents en BDD (strongbox) et en salle (cfe). Paramètre : aucun
	 - check_nom_cfe : Affiche les coffres d'utilisateurs présents en BDD (strongbox) dont le nom n'est pas de la forme "<id coffre>_strongbox_user_<id utilisateur>". Paramètre : aucun
	 - diagnostic_tags : Affiche des informations sur les tags
	 - diagnostic_partages : Affiche des informations sur les partages
EOF
	}

# Init -----------------------------------------------------------------------------------------------

if [ -z $1 ];then
	echo "Parametres requis manquant"
	AfficheAide
	exit
fi

case "$1" in
	creer)
		CoffreCreation;;
	supprimer)
		if [ -z $2 ]; then
			echo "Parametre Numero de coffre manquant."
			exit
		fi	
		CoffreDestruction $2;;
	compter)
		if [ -z $2 ]; then
			echo "Parametre Numero de coffre manquant."
			exit
		fi
		ConteneurNb $2;;
	lister)
		if [ -z $2 ]; then
			echo "Parametre Numero de coffre manquant."
			exit
		fi
		CoffreListe $2;;
	controler_coffre)
		if [ -z $2 ]; then
			echo "Parametre Numero de coffre manquant."
			exit
		fi	
		CoffreControle $2 $3;;
	controler_dossiers)
		if [ -z $2 ]; then
			echo "Parametre Numero de coffre manquant."
			exit
		fi	
		CoffreControleDossiers $2
		exit;;
	connect)
		if [ -z $2 ]; then
			echo "Parametres Numero de coffre et/ou emetteur manquant."
			exit
		fi	
		connect $1 $2 $3 $4;;

	nb_coffres)
		if [ -z $2 ]; then
			echo "Parametres salle des coffres manquant."
			exit
		fi
		NombreCoffres $2;;
	check_docs_by_login)
		if [ -z $2 ]; then
			echo "Parametres login du user manquant."
			exit
		fi
		check_docs_by_login $2;;
	abonnes_emetteurs)
		abonnes_emetteurs;;
	documents_utilisateurs)
		documents_utilisateurs;;
	strongbox_coffre)
		strongbox_coffre;;
	check_nom_cfe)
		check_nom_cfe;;
	diagnostic_tags)
		diagnostic_tags;;
	diagnostic_partages)
		diagnostic_partages;;
	*)
		echo "Option inconnue";
		AfficheAide;
		exit;;
esac
