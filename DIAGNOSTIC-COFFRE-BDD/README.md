# Script de diagnostic coffres et BDD

Le script `gestion-coffres.sh` a pour objectif de donner un diagnostic de l'état des coffres
et de la base de données MySQL au travers de plusieurs indicateurs.

## Utilisation

Ce script fait des requêtes en base de données et des appels `curl` à destination des coffres.  
Il est donc nécessaire de l'exécuter depuis un serveur de la plateforme possédant MySQL : par exemple `worker1-rectech`

	ssh worker1-rectech 'bash -s' < ./gestion-coffres.sh <commande>

Avec `<commande>` parmi :
	
a) `check_docs_by_login` : Affiche les différences entre les documents présents en BDD et ceux présents dans le coffre pour un utilisateur donné. Paramètre : `login_user`  
b) `abonnes_emetteurs` : Affiche le nombre d'abonnés pour tous les émetteurs. Paramètre : `aucun`  
c) `documents_utilisateurs` : Affiche le nombre de documents en bdd et en coffre pour chaque utilisateur. Paramètre : `aucun`  
d) `strongbox_coffre` : Affiche les différences entre les coffres présents en BDD (strongbox) et en salle (cfe). Paramètre : `aucun`  
e) `check_nom_cfe` : Affiche les coffres d'utilisateurs présents en BDD (strongbox) dont le nom n'est pas de la forme `<id coffre>_strongbox_user_<id utilisateur>`. Paramètre : `aucun`  
f) `diagnostic_tags` : Affiche des informations sur les tags  
g) `diagnostic_partages` : Affiche des informations sur les partages  

> Les commandes :
> 
> - b) `abonnes_emetteurs`,
> - c) `documents_utilisateurs`,
> - f) `diagnostic_tags`,
> - g) `diagnostic_partages`
> 
> génèrent des fichiers de données permettant de produire des graphiques.



## Création de graphiques

Pour la génération des graphiques, il est nécessaire d'avoir `perl` et `gnuplot`.

Les instructions dépendent de la commande choisie :

### b) `abonnes_emetteurs`

* Récupérer le fichier `nombre_adhesions.dat` sur `worker1-rectech` :
	* Aller dans `DIAGNOSTIC-COFFRE-BDD/data`
	* Exécuter : `scp worker1-rectech:~/nombre_adhesions.dat .`

* 1 graphique peut être généré :  
	**Nombre d'adhérents par émetteur et par statut de l'adhésion**  
	* Aller dans `DIAGNOSTIC-COFFRE-BDD/plot`
	* Exécuter : `gnuplot nombre_adhesions.plot`

### c) `documents_utilisateurs`
* Récupérer le fichier `documents_utilisateurs.dat` :
	* Aller dans `DIAGNOSTIC-COFFRE-BDD/data`
	* Exécuter : `scp worker1-rectech:~/documents_utilisateurs.dat .`
* Générer le fichier `documents_utilisateurs_erreur.dat`
	* Aller dans `DIAGNOSTIC-COFFRE-BDD/`
	* Exécuter : `perl Data_Shaping.pl documents_utilisateurs_erreur`
* 2 graphiques peuvent être générés :  
	**Nombre de documents dans la BDD et dans le coffre pour tous les utilisateurs (anonymisés)**
	* Aller dans `DIAGNOSTIC-COFFRE-BDD/plot`
	* Exécuter : `gnuplot documents_utilisateurs.plot`
	
	**Nombre de documents dans la BDD et dans le coffre qui diffèrent**
	* Aller dans `DIAGNOSTIC-COFFRE-BDD/plot`
	* Exécuter : `gnuplot documents_utilisateurs_erreur.plot`

### f) `diagnostic_tags`
* Récupérer l'archive `tags.tar.gz` :
	* Aller dans `DIAGNOSTIC-COFFRE-BDD/data`
	* Exécuter : `scp worker1-rectech:~/tags.tar.gz . && tar xzvf tags.tar.gz`
* Générer les fichiers de données (.dat)
	* Aller dans `DIAGNOSTIC-COFFRE-BDD/`
	* Exécuter : `perl Data_Shaping.pl tags_users ; perl Data_Shaping.pl tags_docs`
* 2 graphiques peuvent être générés :  
	**Nombre de tags distincts utilisés par les utilisateurs**
	* Aller dans `DIAGNOSTIC-COFFRE-BDD/plot`
	* Exécuter : `gnuplot tags_users.plot`

	**Nombre de tags utilisés sur les documents**
	* Aller dans `DIAGNOSTIC-COFFRE-BDD/plot`
	* Exécuter : `gnuplot tags_docs.plot`

### g) `diagnostic_partages`
* Récupérer l'archive `partages.tar.gz` :
	* Aller dans `DIAGNOSTIC-COFFRE-BDD/data`
	* Exécuter : `scp worker1-rectech:~/partages.tar.gz . && tar xzvf partages.tar.gz`
* Générer les fichiers de données (.dat)
	* Aller dans `DIAGNOSTIC-COFFRE-BDD/`
	* Exécuter : `perl Data_Shaping.pl partages`
* 1 graphique peut être généré :  
	**Nombre de partages par utilisateurs ET Nombre de docs par partages ET Nombre de destinataires par partages**
	* Aller dans `DIAGNOSTIC-COFFRE-BDD/plot`
	* Exécuter : `gnuplot partages.plot`

Pour toutes questions, `mailto: pierre.cazajous@sopragroup.com`
