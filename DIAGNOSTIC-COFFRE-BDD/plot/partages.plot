set boxwidth 0.9 absolute
set style fill   solid 1.00 border lt -1
set key inside right top vertical Right noreverse noenhanced autotitles nobox
set style histogram clustered gap 1 title  offset character 0, 0, 0
set datafile missing '-'
set style data histograms
#set xtics border in scale 0,0 nomirror rotate by -45  offset character 0, 0, 0
set xtics  norangelimit font ",8"
set xtics   ()
set title "Diagnostic des partages"
set xlabel "Nb partages/Nb docs/Nb destinataires"
set ylabel "Nb utilisateurs/Nb partages/Nb partages"
plot '../data/partages.dat' using 2:xtic(1) ti "Nb utilisateurs/Nb partages", '' u 3 ti "Nb partages/Nb docs", '' u 4 ti "Nb partages/Nb destinataires"

pause -1 "Press Enter to quit"
