#set terminal pngcairo
#set output 'histograms.2.png'
set boxwidth 0.9 absolute
set style fill   solid 1.00 border lt -1
set key inside right top vertical Right noreverse noenhanced autotitles nobox
set style histogram clustered gap 1 title  offset character 0, 0, 0
set datafile missing '-'
set style data histograms
set xtics border in scale 0,0 nomirror rotate by -45  offset character 0, 0, 0
set xtics  norangelimit font ",8"
set xtics   ()
set title "Nombre de documents par utilisateurs en BDD et en coffre" 
set xlabel "Utilisateurs"
set ylabel "Nombre de documents"
plot '../data/documents_utilisateurs.dat' using 2:xtic(1) ti "Documents BDD", '' u 3 ti "Documents Coffre"

pause -1 "Press Enter to quit"
