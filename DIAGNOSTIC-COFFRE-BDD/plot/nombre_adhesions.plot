# set terminal pngcairo  transparent enhanced font "arial,10" fontscale 1.0 size 500, 350 
# set output 'histograms.2.png'
set boxwidth 0.9 absolute
set style fill   solid 1.00 border lt -1
set key inside right top vertical Right noreverse noenhanced autotitles nobox
set style histogram clustered gap 1 title  offset character 0, 0, 0
set datafile missing '-'
set style data histograms
set xtics border in scale 0,0 nomirror rotate by -45  offset character 0, 0, 0
set xtics  norangelimit font ",8"
set xtics   ()
set title "Nombre d'adhésions par émetteur et par statut" 
set xlabel "Emetteurs"
set ylabel "Nombre d'adhésions"
plot '../data/nombre_adhesions.dat' using 2:xtic(1) ti "statut 4", '' u 3 ti "statut 2", '' u 4 ti "statut 7", '' u 5 ti "statut 3", '' u 6 ti "statut 5", '' u 7 ti "statut 6", '' u 8 ti "statut 8", '' u 9 ti "statut 0"

pause -1 "Press Enter to quit"
