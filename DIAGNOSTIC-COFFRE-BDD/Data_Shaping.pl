use strict;
use warnings;

########################################
# Création du fichier "tags_users.dat"
########################################
sub tags_users{
	my $fichierR = "data/tags_users.txt";			# Fichier des données brutes

	if(-e $fichierR){
		my $fh;										# "Pointeur" sur les fichiers
		
		# Récupération des données brutes
		open($fh,'<',$fichierR) or die "Impossible d'ouvrir le fichier '$fichierR' en lecture";
		my $data = <$fh>;
		chomp $data;
		close $fh;

		# Parsing des données brutes dans un tableau
		my @t = split(/ /,$data);
		
		# Création d'une hash table donnant le nombre d'utilisateurs par nombre de tags
		my %hashTab = ();
		foreach my $nbTags ( @t ) {
		   $hashTab{$nbTags}++;
		}
		
		# Ouverture du fichier de sortie
		my $fichierW = "data/tags_users.dat";		# Fichier des données formatées
		open($fh,'>',$fichierW) or die "Impossible d'ouvrir le fichier '$fichierW' en écriture";

		# Formatage des données pour gnuplot
		foreach my $key (sort {$a <=> $b} keys %hashTab) {
			 print $fh "$key\t$hashTab{$key}\n";
		}
		
		close $fh;
	}
	else{
		print "Fichier $fichierR introuvable\n"
	}
}



########################################
# Création du fichier "tags_docs.dat"
########################################
sub tags_docs{
	my $fichierR = "data/tags_docs.txt";			# Fichier des données brutes

	if(-e $fichierR){
		my $fh;										# "Pointeur" sur les fichiers
		
		# Récupération des données brutes
		open($fh,'<',$fichierR) or die "Impossible d'ouvrir le fichier '$fichierR' en lecture";
		my $data = <$fh>;
		chomp $data;
		close $fh;

		# Parsing des données brutes dans un tableau
		my @t = split(/ /,$data);
		
		# Création d'une hash table donnant le nombre docs par nombre de tags
		my %hashTab = ();
		foreach my $nbTags ( @t ) {
		   $hashTab{$nbTags}++;
		}		
	
		my $fichierW = "data/tags_docs.dat";		# Fichier des données formatées
	
		# Ouverture du fichier de sortie
		open($fh,'>',$fichierW) or die "Impossible d'ouvrir le fichier '$fichierW' en écriture";
		
		# Formatage des données pour gnuplot
		foreach my $key (sort {$a <=> $b} keys %hashTab) {
			 print $fh "$key\t$hashTab{$key}\n";
		}
		
		close $fh;
	}
	else{
		print "Fichier $fichierR introuvable\n"
	}
}

########################################
# Création du fichier "documents_utilisateurs_erreur.dat"
########################################
sub documents_utilisateurs_erreur{
	my $fichierR = "data/documents_utilisateurs.dat";				# Fichier des données brutes

	if(-e $fichierR){
		my $fhR;													# "Pointeur" sur le fichier d'entrée
		
		# Récupération des données brutes
		open($fhR,'<',$fichierR) or die "Impossible d'ouvrir le fichier '$fichierR' en lecture";
		
		my $fichierW = "data/documents_utilisateurs_erreur.dat";	# Fichier des données formatées
		my $fhW;													# "Pointeur" sur le fichier de sortie
		
		# Ouverture du fichier de sortie
		open($fhW,'>',$fichierW) or die "Impossible d'ouvrir le fichier '$fichierW' en écriture";
		
		# Récupération de chaque ligne de données brutes
		while(my $line = <$fhR>){
			chomp $line;
			
			# Parsing des données brutes dans un tableau
			my @t = split(/\t/,$line);
			
			# Formatage des données pour gnuplot
			if(($t[1]!=$t[2])){
				print $fhW "$t[0]\t$t[1]\t$t[2]\n";
			}
		}
		
		close $fhR;
		close $fhW;
	}
	else{
		print "Fichier $fichierR introuvable\n"
	}
}

########################################
# Création du fichier "partages.dat"
########################################
sub partages{
	my $fichierRUsers = "data/partages_users.txt";						# Fichier des données brutes utilisateurs
	my $fichierRDocs = "data/partages_docs.txt";						# Fichier des données brutes docs
	my $fichierRRecipients = "data/partages_recipients.txt";			# Fichier des données brutes destinataires

	if(-e $fichierRUsers && -e $fichierRDocs && -e $fichierRRecipients){
		my $fh;															# "Pointeur" sur les fichiers
		
		# Récupération des données brutes utilisateurs
		open($fh,'<',$fichierRUsers) or die "Impossible d'ouvrir le fichier '$fichierRUsers' en lecture";
		my $dataUsers = <$fh>;
		chomp $dataUsers;
		close $fh;
		
		# Récupération des données brutes docs
		open($fh,'<',$fichierRDocs) or die "Impossible d'ouvrir le fichier '$fichierRDocs' en lecture";
		my $dataDocs = <$fh>;
		chomp $dataDocs;
		close $fh;
		
		# Récupération des données brutes destinataires
		open($fh,'<',$fichierRRecipients) or die "Impossible d'ouvrir le fichier '$fichierRRecipients' en lecture";
		my $dataRecipients = <$fh>;
		chomp $dataRecipients;
		close $fh;

		# Parsing des données brutes dans un tableau
		my @tUsers = split(/ /,$dataUsers);
		my @tDocs = split(/ /,$dataDocs);
		my @tRecipients = split(/ /,$dataRecipients);
		
		# Création d'une hash table donnant le nombre d'utilisateurs par nombre de partages
		my %hUsers = ();	
		foreach my $nbTags ( @tUsers ) {
		   $hUsers{$nbTags}++;
		}
		# Création d'une hash table donnant le nombre de partages par nombre de docs
		my %hDocs = ();
		foreach my $nbTags ( @tDocs ) {
		   $hDocs{$nbTags}++;
		}
		# Création d'une hash table donnant le nombre de partages par nombre de destinataires
		my %hRecipients = ();
		foreach my $nbTags ( @tRecipients ) {
		   $hRecipients{$nbTags}++;
		}
		
		# Création d'une hash table globale donnant :
		# - le nombre d'utilisateurs par nombre de partages
		# - le nombre de partages par nombre de docs
		# - le nombre de partages par nombre de destinataires
		my %hashTab = ();		
		foreach my $key (sort {$a <=> $b} keys %hUsers) {
			$hashTab{$key}{"users"}=$hUsers{$key};
		}
		foreach my $key (sort {$a <=> $b} keys %hDocs) {
			$hashTab{$key}{"docs"}=$hDocs{$key};
		}
		foreach my $key (sort {$a <=> $b} keys %hRecipients) {
			$hashTab{$key}{"recipients"}=$hRecipients{$key};
		}
		
		
		my $fichierW = "data/partages.dat";								# Fichier des données formatées
		
		# Ouverture du fichier de sortie
		open($fh,'>',$fichierW) or die "Impossible d'ouvrir le fichier '$fichierW' en écriture";
		
		# Formatage des données pour gnuplot
		foreach my $key (sort {$a <=> $b} keys %hashTab) {
			print $fh "$key\t";
			if($hashTab{$key}{"users"}){
				print $fh "$hashTab{$key}{\"users\"}\t";
			}
			else{
				print $fh "0\t";
			}
			
			if($hashTab{$key}{"docs"}){
				print $fh "$hashTab{$key}{\"docs\"}\t";
			}
			else{
				print $fh "0\t";
			}
			
			if($hashTab{$key}{"recipients"}){
				print $fh "$hashTab{$key}{\"recipients\"}\n";
			}
			else{
				print $fh "0\n";
			}
		}
		close $fh;
	}
	else{
		print "Fichier $fichierRUsers ou $fichierRDocs ou $fichierRRecipients introuvable\n"
	}
}

sub affiche_aide{
	print "Usage : perl Data_Shaping.pl <argument>\n";
	print "<argument> :\n";
	print " - tags_users\n";
	print " - tags_docs\n";
	print " - documents_utilisateurs_erreur\n";
	print " - partages\n";
}


# Init -----------------------------------------------------------------------------------------------
my $nbArg = @ARGV;

if($nbArg == 0){
	print "Paramètre requis manquant\n";
	affiche_aide();
}
else{
	if($ARGV[0] eq "tags_users"){
		tags_users();
	}
	elsif($ARGV[0] eq "tags_docs"){
		tags_docs();
	}
	elsif($ARGV[0] eq "documents_utilisateurs_erreur"){
		documents_utilisateurs_erreur();
	}
	elsif($ARGV[0] eq "partages"){
		partages();
	}
	else{
		affiche_aide();
	}
}
