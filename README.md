#1) Creation d'un nouveau test

- Redaction a partir d'un test existant
	cp ./CHECKS/dinst/cpu-check.sh ./CHECKS/[test-type]/[test=name]-check.sh
    Utilisation des fonctions check_* pour realiser les tests
	Utilisation des fonctions display_* definies dans LIBS/display.fct pour afficher les messages resultats
- Gestion des parametres a envoyer au script par environnment et par hostname
	mkdir ./CONFIGS/[testname]
	cat << EOF > ./CONFIGS/[testname]/[environnement]
	hostname1,[param1 issu de la doc] [param2 issu de la doc] [...]
	hostname2,[param1 issu de la doc] [param2 issu de la doc] [...]
	...
	EOF


#2) Lancer un test unique à la main

ssh hostname1 'bash -s' <  ./CHECKS/[test-type]/[test-name]-check.sh [param1] [param2] ...


#3) Partager le test avec les autres developpeurs

Pour lancer tous les tests sur tous les serveurs d'un environnement le script recette-technique-launcher.sh est utilise
(cf: ./recette-technique-launcher.sh --help)

##3.1. Outil de gestion de travail collaboratif (Git)
- Recuperation du projet en local (a faire une seule fois)
	git clone git@gitlab.fullsix.com:digiposte/tests-techniques.git
- Mise a jour de la copie local (a faire avant chaque commit)
	git pull
- Ajout des nouveaux fichiers
	git add <file_list>
- Sauvegarde des fichiers modifies en local
	git commit -a # Decrire la modification quand git le propose
- Partage des modifications avec les autres developpeurs
	git push

- Creation d'un tag
	git tag -a <nom_tag> -m '<message_tag>'
- Liste des tags existants
	git tag
- Données liées à un tag
	git show <nom_tag>
- Push du tag créé
	git push origin <nom_tag>
- Push de tous les tags
	git push origin --tags
- Obtention du nom de la branche de dev
	git branch
- Affecter les commits au master (Effectuer un "git checkout tags/v1.0" rapatriera les fichiers de ce tag ET vous empêchera d'effectuer des modifications dans le 'master')
	git checkout master


##3.2. Modification du launcher (a eviter)
- Modification dans une copie du script
	cp recette-technique-launcher.sh recette-technique-launcher-developer-name.sh
- Apres le partage des modifications (cf: 3.1), demande d'evolution aupres du mainteneur
	mailto: hguerine@gmail.com avec pour objet:"Demande de merge recette-technique-launcher-developer-name.sh"



#Pour toute question, mailto: hguerine@gmail.com
