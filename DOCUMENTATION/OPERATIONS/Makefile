BROWSER:=firefox
TARGET:=docx
CONV:=pandoc
EXT:=textile

S:=sources
R:=results
OFILE:=$(R)/output

DOCTITLE:=\h1. Dossier d\'exploitation

PARAGRAPHS := $(S)/introduction $(S)/procedures $(S)/monitoring
FILES := $(foreach PARAGRAPH,$(PARAGRAPHS),$(wildcard $(PARAGRAPH)/*.$(EXT)))

all: $(TARGET)

docx: concat
	pandoc -f textile -t docx -o $(OFILE).docx $(OFILE).$(EXT)

rst: concat
	pandoc -f textile -t rst -o $(OFILE).rst $(OFILE).$(EXT)

textile: concat
	pandoc -f textile -t textile -o $(OFILE).textile $(OFILE).$(EXT)

latex: concat
	pandoc -f textile -t latex -o $(OFILE).latex $(OFILE).$(EXT)

pdf: concat
	pandoc -f textile -o $(OFILE).pdf $(OFILE).$(EXT)

html: concat
	pandoc -f textile -t html5 --smart --ascii --number-sections --standalone -H includes/bootstrap.css.inc -A includes/bootstrap.js.inc -o $(OFILE).html $(OFILE).$(EXT)

prettyhtml: html
	sed -ri 's,<table>,<table class="table table-bordered">,' $(OFILE).html
	sed -ri '/<body>/a'"`cat includes/bootstrap.menu.inc | tr -d '\n'`" $(OFILE).html
	sed -ri '/<body>/a<div class="container-fluid"><div class="row-fluid">' $(OFILE).html
	sed -ri '/<div class="row-fluid">/a<div class="span1"></div><div class="span9">' $(OFILE).html
	sed -ri '/<\/body>/i<div class="span2"></div></div></div>' $(OFILE).html
	sed -ri 's,<body>,<body style="padding-top:50px">,' $(OFILE).html

openhtml: prettyhtml
	$(BROWSER) $(OFILE).html &

opendocx: docx
	libreoffice $(OFILE).docx &

concat:
	cat $(FILES) > $(OFILE).$(EXT)
#	sed -i "1i$(DOCTITLE)" $(OFILE).$(EXT)

clean:
	rm $(OFILE).*
