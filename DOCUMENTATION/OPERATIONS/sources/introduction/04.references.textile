h2. Documents de reference

* "Dossier d'architecture technique":https://wiki.collaboratif-courrier.fr/confluence/display/DGPV2/4.5.5+Dossier+d%27architecture+Technique
* "Dossier d'installation socle": https://wiki.collaboratif-courrier.fr/confluence/display/DGPV2/Dossier+d%27installation+Socle
* "Dossier d'installation applicative": https://wiki.collaboratif-courrier.fr/confluence/display/DGPV2/Dossier+d%27installation+release+V6.0.0+-+Auto

