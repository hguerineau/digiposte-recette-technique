h2. Monitoring

L’exploitant monitorera les métriques via l’outil d’exploitation centréon.
Les métriques sont définies dans le document "V6.0.0.0 - Monitoring Digiposte.xlsx", que l'on trouvera en attachement de la page Confluence "Dossier d'Exploitation - Livraisons" (https://wiki.collaboratif-courrier.fr/confluence/pages/viewpageattachments.action?pageId=105611862)

