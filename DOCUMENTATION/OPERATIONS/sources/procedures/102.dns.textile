h3. DNS

h4. Présentation

h5. Vue d'ensemble

Ci-après, un aperçu des serveurs concernés par environnement.

|_.  Env |_.  dev |_.  tma |_.  demo |_.  interop |_.  recfonc |_.  rectech  |_.  tmc      |_.  preprod-a |_.  preprod-b |_.  prod-a |_.  prod-b |
| Role   | dmz    | all    | all     | all        | dmz        | dns1 / dns2 | dns1 / dns2 | dns1         | dns1         | dns1      | dns1      |

Le nom du serveur @servername@ se compose en concaténant @Role@-@Env@
Par exemple : @dns2-rectech@

h5. Le Service

Le composant servant le service de résolution de noms est @bind9@

Un seul DNS primaire sert la plateforme à l'adresse dns.digiposte.local.

Un serveur de secours est présent il sert la plateforme à l'adresse dns-s.digiposte.local, en cas de problème sur le serveur primaire, la vip portant dns.digiposte.local peut-être déplacée sur le serveur de secours.

Tous les serveurs de la plateforme référencent les 2 dns

Le service peut-être rendu soit par :

* un seul et unique serveur : *dev*, *tma*, *demo*, *interop*, *recfonc*
* l'un des 2 serveurs redondé
** sur le meme environnement et sur le meme site : *rectech*, *tmc*
** sur le meme environnement mais sur un site secondaire : *preprod*, *prod*

h5. Récapitulatif

Voici un tableau de correspondance entre les serveurs (principaux et secondaires) et les VIP du service de chaque environnement :

|_.   Env            |_.   dev      |_.   tma      |_.   demo     |_.   interop |_.   recfonc |_.   rectech  |_.   tmc      |_.   preprod    |_.   prod     |
| Serveur principal  | dmz-digi-dev | all-tma      | all-tma      | all-interop | dmz-recfonc | dns1-rectech | dns1-tmc     | dns1-preprod-a | dns1-prod-a  |
| Serveur secondaire |              |              |              |             |             | dns2-rectech | dns2-tmc     | dns1-preprod-b | dns1-prod-b  |
| VIP principale     | ?            | 172.15.11.31 | 172.15.12.31 | 172.16.3.31 | 172.16.4.31 | 172.15.50.31 | 172.24.11.31 | 172.18.11.31   | 172.20.11.31 |
| VIP secondaire     | ?            | 172.15.11.11 | 172.15.12.11 | 172.16.3.11 | 172.16.4.11 | 172.15.50.11 | 172.24.11.11 | 172.18.12.31   | 172.20.12.31 |
| DNS principal      | ?            | dns.digiposte.local | dns.digiposte.local | dns.digiposte.local | dns.digiposte.local | dns.digiposte.local | dns.digiposte.local | dns.digiposte.local   | dns.digiposte.local |
| DNS secondaire     | ?            | dns-s.digiposte.local | ns-s.digiposte.local | ns-s.digiposte.local | ns-s.digiposte.local | ns-s.digiposte.local | ns-s.digiposte.local | ns-s.digiposte.local | ns-s.digiposte.local |

h4. Fiches Techniques DNS/Bind

Ci-après, l'ensemble des fiches techniques nécessaires à la bonne exploitation du service.

h5. Etat du service

Pour vérifier l'état du service, se connecter au @servername@ servant actuellement le service, puis lancer la commande suivante :

bc. service bind9 status

Le résultat attendu est la chaine de caractère suivante :

bc. bind9 is running

h5. Démarrage du service

Pour démarrer le service, se connecter au @servername@ servant actuellement le service avec les *privilèges administrateur*, puis lancer la commande suivante :

bc. service bind9 start

Le résultat attendu est la chaine de caractère suivante :

bc. Starting domain name service... bind9    [ OK ]

h5. Arrêt du service

Pour arrêter le service, se connecter au @servername@ servant actuellement le service avec les *privilèges administrateur*, puis lancer la commande suivante :

bc. service bind9 stop

Le résultat attendu est la chaine de caractère suivante :

bc. waiting for pid [numero_process] to die [ OK ]

h5. Relance du service

Pour relancer le service, se connecter au @servername@ servant actuellement le service avec les *privilèges administrateur*, puis lancer la commande suivante :

bc. service bind9 restart

bc. Stopping domain name service... bind9    [ OK ]
Starting domain name service... bind9    [ OK ]

h5. Bascule

Pour basculer le service de résolution de nom sur le serveur de secours, il convient de

# Se connecter avec les *privilèges administrateur* au @servername@ servant actuellement les requêtes DNS et retirer la VIP du service
# Retirer l'adresse IP virtuelle dédiée au service principal sur le serveur principal
# Se connecter au @servername@ secondaire avec les *privilèges administrateur* et déclarer la VIP du service principal
# Puis ajouter la VIP du DNS secondaire

Exemple pour l'environnement @prod@

bc. sampleuser@workstation$ ssh root@dns1-prod-a #Serveur principal
root@dns1-prod-a$ ip addr del 172.20.11.31/255.255.0.0 dev eth0:0 #VIP principale
root@dns1-prod-a$ exit
sampleuser@workstation$ ssh root@dns1-prod-b #serveur secondaire
root@dns1-prod-b$ ip addr add 172.20.11.31/255.255.0.0 dev eth0:0 #VIP principale
root@dns1-prod-b$ exit

Exemple pour l'environnement @rectech@

bc. sampleuser@workstation$ ssh root@dns1-rectech #Serveur principal
root@dns1-rectech$ ip addr del 172.15.50.31/255.255.0.0 dev eth0:0 #VIP principale
root@dns1-rectech$ exit
sampleuser@workstation$ ssh root@dns2-rectech #serveur secondaire
root@dns2-rectech$ ip addr add 172.15.50.31/255.255.0.0 dev eth0:0 #VIP principale

