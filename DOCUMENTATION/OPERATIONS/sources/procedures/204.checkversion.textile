
h3. Verifier la version applicative

h4. Présentation

Afin de vérifier les version déployées, il suffit de se rendre sur l'URL de l'instance que l'on souhaite vérifier.

|_. Environnement |_. Composant |_. URL |
| DEV | Destinataire/Emetteur/BO/WSProxy | https://secure.dev.u-post.fr/version |
| DEV | API Java | https://admin.dev.u-post.fr/api/version |
| DEV | Batchs | https://admin.dev.u-post.fr/batch/version |
| RECTECH | Destinataire/Emetteur/BO/WSProxy | https://secure.rectech.u-post.fr/version |
| RECTECH | API Java | https://admin.rectech.u-post.fr/api/version |
| RECTECH | Batchs | https://admin.rectech.u-post.fr/batch/version |
| RECFONC | Destinataire/Emetteur/BO/WSProxy | https://secure.recfonc.u-post.fr/version |
| RECFONC | API Java | https://admin.recfonc.u-post.fr/api/version |
| RECFONC | Batchs | https://admin.recfonc.u-post.fr/batch/version |
| INTEROP | Destinataire/Emetteur/BO/WSProxy | https://secure.interop.u-post.fr/version |
| INTEROP | API Java | https://admin.interop.u-post.fr/api/version |
| INTEROP | Batchs | https://admin.interop.u-post.fr/batch/version |
| TMA | Destinataire/Emetteur/BO/WSProxy | https://secure.tma.u-post.fr/version |
| TMA | API Java | https://admin.tma.u-post.fr/api/version |
| TMA | Batchs | https://admin.tma.u-post.fr/batch/version |
| DEMO | Destinataire/Emetteur/BO/WSProxy | https://secure.demo.u-post.fr/version |
| DEMO | API Java | https://admin.demo.u-post.fr/api/version |
| DEMO | Batchs | https://admin.demo.u-post.fr/batch/version |
| TMC | Destinataire/Emetteur/BO/WSProxy | https://secure.tmc.u-post.fr/version |
| TMC | API Java | https://admin.tmc.u-post.fr/api/version |
| TMC | Batchs | https://admin.tmc.u-post.fr/batch/version |
| PREPROD | Destinataire/Emetteur/BO/WSProxy | https://secure.preprod.u-post.fr/version |
| PREPROD | API Java | https://admin.preprod.u-post.fr/api/version |
| PREPROD | Batchs | https://admin.preprod.u-post.fr/batch/version |
| PROD | Destinataire/Emetteur/BO/WSProxy | https://secure.digiposte.fr/version |
| PROD | API Java | https://admin.digiposte.fr/api/version |
| PROD | Batchs | https://admin.digiposte.fr/batch/version |

Pour effectuer la vérification en ligne de commande :
Se connecter au puppet master (ou serveur nommé "support") puis se rendre dans le répertoire "~/deploy" avant d'exécuter la commande suivante :
@git branch | grep ^*@

