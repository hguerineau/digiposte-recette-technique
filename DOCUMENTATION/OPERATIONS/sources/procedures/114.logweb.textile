h3. Log Web

h4. Présentation

h5. Vue d'ensemble

Ci-après, un aperçu des serveurs concernés par environnement.

|_.     Env |_.     dev |_.     tma |_.     demo |_.     interop |_.     recfonc |_.     rectech |_.     tmc |_.  preprod-a / preprod-b |_.  prod-a / prod-b |
| Role      | support   | all       | all        | all           | support       | log-web       | log-web   | log-web                  | log-web            |

Le nom du serveur @servername@ se compose en concaténant @Role@-@Env@

Par exemple : @log-web-prod-a@

h5. Le Service

Les composants exposants le service de consultation des logs web sont @Graylog2@ et @MongoDB@

Le service est rendu par un seul et unique serveur sur tous les environnements : *dev*, *tma*, *demo*, *interop*, *recfonc*, *rectech*, *tmc*, *preprod*, *prod*
Un serveur de secours est disponible sur les environnements : *preprod*, *prod*

h5. Récapitulatif

Voici un tableau référençant les serveurs (principaux et secours) et les VIP de service de chaque environnement.

|_.    Env           |_.    dev         |_.    tma    |_.    demo   |_.    interop |_.    recfonc    |_.    rectech    |_.    tmc    |_.    preprod      |_.    prod      |
| serveur principal  | support-digi-dev | all-tma     | all-tma     | all-interop  | support-recfonc | log-web-rectech | log-web-tmc | log-web-preprod / log-web-preprod-a | log-web-prod-a |
| serveur de secours |                  |             |             |              |                 |                 |             | log-web-preprod-b | log-web-prod-b |
| VIP du service     | ?                | 172.16.1.76 | 172.16.2.76 | 172.16.3.76  | 172.16.4.76     | 172.16.5.76     | 172.23.1.76 | 172.17.1.76       | 172.19.1.76    |


h4. Haute disponibilité par Bascule DRBD

voir le paragraphe général sur les bascules DRBD

Les opérations d'arrêt, relance, statut de service ne doivent être réalisées seulement si le serveur est principal au sens DRBD, on peut connaitre cette information avec la commande 

bc. # passer root
su -
# verifier si on est sur le secondary
facter -p drbd_is_secondary

** Si la chaine de caractère "false" s'affiche, le serveur est principal ou si DRBD n'est pas utilisé
** Si la chaine de caractère "true" s'affiche, le serveur est le secours

h4. Fiches Techniques LOGWEB/Graylog

h5. Etat du service

Pour vérifier l'état du service, se connecter au @servername@ servant actuellement le service, puis lancer la commande suivante :

bc. service apache2 status

Le résultat attendu sur le serveur primaire est le suivant :

bc.  * Apache2 is running (pid @numero_process@) 

Et sur le serveur secondaire : 

bc. Apache2 is NOT running.

h5. Démarrage du service

Pour démarrer le service, se connecter au @servername@ servant actuellement le service avec les *privilèges administrateur*, puis lancer la commande suivante :

bc. service apache2 start

Le résultat attendu est le suivant :

bc.  * Starting web server apache2                                      [ OK ]

h5. Arrêt du service

Pour arrêter le service, se connecter au @servername@ exposant actuellement le service avec les *privilèges administrateur*, puis lancer la commande suivante :

bc. service apache2 stop

Le résultat attendu est le suivant :

bc.  * Stopping web server apache2                                      [ OK ]

h5. Relance du service

Pour relancer le service, se connecter au @servername@ exposant actuellement le service avec les *privilèges administrateur*, puis lancer la commande suivante :

bc. service apache2 restart

Le résultat attendu est le suivant :

bc. ... waiting                                     [ OK ]

h5. Supervision du service

h6. Contrôles de bases

Se connecter à l'interface de gestion ci-après et vérifier la présence du texte « Start Graylog2 » et l'absence de message d'erreur rouge

h5. Interface de gestion

L'interface de monitoring protégée par certificat et login/mot de passe nominatif est disponible en fonction de l'environnement concerné aux adresses suivantes :

- https://admin.recfonc.u-post.fr/logs/
- https://admin.rectech.u-post.fr/logs/
- https://admin.tmc.u-post.fr/logs/
- https://admin.interop.u-post.fr/logs/
- https://admin.demo.u-post.fr/logs/
- https://admin.preprod.digiposte.fr/logs/
- https://admin.digiposte.fr/logs/
- https://admin.prod.u-post.fr/logs/ (de manière temporaire)

h4. Fiches Techniques LOGWEB/MongoDB

Ce composant permet à graylog de requeter les logs

Base de donnée de Graylog-web.

|_.  Env |_.  dev |_.  tma |_.  demo |_.  interop |_.  recfonc |_.  rectech |_.  tmc  |_.  preprod-a / preprod-b |_.  prod-a / prod-b |
| Role   | data   | all    | all     | all        | db         | log-web    | log-web | log-web                  | log-web            |

Le nom dns mongo.digiposte.local permet de pointer sur la bonne machine quel que soit l'environnement

h5. Etat du service

Pour vérifier l'état du service, se connecter au @servername@ servant actuellement le service, puis lancer la commande suivante :

bc. service mongodb status

Le résultat attendu sur le serveur primaire est le suivant :

bc. mongodb start/running, process @numero_process@

Sur le serveur secondaire : 

bc. mongodb stop/waiting

h5. Démarrage du service

Pour démarrer le service, se connecter au @servername@ servant actuellement le service avec les *privilèges administrateur*, puis lancer la commande suivante :

bc. service mongodb start

Le résultat attendu est le suivant :

bc. mongodb start/running, process @numero_de_process@     [ OK ]

h5. Arrêt du service

Pour arrêter le service, se connecter au @servername@ servant actuellement le service avec les *privilèges administrateur*, puis lancer la commande suivante :

bc. service mongodb stop

Le résultat attendu est le suivant :

bc. mongodb stop/waiting

h5. Relance du service

Suivez les procédures décrites dans “Arrêt du service” puis “Démarrage du service”

h5. Supervision du service

h6. Pré-requis

être sur le LAN de l'environnement 

h6. Contrôles de base (MongoDB)

Se connecter au @servername@ servant actuellement le service et lancer la commande suivante

bc. curl -isl http://mongo.digiposte.local:28017/serverStatus | grep uptime

vérifier que le texte JSON renvoyé contient la clé “uptime” avec une valeur > 0

h5. Interface de gestion

Il existe une interface d'administration accéssible par le LAN, elle est donnée à titre d'information mais pas utilisée

bc. http://log-web.digiposte.local:28017



