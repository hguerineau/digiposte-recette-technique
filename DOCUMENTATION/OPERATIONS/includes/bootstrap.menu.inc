
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">Digiposte</a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
              Dossier d'exploitation
            </p>
            <ul class="nav">
                <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">Procedures Infra <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                        <li><a href="#dns">DNS</a></li>
                        <li><a href="#reverse-proxy">Reverse-Proxy</a></li>
                        <li><a href="#load-balancer">Load-Balancer</a></li>
                        <li><a href="#indexeur">Indexeur</a></li>
                        <li><a href="#base-de-donnees">Base de donnees</a></li>
                        <li><a href="#web">Web</a></li>
                        <li><a href="#back">Back</a></li>
                        <li><a href="#message-queue">Message Queue</a></li>
                        <li><a href="#workers">Workers</a></li>
                        <li><a href="#partage">Partage</a></li>
                        <li><a href="#deploy">Deploy</a></li>
                        <li><a href="#kpi">KPI</a></li>
                        <li><a href="#log-web">Log Web</a></li>
                        <li><a href="#log-collector">Log Collector</a></li>
                        <li><a href="#gestion-des-miroirs-ubuntu">Gestion des miroirs Ubuntu</a></li>
			<li><a href="#haute-disponibilité-drbd">Haute disponibilité DRBD</a></li>
                        <li><a href="#certificats">Certificats</a></li>
                  </ul>
                </li>
                <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">Procedures Applicatives <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                        <li><a href="#batchs">Batchs</a></li>
                        <li><a href="#amazon-s3">Amazon S3</a></li>
                        <li><a href="#crontabs">Crontabs</a></li>
                        <li><a href="#verifier-la-version-applicative">Version Applicative</a></li>
                        <li><a href="#dump-de-base-de-données">Dump BDD</a></li>
                        <li><a href="#dump-du-système-de-fichiers">Dump FS</a></li>
                        <li><a href="#clé-privée-de-lutilisateur-admin">MDP Clé privée</a></li>
                  </ul>
                </li>
                <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">Traitements Batch <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                        <li><a href="#liste-des-traitements">Liste des traitements</a></li>
                        <li><a href="#ordonnancements">Ordonnancements</a></li>
                        <li><a href="#liste-des-batchs">Liste des batchs</a></li>
                  </ul>
                </li>
                <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">Monitoring <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                        <li><a href="#monitoring-générique">Monitoring Générique</a></li>
                        <li><a href="#monitoring-spécifique">Monitoring Spécifique</a></li>
                  </ul>
                </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

