# Overview

This folder contains source files to generate digiposte's ops documentation

Source: Textile is the markup language used for the sources. (markdown can't handle tables)
Result: HTML or docx

# Layout
.
├── includes                        => include additionnal content for html rendering
├── results                         => all generated output goes here
└── sources                         => all .textile files are located in sources' subdirectories
    ├── introduction
    ├── procedures
    ├── batchs
    └── monitoring

# Workflow

## Loading

- First subdirectories are loaded in that order : introduction, procedures, batchs, monitoring
- Then all files whithin each subdirectory are loaded by alphabetical order
- Finally the result is concateneted in one big output.textile file

## Compiling

The textile file is then converted to html/docx/pdf using pandoc

    apt-get install pandoc

# How-To

To generate and open the html version of the documentation (useful for quick and nice view of your work)

    make openhtml  # if you just want to generate without opening use "make html" or "make prettyhtml"

To generate and open the docx version of the documentation (userful for final version to import in digiposte's wiki)

    make opendocx  # if you just want to generate whithout opening use "make docx" instead

To remove all generated files (temp et finals)

    make clean

