# Script de test de l'API publique
Ce script teste l'API publique, suite à la refonte technique de Digiposte V7.

Il n'est pour l'instant pas entièrement automatisé : il est nécessaire de créer manuellement un partenaire dans l'admin API et un compte utilisateur.

## Prérequis
### 1- Création d'un partenaire
Se connecter à `https://admin.rectech.u-post.fr/api/login.jsp` avec `BO_API // *********`  
Créer un nouveau partenaire nommé `partenaire // *********` et lui donner les droits d'accès à tous les services

### 2- Création d'un compte utilisateur
Se connecter à `https://secure.rectech.u-post.fr/inscription`  
Créer un nouveau compte nommé `utilisateur // 011111`  
Activez le via le lien envoyé par mail

## Liste des services de l'API actuellement testés
Services de l'API actuellement testés:

- Récupération des tokens
- Regénération d’un access token depuis un fresh token 
- Lister les docs de la Boîte aux lettres
- Lister les docs du Coffre
- Upload d’un fichier dans le coffre
- Ajout d’un tag au doc
- Passage du doc à l’état lu
- Mise à la corbeille d'un document
- Récupération des infos de l’utilisateur
- Génération d’un email Digiposte
- Restauration d’un doc mis à la corbeille
- Lister tous les docs de l’utilisateur
- Lister les docs de la corbeille
- Lister les tags de l'utilisateur
- Lister les émetteurs disponibles 
- Lister toutes les adhésions 
- Lister les adhésions de l’émetteur
- Rechercher un émetteur par son pid 
- Affichage des espaces de partage
- Affichage des documents partagés
- Afficher un espace de partage spécifique
- Afficher les documents d'un espace de partage spécifique


Pour toute question, `mailto: mlemarchand@sopragroup.com`