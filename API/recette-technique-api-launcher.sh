#! /bin/bash

# Author        Mathieu LEMARCHAND
# Description   Teste les services API - requiert la création d'un partenaire dans l'admin API + un compte utilisateur

# A DECOMMENTER
# while true; do
    # read -p "Did you create an API partner? [y/n] " yn
    # case $yn in
        # [Yy]* ) break;;
        # [Nn]* ) explorer.exe https://admin.rectech.u-post.fr/api/login.jsp; break;;
        # * ) echo "Please answer yes or no.";;
    # esac
# done

##Pour Linux: mettre google-chrome URL / firefox URL

# echo "Please enter the API partner's login: ";read partner
# echo "Please enter the API partner's password: ";read -s password

# while true; do
	# echo -e "\n";read -p "Did you create a user on rectech? [y/n] " yn
    # case $yn in
        # [Yy]* ) break;;
        # [Nn]* ) explorer.exe https://secure.rectech.u-post.fr/inscription; break;;
        # * ) echo "Please answer yes or no.";;
    # esac
# done
#/A DECOMMENTER

# A SUPPRIMER
partner="partner221013";password="digiposte"
#/A SUPPRIMER

echo -e "\n";echo -e "Click on this link, identify yourself and get the URL's code:"; echo "https://secure.rectech.u-post.fr/oauth/authorize?response_type=code&client_id=${partner}&display=page&redirect_uri=http%3A%2F%2Flocalhost%2"
echo "Please enter the URL's code: ";read code;echo -e "\n"

# Print the check results on the screen as well as in a log file
NOW=$(date +%Y%m%d-%H%M%S)
exec &> >(tee -a LOGS/check-API_$NOW.log)
LOG_FILE="LOGS/check-API_$NOW.log"

# Récupération des tokens
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --user "${partner}:${password}" --data "grant_type=authorization_code&code=${code}&redirect_uri=http%3A%2F%2Flocalhost%2" https://api.rectech.u-post.fr/oauth/token`;sleep 1
access_token=`echo $retourcurl | cut -d':' -f2 | cut -d',' -f1 | cut -d'"' -f2`
refresh_token=`echo $retourcurl | cut -d':' -f4 | cut -d',' -f1 | cut -d'"' -f2`
echo "$retourcurl" | grep -E "access_token" >/dev/null 2>&1
if [ $? -eq 1 ]; then
    echo -e "\033[31mERROR: access_token not found - $retourcurl.\033[0m"; exit 1
else
	if [ -z "$access_token" ]; then
		echo -e "\033[31mERROR: access_token empty - $retourcurl.\033[0m"; exit 1
	else
		echo -e "\033[32mAccess_token's generation: OK.\033[0m"
	fi
fi

echo "$retourcurl" | grep -E "refresh_token" >/dev/null 2>&1
if [ $? -eq 1 ]; then
    echo -e "\033[31mERROR: refresh_token not found - $retourcurl.\033[0m"; exit 1
else
	if [ -z "$refresh_token" ]; then
		echo -e "\033[31mERROR: refresh_token empty - $retourcurl.\033[0m"; exit 1
	else
		echo -e "\033[32mRefresh_token's generation: OK.\033[0m"
	fi
fi

# Regénération d’un access token depuis un fresh token 
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --user "${partner}:${password}" --data "grant_type=refresh_token&refresh_token=${refresh_token}" https://api.rectech.u-post.fr/oauth/token`;sleep 1
access_token=`echo $retourcurl | cut -d':' -f2 | cut -d',' -f1 | cut -d'"' -f2`
refresh_token=`echo $retourcurl | cut -d':' -f4 | cut -d',' -f1 | cut -d'"' -f2`
echo "$retourcurl" | grep -E "access_token" >/dev/null 2>&1
if [ $? -eq 1 ]; then
    echo -e "\033[31mERROR: can't regen the access_token - $retourcurl.\033[0m"
else
	if [ -z "$access_token" ]; then
		echo -e "\033[31mERROR: access_token empty during regen - $retourcurl.\033[0m"
	else
		echo -e "\033[32mAccess_token's regeneration: OK.\033[0m"
	fi
fi

echo "$retourcurl" | grep -E "refresh_token" >/dev/null 2>&1
if [ $? -eq 1 ]; then
    echo -e "\033[31mERROR: can't regen the refresh_token - $retourcurl.\033[0m"
else
	if [ -z "$refresh_token" ]; then
		echo -e "\033[31mERROR: refresh_token empty during regen - $retourcurl.\033[0m"
	else
		echo -e "\033[32mRefresh_token's regeneration: OK.\033[0m"
	fi
fi

#Utilise le partenaire API pour obtenir un token Basic
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --user "${partner}:${password}" -X POST https://api.rectech.u-post.fr/oauth/token?grant_type=client_credentials`;sleep 1
basic_access_token=`echo $retourcurl | cut -d':' -f2 | cut -d',' -f1 | cut -d'"' -f2`
echo "$retourcurl" | grep -E "access_token" >/dev/null 2>&1
if [ $? -eq 1 ]; then
    echo -e "\033[31mERROR: basic access_token not found - $retourcurl.\033[0m"; exit 1
else
	if [ -z "$basic_access_token" ]; then
		echo -e "\033[31mERROR: basic access_token empty - $retourcurl.\033[0m"; exit 1
	else
		echo -e "\033[32mBasic access_token's generation: OK.\033[0m"
	fi
fi

# Vérification de la validité d'un token XXX
# retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: Bearer ${access_token}" -X GET --header "Accept:application/json" --header "Content-type: application/json" https://api.rectech.u-post.fr/api/v2.0/user/customizationurl/token/${access_token}`;sleep 1
# echo $retourcurl
# echo "$retourcurl" | grep -E "document_types" >/dev/null 2>&1
# if [ $? -eq 1 ]; then
	# echo -e "\033[31mERROR: can't display the docs' facet - $retourcurl.\033[0m"
# else
	# echo -e "\033[32mDocs' facet display: OK.\033[0m"
# fi

# Création d'un compte XXX
# retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X POST --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/user/?registrationId=idducompte&fieldValue1=011111&fieldValue2=011111&tokenCustomization=2345679832453`;sleep 1
# echo $retourcurl
# echo "$retourcurl" | grep -E "document_types" >/dev/null 2>&1
# if [ $? -eq 1 ]; then
	# echo -e "\033[31mERROR: can't display the docs' facet - $retourcurl.\033[0m"
# else
	# echo -e "\033[32mDocs' facet display: OK.\033[0m"
# fi

# Création d'un compte partiel XXX
# retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X POST --header "Accept:application/json" --header "Content-type: application/json" -d "{partner_user_id : 'Aladin20131011142015',title : 'MR',last_name : 'abouda',first_name : 'ala',email : 'aabouda@gmail.com',date_of_birth : '1988-11-11'}" https://api.rectech.u-post.fr/api/v2.0/user/partial`;sleep 1
# echo $retourcurl
# echo "$retourcurl" | grep -E "document_types" >/dev/null 2>&1
# if [ $? -eq 1 ]; then
	# echo -e "\033[31mERROR: can't display the docs' facet - $retourcurl.\033[0m"
# else
	# echo -e "\033[32mDocs' facet display: OK.\033[0m"
# fi

# Création d'un nouveau token pour un compte partiel XXX (changer l'access_token avec celui d'un compte partiel?)
# retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -u digiposte:Digi_Pass -X POST --header "Authorization: bearer ${access_token}" --header "Accept:application/json" --header "Content-type: application/json" --header "X-PartnerUserId: ${partner}" https://api.rectech.u-post.fr/api/v2.0/user/customizationurl`;sleep 1
# echo $retourcurl
# echo "$retourcurl" | grep -E "document_types" >/dev/null 2>&1
# if [ $? -eq 1 ]; then
	# echo -e "\033[31mERROR: can't display the docs' facet - $retourcurl.\033[0m"
# else
	# echo -e "\033[32mDocs' facet display: OK.\033[0m"
# fi

# Suppression d'un compte partiel XXX
# retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X DELETE --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/user/partial`;sleep 1
# echo $retourcurl
# echo "$retourcurl" | grep -E "document_types" >/dev/null 2>&1
# if [ $? -eq 1 ]; then
	# echo -e "\033[31mERROR: can't display the docs' facet - $retourcurl.\033[0m"
# else
	# echo -e "\033[32mDocs' facet display: OK.\033[0m"
# fi

# Lister la "facette" des docs
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X POST --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/documents/facet`;sleep 1
echo "$retourcurl" | grep -E "document_types" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't display the docs' facet - $retourcurl.\033[0m"
else
	echo -e "\033[32mDocs' facet display: OK.\033[0m"
fi

# Lister tous les docs
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/documents?index=0&max_results=1000&sort=CREATION_DATE&direction=DESCENDING`;sleep 1
echo "$retourcurl" | grep -E "count" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't count all the docs - $retourcurl.\033[0m"
else
	echo -e "\033[32mAll docs' count: OK.\033[0m"
fi

# Lister les docs de la Boîte aux lettres
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/documents/inbox?index=0&max_results=1000&sort=CREATION_DATE&direction=DESCENDING`;sleep 1
echo "$retourcurl" | grep -E "count" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't count the docs in the Inbox - $retourcurl.\033[0m"
else
	echo -e "\033[32mInbox docs' count: OK.\033[0m"
	
	#Récupération du nombre de documents retournés
	last_i="init";for i in `echo $retourcurl | sed -e 's/:/ /g' | sed -e 's/,/ /g' | sed -e 's/"/ /g' | sed -e 's/}/ /g'`; do
		if [ ${last_i} = "count" ]; then
			nbr_docs=$i
		else
			last_i=$i
		fi
	done
	#Récupération de trois IDs si assez de documents présents
	if [ $nbr_docs -gt 0 ]; then
		loop=1; last_i="0"; for i in `echo $retourcurl | sed -e 's/:/ /g' | sed -e 's/,/ /g' | sed -e 's/"/ /g' | sed -e 's/}/ /g'`; do
			if [ ${last_i} = "id" ]; then
				last_i="0"
				eval doc_id`echo ${loop}`=$i;loop=`expr $loop + 1`
			else
				last_i=$i
			fi
			if [ $loop -gt 3 ]; then
				break
			fi
		done
	fi
fi

# Classer un doc
if [ $nbr_docs -ge 1 ]; then
	retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X PUT --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/document/${doc_id1}/classify`;sleep 1
	echo "$retourcurl" | grep -E "filename" >/dev/null 2>&1
	if [ $? -eq 1 ]; then
		echo -e "\033[31mERROR: can't classify the doc - $retourcurl.\033[0m"
	else
		echo -e "\033[32mDoc's classification: OK.\033[0m"
	fi
else
	echo "INFO: can't test classification, no doc found in inbox."
fi
	
# Classer de multiples fichiers
if [ $nbr_docs -ge 3 ]; then
	retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X POST --header "Authorization: bearer ${access_token}" --header "Accept:application/json" --header "Content-type: application/json" -d "{ids : ['${doc_id2}','${doc_id3}']}" https://api.rectech.u-post.fr/api/v2.0/documents/multiClassify`;sleep 1
	if [ ! -z "${retourcurl}" ]; then
		echo -e "\033[31mERROR: can't multiclassify docs - $retourcurl.\033[0m"
	else
		retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/documents/inbox?index=0&max_results=1000&sort=CREATION_DATE&direction=DESCENDING`;sleep 1
		echo ${retourcurl} | grep -E "${doc_id2}|${doc_id3}" >/dev/null 2>&1
		if [ $? -eq 0 ]; then
			echo -e "\033[31mERROR: can't multiclassify docs (check).\033[0m"
		else
			echo -e "\033[32mDocs multiclassification: OK.\033[0m"
		fi
	fi
else
	echo "INFO: can't test multiclassification, need 2 docs in inbox."
fi

# Lister les docs du Coffre
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/documents/safe?index=0&max_results=1000&sort=CREATION_DATE&direction=DESCENDING`;sleep 1
echo "$retourcurl" | grep -E "count" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't count the docs in the Safe - $retourcurl.\033[0m"
else
	echo -e "\033[32mSafe docs' count: OK.\033[0m"
fi

# Lister les docs de la Corbeille
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/documents/trash?index=0&max_results=1000&sort=CREATION_DATE&direction=DESCENDING`;sleep 1
echo "$retourcurl" | grep -E "count" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't count the docs in the Trash - $retourcurl.\033[0m"
else
	echo -e "\033[32mTrash docs' count: OK.\033[0m"
fi

# Upload d’un fichier dans le coffre
echo "contenu du document">TEST.pdf; retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -f  --header "Authorization: bearer ${access_token}" --header "Content-Type:multipart/form-data" --form "title=MONDOC" --form "tag=MONTAG" --form "archive=@TEST.pdf;type=application/pdf" -X POST https://api.rectech.u-post.fr/api/v2.0/document`; rm TEST.pdf
doc_id=`echo "$retourcurl"  | cut -d':' -f2 | cut -d',' -f1 | cut -d'"' -f2`
echo "$retourcurl" | grep -E "filename" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't upload a doc in the Safe - $retourcurl.\033[0m"
else
	if [ -z $doc_id ]; then
		echo -e "\033[31mERROR: can't upload a doc in the Safe (no id found) - $retourcurl.\033[0m"
	else
		echo -e "\033[32mFile upload: OK.\033[0m"
	fi
fi

# Rechercher des docs
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Content-type: application/json" -X POST -d "{text : 'TEST.pdf'}" --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/documents/search`;sleep 1
echo "$retourcurl" | grep -E "TEST.pdf" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't search for docs - $retourcurl.\033[0m"
else
	echo -e "\033[32mDocs' search: OK.\033[0m"
fi

# Affichage des metadatas du doc
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/document/${doc_id}/`;sleep 1
echo "$retourcurl" | grep -E "filename" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't display the doc's metadata - $retourcurl.\033[0m"
else
	echo -e "\033[32mDoc metadatas' display: OK.\033[0m"
fi

# Affichage du contenu du doc
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/document/${doc_id}/content`;sleep 1
echo "$retourcurl" | grep -E "contenu du document" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't display the doc's content - $retourcurl.\033[0m"
else
	echo -e "\033[32mDoc content's display: OK.\033[0m"
fi

# Ajout d’un tag au doc
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X PUT --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/document/${doc_id}/tags/MONTAG2`;sleep 1
echo "$retourcurl" | grep -E "filename" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't change the doc's tag - $retourcurl.\033[0m"
else
	echo -e "\033[32mDoc's tag change: OK.\033[0m"
fi

# Lister les tags de l'utilisateur
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/documents/userTags`;sleep 1
echo "$retourcurl" | grep -E "tags" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't list the user's tags - $retourcurl.\033[0m"
else
	echo -e "\033[32mUser's tags list: OK.\033[0m"
fi

# Passage du doc à l’état lu
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X PUT --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/document/${doc_id}/read_status/true`;sleep 1
echo "$retourcurl" | grep -E "filename" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't change the doc's read status to true - $retourcurl.\033[0m"
else
	doc_readstatus=`echo "$retourcurl" | cut -d',' -f12 | cut -d':' -f2`
	if [ ${doc_readstatus} = "true" ]; then
		echo -e "\033[32mDoc's read status changed to true: OK.\033[0m"
	fi
fi

# Passage du doc à l’état non lu
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X PUT --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/document/${doc_id}/read_status/false`;sleep 1
echo "$retourcurl" | grep -E "filename" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't change the doc's read status to false - $retourcurl.\033[0m"
else
	doc_readstatus=`echo "$retourcurl" | cut -d',' -f12 | cut -d':' -f2`
	if [ ${doc_readstatus} = "false" ]; then
		echo -e "\033[32mDoc's read status changed to false: OK.\033[0m"
	fi
fi

# Mise à la corbeille d'un document
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X PUT --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/document/${doc_id}/trash`;sleep 1
echo "$retourcurl" | grep -E "filename" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't move the doc to trash - $retourcurl.\033[0m"
else
	echo -e "\033[32mDoc's trash move: OK.\033[0m"
fi

# Restauration d'un document
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X PUT --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/document/${doc_id}/restore`;sleep 1
echo "$retourcurl" | grep -E "filename" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't restore the doc - $retourcurl.\033[0m"
else
	echo -e "\033[32mDoc's restoration: OK.\033[0m"
fi

# Suppression d'un document mis à la corbeille
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X PUT --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/document/${doc_id}/trash`;sleep 1
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X DELETE --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/document/${doc_id}/`;sleep 1
if [ ! -z "${retourcurl}" ]; then
	echo -e "\033[31mERROR: can't delete doc in trash - $retourcurl.\033[0m"
else
	#Vérification que le fichier a bien été supprimé
	retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/documents/trash?index=0&max_results=1000&sort=CREATION_DATE&direction=DESCENDING`;sleep 1
	echo "$retourcurl" | grep -E "${doc_id}" >/dev/null 2>&1
	if [ $? -eq 0 ]; then
		echo -e "\033[31mERROR: can't delete doc in trash (check).\033[0m"
	else
		echo -e "\033[32mDoc's trash deletion: OK.\033[0m"
	fi
fi

# Upload de quatre fichiers (prérequis)
echo "contenu du document">TEST1.pdf; retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -f  --header "Authorization: bearer ${access_token}" --header "Content-Type:multipart/form-data" --form "title=MONDOC" --form "tag=MONTAG" --form "archive=@TEST1.pdf;type=application/pdf" -X POST https://api.rectech.u-post.fr/api/v2.0/document`; rm TEST1.pdf;sleep 1
doc_id1=`echo "$retourcurl"  | cut -d':' -f2 | cut -d',' -f1 | cut -d'"' -f2`
echo "contenu du document">TEST2.pdf; retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -f  --header "Authorization: bearer ${access_token}" --header "Content-Type:multipart/form-data" --form "title=MONDOC" --form "tag=MONTAG" --form "archive=@TEST2.pdf;type=application/pdf" -X POST https://api.rectech.u-post.fr/api/v2.0/document`; rm TEST2.pdf;sleep 1
doc_id2=`echo "$retourcurl"  | cut -d':' -f2 | cut -d',' -f1 | cut -d'"' -f2`
echo "contenu du document">TEST3.pdf; retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -f  --header "Authorization: bearer ${access_token}" --header "Content-Type:multipart/form-data" --form "title=MONDOC" --form "tag=MONTAG" --form "archive=@TEST3.pdf;type=application/pdf" -X POST https://api.rectech.u-post.fr/api/v2.0/document`; rm TEST3.pdf;sleep 1
doc_id3=`echo "$retourcurl"  | cut -d':' -f2 | cut -d',' -f1 | cut -d'"' -f2`
echo "contenu du document">TEST4.pdf; retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -f  --header "Authorization: bearer ${access_token}" --header "Content-Type:multipart/form-data" --form "title=MONDOC" --form "tag=MONTAG" --form "archive=@TEST4.pdf;type=application/pdf" -X POST https://api.rectech.u-post.fr/api/v2.0/document`; rm TEST4.pdf;sleep 1
doc_id4=`echo "$retourcurl"  | cut -d':' -f2 | cut -d',' -f1 | cut -d'"' -f2`
echo "contenu du document">TEST5.pdf; retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -f  --header "Authorization: bearer ${access_token}" --header "Content-Type:multipart/form-data" --form "title=MONDOC" --form "tag=MONTAG" --form "archive=@TEST5.pdf;type=application/pdf" -X POST https://api.rectech.u-post.fr/api/v2.0/document`; rm TEST5.pdf;sleep 1
doc_id5=`echo "$retourcurl"  | cut -d':' -f2 | cut -d',' -f1 | cut -d'"' -f2`

# Récupération des tags de plusieurs documents XXX
# retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X POST --header "Authorization: bearer ${access_token}" --header "Accept:application/json" --header "Content-type: application/json" -d "{'tags': 'Map'}" https://api.rectech.u-post.fr/api/v2.0/documents/multiTag`;sleep 1
# echo $retourcurl
# echo "$retourcurl" | grep -E "filename" >/dev/null 2>&1
# if [ $? -eq 1 ]; then
	# echo -e "\033[31mERROR: can't change the doc's tag - $retourcurl.\033[0m"
# else
	# echo -e "\033[32mDoc's tag change: OK.\033[0m"
# fi	

# Lister les tags de l'utilisateur spécifiques à plusieurs documents
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X POST --header "Authorization: bearer ${access_token}" --header "Accept:application/json" --header "Content-type: application/json" -d "{ids : ['${doc_id1}','${doc_id2}','${doc_id3}','${doc_id4}']}" https://api.rectech.u-post.fr/api/v2.0/documents/getMultiTag`;sleep 1
echo "$retourcurl" | grep -E "tags" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't list the user tags of multiple documents - $retourcurl.\033[0m"
else
	echo -e "\033[32mUser's multiple documents tags list: OK.\033[0m"
fi

# Passage de multiples docs à l’état lu
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X POST --header "Authorization: bearer ${access_token}" --header "Accept:application/json" --header "Content-type: application/json" -d "{ids : ['${doc_id1}','${doc_id2}','${doc_id3}','${doc_id4}']}" https://api.rectech.u-post.fr/api/v2.0/documents/multiReadStatus/true`;sleep 1
if [ ! -z "${retourcurl}" ]; then
	echo -e "\033[31mERROR: can't change multiple docs' read_status to true - $retourcurl.\033[0m"
else
	multi_readtrue=0;for i in ${doc_id1} ${doc_id2} ${doc_id3} ${doc_id4}; do
		doc_readstatus=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/document/${i}/ | cut -d',' -f12  | cut -d':' -f2`;sleep 1
		if [ ${doc_readstatus} = "false" ]; then
			multi_readtrue=1
		fi
	done
	if [ ${multi_readtrue} -eq 0 ]; then
		echo -e "\033[32mDoc's multiple docs' read status changed to true: OK.\033[0m"
	else
		echo -e "\033[31mERROR: can't change the doc's read status to true (check).\033[0m"
	fi
fi

# Passage de multiples docs à l’état non lu
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X POST --header "Authorization: bearer ${access_token}" --header "Accept:application/json" --header "Content-type: application/json" -d "{ids : ['${doc_id1}','${doc_id2}','${doc_id3}','${doc_id4}']}" https://api.rectech.u-post.fr/api/v2.0/documents/multiReadStatus/false`;sleep 1
if [ ! -z "${retourcurl}" ]; then
	echo -e "\033[31mERROR: can't change multiple docs' read_status to false - $retourcurl.\033[0m"
else
	multi_readfalse=0;for i in ${doc_id1} ${doc_id2} ${doc_id3} ${doc_id4}; do
		doc_readstatus=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/document/${i}/ | cut -d',' -f12  | cut -d':' -f2`;sleep 1
		if [ ${doc_readstatus} = "true" ]; then
			multi_readfalse=1
		fi
	done
	if [ ${multi_readfalse} -eq 0 ]; then
		echo -e "\033[32mDoc's multiple docs' read status changed to false: OK.\033[0m"
	else
		echo -e "\033[31mERROR: can't change the doc's read status to false (check) - $retourcurl.\033[0m"
	fi
fi

# Mise à la corbeille de multiples fichiers
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X POST --header "Authorization: bearer ${access_token}" --header "Accept:application/json" --header "Content-type: application/json" -d "{ids : ['${doc_id1}','${doc_id2}','${doc_id3}','${doc_id4}']}" https://api.rectech.u-post.fr/api/v2.0/documents/multiTrash`;sleep 1
if [ ! -z "$retourcurl" ]; then
	echo -e "\033[31mERROR: can't multitrash docs.\033[0m"
else
	retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/documents/trash?index=0&max_results=1000&sort=CREATION_DATE&direction=DESCENDING`;sleep 1
	echo ${retourcurl} | grep -E "${doc_id1}|${doc_id2}|${doc_id3}|${doc_id4}" >/dev/null 2>&1
	if [ ${?} -ne 0 ]; then
		echo -e "\033[31mERROR: can't multitrash docs (check).\033[0m"
	else
		echo -e "\033[32mDocs multitrashing: OK.\033[0m"
	fi
fi

# Restauration de multiples fichiers
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X POST --header "Authorization: bearer ${access_token}" --header "Accept:application/json" --header "Content-type: application/json" -d "{ids : ['${doc_id3}','${doc_id4}']}" https://api.rectech.u-post.fr/api/v2.0/documents/multiRestore`;sleep 1
if [ ! -z "${retourcurl}" ]; then
	echo -e "\033[31mERROR: can't multirestore docs from trash - $retourcurl.\033[0m"
else
	retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/documents/trash?index=0&max_results=1000&sort=CREATION_DATE&direction=DESCENDING`;sleep 1
	echo ${retourcurl} | grep -E "${doc_id3}|${doc_id4}" >/dev/null 2>&1
	if [ $? -eq 0 ]; then
		echo -e "\033[31mERROR: can't multirestore docs from trash (check).\033[0m"
	else
		echo -e "\033[32mDocs multirestoration from trash: OK.\033[0m"
	fi
fi

#Suppression de deux fichiers de la corbeille
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X POST --header "Authorization: bearer ${access_token}" --header "Accept:application/json" --header "Content-type: application/json" -d "{ids : ['${doc_id1}','${doc_id2}']}" https://api.rectech.u-post.fr/api/v2.0/documents/multiDelete`;sleep 1
echo "$retourcurl" | grep -E "${doc_id1}|${doc_id2}" >/dev/null 2>&1
if [ $? -eq 0 ]; then
	echo -e "\033[31mERROR: can't multidelete docs in trash.\033[0m"
else
	echo -e "\033[32mDocs in trash multideletion: OK.\033[0m"
fi

#Vidage de la corbeille
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X DELETE --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/documents/trash`;sleep 1
if [ ! -z "$retourcurl" ]; then
	echo -e "\033[31mERROR: can't delete all docs in trash.\033[0m"
else
	retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/documents/trash?index=0&max_results=1000&sort=CREATION_DATE&direction=DESCENDING`;sleep 1
	count_zero=`echo $retourcurl | cut -d',' -f2 | cut -d':' -f2`
	if [ ${count_zero} -ne 0 ]; then
		echo -e "\033[31mERROR: can't delete all docs in trash (check).\033[0m"
	else
		echo -e "\033[32mAll docs in trash deletion: OK.\033[0m"
	fi
fi

# Récupération des infos de l’utilisateur
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/profile`;sleep 1
echo "- $retourcurl -"
echo "$retourcurl" | grep -E "first_name" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't display user status - $retourcurl.\033[0m"
else
	echo -e "\033[32mUser status' display: OK.\033[0m"
fi

address_id="05fc22e021a4452e82ce86b3a4fc4691"

# Génération d’un email Digiposte
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X POST --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/profile/emailtodigiposte/generate`;sleep 1
echo "$retourcurl" | grep -E "mailtodigiposte" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't generate a mail - $retourcurl.\033[0m"
else
	echo -e "\033[32mMail generation: OK.\033[0m"
fi

# Récupération de l'email Digiposte de l'utilisateur
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/profile/emailtodigiposte`;sleep 1
echo $retourcurl
echo "$retourcurl" | grep -E "mailtodigiposte" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't return the emailtodigiposte of the logged user - $retourcurl.\033[0m"
else
	echo -e "\033[32mLogged user's emailtodigiposte: OK.\033[0m"
fi

# Récupère les données de l'adresse postale Digiposte (on ne l'obtient sûrement qu'après création manuelle d'un compte) XXX
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X GET --header "Accept:application/json" --header "Content-type: application/json" --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/profile/address/${address_id}`;sleep 1
echo $retourcurl

# Supprime l'email Digiposte du profil de l'utilisateur XXX
# retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X DELETE --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/profile/email/${mail_id}`;sleep 1
# echo $retourcurl

# Révocation de l'email Digiposte de l'utilisateur
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X DELETE --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/profile/emailtodigiposte`;sleep 1
if [ ! -z "$retourcurl" ]; then
	echo -e "\033[31mERROR: can't revoke the given emailtodigiposte address from the user profile.\033[0m"
else
	retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/profile/emailtodigiposte`;sleep 1
	echo "$retourcurl" | grep -E "mailtodigiposte" >/dev/null 2>&1
	if [ ${?} -eq 0 ]; then
		echo -e "\033[31mERROR: can't revoke the given emailtodigiposte address from the user profile (check).\033[0m"
	else
		echo -e "\033[32mLogged user's emailtodigiposte revocation: OK.\033[0m"
	fi
fi

# All memberships
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/memberships/`;sleep 1
echo "$retourcurl" | grep -E "memberships" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't list all the memberships - $retourcurl.\033[0m"
else
	echo -e "\033[32mAll memberships' list: OK.\033[0m"
fi

# Lister les émetteurs
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X GET --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/senders/?index=0&max_results=1000`;sleep 1
echo "$retourcurl" | grep -E "senders" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't list the senders - $retourcurl.\033[0m"
else
	echo -e "\033[32mSenders' list: OK.\033[0m"
fi

# Lister tous les émetteurs disponibles
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/user/senders/available`;sleep 1
echo "$retourcurl" | grep -E "senders" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't list the available senders - $retourcurl.\033[0m"
else
	echo -e "\033[32mAvailable senders' list: OK.\033[0m"
fi

# Récupération de l'ip de l'émetteur
sender_id=`echo $retourcurl | cut -d':' -f3 | cut -d'"' -f2`

# Afficher le fichier d'acceptation de l'émetteur
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/sender/pid/${sender_id}/agreement/content`;sleep 1
if [ -z "${retourcurl}" ]; then
	echo -e "\033[31mERROR: can't get a sender's agreement file.\033[0m"
else
	echo -e "\033[32mSender's agreement file display: OK.\033[0m"
fi

# Afficher l'acceptation de l'émetteur XXX
# retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/sender/pid/${sender_id}/agreement`;sleep 1
# echo $sender_id
# echo $retourcurl
# if [ -z "${retourcurl}" ]; then
	# echo -e "\033[31mERROR: can't get a sender's agreement.\033[0m"
# else
	# echo -e "\033[32mSender's agreement display: OK.\033[0m"
# fi

# Adhérer à un émetteur XXX
# retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X POST --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/membership`;sleep 1
# echo $retourcurl
# echo "$retourcurl" | grep -E "memberships" >/dev/null 2>&1
# if [ $? -eq 1 ]; then
	# echo -e "\033[31mERROR: can't list the sender's memberships - $retourcurl.\033[0m"
# else
	# echo -e "\033[32mSender Memberships' list: OK.\033[0m"
# fi

# Récupérer une adhésion XXX
# retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X GET --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/membership/${membership_id}`;sleep 1
# echo $retourcurl
# echo "$retourcurl" | grep -E "memberships" >/dev/null 2>&1
# if [ $? -eq 1 ]; then
	# echo -e "\033[31mERROR: can't list the sender's memberships - $retourcurl.\033[0m"
# else
	# echo -e "\033[32mSender Memberships' list: OK.\033[0m"
# fi

# Lister les adhésions de l'émetteur
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/sender/pid/${sender_id}/memberships`;sleep 1
echo "$retourcurl" | grep -E "memberships" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't list the sender's memberships - $retourcurl.\033[0m"
else
	echo -e "\033[32mSender Memberships' list: OK.\033[0m"
fi

# Rechercher un émetteur
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/sender/pid/${sender_id}/search`;sleep 1
echo "$retourcurl" | grep -E "SENDER" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't list the available senders - $retourcurl.\033[0m"
else
	echo -e "\033[32mAvailable senders' list: OK.\033[0m"
fi

# Créer un espace de partage
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X POST --header "Accept:application/json" --header "Content-type: application/json" --header "Authorization: bearer ${access_token}" -d "{recipient_mails : ['mlemarchand@sopragroup.com'], end_date: '2015-06-10', title: 'Titre', description: 'Descript°', start_date: '2014-07-10'}" https://api.rectech.u-post.fr/api/v2.0/share`;sleep 1
echo "$retourcurl" | grep -E "{\"id\":\"" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't create a new share for the current user - $retourcurl.\033[0m"
else
	echo -e "\033[32mCurrent user's share creation: OK.\033[0m"
fi

share_id=`echo $retourcurl | cut -d':' -f2| cut -d'"' -f2`

# Modifier un espace de partage
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X PUT --header "Accept:application/json" --header "Content-type: application/json" --header "Authorization: bearer ${access_token}" -d "{recipient_mails : ['mlemarchand@sopragroup.com'], end_date: '2015-06-10', title: 'Updated_Title', description: 'Description', start_date: '2014-07-10', id: '${share_id}'}" https://api.rectech.u-post.fr/api/v2.0/share/${share_id}`;sleep 1
echo "$retourcurl" | grep -E "{\"id\":\"" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't update a share for the current user - $retourcurl.\033[0m"
else
	echo -e "\033[32mCurrent user's share update: OK.\033[0m"
fi

# Afficher les espaces de partage
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/shares`;sleep 1
echo "$retourcurl" | grep -E "share_datas" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't list the user's shared data - $retourcurl.\033[0m"
else
	echo -e "\033[32mUser shared data's list: OK.\033[0m"
fi

# Afficher les documents partagés
retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/shares/with_documents`;sleep 1
echo "$retourcurl" | grep -E "share_data" >/dev/null 2>&1
if [ $? -eq 1 ]; then
	echo -e "\033[31mERROR: can't list the user's shared documents - $retourcurl.\033[0m"
else
	echo -e "\033[32mUser shared documents' list: OK.\033[0m"
fi

if [ ! -z "${share_id}" ]; then
	
	# Ajouter des documents dans un espace de partage spécifique
	retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X PUT --header "Accept:application/json" --header "Content-type: application/json" --header "Authorization: bearer ${access_token}" -d "{ids : ['${doc_id3}','${doc_id4}','${doc_id5}']}" https://api.rectech.u-post.fr/api/v2.0/share/${share_id}/documents`;sleep 1
	if [ ! -z "${retourcurl}" ]; then
		echo -e "\033[31mERROR: can't update the documents of a share - $retourcurl.\033[0m"
	else
		retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/share/${share_id}/documents`;sleep 1
		echo "$retourcurl" | grep -E "${doc_id5}" >/dev/null 2>&1
		if [ $? -eq 1 ]; then
			echo -e "\033[31mERROR: can't update the documents of a share (check) - $retourcurl.\033[0m"
		else
			echo -e "\033[32mUser shared documents' update: OK.\033[0m"
		fi
	fi
	
	# Afficher un espace de partage spécifique
	retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/share/${share_id}`;sleep 1
	echo "$retourcurl" | grep -E "title" >/dev/null 2>&1
	if [ $? -eq 1 ]; then
		echo -e "\033[31mERROR: can't list user's specific shared datas - $retourcurl.\033[0m"
	else
		echo -e "\033[32mUser specific shared datas: OK.\033[0m"
	fi

	# Afficher les documents d'un espace de partage spécifique
	retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/share/${share_id}/documents`;sleep 1
	echo "$retourcurl" | grep -E "title" >/dev/null 2>&1
	if [ $? -eq 1 ]; then
		echo -e "\033[31mERROR: can't list user's specific shared documents - $retourcurl.\033[0m"
	else
		echo -e "\033[32mUser specific shared documents' list: OK.\033[0m"
	fi

	# Supprimer un espace de partage
	retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k -X DELETE --header "Accept:application/json" --header "Content-type: application/json" --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/share/${share_id}`;sleep 1
	if [ ! -z "${retourcurl}" ]; then
		echo -e "\033[31mERROR: can't delete a share for the current user - $retourcurl.\033[0m"
	else
		retourcurl=`curl --proxy ptx.proxy.corp.sopra:8080 -s -k --header "Authorization: bearer ${access_token}" https://api.rectech.u-post.fr/api/v2.0/share/${share_id}`;sleep 1
		echo "$retourcurl" | grep -E "does not exist" >/dev/null 2>&1
		if [ $? -eq 1 ]; then
			echo -e "\033[31mERROR: can't delete a share for the current user (check) - $retourcurl.\033[0m"
		else
			echo -e "\033[32mCurrent user's share delete: OK.\033[0m"
		fi
	fi
	
else
	echo "INFO: can't test specific shared datas and documents, the user doesn't share any."
fi