#!/bin/bash

# Author Ayoub EL MAAROUF (WWSight)
# @Desciption : The script if the memory is allocated and mounted for drbd

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "drbd-status"

OUT=$(sudo drbd-overview)
isConnected=$(echo "$OUT" | awk '{print $2}')
state=$(echo "$OUT" | awk '{print $3}' | cut -d '/' -f 1)
upTodate=$(echo "$OUT" | awk '{print $4}' | cut -d '/' -f 1)
isMounted=$(echo "$OUT" | awk '{print $7}')

if ! check_test_critical "echo $isConnected" "Connected"
then
    display_error "drbd not connected"
fi
if ! check_test_critical "echo $upTodate" "UpToDate"
then
    display_error "drbd is not up to date"
fi
if [[ $state == "Primary" ]]
then
    if ! check_test_critical "echo $isMounted" "/data"
    then
        display_error "drbd device not mounted"
    fi
else
    if ! check_test_critical "echo $isMounted" ""
    then
        display_error "drbd device mounted on the secondary server"
    fi
fi

check_exit "drbd status OK : $OUT" ""
