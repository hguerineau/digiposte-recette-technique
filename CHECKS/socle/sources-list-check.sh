#!/bin/bash

# Author Hugo GUERINEAU

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "sources-list"

if ! check_test_critical "cat /etc/apt/sources.list" "^deb http://172.14.1.25/v9/ precise main restricted universe multiverse"
then
	display_error "Repository PRECISE is not configured in /etc/apt/sources.list"
fi

if ! check_test_critical "cat /etc/apt/sources.list" "^deb http://172.14.1.25/v9/ precise-security main restricted universe multiverse"
then
	display_error "repository PRECISE-SECURITY is not configured in /etc/apt/sources.list"
fi

if ! check_test_critical "cat /etc/apt/sources.list" "^deb http://172.14.1.25/v9/ precise-updates main restricted universe multiverse"
then
	display_error "repository PRECISE-UPDATE is not configured in /etc/apt/sources.list"
fi

check_exit "Repositories are properly configured" ""
