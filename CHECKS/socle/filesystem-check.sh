#!/bin/bash

# Author        Hugo GUERINEAU
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct
#modif 2
check_init "filesystem"

for fs_config in "$@"
do
    fs_mount_point="$(echo "$fs_config" | cut -d : -f 1)"
    fs_size_needed="$(echo "$fs_config" | cut -d : -f 2)"
    
    disk_name="$(echo "$fs_config" | cut -d : -f 3)"
    isSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

    if [[ -z "$disk_name" ]] || ! $isSecondary
    then
        fs_size_needed="${fs_size_needed}G"
        fs_size=$(df -h | grep -E "$fs_mount_point$" | awk '{print $2}')
    else
        fs_size_needed="${fs_size_needed},00GiB"
        fs_size=$(sudo lvdisplay "$disk_name" | grep "LV Size" | awk '{print $3 $4}')
    fi

    fs_size_accepted="${fs_size_needed:0:1}$(echo ${fs_size_needed:1} | sed 's/[0-9]/./g')"
    
    # Check the size of the filesystem
    if check_test_warning "echo '$fs_size'" "^${fs_size_needed}$"
    then
        display_success "The size of the filesystem $fs_mount_point ($fs_size) matches the documentation (${fs_size_needed})"
    elif  check_test_warning "echo $fs_size" "^$fs_size_accepted$"
    then
        display_success "The size of the filesystem $fs_mount_point ($fs_size) approximatly matches the documentation (${fs_size_needed})"
	else
        display_error "The size of the filesystem $fs_mount_point ($fs_size) does not match the documentation (${fs_size_needed})"
    fi

    if [[ -z "$disk_name" ]]
    then
        # Check if the filesystem exits in FSTAB
        if check_test_critical "cat /etc/fstab" "\s$fs_mount_point\s"
        then
            display_success "Filesystem $fs_mount_point is properly defined in /etc/fstab"
        else
            display_error "Filesystem $fs_mount_point is not defined in /etc/fstab"
        fi
    fi
done

check_exit
