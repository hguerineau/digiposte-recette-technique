#!/bin/bash

# Author        ???
# Contributor   Pierre CAZAJOUS

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "gem-list"

if ! check_test_critical "gem list" "capistrano"
then
	display_error "capistrano absent from gem list"
fi

if ! check_test_critical "gem list" "deep_merge"
then
	display_error "deep_merge absent from gem list"
fi

if ! check_test_critical "gem list" "hiera"
then
	display_error "hiera absent from gem list"
fi

if ! check_test_critical "gem list" "hiera-puppet"
then
	display_error "hiera-puppet absent from gem list"
fi

if ! check_test_critical "gem list" "highline"
then
	display_error "highline absent from gem list"
fi

if ! check_test_critical "gem list" "json"
then
	display_error "json absent from gem list"
fi

if ! check_test_critical "gem list" "net-scp"
then
	display_error "net-scp absent from gem list"
fi

if ! check_test_critical "gem list" "net-sftp"
then
	display_error "net-sftp absent from gem list"
fi

if ! check_test_critical "gem list" "net-ssh"
then
	display_error "net-ssh absent from gem list"
fi

if ! check_test_critical "gem list" "net-ssh-gateway"
then
	display_error "net-ssh-gateway absent from gem list"
fi

check_exit "capistrano, deep_merge, hiera, hiera-puppet, highline, json, net-scp, net-sftp, net-ssh and net-ssh-gateway must be in gem list"
