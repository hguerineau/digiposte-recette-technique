#!/bin/bash

# Author Hugo GUERINEAU

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "swap"

SWAP_FS_SIZE_NEEDED=$1
SWAP_FS_SIZE_ACCEPTED="${SWAP_FS_SIZE_NEEDED:0:2}$(echo ${SWAP_FS_SIZE_NEEDED:2} | sed 's/[0-9]/./g')"
SWAP_FS_SIZE=`swapon -s | grep swap | awk '{print $3}'`

if check_test_warning "echo $SWAP_FS_SIZE" "^$SWAP_FS_SIZE_NEEDED$"
then
	display_success "Needed swap: $SWAP_FS_SIZE_NEEDED = Configured swap: $SWAP_FS_SIZE Ok"
elif  check_test_warning "echo $SWAP_FS_SIZE" "^$SWAP_FS_SIZE_ACCEPTED$"
then
	display_success "Needed swap: $SWAP_FS_SIZE_NEEDED approximatly = Configured swap: $SWAP_FS_SIZE Ok"
else
	display_error "Needed swap: $SWAP_FS_SIZE_NEEDED Ko - Configured swap: $SWAP_FS_SIZE Ko"
fi
