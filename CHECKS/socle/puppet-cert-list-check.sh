#!/bin/bash

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "puppet-cert-list"

for serv in $1; do
	if ! check_test_critical "sudo puppet cert list --all" "$serv"
	then
		display_error "$serv is absent from puppet cert list"
	fi
done

check_exit "Missing servers in puppet cert list"
