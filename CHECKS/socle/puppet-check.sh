#! /bin/bash

# Author Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "puppet"

if ! check_test_critical "cat /etc/puppet/puppet.conf" "^logdir=/var/log/puppet$"
then
    display_error "logdir value does not match the documentation"
fi

if ! check_test_critical "cat /etc/puppet/puppet.conf" "^vardir=/var/lib/puppet$"
then
    display_error "vardir value does not match the documentation"
fi

if ! check_test_critical "cat /etc/puppet/puppet.conf" "^ssldir=/var/lib/puppet/ssl$"
then
    display_error "ssldir value does not match the documentation"
fi

if ! check_test_critical "cat /etc/puppet/puppet.conf" "^rundir=/var/run/puppet$"
then
    display_error "rundir value does not match the documentation"
fi

if ! check_test_critical "cat /etc/puppet/puppet.conf" "^factpath=/var/lib/puppet/facter$"
then
    display_error "factpath value does not match the documentation"
fi

if ! check_test_critical "cat /etc/puppet/puppet.conf" "^templatedir=/etc/puppet/templates$"
then
    display_error "templatedir value does not match the documentation"
fi

if ! check_test_critical "cat /etc/puppet/puppet.conf" "^prerun_command=/etc/puppet/etckeeper-commit-pre$"
then
    display_error "prerun_command value does not match the documentation"
fi

if ! check_test_critical "cat /etc/puppet/puppet.conf" "^postrun_command=/etc/puppet/etckeeper-commit-post$"
then
    display_error "postrun_command value does not match the documentation"
fi

if ! check_test_critical "cat /etc/puppet/puppet.conf" "^server=deploy.digiposte.local$"
then
    display_error "server value does not match the documentation"
fi

if ! check_test_critical "cat /etc/puppet/puppet.conf" "^report=true$"
then
    display_error "report value does not match the documentation"
fi

if ! check_test_critical "cat /etc/puppet/puppet.conf" "^pluginsync=true$"
then
    display_error "pluginsync value does not match the documentation"
fi

if ! check_test_critical "cat /etc/puppet/puppet.conf" "!\[master\]"
then
    display_error "The configuration should not contain a section [master]"
fi

check_exit "Puppet is configured properly" ""
