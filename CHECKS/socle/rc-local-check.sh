#!/bin/bash

# Author        Hugo GUERINEAU
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "rc-local"

check_test_critical "cat /etc/rc.local" "!^[^#].*add"

check_exit "Routes and IP are configured outside rc.local" "IP and/or routes are configured in /etc/rc.local"
