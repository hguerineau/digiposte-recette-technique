#!/bin/bash

# Author Hugo GUERINEAU

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "cpu"

CPU_NEEDED=$1
SERVER_CPU_NB=$(( $(cat /proc/cpuinfo | grep processor | tail -n 1 |cut -d ':' -f 2 |awk 'sub(/^[ \t]+/, "");') + 1 ))

check_test_critical "echo $SERVER_CPU_NB" "$CPU_NEEDED"

check_exit "Needed CPU: $CPU_NEEDED, Found CPU: $SERVER_CPU_NB"
