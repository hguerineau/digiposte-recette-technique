#!/bin/bash

# Author Hugo GUERINEAU

source ../../LIBS/display.fct
source ../../LIBS/check.fct

REFERENCE_IP_LIST=$1
SERVER_IP_LIST=$(ip addr | grep inet | grep -v inet6 | grep -v 127.0.0 | awk '{print $2}' | cut -d '/' -f 1 | tr '\n' ' ')

# Get the files in which the interfaces are defined
FILES_TO_BE_CHECKED="/etc/network/interfaces"
if [[ -f '/etc/keepalived/keepalived.conf' ]]
then
	FILES_TO_BE_CHECKED="$FILES_TO_BE_CHECKED /etc/keepalived/keepalived.conf"
fi

check_init "ip"

# Check the IP addresses defined in the document are properly configured
for reference_ip in $REFERENCE_IP_LIST
do
    if ! check_test_critical "grep \"$reference_ip\" $FILES_TO_BE_CHECKED" "$reference_ip"
    then
        display_error "IP $reference_ip not configured in $FILES_TO_BE_CHECKED"
    fi

    if ! check_test_critical "echo $SERVER_IP_LIST" "$reference_ip"
    then
        display_error "IP $reference_ip is not defined"
    fi
done

check_exit "IP addresses are properly configured" ""
