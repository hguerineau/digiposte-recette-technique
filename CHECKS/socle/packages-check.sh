#!/bin/bash

# Author Samuel LY

source ../../LIBS/display.fct
source ../../LIBS/check.fct

PACKAGES_LIST=$(echo $1 | tr '_' ' ');

check_init "packages"

for package in $PACKAGES_LIST
do
    if check_test_critical "dpkg -l" "^ii\s+$package"
	then
		display_success "$package installed"
	else 
		display_error "$package NOT installed"
	fi
done

check_exit
