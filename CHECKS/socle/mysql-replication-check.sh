#!/bin/bash

# Author Ayoub EL MAAROUF (WWSight)
# @Description : The script checks the status of the replication of db1 in db2 and the seconds behind db1
  
source ../../LIBS/display.fct
source ../../LIBS/check.fct

PSWD=$1

Slave_IO_Running=$(eval "mysql -uroot -p${PSWD} -e 'SHOW SLAVE STATUS \G' | grep 'Slave_IO_Running' | awk '{ print \$2 }'")
Slave_SQL_Running=$(eval "mysql -uroot -p${PSWD} -e 'SHOW SLAVE STATUS \G' | grep 'Slave_SQL_Running' | awk '{ print \$2 }'")
Last_Error=$(eval "mysql -uroot -p${PSWD} -e 'SHOW SLAVE STATUS \G' | grep 'Last_Errno' | awk '{ print \$2 }'")
Seconds_Behind_Master=$(eval "mysql -uroot -p${PSWD} -e 'SHOW SLAVE STATUS \G' | grep 'Seconds_Behind_Master' | awk '{ print \$2 }'")

check_init "mysql-replication"

if ! check_test_critical "echo $Slave_IO_Running" "Yes"
then
    display_error "Slave IO not running"
fi
if ! check_test_critical "echo $Slave_SQL_Running" "Yes"
then
    display_error "Slave SQL not running"
fi
if ! check_test_critical "echo $Last_Error" "^0$"
then
	out=$(eval $Last_Error | cut -d':' -f2)
    display_error "Last error encountred $out"
fi
if ! check_test_critical "echo $Seconds_Behind_Master" "!NULL"
then
	out=$(eval $Seconds_Behind_Master | cut -d':' -f2)
    display_error "Seconds behind Master : $out"
fi

check_exit "MySQL replication works properly" ""
