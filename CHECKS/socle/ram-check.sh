#!/bin/bash

# Author Hugo GUERINEAU

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "ram"

RAMSIZE_NEEDED=$1
RAMSIZE_ACCEPTED="${RAMSIZE_NEEDED:0:1}$(echo ${RAMSIZE_NEEDED:1} | sed 's/[0-9]/./g')"
SERVER_RAMSIZE=`cat /proc/meminfo | grep MemTotal |cut -d':' -f2 | awk 'sub(/^[ \t]+/, "") ;1' |cut -d ' ' -f 1 |tail -n 1`

if check_test_warning "echo $SERVER_RAMSIZE" "^$RAMSIZE_NEEDED$"
then
        display_success "Needed ram: $RAMSIZE_NEEDED = Configured ram: $SERVER_RAMSIZE Ok"
elif  check_test_warning "echo $SERVER_RAMSIZE" "^$RAMSIZE_ACCEPTED$"
then
        display_success "Needed ram: $RAMSIZE_NEEDED approximatly = Configured ram: $SERVER_RAMSIZE Ok"
else
        display_error "Needed ram: $RAMSIZE_NEEDED Ko - Configured ram: $SERVER_RAMSIZE Ko"
fi
