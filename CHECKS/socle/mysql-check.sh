#!/bin/bash

# Author Hugo GUERINEAU

source ../../LIBS/display.fct
source ../../LIBS/check.fct

SERVER_STATUS_OK="OK MySQL is running"
SERVER_STATUS_KO="KO MySQL is down"

check_init "mysql"

check_test_critical "service mysql status" "mysql start/running"

check_exit "MySQL is running" "MySQL is down"
