#!/bin/bash

# Author Hugo GUERINEAU

source ../../LIBS/display.fct
source ../../LIBS/check.fct

DEFAULT_ACC_SRC_RTE=`cat /etc/sysctl.conf |grep "^net.ipv4.conf.default.accept_source_route"`
LO_ACC_SRC_RTE=`cat /etc/sysctl.conf |grep "^net.ipv4.conf.lo.accept_source_route"`
ETH0_ACC_SRC_RTE=`cat /etc/sysctl.conf |grep "^net.ipv4.conf.eth0.accept_source_route"`
IP_NONLOCAL_BIND=`cat /etc/sysctl.conf |grep "^net.ipv4.ip_nonlocal_bind"`
ICMP_ECHO_IGNORE_BROADCASTS=`cat /etc/sysctl.conf |grep "^net.ipv4.icmp_echo_ignore_broadcasts"`
CONF_ALL_LOG_MARTIANS=`cat /etc/sysctl.conf |grep "^net.ipv4.conf.all.log_martians"`
TCP_TIMESTAMPS=`cat /etc/sysctl.conf |grep "^net.ipv4.tcp_timestamps"`

check_init "sysctl"

if ! check_test_critical "echo $DEFAULT_ACC_SRC_RTE | cut -d '=' -f 2" "0"
then
	display_error "In /etc/sysctl.conf $DEFAULT_ACC_SRC_RTE should be 0"
fi

if ! check_test_critical "echo $LO_ACC_SRC_RTE | cut -d '=' -f 2" "0"
then
	display_error "In /etc/sysctl.conf $LO_ACC_SRC_RTE should be 0"
fi

if ! check_test_critical "echo $ETH0_ACC_SRC_RTE | cut -d '=' -f 2" "0"
then
	display_error "In /etc/sysctl.conf $ETH0_ACC_SRC_RTE should be 0"
fi

if ! check_test_critical "echo $IP_NONLOCAL_BIND | cut -d '=' -f 2" "1"
then
    display_error "In /etc/sysctl.conf $IP_NONLOCAL_BIND should be 1"
fi

if ! check_test_critical "echo $ICMP_ECHO_IGNORE_BROADCASTS | cut -d '=' -f 2" "1"
then
	display_error "In /etc/sysctl.conf $ICMP_ECHO_IGNORE_BROADCASTS should be 1"
fi

if ! check_test_critical "echo $CONF_ALL_LOG_MARTIANS | cut -d '=' -f 2" "1"
then
	display_error "In /etc/sysctl.conf $CONF_ALL_LOG_MARTIANS should be 1"
fi

if ! check_test_critical "echo $TCP_TIMESTAMPS | cut -d '=' -f 2" "0"
then
    display_error "In /etc/sysctl.conf $TCP_TIMESTAMPS should be 0"
fi

check_exit "sysctl is properly configured" ""
