#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "php-fpm"

if ! check_test_critical "sudo service php5-fpm status" "php5-fpm is running"
then
    check_exit "PHP-FPM is not running"
fi

if ! check_test_critical "sudo service php5-fpm stop" "" ||
    ! check_test_critical "sudo service php5-fpm status" "php5-fpm is not running"
then
    display_error "Cannot stop PHP-FPM"
fi

if ! check_test_critical "sudo service php5-fpm start" "" ||
    ! check_test_critical "sudo service php5-fpm status" "php5-fpm is running"
then
    display_error "Cannot start PHP-FPM"
fi

if ! check_test_critical "sudo service php5-fpm restart | tr -d '\n'" "Restarting PHP5 FastCGI.*done"
then
    display_error "Cannot restart PHP-FPM"
fi

if ! check_test_critical "sudo service php5-fpm reload | tr -d '\n'" "Reloading PHP5 FastCGI.*done"
then
    display_error "Cannot reload PHP-FPM"
fi

sleep 1 # Wait for the child process to be launched
if ! check_test_critical "sudo ps aux" "php-fpm: pool www"
then
    display_error "Cannot find the pid related to PHP-FPM"
fi

check_exit "PHP-FPM is working properly" ""
