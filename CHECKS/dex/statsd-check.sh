#! /bin/bash

# Author Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "statsd"

if ! check_test_critical "sudo service statsd status" "statsd is running"
then
    check_exit "statsd is not running"
fi

if ! check_test_critical "sudo service statsd stop | tr -d '\n'" "Stopping StatsD - Simple daemon for easy stats aggregation statsd.*statsd stopped.*done"
then
    display_error "Cannot stop statsd"
fi

if ! check_test_critical "sudo service statsd start | tr -d '\n'" "Starting StatsD - Simple daemon for easy stats aggregation statsd.*done"
then
    display_error "Cannot start statsd"
fi

if ! check_test_critical "sudo service statsd restart | tr -d '\n'" "Restarting StatsD - Simple daemon for easy stats aggregation statsd.*statsd stopped.*done"
then
    display_error "Cannot restart statsd"
fi

if ! check_test_critical "ps -p \$(cat /var/run/statsd.pid) -o command" ".+"
then
    display_error "Cannot find the pid related to statsd"
fi

check_exit "statsd works properly" ""
