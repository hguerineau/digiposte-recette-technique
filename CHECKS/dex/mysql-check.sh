#! /bin/bash

# Author Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

DATETIME=$(date +%Y%m%d-%H%M%S)

check_init "mysql"

isSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isSecondary
then
    if ! check_test_critical "sudo service mysql status" "mysql start/running"
    then
        check_exit "MySQL is not running"
    fi

    if ! check_test_critical "sudo service mysql stop" "mysql stop/waiting"
    then
        display_error "Cannot stop MySQL"
    fi

    if ! check_test_critical "sudo service mysql start" "mysql start/running"
    then
        display_error "Cannot start MySQL"
    fi

    if ! check_test_critical "sudo service mysql restart | tr -d '\n'" "mysql stop/waiting.*mysql start/running"
    then
        display_error "Cannot restart MySQL"
    fi
else
    # This test is not described in the DEX
    if ! check_test_critical "sudo service mysql status" "mysql stop/pause|mysql stop/waiting"
    then
        display_error "MySQL is running on the secondary server"
    fi
fi

check_exit "MySQL works properly" ""
