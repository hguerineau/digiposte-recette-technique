#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "jmxtrans"

isSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isSecondary
then
	if ! check_test_critical "sudo ps aux | grep -c jmxtrans" "2"
    then
        display_error "Some issues with the monitoring"
    fi

    if ! check_test_critical "sudo service jmxtrans status" "jmxtrans appears to be running"
    then
        check_exit "JMX Transformer is not running"
    fi

    if ! check_test_critical "sudo service jmxtrans stop | tr -d '\n'" "Stopping jmxtrans.*done"
    then
        display_error "Cannot stop JMX Transformer"
    fi

    if ! check_test_critical "sudo service jmxtrans start | tr -d '\n'" "Starting jmxtrans.*done"
    then
        display_error "Cannot start JMX Transformer"
    fi

    if ! check_test_critical "sudo service jmxtrans restart | tr -d '\n'" "Restarting jmxtrans.*done"
    then
        display_error "Cannot restart JMX Transformer"
    fi

    if ! check_test_critical "sudo ps aux" "jmxtrans"
    then
        display_error "Cannot find the pid related to JMX Transformer"
    fi
else
    # This test is not described in the DEX
    if ! check_test_critical "sudo service jmxtrans status" "jmxtrans appears to be stopped|jmxtrans is not running"
    then
        display_error "JMX Transformer is running on the secondary server"
    fi
fi

check_exit "JMX Transformer is working properly" ""
