#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "hoardd"

isSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isSecondary
then
    if ! check_test_critical "sudo service hoardd status" "hoardd.*is running"
    then
        check_exit "HoardD is not running"
    fi

    if ! check_test_critical "sudo service hoardd stop | tr -d '\n'" "Hoardd - Collect server statistics and send to graphite stopped hoardd.*done"
    then
        display_error "Cannot stop HoardD"
    fi

    if ! check_test_critical "sudo service hoardd start | tr -d '\n'" "started Hoardd - Collect server statistics and send to graphite hoardd.*done"
    then
        display_error "Cannot start HoardD"
    fi

    if ! check_test_critical "sudo service hoardd restart | tr -d '\n'" "started Hoardd - Collect server statistics and send to graphite hoardd.*done"
    then
        display_error "Cannot restart HoardD"
    fi

    if ! check_test_critical "sudo ps -p \$(cat /data/var/run/hoardd.pid 2> /dev/null) -o command" "\/usr\/bin\/node \/data\/usr\/share\/hoardd\/default\/hoardd.js --no-color -c \/data\/etc\/hoardd\/config.json"
    then
        display_error "Cannot find the pid related to HoardD"
    fi
else
    if ! check_test_critical "sudo service hoardd status" "no instance launched by.* hoardd"
    then
        display_error "HoardD is running on the secondary server"
    fi
fi

check_exit "HoardD is working properly" ""
