#! /bin/bash

# Author    Pierre CAZAJOUS

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "tomcat-web-wsemetteurs"

if ! check_test_critical "service tomcat-digiposte-java-ws-sender status" "tomcat-digiposte-java-ws-sender .* is running"
then
    check_exit "Tomcat for the Digiposte WS_Emetteurs is not running"
fi

if ! check_test_critical "sudo service tomcat-digiposte-java-ws-sender stop" "Stopped tomcat-digiposte-java-ws-sender"
then
    display_error "Cannot stop tomcat for the Digiposte WS_Emetteurs"
fi

if ! check_test_critical "sudo service tomcat-digiposte-java-ws-sender start" "Started tomcat-digiposte-java-ws-sender"
then
    display_error "Cannot start tomcat for the Digiposte WS_Emetteurs"
fi

if ! check_test_critical "sudo service tomcat-digiposte-java-ws-sender restart" "Restarted tomcat-digiposte-java-ws-sender"
then
    display_error "Cannot restart tomcat for the Digiposte WS_Emetteurs"
fi

if ! check_test_critical "ps aux" "digiposte-java-ws-sender"
then
    display_error "Cannot find the pid related to tomcat for the Digiposte WS_Emetteurs"
fi

check_exit "Tomcat for the Digiposte WS_Emetteurs works properly" ""
