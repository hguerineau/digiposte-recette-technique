#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON, Pierre CAZAJOUS

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "mongodb"

isSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isSecondary
then
    if ! check_test_critical "sudo service mongodb status" "mongodb start\/running"
    then
        check_exit "MongoDB is not running"
    fi

    if ! check_test_critical "sudo service mongodb stop" "mongodb stop\/waiting"
    then
        display_error "Cannot stop MongoDB"
    fi

    if ! check_test_critical "sudo service mongodb start" "mongodb start\/running"
    then
        display_error "Cannot start MongoDB"
    fi

	sleep 20
	
#    if ! check_test_critical "curl -isl http://log-web.digiposte.local:28017/serverStatus" "uptime"
#    then
#        display_error "Cannot access http://log-web.digiposte.local:28017/serverStatus"
#    fi

	# This test is similar to the commented test above but it tests if the uptime value is
	# greater than 0, as described in the DEX
	if [[ $(curl -isl http://log-web.digiposte.local:28017/serverStatus | grep "uptime" | sed -e 's/.*\"uptime\" : //' -e 's/,.*//') -gt 0 ]]
	then
		result_msg="Uptime value is greater than 0"
	else
		result_msg="Uptime value is lesser than 0"
	fi
	if ! check_test_critical "echo $result_msg" "Uptime value is greater than 0"
	then
		display_error "Cannot access http://log-web.digiposte.local:28017/serverStatus OR uptime value lesser than 0"
	fi
else
    # This test is not described in the DEX
    if ! check_test_critical "sudo service mongodb status" "mongodb stop/pause|mongodb stop/waiting"
    then
        display_error "MongoDB is running on the secondary server"
    fi
fi

check_exit "MongoDB is working properly" ""
