#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "tomcat-solr"

DATETIME=$(date +%Y%m%d-%H%M%S)
isSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isSecondary
then
    if ! check_test_critical "sudo service tomcat-solr status" "tomcat-solr.*is running..."
    then
        check_exit "Tomcat SolR is not running"
    fi

    if ! check_test_critical "sudo service tomcat-solr stop" "Stopped tomcat-solr"
    then
        display_error "Cannot stop Tomcat SolR"
    fi

    if check_test_critical "sudo service tomcat-solr start" "Started tomcat-solr"
    then
        startTime=$(date +%s)
        tail -n0 -f $(locate catalina.out) | while read line
        do
            if echo "$line" | grep 'INFO: Server startup in' > /dev/null
            then
                pkill -SIGKILL tail -P $$
            elif [[ $(( $startTime + 300 )) -lt $(date +%s) ]]
            then
                display_error "Cannot start Tomcat SolR"
                pkill -SIGKILL tail -P $$
            fi
        done
    else
        display_error "Cannot start Tomcat SolR"
    fi

    if check_test_critical "sudo service tomcat-solr restart" "Restarted tomcat-solr"
    then
        startTime=$(date +%s)
        tail -n0 -f $(locate catalina.out) | while read line
        do
            if echo "$line" | grep 'INFO: Server startup in' > /dev/null
            then
                pkill -SIGKILL tail -P $$
            elif [[ $(( $startTime + 300 )) -lt $(date +%s) ]]
            then
                pkill -SIGKILL tail -P $$
                display_error "Cannot restart Tomcat SolR"
            fi
        done
    else
        display_error "Cannot restart Tomcat SolR"
    fi

    if ! check_test_critical "curl -sIL idx.digiposte.local:8414/solr" "HTTP/1.1 200 OK"
    then
        display_error "Cannot access idx.digiposte.local:8414.solr"
    fi

    if ! check_test_critical "sudo /home/nagios/solr-sonde-cores.py -d http://idx.digiposte.local:8414/solr/" "OK solr-cores |'exec_time'="
    then
        display_error "Centreon probe execution does not end properly"
    fi

    if ! check_test_critical "sudo /data/var/lib/solr/cores/solr-optimize.sh" ""
    then
        display_error "solr-optimize.sh does not end properly"
    fi
else
    # This test is not described in the DEX
    if ! check_test_critical "sudo service tomcat-solr status" "tomcat-solr.*is stopped"
    then
        display_error "Tomcat SolR is running on the secondary server"
    fi
fi

check_exit "Tomcat SolR is working properly" ""
