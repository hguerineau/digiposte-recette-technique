#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "apache"

isSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isSecondary
then
    if ! check_test_critical "sudo service apache2 status" "Apache2 is running"
    then
        check_exit "Apache is not running"
    fi

    if ! check_test_critical "sudo service apache2 stop | tr -d '\n'" "Stopping web server apache2.*done|Stopping web server apache2.*OK"
    then
        display_error "Cannot stop apache"
    fi

    if ! check_test_critical "sudo service apache2 start | tr -d '\n'" "Starting web server apache2.*done|Starting web server apache2.*OK"
    then
        display_error "Cannot start apache"
    fi

    sleep 1
    if ! check_test_critical "sudo service apache2 restart | tr -d '\n'" "Restarting web server apache2.*done|Restarting web server apache2.*OK"
    then
        display_error "Cannot restart apache"
    fi

    sleep 1
    if ! check_test_critical "sudo service apache2 reload | tr -d '\n'" "Reloading web server config apache2.*done|Reloading web server config apache2.*OK"
    then
        display_error "Cannot reload apache"
    fi

    if ! check_test_critical "sudo apache2ctl -M 2>&1" "Syntax OK"
    then
        display_error "Syntax error in configuration files"
    fi
    for module in "$@"
    do
        if ! check_test_critical "sudo apache2ctl -M 2>&1" "$module"
        then
            display_error "$module cannot be found"
        fi
    done

    if ! check_test_critical "curl -s http://localhost/server-status" "Apache Server Status for localhost"
    then
        display_error "Cannot acccess http://localhost/server-status"
    fi
else
    if ! check_test_critical "sudo service apache2 status" "Apache2 is NOT running"
    then
        display_error "Apache is running on the secondary server"
    fi
fi

check_exit "Apache is working properly" ""
