#! /bin/bash

# Author Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

BACKUP_SERVER=$1

check_init "keepalived"

# No service status for keepalived
if ! check_test_critical "sudo ps xf" "keepalived"
then
    check_exit "keepalived is not running"
fi

if ! check_test_critical "sudo service keepalived stop | tr -d '\n'" "Stopping keepalived keepalived.*done"
then
    display_error "Cannot stop keepalived"
fi

if ! check_test_critical "sudo service keepalived start | tr -d '\n'" "Starting keepalived keepalived.*done"
then
    display_error "Cannot start keepalived"
fi

if ! check_test_critical "sudo service keepalived restart | tr -d '\n'" "Restarting keepalived keepalived.*done"
then
    display_error "Cannot restart keepalived"
fi

check_exit "keepalived works properly" ""
