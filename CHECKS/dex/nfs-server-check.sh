#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "nfs-server"

isSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isSecondary
then
    if ! check_test_critical "sudo service nfs-kernel-server status" "nfsd running"
    then
        check_exit "NFS daemon is not running"
    fi

    if ! check_test_critical "sudo service nfs-kernel-server stop | tr -d '\n'" "Stopping NFS kernel daemon.*done"
    then
        display_error "Cannot stop NFS daemon"
    fi

    if ! check_test_critical "sudo service nfs-kernel-server start | tr -d '\n'" "Starting NFS kernel daemon.*done"
    then
        display_error "Cannot start NFS daemon"
    fi

    if ! check_test_critical "sudo service nfs-kernel-server restart | tr -d '\n'" "Stopping NFS kernel daemon.*done.*Starting NFS kernel daemon.*done"
    then
        display_error "Cannot restart NFS daemon"
    fi

    for nfs in "$@"
    do
        directory="$(echo "$nfs" | cut -d : -f 1)"
        network="$(echo "$nfs" | cut -d : -f 2)"
        if ! check_test_critical "showmount -e" "$directory.*$network"
        then
            display_error "$directory is not available"
        fi
    done
else
    # This test is not described in the DEX
    if ! check_test_critical "sudo service nfs-kernel-server status" "nfsd not running"
    then
        display_error "NFS daemon is running on the secondary server"
    fi
fi

check_exit "NFS daemon works properly" ""
