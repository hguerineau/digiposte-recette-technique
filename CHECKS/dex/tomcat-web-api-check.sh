#! /bin/bash

# Author    Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "tomcat-web-api"

if ! check_test_critical "sudo service tomcat-digiposte-java-api status" "tomcat-digiposte-java-api .* is running"
then
    check_exit "Tomcat for the Digiposte API is not running"
fi

if ! check_test_critical "sudo service tomcat-digiposte-java-api stop" "Stopped tomcat-digiposte-java-api"
then
    display_error "Cannot stop tomcat for the Digiposte API"
fi

if ! check_test_critical "sudo service tomcat-digiposte-java-api start" "Started tomcat-digiposte-java-api"
then
    display_error "Cannot start tomcat for the Digiposte API"
fi

if ! check_test_critical "sudo service tomcat-digiposte-java-api restart" "Restarted tomcat-digiposte-java-api"
then
    display_error "Cannot restart tomcat for the Digiposte API"
fi

if ! check_test_critical "ps aux" "digiposte-java-api"
then
    display_error "Cannot find the pid related to tomcat for the Digiposte API"
fi

check_exit "Tomcat for the Digiposte API works properly" ""
