#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON, Pierre CAZAJOUS

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "logstash"

isDrbdSecondary=$([[ -z "$(drbd-overview | grep 'Secondary/')" ]] && echo false || echo true)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service logstash-agent status" "logstash-agent start/running, process [0-9]+"
    then
        check_exit "Logstash is not running"
    fi

    if ! check_test_critical "sudo service logstash-agent stop" "logstash-agent stop/waiting"
    then
        display_error "Cannot stop Logstash"
    fi

    if ! check_test_critical "sudo service logstash-agent start" "logstash-agent start/running, process [0-9]+"
    then
        display_error "Cannot start Logstash"
    fi

    if ! check_test_critical "sudo service logstash-agent restart | tr -d '\n'" "logstash-agent stop/waiting.*logstash-agent start/running, process [0-9]+"
    then
        display_error "Cannot restart Logstash"
    fi

    if ! check_test_critical "ps -p \$(cat /var/run/logstash/agent/logstash.pid 2> /dev/null) -o command" "java"
    then
        display_error "Cannot find the pid related to Logstash"
    fi
else
    if ! check_test_critical "sudo service logstash-agent status" "logstash-agent stop/waiting"
    then
        display_error "Logstash is running on the secondary server"
    fi
fi

check_exit "Logstash is working properly" ""
