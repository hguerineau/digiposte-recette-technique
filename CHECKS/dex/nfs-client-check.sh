#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "nfs-client"

for nfs_dir in "$@"
do
    dirName=$(echo "$nfs_dir" | cut -d  ':' -f 1)
    dirContent=$(echo "$nfs_dir" | cut -d ':' -f 2)

    if ! check_test_critical "sudo mount" "on $dirName type nfs"
    then
        check_exit "$dirName is not mounted"
    fi
    if ! check_test_critical "ls -l $dirName" "$dirContent"

    then
        display_error "$dirName does not contain $dirContent"
    fi

    if ! check_test_critical "sudo umount $dirName" ""
    then
        display_error "Umount failed $dirName"
    fi

    if ! check_test_critical "sudo mount" "!$dirName"
    then
        display_error "$dirName can still be found while it should be dismounted"
    fi

    if ! check_test_critical "sudo mount $dirName" ""
    then
        display_error "Cannot mount back $dirName"
    fi

    if ! check_test_critical "sudo mount" "$dirName type nfs"
    then
        display_error "$dirName cannot be found while it should be mounted back"
    fi
done

check_exit "NFSs are mounted properly" ""
