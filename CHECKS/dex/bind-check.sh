#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "bind"

if ! check_test_critical "sudo service bind9 status" "bind9 is running"
then
    check_exit "Bind is not running"
fi

if ! check_test_critical "sudo service bind9 stop | tr -d '\n'" "waiting for pid .* to die.*done"
then
    display_error "Cannot stop Bind"
fi

if ! check_test_critical "sudo service bind9 start | tr -d '\n'" "Starting domain name service... bind9.*done"
then
    display_error "Cannot start Bind"
fi

if ! check_test_critical "sudo service bind9 restart | tr -d '\n'" "Stopping domain name service... bind9.*done.*Starting domain name service... bind9.*done"
then
    display_error "Cannot restart Bind"
fi

check_exit "Bind is working properly" ""
