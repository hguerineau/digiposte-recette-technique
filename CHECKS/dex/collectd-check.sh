#! /bin/bash

# Author Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "collectd"

isSecondary=$(sudo facter -p drbd_is_secondary)

if ! $isSecondary
then
	if ! check_test_critical "sudo service collectd status" "collectd is running"
	then
	    check_exit "collectd is not running"
	fi
	if ! check_test_critical "sudo service collectd stop | tr -d '\n'" "Stopping statistics collection and monitoring daemon collectd.*done|Stopping statistics collection and monitoring daemon collectd.*OK"
	then
	    display_error "Cannot stop collectd"
	fi
	if ! check_test_critical "sudo service collectd start | tr -d '\n'" "Starting statistics collection and monitoring daemon collectd.*done|Starting statistics collection and monitoring daemon collectd.*OK"
	then
	    display_error "Cannot start collectd"
	fi
	if ! check_test_critical "sudo service collectd restart | tr -d '\n'" "Restarting statistics collection and monitoring daemon collectd.*done|Restarting statistics collection and monitoring daemon collectd.*OK"
	then
	    display_error "Cannot restart collectd"
	fi
	if ! check_test_critical "ps -p $(cat /var/run/collectd.pid) -o command" ".+"
	then
	    display_error "Some issues with the monitoring : empty string returned"
	fi
else
    if ! check_test_critical "sudo service collectd status" "collectd is stopped"
    then
        display_error "Collectd is running on the secondary server"
    fi
fi

check_exit "collectd works properly" ""
