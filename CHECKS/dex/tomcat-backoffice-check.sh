#! /bin/bash

# Author    Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "tomcat-backoffice"

if ! check_test_critical "sudo service tomcat-digiposte-java-backoffice status" "tomcat-digiposte-java-backoffice .* is running"
then
    check_exit "Tomcat for the Digiposte Backoffice is not running"
fi

if ! check_test_critical "sudo service tomcat-digiposte-java-backoffice stop" "Stopped tomcat-digiposte-java-backoffice"
then
    display_error "Cannot stop tomcat for the Digiposte Backoffice"
fi

if ! check_test_critical "sudo service tomcat-digiposte-java-backoffice start" "Started tomcat-digiposte-java-backoffice"
then
    display_error "Cannot start tomcat for the Digiposte Backoffice"
fi

if ! check_test_critical "sudo service tomcat-digiposte-java-backoffice restart" "Restarted tomcat-digiposte-java-backoffice"
then
    display_error "Cannot restart tomcat for the Digiposte Backoffice"
fi

if ! check_test_critical "ps aux | grep tomcat-digiposte-java-backoffice | grep -v grep" "digiposte-java-backoffice"
then
    display_error "Cannot find the pid related to tomcat for the Digiposte Backoffice"
fi

check_exit "Tomcat for the Digiposte Backoffice works properly" ""
