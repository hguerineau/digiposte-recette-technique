#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "rabbitmq"

isSecondary=$(sudo facter -p drbd_is_secondary)

if ! $isSecondary
then
    if ! check_test_critical "sudo service rabbitmq-server status" "RabbitMQ Management Console"
    then
        display_error "RabbitMQ is not running"
	elif ! check_test_critical "sudo rabbitmqctl cluster_status" "running_nodes.*rabbit@mq1.*rabbit@mq2|running_nodes.*rabbit@mq2.*rabbit@mq1"
	then
		display_error "Node(s) missing in running_nodes"
    elif ! check_test_critical "sudo rabbitmqctl list_queues -p digiposte" "digiposte.queue.user.documents.delete"
    then
        display_error "$(sudo rabbitmqctl list_queues -p digiposte)"
    fi

    if ! check_test_critical "sudo service rabbitmq-server stop | tr -d '\n'" "Stopping.*rabbitmq-server.*done"
    then
        display_error "Cannot stop RabbitMQ"
    fi

    if ! check_test_critical "sudo service rabbitmq-server start | tr -d '\n'" "Starting.*rabbitmq-server.*done"
    then
        display_error "Cannot start RabbitMQ"
    fi

    if ! check_test_critical "sudo service rabbitmq-server restart | tr -d '\n'" "Restarting.*rabbitmq-server.*done"
    then
        display_error "Cannot restart RabbitMQ"
    fi
else
    if ! check_test_critical "sudo ps aux | grep ^rabbit | grep -c mnesia" "^0$"
    then
        display_error "RabbitMQ is running on the secondary server"
    fi
fi

check_exit "RabbitMQ is working properly" ""
