#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "haproxy"

if ! check_test_critical "sudo service haproxy status" "haproxy is running"
then
    check_exit "HAProxy is not running"
fi

if ! check_test_critical "sudo service haproxy stop | tr -d '\n'" "Stopping haproxy haproxy.*done"
then
    display_error "Cannot stop HAProxy"
fi

if ! check_test_critical "sudo service haproxy start | tr -d '\n'" "Starting haproxy haproxy.*done"
then
    display_error "Cannoy start HAProxy"
fi

if ! check_test_critical "sudo service haproxy restart | tr -d '\n'" "Restarting haproxy haproxy.*done"
then
    display_error "Cannot restart HAProxy"
fi

check_exit "HAProxy is working properly" ""
