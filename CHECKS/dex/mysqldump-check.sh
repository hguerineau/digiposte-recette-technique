#! /bin/bash

# Author Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

PASSWORD=$1
DATABASE_NAME=$2
TABLE_NAMES=$3
DATETIME=$(date +%Y%m%d-%H%M%S)

check_init "mysqldump"

isSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isSecondary
then
    if check_test_critical "mysqldump -uroot -p${PASSWORD} $DATABASE_NAME --where=\"true LIMIT 10\" | gzip > $DATETIME.mysql-backup.sql.gz" ""
    then
        rm $DATETIME.mysql-backup.sql.gz
    else
        display_error "Cannot dump the database $DATABASE_NAME"
    fi
fi

check_exit "MySQL works properly" ""
