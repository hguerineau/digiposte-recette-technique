#!/bin/bash

#@todo liste des serveurs à actualiser car non défini dans DEX
# ssh worker1-rectech 'bash -s ' <check_dex_kpi_jmxtrans.sh
# ssh worker2-rectech 'bash -s ' <check_dex_kpi_jmxtrans.sh
# ssh back1-rectech 'bash -s ' <check_dex_kpi_jmxtrans.sh
# ssh web1-rectech 'bash -s ' <check_dex_kpi_jmxtrans.sh
#source ../LIBS/display.fct


testName="check_dex_kpi_jmxtrans"

#si besoin des droits admin pour faire les tests	
adminRight="sudo "


if [[ $1 == "-a" ]] ; 
then
	serverList=""
	#serverList=$serverList" db-recfonc web1-recfonc dmz-recfonc support-recfonc worker-recfonc"
	
	serverList=$serverList" dns1-rectech dns2-rectech rp1-rectech rp2-rectech lb1-rectech lb2-rectech web1-rectech web2-rectech deploy-rectech  back1-rectech back2-rectech worker1-rectech worker2-rectech db1-rectech db2-rectech kpi-rectech log-web-rectech log-collector-rectech mq1-rectech mq2-rectech nfs-rectech idx1-rectech idx2-rectech" 
	
	serverList=$serverList" dns1-tmc dns2-tmc rp1-tmc rp2-tmc lb1-tmc lb2-tmc web1-tmc web2-tmc deploy-tmc back1-tmc back2-tmc worker1-tmc worker2-tmc db1-tmc db2-tmc kpi-tmc log-web-tmc log-collector-tmc mq1-tmc mq2-tmc nfs-tmc idx1-tmc idx2-tmc" 

	#serverList=$serverList" dns1-prod-a nfs-prod-a mq1-prod-a mq2-prod-a worker1-prod-a worker2-prod-a db1-prod-a db2-prod-a idx1-prod-a kpi-prod-a log-web-prod-a log-collector-prod-a rp1-prod-a lb1-prod-a web11-prod-a web12-prod-a web13-prod-a dns1-prod-b nfs-prod-b mq1-prod-b mq2-prod-b worker1-prod-b worker2-prod-b db1-prod-b db2-prod-b idx1-prod-b kpi-prod-b log-web-prod-b log-collector-prod-b rp1-prod-b lb1-prod-b web21-prod-b web22-prod-b web23-prod-b"

	#serverList=$serverList" dns1-preprod-a nfs-preprod-a mq1-preprod-a mq2-preprod-a worker1-preprod-a worker2-preprod-a db1-preprod-a db2-preprod-a idx1-preprod-a kpi-preprod-a log-web-preprod-a log-collector-preprod-a rp1-preprod-a lb1-preprod-a web11-preprod-a web12-preprod-a web13-preprod-a dns1-preprod-b nfs-preprod-b mq1-preprod-b mq2-preprod-b worker1-preprod-b worker2-preprod-b db1-preprod-b db2-preprod-b idx1-preprod-b kpi-preprod-b log-web-preprod-b log-collector-preprod-b rp1-preprod-b lb1-preprod-b web21-preprod-b web22-preprod-b web23-preprod-b"

	for servertmp in $serverList; do
		# grep -v ATTENTION : enleve le message d'avertissement à chaque connexion machine
		ssh $servertmp 'bash -s ' 2>&1 <$0 | grep -v ATTENTION
	done
	exit
fi


nbResultsOK=0
nbResultsKO=0

#affiche le détail des tests et des erreurs
if [[ $1 == "-v" ]] || [[ $2 == "-v" ]] || [[ $3 == "-v" ]]; 
then
    NAGIOSSTYLEEXIT=1
else
    NAGIOSSTYLEEXIT=0
    ECHOCOLORGREEN="\\033[1;32m"
    ECHOCOLORRED="\\033[1;31m"
    ECHOCOLORNORMAL="\\033[0;39m"
    #sortie pour html
    #ECHOCOLORGREEN="<font color='green'>"
    #ECHOCOLORRED="<font color='red'>"
    #ECHOCOLORORANGE="<font color='orange'>"
    #ECHOCOLORNORMAL="</font><br />"
fi

function test {
	#echo "command : " $command
	result=$($command 2>&1)
	echo $result | egrep  "$expected" >/dev/null
	return=$?
	
	if [ "$expected" == "" ]
	then
		if [ "$result" != "" ]
		then
			return=1
		fi
	fi
	if  [ $return -eq 0 ]
	    then 
		echo -e $ECHOCOLORGREEN "OK - "$testName" - "$HOSTNAME" - COMMAND: " $command " - EXPECT: " $expected " - RESULT: " $return  $ECHOCOLORNORMAL; 
		nbResutsOK=$(($nbResutsOK+1))
	    else 
		echo -e $ECHOCOLORRED "CRITICAL - "$testName" - "$HOSTNAME" -COMMAND: " $command " - EXPECT: " $expected " - RESULT: "  $return  $ECHOCOLORNORMAL; 
		nbResutsKO=$(($nbResutsKO+1))
	fi

}



#@todo liste des serveurs à actualiser car non défini dans DEX
echo $HOSTNAME| egrep 'back|web|worker|kpi' > /dev/null 2>&1
if [[ $? -eq 0 ]] ;
then


	command=$adminRight"service jmxtrans status"
	expected="jmxtrans appears to be running at pid"
	test $command $expected

	command=$adminRight"service jmxtrans status"
	expected="[ OK ]"
	test $command $expected

	command=$adminRight"service jmxtrans stop"
	expected="jmxtrans stopped"
	test $command $expected

	command=$adminRight"service jmxtrans start"
	expected="Starting jmxtrans"
	test $command $expected
	sleep 1

	command=$adminRight"service jmxtrans stop"
	expected="jmxtrans stopped"
	test $command $expected

	command=$adminRight"service jmxtrans start"
	expected="[ OK ]"
	test $command $expected
	sleep 1

	command=$adminRight"service jmxtrans stop"
	expected="jmxtrans stopped"
	test $command $expected

	command=$adminRight"service jmxtrans restart"
	expected="[ OK ]"
	test $command $expected
	sleep 1

	command=$adminRight"service jmxtrans stop"
	expected="jmxtrans stopped"
	test $command $expected

	command=$adminRight"service jmxtrans restart"
	expected="[ OK ]"
	test $command $expected
	sleep 1

	#@todo ne marche pas dans l'outil de test, mais fonctionne manuellement, à retravailler
	command=$adminRight"ps aux | grep -c jmxtrans"
	expected="2"
	#test $command $expected




fi






if [[ $nbResutsKO -eq 0 ]] 
then
	nbResutsKOText="0"
else
	nbResutsKOText=$nbResutsKO
fi


if  [[ $nbResutsKO -eq 0 ]] && [[ $nbResutsOK -eq 0 ]]
    then 
    exit 0;
fi

if  [[ $nbResutsKO -eq 0 ]] && [[ $nbResutsOK -gt 0 ]]
    then 
    echo -e $ECHOCOLORGREEN "OK - "$testName" - "$HOSTNAME" | test_success="$nbResutsOK " test_error="$nbResutsKOText  $ECHOCOLORNORMAL
    exit 0;
elif [[ $nbResutsKO -gt 0 ]] && [[ $nbResutsOK -eq 0 ]]
    then 
    echo -e $ECHOCOLORRED "UNKNOWN - "$testName" - "$HOSTNAME" | test_success="$nbResutsOK " test_error="$nbResutsKOText $ECHOCOLORNORMAL
    exit 3;
else 
    echo -e $ECHOCOLORRED "CRITICAL - "$testName" - "$HOSTNAME" | test_success="$nbResutsOK " test_error="$nbResutsKOText $ECHOCOLORNORMAL
    exit 2;
fi


