#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "carbon-cache"

isSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isSecondary
then
    if ! check_test_critical "sudo service carbon-cache status" "carbon-cache is running"
    then
        check_exit "Carbon-Cache is not running"
    fi

    if ! check_test_critical "sudo service carbon-cache stop" "Stopping Graphite backend daemon carbon-cache"
    then
        display_error "Cannot stop carbon-cache"
    fi

    if ! check_test_critical "sudo service carbon-cache start" ""
    then
        display_error "Cannot start carbon-cache"
    fi

    if ! check_test_critical "sudo service carbon-cache restart" "Restarting Graphite backend daemon carbon-cache"
    then
        display_error "Cannot restart carbon-cache"
    fi

    sleep 1
    if ! check_test_critical "sudo ps -p \$(cat /data/opt/graphite/storage/carbon-cache*.pid 2> /dev/null) -o command" "carbon-cache.py start"
    then
        display_error "Cannot find the pid related to carbon-cache"
    fi
else
    # This test is not described in the DEX
    if ! check_test_critical "sudo service carbon-cache status" "carbon-cache is not running|carbon-cache is stopped"
    then
        display_error "Carbon-Cache is running on the secondary server"
    fi
fi

check_exit "Carbon-Cache is working properly" ""
