#! /bin/bash

# Author Damien PELLISSON

source ../../../LIBS/display-centreon.fct
source ../../../LIBS/check.fct

check_init "metrics"

FROM_1DAY="from=-1day&until=now"
FROM_1HOUR="from=-1hour&until=now"
FROM_10MIN="from=-10minutes&until=now"
FROM_1MIN="from=-1minute&until=now"
FROM_10SEC="from=-10seconds&until=now"

ENVIRONMENT=$1
BASE_URL=$2
CERTIFICATE=$3
USER=$4
SERVER=$5
shift 5

CHECKS=()

for args in "$@"
do
    component=$(echo "$args" | cut -d : -f 1)
    args=$(echo "$args" | cut -d : -f 2-)

    case "$component" in
        'system' )
            SYSTEM_METRICS=(
                'cpu.*.cpu.system $FROM_10SEC&target=$metric&format=csv'
                'cpu.*.cpu.nice $FROM_10SEC&target=$metric&format=csv'
                'cpu.*.cpu.user $FROM_10SEC&target=$metric&format=csv'
                'cpu.*.cpu.steal $FROM_10SEC&target=$metric&format=csv'
                'cpu.*.cpu.softirq $FROM_10SEC&target=$metric&format=csv'
                'cpu.*.cpu.wait $FROM_10SEC&target=$metric&format=csv'
                'cpu.*.cpu.interrupt $FROM_10SEC&target=$metric&format=csv'
                'cpu.*.cpu.idle $FROM_10SEC&target=$metric&format=csv'
                'df.*.df_complex.used $FROM_10SEC&target=$metric&format=csv'
                'df.*.df_complex.free $FROM_10SEC&target=$metric&format=csv'
                'df.*.df_complex.reserved $FROM_10SEC&target=$metric&format=csv'
                'disk.*.disk_octets.write $FROM_10SEC&target=$metric&format=csv'
                'disk.*.disk_octets.read $FROM_10SEC&target=$metric&format=csv'
                'disk.*.disk_merged.write $FROM_10SEC&target=$metric&format=csv'
                'disk.*.disk_merged.read $FROM_10SEC&target=$metric&format=csv'
                'disk.*.disk_time.read $FROM_10SEC&target=$metric&format=csv'
                'disk.*.disk_ops.read $FROM_10SEC&target=$metric&format=csv'
                'interface.*.if_errors.rx $FROM_10SEC&target=$metric&format=csv'
                'interface.*.if_errors.tx $FROM_10SEC&target=$metric&format=csv'
                'interface.*.if_packets.tx $FROM_10SEC&target=$metric&format=csv'
                'interface.*.if_octets.rx $FROM_10SEC&target=$metric&format=csv'
                'interface.*.if_octets.tx $FROM_10SEC&target=$metric&format=csv'
                'load.load.shortterm $FROM_10SEC&target=$metric&format=csv'
                'load.load.shortterm $FROM_10SEC&target=$metric&format=csv'
                'load.load.shortterm $FROM_10SEC&target=$metric&format=csv'
                'load.load.midterm $FROM_10SEC&target=$metric&format=csv'
                'load.load.longterm $FROM_10SEC&target=$metric&format=csv'
                'memory.memory.used $FROM_10SEC&target=$metric&format=csv'
                'memory.memory.buffered $FROM_10SEC&target=$metric&format=csv'
                'memory.memory.free $FROM_10SEC&target=$metric&format=csv'
                'memory.memory.cached $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.create $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.fsstat $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.getattr $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.link $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.lookup $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.mkdir $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.null $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.read $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.readdir $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.readlink $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.remove $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.rename $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.rmdir $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.root $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.setattr $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.symlink $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.wrcache $FROM_10SEC&target=$metric&format=csv'
                'nfs.v2client.nfs_procedure.write $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.access $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.commit $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.create $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.fsinfo $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.fsstat $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.getattr $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.lookup $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.link $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.mkdir $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.mknod $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.null $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.pathconf $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.read $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.readdir $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.readdirplus $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.readlink $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.remove $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.rename $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.rmdir $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.setattr $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.symlink $FROM_10SEC&target=$metric&format=csv'
                'nfs.v3client.nfs_procedure.write $FROM_10SEC&target=$metric&format=csv'
                'ntpd.frequency_offset.loop $FROM_10SEC&target=$metric&format=csv'
                'ntpd.time_dispersion.* $FROM_10SEC&target=$metric&format=csv'
                'ntpd.time_offset.* $FROM_10SEC&target=$metric&format=csv'
                'ntpd.time_offset.loop $FROM_10SEC&target=$metric&format=csv'
                'ntpd.time_offset.error $FROM_10SEC&target=$metric&format=csv'
                'processes.ps_state.paging $FROM_10SEC&target=$metric&format=csv'
                'processes.ps_state.running $FROM_10SEC&target=$metric&format=csv'
                'processes.ps_state.stopped $FROM_10SEC&target=$metric&format=csv'
                'processes.ps_state.zombies $FROM_10SEC&target=$metric&format=csv'
                'processes.ps_state.sleeping $FROM_10SEC&target=$metric&format=csv'
                'processes.ps_state.blocked $FROM_10SEC&target=$metric&format=csv'
                'processes.fork_rate $FROM_10SEC&target=$metric&format=csv'
                'swap.swap.cached $FROM_10SEC&target=$metric&format=csv'
                'swap.swap.free $FROM_10SEC&target=$metric&format=csv'
                'swap.swap.used $FROM_10SEC&target=$metric&format=csv'
                'swap.swap_io.in $FROM_10SEC&target=$metric&format=csv'
                'swap.swap_io.out $FROM_10SEC&target=$metric&format=csv'
                'uptime.uptime $FROM_10SEC&target=$metric&format=csv'
                'users.users $FROM_1MIN&target=$metric&format=csv'
            )
            for metric in "${SYSTEM_METRICS[@]}"
            do
                CHECKS[${#CHECKS[@]}]="$metric"
            done
            ;;
        'haproxy' )
            HAPROXY_METRICS=(
                'haproxy.counter.backend_haproxy_*_downtime $FROM_10SEC&target=$metric&format=csv'
                'haproxy.counter.backend_haproxy_*_retries $FROM_10SEC&target=$metric&format=csv'
                'haproxy.counter.backend_haproxy_*_session_total $FROM_10SEC&target=$metric&format=csv'
                'haproxy.counter.backend_haproxy_*_session_total $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.backend_haproxy_*_bytes_in $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.backend_haproxy_*_bytes_out $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.backend_haproxy_*_denied_request $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.backend_haproxy_*_denied_response $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.backend_haproxy_*_error_response $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.frontend_haproxy_*_bytes_in $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.frontend_haproxy_*_bytes_out $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.frontend_haproxy_*_denied_request $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.frontend_haproxy_*_denied_response $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.frontend_haproxy_*_error_request $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.frontend_haproxy_stats_response_1xx $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.frontend_haproxy_stats_response_2xx $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.frontend_haproxy_stats_response_3xx $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.frontend_haproxy_stats_response_4xx $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.frontend_haproxy_stats_response_5xx $FROM_10SEC&target=$metric&format=csv'
                'haproxy.derive.frontend_haproxy_stats_response_other $FROM_10SEC&target=$metric&format=csv'
                'haproxy.gauge.backend_haproxy_*_queue_current $FROM_10SEC&target=$metric&format=csv'
                'haproxy.gauge.backend_haproxy_*_session_current $FROM_10SEC&target=$metric&format=csv'
                'haproxy.gauge.backend_haproxy_*_session_rate $FROM_10SEC&target=$metric&format=csv'
                'haproxy.gauge.frontend_haproxy_*_request_rate $FROM_10SEC&target=$metric&format=csv'
                'haproxy.gauge.frontend_haproxy_*_session_current $FROM_10SEC&target=$metric&format=csv'
                'haproxy.gauge.frontend_haproxy_*_session_rate $FROM_10SEC&target=$metric&format=csv'
            )
            for metric in "${HAPROXY_METRICS[@]}"
            do
                CHECKS[${#CHECKS[@]}]="$metric"
            done
            ;;
        'mysql' )
            MYSQL_METRICS=(
                'mysql.digiposte.cache_result.qcache-hits $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.cache_result.qcache-inserts $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.cache_result.qcache-not_cached $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.cache_result.qcache-prunes $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.cache_size.qcache $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.admin_commands $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.alter_db $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.alter_table $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.begin $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.change_db $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.commit $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.create_db $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.create_table $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.create_user $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.delete $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.drop_db $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.drop_table $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.drop_user $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.flush $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.grant $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.insert $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.lock_tables $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.rollback $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.select $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.set_option $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_collations $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_create_db $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_create_table $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_databases $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_events $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_fields $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_function_status $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_grants $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_keys $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_privileges $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_procedure_status $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_status $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_storage_engines $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_tables $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_table_status $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_triggers $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_variables $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.show_warnings $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.truncate $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.unlock_tables $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_commands.update $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_handler.commit $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_handler.delete $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_handler.read_first $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_handler.read_key $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_handler.read_last $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_handler.read_next $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_handler.read_prev $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_handler.read_rnd $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_handler.read_rnd_next $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_handler.rollback $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_handler.update $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_handler.write $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.buffer_pool_pages_data $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.buffer_pool_pages_flushed $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.buffer_pool_pages_misc $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.buffer_pool_pages_total $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.buffer_pool_read_ahead $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.buffer_pool_read_ahead_evicted $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.buffer_pool_read_requests $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.buffer_pool_reads $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.buffer_pool_write_requests $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.data_fsyncs $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.data_read $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.data_reads $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.data_writes $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.data_written $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.dblwr_pages_written $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.dblwr_writes $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.log_write_requests $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.log_writes $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.os_log_fsyncs $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.os_log_written $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.pages_created $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.page_size $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.pages_read $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.pages_written $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.rows_deleted $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.rows_inserted $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.rows_read $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_innodb.rows_updated $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_locks.immediate $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_locks.waited $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_max_used_connections $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_locks.immediate $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_locks.waited $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_octets.rx $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_octets.tx $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_opened.files $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_opened.table_definitions $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_opened.tables $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_open.files $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_open.table_definitions $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_open.tables $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_slow.queries $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.mysql_table.locks_immediate $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.threads.cached $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.threads.connected $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.threads.running $FROM_10SEC&target=$metric&format=csv'
                'mysql.digiposte.total_threads.created $FROM_10SEC&target=$metric&format=csv'
            )
            for metric in "${MYSQL_METRICS[@]}"
            do
                CHECKS[${#CHECKS[@]}]="$metric"
            done
            ;;
        'business.sql' )
            BUSISQL_METRICS=(
                'business.sql.mysql_info_tables.digiposte.document.data $FROM_1DAY&target=$metric&format=csv'
                'business.sql.mysql_info_tables.digiposte.document.indexes $FROM_1DAY&target=$metric&format=csv'
                'business.sql.mysql_info_tables.digiposte.document.rows $FROM_1DAY&target=$metric&format=csv'
                'business.sql.mysql_info_tables.digiposte.document.total $FROM_1DAY&target=$metric&format=csv'
                'business.sql.documents.by_size.size_00000 $FROM_1DAY&target=$metric&format=csv'
                'business.sql.documents.by_size.failed $FROM_1DAY&target=$metric&format=csv'
                'business.sql.documents.by_user.range_00000 $FROM_1DAY&target=$metric&format=csv'
                'business.sql.documents.by_user.failed $FROM_1DAY&target=$metric&format=csv'
                'business.sql.documents.by_sender.count $FROM_1DAY&target=$metric&format=csv'
                'business.sql.documents.by_sender.failed $FROM_1DAY&target=$metric&format=csv'
                'business.sql.accounts.active.count $FROM_1DAY&target=$metric&format=csv'
                'business.sql.accounts.active.failed $FROM_1DAY&target=$metric&format=csv'
                'business.sql.accounts.closed.count $FROM_1DAY&target=$metric&format=csv'
                'business.sql.accounts.closed.failed $FROM_1DAY&target=$metric&format=csv'
                'business.sql.accounts.closing.count $FROM_1DAY&target=$metric&format=csv'
                'business.sql.accounts.closing.failed $FROM_1DAY&target=$metric&format=csv'
                'business.sql.accounts.created.count $FROM_1DAY&target=$metric&format=csv'
                'business.sql.accounts.created.failed $FROM_1DAY&target=$metric&format=csv'
                'business.sql.accounts.to_purge.count $FROM_1DAY&target=$metric&format=csv'
                'business.sql.accounts.to_purge.failed $FROM_1DAY&target=$metric&format=csv'
                'business.sql.accounts.valid_temporary.count $FROM_1DAY&target=$metric&format=csv'
                'business.sql.accounts.valid_temporary.failed $FROM_1DAY&target=$metric&format=csv'
                'business.sql.accounts.with_new_auth.count $FROM_1DAY&target=$metric&format=csv'
                'business.sql.accounts.with_new_auth.failed $FROM_1DAY&target=$metric&format=csv'
                'business.sql.documents.dispatch.ok.count $FROM_1HOUR&target=$metric&format=csv'
                'business.sql.documents.dispatch.ok.failed $FROM_1HOUR&target=$metric&format=csv'
                'business.sql.documents.dispatch.ko.count $FROM_1HOUR&target=$metric&format=csv'
                'business.sql.documents.dispatch.ko.failed $FROM_1HOUR&target=$metric&format=csv'
            )
            for metric in "${BUSISQL_METRICS[@]}"
            do
                CHECKS[${#CHECKS[@]}]="$metric"
            done
            ;;
        'apache' )
            APACHE_METRICS=(
                'apache.server_status.apache_connections $FROM_10SEC&target=$metric&format=csv'
                'apache.server_status.apache_idle_workers $FROM_10SEC&target=$metric&format=csv'
                'apache.server_status.apache_requests $FROM_10SEC&target=$metric&format=csv'
                'apache.server_status.apache_bytes $FROM_10SEC&target=$metric&format=csv'
                'apache.server_status.apache_scoreboard.finishing $FROM_10SEC&target=$metric&format=csv'
                'apache.server_status.apache_scoreboard.keepalive $FROM_10SEC&target=$metric&format=csv'
                'apache.server_status.apache_scoreboard.closing $FROM_10SEC&target=$metric&format=csv'
                'apache.server_status.apache_scoreboard.idle_cleanup $FROM_10SEC&target=$metric&format=csv'
                'apache.server_status.apache_scoreboard.dnslookup $FROM_10SEC&target=$metric&format=csv'
                'apache.server_status.apache_scoreboard.waiting $FROM_10SEC&target=$metric&format=csv'
                'apache.server_status.apache_scoreboard.logging $FROM_10SEC&target=$metric&format=csv'
                'apache.server_status.apache_scoreboard.sending $FROM_10SEC&target=$metric&format=csv'
                'apache.server_status.apache_scoreboard.starting $FROM_10SEC&target=$metric&format=csv'
                'apache.server_status.apache_scoreboard.open $FROM_10SEC&target=$metric&format=csv'
                'apache.server_status.apache_scoreboard.reading $FROM_10SEC&target=$metric&format=csv'
            )
            for metric in "${APACHE_METRICS[@]}"
            do
                CHECKS[${#CHECKS[@]}]="$metric"
            done
            ;;
        'business.apache' )
            BUSIAPACHE_METRICS=(
                'business.apache.secure_access_log.request_time_original.average $FROM_1MIN&target=$metric&format=csv'
                'business.apache.secure_access_log.request_time_final.average $FROM_1MIN&target=$metric&format=csv'
                'business.apache.secure_access_log.http_status_code.http_1xx.count $FROM_1MIN&target=$metric&format=csv'
                'business.apache.secure_access_log.http_status_code.http_2xx.count $FROM_1MIN&target=sumSeries($metric)&format=csv'
            )
            for metric in "${BUSIAPACHE_METRICS[@]}"
            do
                CHECKS[${#CHECKS[@]}]="$metric"
            done
            ;;
        'rabbitmq' )
            RABBITTMQ_METRICS=(
                'rabbitmq.disk_free $FROM_10SEC&target=$metric&format=csv'
                'rabbitmq.queues.digiposte.queue.batch.routage.notif.consumers $FROM_10SEC&target=$metric&format=csv'
                'rabbitmq.queues.digiposte.queue.lot.index.consumers $FROM_10SEC&target=$metric&format=csv'
                'rabbitmq.queues.digiposte.queue.batch.routage.notif.mail.consumers $FROM_10SEC&target=$metric&format=csv'
                'rabbitmq.queues.digiposte.queue.batch.routage.notif.mobile.consumers $FROM_10SEC&target=$metric&format=csv'
            )
            for metric in "${RABBITTMQ_METRICS[@]}"
            do
                CHECKS[${#CHECKS[@]}]="$metric"
            done
            ;;
        'jvm' )
            JVM_METRICS=(
                'java.apps.$jvmname.jvm.memory.HeapMemoryUsage_init $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory.HeapMemoryUsage_used $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory.HeapMemoryUsage_committed $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory.HeapMemoryUsage_max $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory.NonHeapMemoryUsage_init $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory.NonHeapMemoryUsage_used $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory.NonHeapMemoryUsage_committed $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory.NonHeapMemoryUsage_max $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_cms_old_gem.Usage_init $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_cms_old_gem.Usage_used $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_cms_old_gem.Usage_committed $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_cms_old_gem.Usage_max $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_par_eden_space.Usage_init $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_par_eden_space.Usage_used $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_par_eden_space.Usage_committed $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_par_eden_space.Usage_max $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_par_survivor_space.Usage_init $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_par_survivor_space.Usage_used $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_par_survivor_space.Usage_committed $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_par_survivor_space.Usage_max $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_code_cache.Usage_init $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_code_cache.Usage_used $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_code_cache.Usage_committed $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_code_cache.Usage_max $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.operating_system.ProcessCpuTime $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.threading.ThreadCount $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_cms_perm_gen.Usage_init $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_cms_perm_gen.Usage_used $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_cms_perm_gen.Usage_committed $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.memory_pool_cms_perm_gen.Usage_max $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.garbage_collector_concurrent_mark_sweep.CollectionCount $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.garbage_collector_concurrent_mark_sweep.CollectionTime $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.garbage_collector_par_new.CollectionCount $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.jvm.garbage_collector_par_new.CollectionTime $FROM_10SEC&target=$metric&format=csv'
            )
            for jvmname in $( echo "$args" | tr -s ':' ' ')
            do
                for metric in "${JVM_METRICS[@]}"
                do
                    CHECKS[${#CHECKS[@]}]="$(echo "$metric" | sed "s/\$jvmname/$jvmname/g")"
                done
            done
            ;;
        'tomcat' )
            TOMCAT_METRICS=(
                'java.apps.$jvmname.catalina.data_source.numActive $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.data_source.numIdle $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.data_source.maxActive $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.executor.activeCount $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.executor.queueSize $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.executor.completedTaskCount $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.executor.maxThreads $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.global_request_processor_ajp.bytesReceived $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.global_request_processor_ajp.requestCount $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.global_request_processor_ajp.maxTime $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.global_request_processor_ajp.errorCount $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.global_request_processor_ajp.processingTime $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.global_request_processor_ajp.bytesSent $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.manager.activeSessions $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.global_request_processor_http.bytesReceived $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.global_request_processor_http.requestCount $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.global_request_processor_http.maxTime $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.global_request_processor_http.errorCount $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.global_request_processor_http.processingTime $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.global_request_processor_http.bytesSent $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.thread_pool.http.currentThreadsBusy $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.thread_pool.http.currentThreadCount $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.thread_pool.ajp.currentThreadsBusy $FROM_10SEC&target=$metric&format=csv'
                'java.apps.$jvmname.catalina.thread_pool.ajp.currentThreadCount $FROM_10SEC&target=$metric&format=csv'
            )
            for jvmname in $( echo "$args" | tr -s ':' ' ')
            do
                for metric in "${TOMCAT_METRICS[@]}"
                do
                    CHECKS[${#CHECKS[@]}]="$(echo "$metric" | sed "s/\$jvmname/$jvmname/g")"
                done
            done
            ;;
        'api.subscriber' )
            API_SUBSCRIBER_METRICS=(
                'java.api.subscriber.1_0.GET.error_metered.count $FROM_10SEC&target=$metric&format=csv'
                'java.api.subscriber.1_0.GET.timed.count $FROM_10SEC&target=$metric&format=csv'
                'java.api.subscriber.1_0.GET.timed.max $FROM_10SEC&target=$metric&format=csv'
                'java.api.subscriber.1_0.GET.timed.min $FROM_10SEC&target=$metric&format=csv'
            )
            for metric in "${API_SUBSCRIBER_METRICS[@]}"
            do
                CHECKS[${#CHECKS[@]}]="$metric"
            done
            ;;
        'api.strongbox' )
            API_STRONGBOX_METRICS=(
                'java.api.strongbox.*.timed.count $FROM_10SEC&target=$metric&format=csv'
                'java.api.strongbox.*.timed.max $FROM_10SEC&target=$metric&format=csv'
                'java.api.strongbox.*.timed.min $FROM_10SEC&target=$metric&format=csv'
            )
            for metric in "${API_STRONGBOX_METRICS[@]}"
            do
                CHECKS[${#CHECKS[@]}]="$metric"
            done
            ;;
        'batch' )
            BATCH_METRICS=(
                'java.batch.routageJob.*.status.COMPLETED.timed.count $FROM_10SEC&target=$metric&format=csv'
                'java.batch.routageJob.*.status.COMPLETED_WITH_WARNING.timed.count $FROM_10SEC&target=$metric&format=csv'
                'java.batch.routageJob.*.status.COMPLETED_WITH_ERROR.timed.count $FROM_10SEC&target=$metric&format=csv'
                'java.batch.routageJob.*.status.FAILED.timed.count $FROM_10SEC&target=$metric&format=csv'
                'java.batch.routageJob.*.status.COMPLETED.timed.max $FROM_10SEC&target=$metric&format=csv'
                'java.batch.routageJob.*.status.COMPLETED_WITH_WARNING.timed.max $FROM_10SEC&target=$metric&format=csv'
                'java.batch.routageJob.*.status.COMPLETED_WITH_ERROR.timed.max $FROM_10SEC&target=$metric&format=csv'
                'java.batch.routageJob.*.status.FAILED.timed.max $FROM_10SEC&target=$metric&format=csv'
                'java.batch.routageJob.*.status.COMPLETED.timed.min $FROM_10SEC&target=$metric&format=csv'
                'java.batch.routageJob.*.status.COMPLETED_WITH_WARNING.timed.min $FROM_10SEC&target=$metric&format=csv'
                'java.batch.routageJob.*.status.COMPLETED_WITH_ERROR.timed.min $FROM_10SEC&target=$metric&format=csv'
                'java.batch.routageJob.*.status.FAILED.timed.min $FROM_10SEC&target=$metric&format=csv'
                'java.batch.fullIndexerJob.*.status.COMPLETED.timed.count $FROM_10SEC&target=$metric&format=csv'
                'java.batch.fullIndexerJob.*.status.COMPLETED_WITH_WARNING.timed.count $FROM_10SEC&target=$metric&format=csv'
                'java.batch.fullIndexerJob.*.status.COMPLETED_WITH_ERROR.timed.count $FROM_10SEC&target=$metric&format=csv'
                'java.batch.fullIndexerJob.*.status.FAILED.timed.count $FROM_10SEC&target=$metric&format=csv'
                'java.batch.fullIndexerJob.*.status.COMPLETED.timed.max $FROM_10SEC&target=$metric&format=csv'
                'java.batch.fullIndexerJob.*.status.COMPLETED_WITH_WARNING.timed.max $FROM_10SEC&target=$metric&format=csv'
                'java.batch.fullIndexerJob.*.status.COMPLETED_WITH_ERROR.timed.max $FROM_10SEC&target=$metric&format=csv'
                'java.batch.fullIndexerJob.*.status.FAILED.timed.max $FROM_10SEC&target=$metric&format=csv'
                'java.batch.fullIndexerJob.*.status.COMPLETED.timed.min $FROM_10SEC&target=$metric&format=csv'
                'java.batch.fullIndexerJob.*.status.COMPLETED_WITH_WARNING.timed.min $FROM_10SEC&target=$metric&format=csv'
                'java.batch.fullIndexerJob.*.status.COMPLETED_WITH_ERROR.timed.min $FROM_10SEC&target=$metric&format=csv'
                'java.batch.fullIndexerJob.*.status.FAILED.timed.min $FROM_10SEC&target=$metric&format=csv'
            )
            for metric in "${BATCH_METRICS[@]}"
            do
                CHECKS[${#CHECKS[@]}]="$metric"
            done
            ;;
        'imap' )
            IMAP_METRICS=(
                'business.imap.imap_failed.count $FROM_1DAY&target=$metric&format=csv'
                'business.imap.imap_failed.failed $FROM_1DAY&target=$metric&format=csv'
                'business.imap.imap_inbox.count $FROM_1DAY&target=$metric&format=csv'
                'business.imap.imap_inbox.failed $FROM_1DAY&target=$metric&format=csv'
                'business.imap.imap_import.count $FROM_1DAY&target=$metric&format=csv'
                'business.imap.imap_import.failed $FROM_1DAY&target=$metric&format=csv'
                'business.imap.imap_mail2digiposte.count $FROM_1DAY&target=$metric&format=csv'
                'business.imap.imap_mail2digiposte.failed $FROM_1DAY&target=$metric&format=csv'
                'business.imap.imap_mail2digiposte_failed.count $FROM_1DAY&target=$metric&format=csv'
                'business.imap.imap_mail2digiposte_failed.failed $FROM_1DAY&target=$metric&format=csv'
                'business.imap.imap_spam.count $FROM_1DAY&target=$metric&format=csv'
                'business.imap.imap_spam.failed $FROM_1DAY&target=$metric&format=csv'
                'business.imap.imap_junk.count $FROM_1DAY&target=$metric&format=csv'
                'business.imap.imap_junk.failed $FROM_1DAY&target=$metric&format=csv'
            )
            for metric in "${IMAP_METRICS[@]}"
            do
                CHECKS[${#CHECKS[@]}]="$metric"
            done
            ;;
        'solr' )
            SOLR_METRICS=(
                'business.solr.time $FROM_10MIN&target=$metric&format=csv'
                'business.solr.ok $FROM_10MIN&target=$metric&format=csv'
                'business.solr.warning $FROM_10MIN&target=$metric&format=csv'
                'business.solr.failed $FROM_10MIIN&target=$metric&format=csv'
                'business.solr.critical $FROM_10MIN&target=$metric&format=csv'
            )
            for metric in "${SOLR_METRICS[@]}"
            do
                CHECKS[${#CHECKS[@]}]="$metric"
            done
            ;;
        'elasticsearch' )
            ELASTICSEARCH_METRICS=(
                'logging.elasticsearch.index_size $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.docs_count $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.docs_deleted $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.indexing_total_count $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.indexing_total_time_ms $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.get_total_count $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.get_total_time_ms $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.query_total_count $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.query_total_time_ms $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.refresh_total_count $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.refresh_total_time_ms $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.flush_total_count $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.flush_total_time_ms $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.merges_total_count $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.merges_total_time_ms $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.cpu $FROM_10SEC&target=$metric&format=csv'
                'logging.elasticsearch.file_descriptors $FROM_10SEC&target=$metric&format=csv'
            )
            for metric in "${ELASTICSEARCH_METRICS[@]}"
            do
                CHECKS[${#CHECKS[@]}]="$metric"
            done
            ;;
        'mongodb' )
            MONGODB_METRICS=(
                'logging.mongodb.mem_resident $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.mem_virtual $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.mem_mapped $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.heap_usage_bytes $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.insert $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.query $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.update $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.delete $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.command $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.flushes_count $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.flushes_total_ms $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.flushes_average_ms $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.bytes_in $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.bytes_out $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.num_requests $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.cursors_total_open $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.cursors_timed_out $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.connections_current $FROM_10SEC&target=$metric&format=csv'
                'logging.mongodb.connections_available $FROM_10SEC&target=$metric&format=csv'
            )
            for metric in "${MONGODB_METRICS[@]}"
            do
                CHECKS[${#CHECKS[@]}]="$metric"
            done
            ;;
    esac
done

nbCheckedMetrics=0
nbMetricsKO=0
for metricCheck in "${CHECKS[@]}"
do
    metric="$ENVIRONMENT.$SERVER.$(echo "$metricCheck" | awk '{print $1}')"
    url="https://$BASE_URL/render?$(echo "$metricCheck" | awk '{print $2}')"
    url=$(eval "echo \"$url\"")

    metricValue=$(curl -E $CERTIFICATE -u $USER "$url" 2> /dev/null | sed ':a;N;$!ba;s/[\r]\n/\\n/g')

    # Check if the metric exists
    if ! check_test_critical "echo -e \"$metricValue\"" "${metric//\*/.*}"
    then
        display_error "The metric $metric ($url) cannot be found for $SERVER"
        ((nbMetricsKO++))
#    else
#        # Check that the value of the metric is greater than 0
#        if ! echo "$url" | grep "target=$metric" > /dev/null
#        then
#            if ! check_test_critical \
#                "echo -e \"$metricValue\" | sed 's/.\+,//g' | awk '{ if (\$1 >= 0.1) { print "1"; } else  { print "0"; } }'" \
#                "1"
#            then
#                display_error "The metric $metric ($url) equals to 0 for $SERVER"
#            fi
#        fi
    fi
    ((nbCheckedMetrics++))
done

check_exit "All the metrics ($nbCheckedMetrics) seems good for $SERVER" "$nbMetricsKO metrics have not been found ($nbCheckedMetrics metrics checked) for $SERVER"
