#! /bin/bash

# Author Damien PELLISSON

source ../../../LIBS/display.fct
source ../../../LIBS/check.fct

URLS_WITH_CERTIF=$1
ULS_WITHOUT_CERTIF=$2
URLS="$URLS_WITH_CERTIF $URL_WITHOUT_CERTIF"

check_init "security"

for url in $URLS
do
    # Check the redirection from 80 to 443
    response=$(curl -sil ${url/https:/http:})
    if ! check_test_critical "echo \"$response\"" "302 Found" || ! check_test_critical "echo \"$response\"" "$url"
    then
        display_error "$url is not redirected on https"
    fi

    if echo $URLS_WITH_CERTIF | grep $url > /dev/null
    then
        # Check the site is not accessible without certificate
        response=$(curl -sil $url)
        if ! check_test_critical "echo \"$response\"" "!HTTP/1.1 (200|3..)"
        then
            display_error "$url is accessible without certificate"
        fi
        # Check the site is accessible with certificate
        if ! check_test_critical \
            "curl -sil $url --user digiposte:Digi_Pass --cert ../../../RESOURCES/CERTIFICATES/certificat-acces-admin.pem:digiposte" \
            "HTTP/1.1 (200|3..)"
        then
            display_error "$url is not accessible with certificate"
        fi
    else
        # Check the site is accessible without certificate
        if ! check_test_critical "curl -sil $url" "HTTP/1.1 (200|3..)"
        then
            display_error "$url is not accessible without certificate"
        fi
    fi
done

check_exit "Security for all URLs checked" ""
