# Script de coregistration
<div id="description"></div>
## Description du script 

Le script `volumetrie_coregistration.sh` permet de tester la volumétrie du batch de coregistration.

Le script a pour paramètres obligatoires :
	
	-n <NB>
        Volumétrie à tester. Doit être supérieure à 0
    -s <SENDER_PID>, --sender <SENDER_PID>
        PID de l'émetteur
	-seq <SEQUENCE>
        Séquence de la coregistration. Doit être de la forme 9999
   
Le script génère 2 fichiers (un XML `<SENDER_PID>_ddmmyyyy_<SEQUENCE>.xml` et un SHA1 `<SENDER_PID>_ddmmyyyy_<SEQUENCE>.sha1`) dans le dossier `TEMP/` qu'il faudra ensuite uploader sur le sFTP correspondant.

### Exemple de commande

	./volumetrie_coregistration.sh -n 1000 -s 9738585 -seq 0001

La commande ci-dessus va générer les fichiers de coregistration de **1000** comptes pour l'émetteur (pid=**9738585**). La séquence **0001** indique qu'il s'agit de la première coregistration pour cet émetteur.

## Mode opératoire de test en volumétrie sur TMC
> **Prérequis**  
> Disposer d'un émetteur configuré pour le coregistration. Il existe normalement sur TMC l'émetteur **test_coreg** (pid=9738585)  
> Disposer d'un accès au sFTP de TMC 

#### Mode opératoire
1. Générer le(s) fichier(s) de coregistration souhaité(s) : cf. <a href="#description">Description du script</a>
2. Déposer le(s) fichier(s) (xml+sha1) dans le répertoire `coregistration/9738585/in` du sFTP de TMC
3. Déplacer le(s) fichier(s) (xml+sha1) vers le répertoire `coregistration/9738585/in/a_traiter`
4. Attendre que le batch traite le(s) fichier(s)
5. Vérifier le résultat du test (cf. ci-dessous)

#### Vérification du test
1. Vérifier que le(s) fichier(s) (xml+sha1) a(ont) été déplacé(s) dans le répertoire `coregistration/9738585/in/a_traiter/ok`
2. Récupérer le(s) fichier(s) xml "out" du répertoire `coregistration/9738585/out`
3. Vérifier que le(s) fichier(s) "out" contiennent le bon nombre de `<cr:status>OK</cr:status>`  
*Exemple* :

		cat 9738585_12062014_0002_OUT.xml | grep -o "<cr:status>OK</cr:status>" | wc -l