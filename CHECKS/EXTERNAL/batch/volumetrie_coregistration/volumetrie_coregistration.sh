#! /bin/bash

#***************************************************#
#                     FONCTIONS                     #
#***************************************************#

function print_usage()
{
cat << EOF

Usage:
    $(basename $1) [OPTIONS]

Options:
    -n <NB_USERS>
        Volumétrie à tester. Doit être supérieure à 0
    -s <SENDER_PID>, --sender <SENDER_PID>
        PID de l émetteur
    -seq <SEQUENCE>
        Séquence de la coregistration. Doit être de la forme 9999
EOF
}

function mysqlexec()
{
    ssh ${DB_SERVER} "mysql -u${_G_SRV_BDD_LOG_} -p${_G_SRV_BDD_PASS_} -h${_G_SRV_BDD_HOST_} -P${_G_SRV_BDD_PORT_} ${_G_SRV_BDD_NAME_} -Bse \"$1\""
}

function coregistration()
{
    readonly TIMESTAMP=$(date +%Y%m%d%H%M%S);
    readonly TIMESTAMP2=$(date +%d%m%Y);

    readonly XML_FILENAME="TEMP/${SENDER_PID}_${TIMESTAMP2}_${SEQUENCE}.xml"
    readonly SHA1_FILENAME="TEMP/${SENDER_PID}_${TIMESTAMP2}_${SEQUENCE}.sha1"

    readonly HEADER=TEMPLATES_XML/header.xml
    readonly ENTRY=TEMPLATES_XML/entry.xml
    readonly FOOTER=TEMPLATES_XML/footer.xml
    [[ ! -r ${HEADER} || ! -r ${ENTRY} || ! -r ${FOOTER} ]] && echo -e "\nAt least one of the template file is missing or not readable. Please check the installation." && exit 1

    echo "Création du fichier XML..."

    # Header
    sed -e "s@__SENDER_PID__@${SENDER_PID}@g" ${HEADER} > ${XML_FILENAME}

    for i in `seq ${NB_USERS}`
    do
        USER_ID=$((${TIMESTAMP} + $i));
        sed -e "s/__USER_ID__/${USER_ID}/g" \
            -e "s/__EMAIL__/${EMAIL}/g" ${ENTRY} >> ${XML_FILENAME}
    done

    # Footer
    cat ${FOOTER} >> ${XML_FILENAME}

    # Calcul de l'empreinte sha1 et création du fichier .sha1
    sha1sum ${XML_FILENAME} | awk '{print $1}' > ${SHA1_FILENAME}


    # TODO : upload sur le sFTP

    echo "Les fichiers sont disponibles :"
    echo "${XML_FILENAME}"
    echo "${SHA1_FILENAME}"
}



#***************************************************#
#                       MAIN                        #
#***************************************************#

while [[ $# -gt 0 ]]
do
    case "$1" in
        '-n' )
            readonly NB_USERS=$2
            shift 2
            ;;
        '-s' | '--sender' )
            readonly SENDER_PID=$2
            shift 2
            ;;
        '-seq' )
            readonly SEQUENCE=$2
            shift 2
            ;;
        * )
            print_usage "$0"
            exit 1
    esac
done

if  [[ -z "${NB_USERS}" ]] || \
    [[ -z "${SENDER_PID}" ]] || \
    [[ -z "${SEQUENCE}" ]]
then
    print_usage "$0"
    exit 1
fi


# Vérification des paramètres
[[ ${NB_USERS} -le 0 ]] && echo "La volumétrie doit être supérieure 0" && exit 1
echo ${SEQUENCE} | grep -E "^[0-9]{4}$" > /dev/null ; [[ $? -ne 0 ]] && echo "La séquence doit être de la forme 9999" && exit 1


readonly EMAIL="pcazajous.dgp@gmail.com"

echo "Récupération de la configuration..."

# Récupération des infos de connexion BDD
BACK_SERVER="back1-tmc"
DB_SERVER="db1-tmc"
eval $(ssh ${BACK_SERVER} "php /data/usr/local/apps/digiposte-php/current/php/legacy/digiposte/batch/batch_config.php _G_SRV_BDD_LOG_ _G_SRV_BDD_PASS_ _G_SRV_BDD_HOST_")
_G_SRV_BDD_NAME_=digiposte
_G_SRV_BDD_PORT_=3306


# Recherche de l'émetteur en BDD
echo "Recherche de l'émetteur..."
sender=$(mysqlexec "SELECT sender.name, sender.id
                    FROM sender
                    WHERE sender.pid = '${SENDER_PID}'")
[[ "${sender}" == "" ]] && echo -e "Sender non trouvé" && exit 1

readonly SENDER_NAME=$(echo "${sender}" | cut -d $'\t' -f 1)
readonly SENDER_ID=$(echo "${sender}" | cut -d $'\t' -f 2)
echo -e "Nom émetteur: ${SENDER_NAME}\tPID: ${SENDER_PID}"

# Création du dossier TEMP/ s'il n'existe pas
if ! [[ -d "TEMP" ]]
then
    mkdir TEMP && mkdir TEMP/DEMO && mkdir TEMP/PREINSCRIPTION
fi

# Appel de la fonction de coregistration
coregistration