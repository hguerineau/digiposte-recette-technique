#!/bin/bash

echo -n "Getting the configuration."

function print_usage()
{
cat << EOF

Usage:
    $(basename $1) [OPTIONS]

Options:
    -a <ARCHIVE>, --archive <ARCHIVE>
        Path to the archive to send
    --back-host <BACK_HOST>
        The backoffice server name (e.g.: back1-tmc)
    --db-host <DB_HOST>
        The database server name (e.g.: db2-tmc)
    -h <FTP_URL>, --ftp-url <FTP_URL>
        The URL of the FTP server to which the archive will be sent
    -s <SENDER>, --sender <SENDER>
        FTP username

EOF
}

while [[ $# -gt 0 ]]
do
    case "$1" in
        '-a' | '--archive' )
            readonly ARCHIVE=$2
            [[ ! -r ${ARCHIVE} ]] && echo -e "\n${ARCHIVE} is not readable" && exit 1
            shift 2
            ;;
        '--back-host' )
            readonly BACK_SERVER=$2
            shift 2
            ;;
        '--db-host' )
            readonly DB_SERVER=$2
            shift 2
            ;;
        '-h' | '--ftp-url' )
            readonly SENDER_FTP_URL=$(echo "$2" | cut -d : -f 1)
            readonly SENDER_FTP_PORT=$(echo "$2" | cut -d : -f 2)
            shift 2
            ;;
        '-s' | '--sender' )
            readonly SENDER_FTP_USER=$2
            shift 2
            ;;
        * )
            print_usage "$0"
            exit 1
    esac
done

if [[ -z "${ARCHIVE}" ]] || \
    [[ -z "${DB_SERVER}" ]] || \
    [[ -z "${BACK_SERVER}" ]] || \
    [[ -z "${SENDER_FTP_URL}" ]] || \
    [[ -z "${SENDER_FTP_PORT}" ]] || \
    [[ -z "${SENDER_FTP_USER}" ]]
then
    print_usage "$0"
    exit 1
fi

readonly OUTPUT_FILENAME="$(basename ${ARCHIVE})_$(date +%s)"

eval $(ssh ${BACK_SERVER} "php /data/usr/local/apps/digiposte-php/current/php/legacy/digiposte/batch/batch_config.php _G_SRV_BDD_LOG_ _G_SRV_BDD_PASS_ _G_SRV_BDD_HOST_")
_G_SRV_BDD_NAME_=digiposte
_G_SRV_BDD_PORT_=3306

function mysqlexec()
{
    ssh ${DB_SERVER} "mysql -u${_G_SRV_BDD_LOG_} -p${_G_SRV_BDD_PASS_} -h${_G_SRV_BDD_HOST_} -P${_G_SRV_BDD_PORT_} ${_G_SRV_BDD_NAME_} -Bse \"$1\""
}
#function curlexec()
#{
#    curl -sil --cookie ${COOKIE} --user-agent Mozilla/4.0 -E ${WEB_CERT} -u ${WEB_USER}:${WEB_PSWD} $*
#}

echo "."

readonly LOT_ID=$(uuidgen | tr -d - | cut -c1-16)
echo "Updating lot_id in the archive... (${LOT_ID})"

[[ -f TEMP/flux.xml ]]  && rm TEMP/flux.xml
unzip ${ARCHIVE} flux.xml -d TEMP/ > /dev/null
mv TEMP/flux.xml TEMP/flux.xml.ori
sed -e "s@__UID__@${LOT_ID}@g" \
    -e "s@__TITLE__@${OUTPUT_FILENAME}@g" \
    TEMP/flux.xml.ori > TEMP/flux.xml
zip -j ${ARCHIVE} TEMP/flux.xml > /dev/null

echo "Sending the archive by FTP"

#sent=false
#nb_tries=10
#until ${sent} || [[ $nb_tries -le 0 ]]
#do
#    # Connexion
#    curlexec --cookie-jar ${COOKIE} ${BACKUP_URL}/compte/authenticate \
#        -F approved="" \
#        -F dpaction=valider \
#        -F bo_login=${BACKUP_USER} \
#        -F bo_pwd=${BACKUP_PSWD} > /dev/null
#
#    # Listen network to know the upload progress
#    (
#        trap 'sleep .2; responseReceived=true' SIGTERM
#        responseReceived=false
#        sleep 0.8 && sudo tcpdump dst host ${BACKUP_IP} and port 443 -l 2> /dev/null | grep 'Flags.*seq' | while read packet && ! ${responseReceived}
#        do
#            uploadedSize=$(echo "${packet}" | awk '{print $9}' | cut -d : -f 2 | tr -dc '[:digit:]')
#            if [[ ${uploadedSize} -ge 0 ]] && [[ ${uploadedSize} -le ${ARCHIVE_SIZE} ]]
#            then
#                echo -ne "\rEnvoi de l'archive au backoffice :\t$(( ${uploadedSize} * 100 / ${ARCHIVE_SIZE} ))%"
#            fi
#        done
#    ) &
#    listenerId="$!"
#
#    # Envoi des donnees au formulaire
#    beforeSending=$(date +%s)
#    curlexec ${BACKUP_URL}/routage/v6/send \
#        -F archive=@${ARCHIVE} \
#        -F request_ws=true | grep '"message": "Le lot va être traité"' > /dev/null && sent=true
#    afterSending=$(date +%s)
#
#    kill ${listenerId}
#    fg ${listenerId} > /dev/null 2>&1
#    sleep .2
#
#    if $sent
#    then
#        echo -e "\rEnvoi de l'archive au backoffice :\t100%"
#    else
#        echo -e "\rEnvoi de l'archive au backoffice :\tFAILED"
#    fi
#
#    (( nb_tries-- ))
#done
#
#if ! $sent
#then
#    exit 1
#fi

beforeSending=$(date +%s)
sshpass -p docapost sftp -P ${SENDER_FTP_PORT} ${SENDER_FTP_USER}@${SENDER_FTP_URL} << EOF
cd /input
put ${ARCHIVE}
bye
EOF
afterSending=$(date +%s)

cp ${ARCHIVE} ${ARCHIVE}.sent
mv TEMP/flux.xml.ori TEMP/flux.xml
zip -j ${ARCHIVE} TEMP/flux.xml > /dev/null
rm TEMP/flux.xml


echo "Attente du traitement par docapost"

while echo -e 'ls input/\nbye' | \
    sshpass -p docapost sftp -P ${SENDER_FTP_PORT} ${SENDER_FTP_USER}@${SENDER_FTP_URL} 2>&1 | \
    grep "$(basename ${ARCHIVE})" > /dev/null
do
    sleep 1
done


echo -n "Attente de l'execution du batch de routage"

readonly NB_FILES_TO_ROUTE=$(( $(unzip -l ${ARCHIVE} | tail -1 | awk '{print $2}') -  1 ))
readonly NB_TRIES=$([[ ${NB_FILES_TO_ROUTE} -gt 100 ]] && echo 100 || echo ${NB_FILES_TO_ROUTE})
readonly DELAYING_CHECK=$(( ${NB_FILES_TO_ROUTE} / ${NB_TRIES} ))


# Attente du declenchement du batch"
sleep 60

beforeBatch=$(date +%s)
for i in $(seq ${NB_TRIES})
do
    sleep ${DELAYING_CHECK}

    nbDocumentsRoutes=$(mysqlexec "SELECT COUNT(*)
            FROM document
            WHERE title = '${OUTPUT_FILENAME}'")

    echo -ne "\rAttente de l'execution du batch de routage (${i}) :\t$(( ${nbDocumentsRoutes} * 100 / ${NB_FILES_TO_ROUTE} ))%"

    [[ ${nbDocumentsRoutes} -ge ${NB_FILES_TO_ROUTE} ]] && break
done
afterBatch=$(date +%s)

echo


sendDuration=$(( ${afterSending} - $beforeSending ))
batchDuration=$(( ${afterBatch} - $beforeBatch ))
echo "Reporting :"
echo "    - Temps nécessaire a l'envoi : ${sendDuration} secondes"
echo "    - Temps nécessaire au batch : entre $(( ${batchDuration} - ${DELAYING_CHECK} )) et ${batchDuration} secondes"
echo "    - Nombre de documents à router : ${NB_FILES_TO_ROUTE}"
echo "    - Nombre de documents présents en base de données : ${nbDocumentsRoutes}"
echo "    - Nombre de documents routés par seconde : $(echo "scale=2; ${nbDocumentsRoutes} / ${batchDuration}" | bc)"
