#! /bin/bash

#***************************************************#
#                     FONCTIONS                     #
#***************************************************#

function print_usage()
{
cat << EOF

Usage:
    $(basename $1) [OPTIONS]

Options:
    -m <MODE>
        Mode de provisioning :
            demo
            preinscription
    -n <NB_USERS>
        Volumétrie à tester. Doit être supérieure à 0
    -s <SENDER_PID>, --sender <SENDER_PID>
        PID de l émetteur
EOF
}

function mysqlexec()
{
    ssh ${DB_SERVER} "mysql -u${_G_SRV_BDD_LOG_} -p${_G_SRV_BDD_PASS_} -h${_G_SRV_BDD_HOST_} -P${_G_SRV_BDD_PORT_} ${_G_SRV_BDD_NAME_} -Bse \"$1\""
}

function demo()
{
    CAMPAIGN_ID=$(uuidgen | tr -d - | cut -c1-16)
    SEQUENCE="0001"

    readonly XML_FILENAME="TEMP/DEMO/${CAMPAIGN_ID}_${DAY}${MONTH}${YEAR}_${SEQUENCE}.xml"
    readonly SHA1_FILENAME="TEMP/DEMO/${CAMPAIGN_ID}_${DAY}${MONTH}${YEAR}_${SEQUENCE}.sha1"
    readonly ZIP_FILENAME="TEMP/DEMO/${CAMPAIGN_ID}_${DAY}${MONTH}${YEAR}_${SEQUENCE}.zip"
    readonly HEADER=TEMPLATES_XML/DEMO/header.xml
    readonly ENTRY=TEMPLATES_XML/DEMO/entry.xml
    readonly FOOTER=TEMPLATES_XML/DEMO/footer.xml
    [[ ! -r ${HEADER} || ! -r ${ENTRY} || ! -r ${FOOTER} ]] && echo -e "\nAt least one of the template file is missing or not readable. Please check the installation." && exit 1

    echo "Création du fichier XML..."

    # Header
    sed -e "s@__DAY__@${DAY}@g" \
        -e "s@__MONTH__@${MONTH}@g" \
        -e "s@__YEAR__@${YEAR}@g" \
        -e "s@__EXPYEAR__@$(( ${YEAR} + 50 ))@g" \
        -e "s@__HOUR__@${HOUR}@g" \
        -e "s@__MINUTE__@${MINUTE}@g" \
        -e "s@__SECONDE__@${SECOND}@g" \
        -e "s@__EXPDATE__@${EXPDATE}@g" \
        -e "s@__CAMPAIGN_ID__@${CAMPAIGN_ID}@g" \
        -e "s@__SEQUENCE__@${SEQUENCE}@g" \
        -e "s@__SENDER_PID__@${SENDER_PID}@g" ${HEADER} > ${XML_FILENAME}

    for i in `seq ${NB_USERS}`
    do
        sed -e "s/__EMAIL__/${EMAIL}/g" \
            -e "s/__CLIENT_ID__/${HOUR}${MINUTE}${SECOND}${i}/g" ${ENTRY} >> ${XML_FILENAME}
    done

    # Footer
    cat ${FOOTER} >> ${XML_FILENAME}

    # Calcul de l'empreinte sha1 et création du fichier .sha1
    sha1sum ${XML_FILENAME} | awk '{print $1}' > ${SHA1_FILENAME}

    # Zip
    zip -j ${ZIP_FILENAME} ${XML_FILENAME} ${SHA1_FILENAME} > /dev/null

    echo -e "Création de l'archive:\t100%"

    rm ${XML_FILENAME} ${SHA1_FILENAME}

    # TODO : upload sur le sFTP

    echo "L'archive est disponible : ${ZIP_FILENAME}"
}

function preinscription()
{
    CAMPAIGN_ID=$(uuidgen | tr -d - | cut -c1-16)
    SEQUENCE="0001"

    readonly XML_FILENAME="TEMP/PREINSCRIPTION/${CAMPAIGN_ID}_${DAY}${MONTH}${YEAR}_${SEQUENCE}.xml"
    readonly SHA1_FILENAME="TEMP/PREINSCRIPTION/${CAMPAIGN_ID}_${DAY}${MONTH}${YEAR}_${SEQUENCE}.sha1"
    readonly ZIP_FILENAME="TEMP/PREINSCRIPTION/${CAMPAIGN_ID}_${DAY}${MONTH}${YEAR}_${SEQUENCE}.zip"
    readonly HEADER=TEMPLATES_XML/PREINSCRIPTION/header.xml
    readonly ENTRY=TEMPLATES_XML/PREINSCRIPTION/entry.xml
    readonly FOOTER=TEMPLATES_XML/PREINSCRIPTION/footer.xml
    [[ ! -r ${HEADER} || ! -r ${ENTRY} || ! -r ${FOOTER} ]] && echo -e "\nAt least one of the template file is missing or not readable. Please check the installation." && exit 1

    echo "Création du fichier XML..."

    # Header
    sed -e "s@__DAY__@${DAY}@g" \
        -e "s@__MONTH__@${MONTH}@g" \
        -e "s@__YEAR__@${YEAR}@g" \
        -e "s@__EXPYEAR__@$(( ${YEAR} + 50 ))@g" \
        -e "s@__HOUR__@${HOUR}@g" \
        -e "s@__MINUTE__@${MINUTE}@g" \
        -e "s@__SECONDE__@${SECOND}@g" \
        -e "s@__EXPDATE__@${EXPDATE}@g" \
        -e "s@__CAMPAIGN_ID__@${CAMPAIGN_ID}@g" \
        -e "s@__SEQUENCE__@${SEQUENCE}@g" \
        -e "s@__SENDER_PID__@${SENDER_PID}@g" ${HEADER} > ${XML_FILENAME}

    for i in `seq ${NB_USERS}`
    do
        sed -e "s/__EMAIL__/${EMAIL}/g" \
            -e "s/__CLIENT_ID__/${HOUR}${MINUTE}${SECOND}${i}/g" ${ENTRY} >> ${XML_FILENAME}
    done

    # Footer
    cat ${FOOTER} >> ${XML_FILENAME}

    # Calcul de l'empreinte sha1 et création du fichier .sha1
    sha1sum ${XML_FILENAME} | awk '{print $1}' > ${SHA1_FILENAME}

    # Zip
    zip -j ${ZIP_FILENAME} ${XML_FILENAME} ${SHA1_FILENAME} > /dev/null

    echo -e "Création de l'archive:\t100%"

    rm ${XML_FILENAME} ${SHA1_FILENAME}

    # TODO : upload sur le sFTP

    echo "L'archive est disponible : ${ZIP_FILENAME}"
}

#***************************************************#
#                       MAIN                        #
#***************************************************#

while [[ $# -gt 0 ]]
do
    case "$1" in
        '-m' )
            readonly MODE=$2
            shift 2
            ;;
        '-n' )
            readonly NB_USERS=$2
            shift 2
            ;;
        '-s' | '--sender' )
            readonly SENDER_PID=$2
            shift 2
            ;;
        * )
            print_usage "$0"
            exit 1
    esac
done

if  [[ -z "${MODE}" ]] || \
    [[ -z "${NB_USERS}" ]] || \
    [[ -z "${SENDER_PID}" ]]
then
    print_usage "$0"
    exit 1
fi


# Vérification des paramètres
[[ ${MODE} != "demo" ]] && [[ ${MODE} != "preinscription" ]] && echo "Le mode doit valoir \"demo\" ou \"preinscription\"" && exit 1
[[ ${NB_USERS} -le 0 ]] && echo "La volumétrie doit être supérieure 0" && exit 1


# Date
readonly TIMESTAMP=$(date +%s)
readonly FULLDATE=$(date --date=@${TIMESTAMP} +%Y%m%d-%H%M%S)
readonly YEAR=$(date --date=@${TIMESTAMP} +%Y)
readonly MONTH=$(date --date=@${TIMESTAMP} +%m)
readonly DAY=$(date --date=@${TIMESTAMP} +%d)
readonly HOUR=$(date --date=@${TIMESTAMP} +%H)
readonly MINUTE=$(date --date=@${TIMESTAMP} +%M)
readonly SECOND=$(date --date=@${TIMESTAMP} +%S)
readonly TIMEZONE=$(date +%:z)
readonly EXPDATE=$(date --date "+60 day" +%Y-%m-%d)

readonly EMAIL="pcazajous.dgp@gmail.com"

echo "Récupération de la configuration..."

# Récupération des infos de connexion BDD
BACK_SERVER="back1-tmc"
DB_SERVER="db1-tmc"
eval $(ssh ${BACK_SERVER} "php /data/usr/local/apps/digiposte-php/current/php/legacy/digiposte/batch/batch_config.php _G_SRV_BDD_LOG_ _G_SRV_BDD_PASS_ _G_SRV_BDD_HOST_")
_G_SRV_BDD_NAME_=digiposte
_G_SRV_BDD_PORT_=3306


# Recherche de l'émetteur en BDD
echo "Recherche de l'émetteur..."
sender=$(mysqlexec "SELECT sender.name, sender.id
                    FROM sender
                    WHERE sender.pid = '${SENDER_PID}'")
[[ "${sender}" == "" ]] && echo -e "Sender non trouvé" && exit 1

readonly SENDER_NAME=$(echo "${sender}" | cut -d $'\t' -f 1)
readonly SENDER_ID=$(echo "${sender}" | cut -d $'\t' -f 2)
echo -e "Nom émetteur: ${SENDER_NAME}\tPID: ${SENDER_PID}"

# Création du dossier TEMP/ s'il n'existe pas
if ! [[ -d "TEMP" ]]
then
    mkdir TEMP && mkdir TEMP/DEMO && mkdir TEMP/PREINSCRIPTION
fi

# Appel de la fonction choisie
case ${MODE} in
    "demo" )
        demo
        ;;
    "preinscription" )
        preinscription
        ;;
    * )
        print_usage "$0"
        exit 1
esac