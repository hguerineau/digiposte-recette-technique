# Script de provisioning
<div id="description"></div>
## Description du script 

Le script `volumetrie_provisioning.sh` permet de tester la volumétrie du batch de provisioning selon ses 2 modes :

- création de comptes de démo (demo)
- préinscription (preinscription)

Le script génère un fichier zip qu'il faudra déposer sur le sFTP de provisioning.

Le script a pour paramètres obligatoires :
    
    -m <MODE>
        Mode de provisioning :
            demo
            preinscription
    -n <NB>
        Volumétrie à tester. Doit être supérieure à 0
    -s <SENDER_PID>, --sender <SENDER_PID>
        PID de l'émetteur
    
### Exemple de commande
#### Création de comptes de démo

    ./volumetrie_provisioning.sh -m demo -n 1000 -s pca24011

La commande ci-dessus va générer un zip de création de **1000** comptes de démonstration ( **demo** ) via le batch de provisioning pour l'émetteur (pid=**pca24011**).


#### Préinscription

    ./volumetrie_provisioning.sh -m preinscription -n 1000 -s pca24011

La commande ci-dessus va générer un zip de création de **1000** préinscriptions ( **preinscription** ) via le batch de provisioning pour l'émetteur (pid=**pca24011**).


## Mode opératoire de test en volumétrie sur TMC

> **Prérequis**  
> Disposer d'un émetteur configuré pour le provisioning. Il existe normalement sur TMC l'émetteur **emetteur110214** (pid=pca24011)  
> Disposer d'un accès au sFTP de TMC

#### Mode opératoire
1. Générer le(s) fichier(s) de provisioning souhaité(s) : cf. <a href="#description">Description du script</a>
2. Déposer le(s) fichier(s) zip dans le répertoire `provisioning/pca24011/in` du sFTP de TMC
3. Déplacer le(s) fichier(s) zip vers le répertoire `provisioning/pca24011/in/a_traiter`
4. Attendre que le batch traite le(s) fichier(s)
5. Vérifier le résultat du test (cf. ci-dessous)

#### Vérification du test
1. Vérifier que le(s) fichier(s) zip a(ont) été déplacé(s) dans le répertoire `provisioning/pca24011/in/a_traiter/ok`
2. Récupérer le(s) fichier(s) xml "out" du répertoire `provisioning/pca24011/out`
3. Vérifier que le(s) fichier(s) "out" contiennent le bon nombre de `<status>OK</status>`  
*Exemple* :

        cat cc1b373938d9430e_12062014_0001_out.xml | grep -c "<status>OK</status>"