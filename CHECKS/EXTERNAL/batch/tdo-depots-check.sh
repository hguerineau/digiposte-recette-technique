#! /bin/bash

# Author    Damien PELLISSON

source ../../../LIBS/display.fct
source ../../../LIBS/check.fct

check_init "tdb-depots"

BACK_SERVER=$1
DB_SERVER=$2

# Retrieve MySQL credentials
eval $(ssh ${BACK_SERVER} "php /data/usr/local/apps/digiposte-php/current/php/legacy/digiposte/batch/batch_config.php _G_SRV_BDD_LOG_ _G_SRV_BDD_PASS_ _G_SRV_BDD_HOST_")
_G_SRV_BDD_NAME_=digiposte
_G_SRV_BDD_PORT_=3306

ssh -n ${DB_SERVER} "mysql -u${_G_SRV_BDD_LOG_} -p${_G_SRV_BDD_PASS_} -h${_G_SRV_BDD_HOST_} -P${_G_SRV_BDD_PORT_} ${_G_SRV_BDD_NAME_} -Bs << EOF
    SELECT DATE(dispatch_lot.created_at) as date,
        sender.id as sender_id,
        SUM(CASE dispatch_lot.status WHEN 0 THEN 1 ELSE 0 END) as flow_count_ok,
        SUM(CASE dispatch_lot.status WHEN 0 THEN dispatch_lot.entries_ok ELSE 0 END) as document_count_ok,
        SUM(CASE dispatch_lot.status WHEN 1 THEN 1 ELSE 0 END) as flow_count_warning,
        SUM(CASE dispatch_lot.status WHEN 1 THEN dispatch_lot.entries_ok ELSE 0 END) as document_count_warning_ok,
        SUM(CASE dispatch_lot.status WHEN 1 THEN dispatch_lot.entries_ko ELSE 0 END) as document_count_warning_ko,
        SUM(CASE dispatch_lot.status WHEN 2 THEN 1 ELSE 0 END) as flow_count_ko,
        SUM(CASE dispatch_lot.status WHEN 2 THEN dispatch_lot.entries_ko ELSE 0 END) as document_count_ko
    FROM sender
    JOIN dispatch_lot ON sender.id = dispatch_lot.sender_id
    WHERE DATE(dispatch_lot.created_at) = CURDATE() - 1
    GROUP BY date, sender_id
    ORDER BY date, sender_id
EOF" > my-query.result

if [[ $? -eq 255 ]]
then
    display_error "Cannot connect ${DB_SERVER}"
    exit 2
fi

ssh -n ${DB_SERVER} "mysql -u${_G_SRV_BDD_LOG_} -p${_G_SRV_BDD_PASS_} -h${_G_SRV_BDD_HOST_} -P${_G_SRV_BDD_PORT_} ${_G_SRV_BDD_NAME_} -Bs << EOF
    SELECT DATE(date) as date,
        sender_id,
        flow_count_ok,
        document_count_ok,
        flow_count_warning,
        document_count_warning_ok,
        document_count_warning_ko,
        flow_count_ko,
        document_count_ko
    FROM report_dashboard
    WHERE DATE(date) = CURDATE() - 1
    ORDER BY date, sender_id
EOF" > dashboard.result

if ! check_test_critical "diff my-query.result dashboard.result" ""
then
    if [[ $(cat my-query.result | wc -l) -gt $(cat dashboard.result | wc -l) ]]
    then
        display_error "The batch batch-tdb-depots forget some flows"
    elif [[ $(cat my-query.result | wc -l) -lt $(cat dashboard.result | wc -l) ]]
    then
        display_error "The batch batch-tdb-depots create to many flows"
    else
        display_error "The batch batch-tdb-depots has errors in its counting"
    fi
fi

rm my-query.result dashboard.result

check_exit "The batch batch-tdb-depots works properly" ""
