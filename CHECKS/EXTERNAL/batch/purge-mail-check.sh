#! /bin/bash

# Author    Damien PELLISSON

source ../../../LIBS/display.fct
source ../../../LIBS/check.fct

check_init "purge-mail"

BACK_SERVER=$1
DB_SERVER=$2

eval $(ssh ${BACK_SERVER} "php /data/usr/local/apps/digiposte-php/current/php/legacy/digiposte/batch/batch_config.php _G_SRV_BDD_LOG_ _G_SRV_BDD_PASS_ _G_SRV_BDD_HOST_ _BDD_RETENTION_MAIL_")
_G_SRV_BDD_NAME_=digiposte
_G_SRV_BDD_PORT_=3306

nbOldMails=$(ssh ${DB_SERVER} "mysql -u${_G_SRV_BDD_LOG_} -p${_G_SRV_BDD_PASS_} -h${_G_SRV_BDD_HOST_} -P${_G_SRV_BDD_PORT_} ${_G_SRV_BDD_NAME_} -Bs << EOF
    SELECT COUNT(*)
    FROM mail
    WHERE type != 'internet'
      AND DATE(end_at) < CURDATE() - ${_BDD_RETENTION_MAIL_}
EOF")

check_test_critical "echo $nbOldMails" "0"

check_exit "The purge of old mails works properly" "$nbOldMails old mails are not deleted"
