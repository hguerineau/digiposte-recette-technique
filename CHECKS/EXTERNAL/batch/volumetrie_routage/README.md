# Procédure de routage

> **Veillez à bien faire attention à l'utilisateur et le répertoire dans lequel sont exécutées les commandes de cette procédure**

1. Compresser le dossier `volumetrie_routage/` et l'envoyer sur `deploy-tmc:/tmp/` et `web1-tmc:/tmp/` :
	
		user@localhost$ tar czvf volumetrie_routage.tgz volumetrie_routage/
		user@localhost$ scp volumetrie_routage.tgz deploy-tmc:/tmp/
		user@localhost$ scp volumetrie_routage.tgz web1-tmc:/tmp/

2. Aller sur `deploy-tmc:/tmp/` et décompresser `volumetrie_routage.tgz` :
		
		user@deploy-tmc:/tmp$ tar xzvf volumetrie_routage.tgz
		user@deploy-tmc:/tmp$ sudo chown -R admin:admin volumetrie_routage

3. Générer les archives voulues (cf. [generateArchive.sh](#generateArchive))

4. Compresser l'ensemble des archives :

		admin@deploy-tmc:/tmp/volumetrie_routage$ tar czvf TEMP.tgz TEMP/

5. Envoyer le tgz sur `web1-tmc` : 

		admin@deploy-tmc:/tmp/volumetrie_routage$ scp /tmp/volumetrie_routage/TEMP.tgz web1-tmc:/tmp/

6. Aller sur `web1-tmc:/tmp/` et décompresser `volumetrie_routage.tgz` : 

		user@web1-tmc:/tmp$ tar xzvf volumetrie_routage.tgz
		user@web1-tmc:/tmp$ sudo chown -R admin:admin volumetrie_routage

7. Décompresser le tgz : 

		admin@web1-tmc:/tmp$ tar xzvf /tmp/TEMP.tgz
		admin@web1-tmc:/tmp$ rm -rf /tmp/volumetrie_routage/TEMP
		admin@web1-tmc:/tmp$ mv /tmp/TEMP/ /tmp/volumetrie_routage/

8. Exécuter le script de routage (cf. [routeArchives.sh](#routeArchives))

9. Supprimer les dossiers de `deploy-tmc` et `web1-tmc` :

		user@deploy-tmc$ sudo rm -rf /tmp/volumetrie_routage
		user@deploy-tmc$ sudo rm -rf /tmp/volumetrie_routage.tgz
		user@web1-tmc$ sudo rm -rf /tmp/volumetrie_routage
		user@web1-tmc$ sudo rm -rf /tmp/volumetrie_routage.tgz
		user@web1-tmc$ sudo rm -rf /tmp/TEMP.tgz


# Description des scripts

<a id="generateArchive"></a>
## generateArchive.sh

Ce script permet de générer `<nb_archives>` archives contenant `<nb_plis>` plis
suivant le modèle `<template_pli>`.
> Ce script doit être lancé depuis la machine `deploy-tmc` avec le user `admin`.

#### Usage
	bash generateArchive.sh -a <nb_archives> -f <template_pli> -n <nb_plis> --back-host back1-tmc --db-host db2-tmc -s 3441

#### Exemple
	admin@deploy-tmc:/tmp/volumetrie_routage$ bash generateArchive.sh -a 5 -f TEMPLATES/FILE_TO_ROUTE/file_2K.pdf -n 10000 --back-host back1-tmc --db-host db2-tmc -s 3441

Cette commande va générer 5 archives de 10000 plis de 2ko.


<a id="routeArchives"></a>
## routeArchives.sh

Ce script permet de router (upload dans le coffre émetteur + notification d'un lot à traiter) toutes les archives présentes dans le dossier local `TEMP/` vers le coffre `<cfe_id>` de l'émetteur.
> Ce script doit être lancé depuis la machine `web1-tmc`.

#### Usage
	bash routeArchives.sh <cfe_id>

#### Exemple
	admin@web1-tmc:/tmp/volumetrie_routage$ bash routeArchives.sh 327875

Cette commande route toutes les archives présentes dans `TEMP/` dans le coffre de DORH (`cfe_id=327875`)
