#! /bin/bash

[ $# -ne 1 ] && echo "USAGE : $( basename $0 ) <NUMERO_DE_COFFRE>" && exit 1

[ ! -f library -o ! -x library ] && echo "Fichier de fonctions absent." && exit 1
    . library

for file in `ls TEMP/`
do
    # Upload du fichier dans le coffre
    [[ ! -f "TEMP/"$file || ! -r "TEMP/"$file ]] && echo "Le fichier $file n'est pas accessible." && exit 1
    uploadedFile="TEMP/"$file

    session=$( openSession $1 )
    targetId=
    ## Récupération de la liste de tous les dossiers du coffre + parcours sous pseudo forme de tableau.
    for dir in $( getCfeFoldersName ${session} | sed -e "s/${LINE_SEPARATOR}/ /g" )
    do
        [[ "$( echo ${dir} | cut -d "${COL_SEPARATOR}" -f 2 )" == "in" ]] && targetId=$( echo ${dir} | cut -d "${COL_SEPARATOR}" -f 1 )
    done

    if [[ "${targetId}" == "" ]]
    then
    ## Le dossier in n'existe pas; il faut le créer.
        targetId=$(createFolder ${session} "-1${COL_SEPARATOR}in" )
    fi

    [[ "${targetId}" == "" ]] && log "Le dossier in est introuvable et impossible à créer." && exit 1
    log "Le document va être déposé dans le conteneur ${targetId}."
    archiveMd5=$( md5sum ${uploadedFile} | awk '{print $1}' )
    archiveSha1=$( sha1sum ${uploadedFile} | awk '{print $1}' )
    log "Le fichier ${uploadedFile} a : "
    log "     md5 : ${archiveMd5}"
    log "     sha1 : ${archiveSha1}"

    upResult=$( uploadFile ${session} ${targetId} ${uploadedFile} )
    archive_id=$(echo $upResult | awk '{print $NF}')
    echo "Le document ${uploadedFile} a été déposé dans le coffre $1 avec le numéro d'archive ${archive_id}."

    closeSession ${session}

    # Notification de dépôt d'un fichier dans le coffre
    echo "############################################################"
    cfe_id=$1
    archive_name=$(basename ${uploadedFile})
    archive_Sha256=$( sha256sum ${uploadedFile} | awk '{print $1}' )
    archive_depot_datetime=$(date +"%Y-%m-%dT%H:%M:%SZ")
    curl -X POST -H "Content-Type: application/json" -d "{\"cfe_id\":${cfe_id},\"archive_id\":${archive_id},\"archive_name\":\"${archive_name}\",\"archive_hash\":\"{SHA-256}${archive_Sha256}\",\"archive_depot_datetime\":\"${archive_depot_datetime}\",\"report_deposit_id\":123456,\"report_treatment_id\":54321,\"status\":\"OK\",\"code\":\"1000\",\"message\":\"L archive est correcte\"}" http://api-php.digiposte.local/v3/flux/notification
    echo
done