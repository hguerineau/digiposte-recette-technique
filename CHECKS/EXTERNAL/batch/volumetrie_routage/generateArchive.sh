#! /bin/bash

echo -n "Getting the configuration."

function print_usage()
{
cat << EOF

Usage:
    $(basename $1) [OPTIONS]

Options:
    -a <NB_ARCHIVE>
        Define the number of archives to generate
    -f <FILE>, --file <FILE>
        Define the PDF file used to create the archive
    -n <NB_FILES>
        Define the number of times <FILE> must be duplicated in the archive
    --back-host <BACK_HOST>
        The backoffice server name (e.g.: back1-tmc)
    --db-host <DB_HOST>
        The database server name (e.g.: db2-tmc)
    -s <SENDER_PID>, --sender <SENDER_PID>
        The PID of the sender

EOF
}

while [[ $# -gt 0 ]]
do
    case "$1" in
		'-a' )
            readonly NB_ARCHIVES=$2
            [[ ${NB_ARCHIVES} -le 0 ]] && echo "The number of archives to generate must be greater than 0" && exit 1
            shift 2
            ;;
        '-f' | '--file' )
            readonly FILE_TO_ROUTE=$2
            [[ ! -r ${FILE_TO_ROUTE} ]] && echo -e "\n${FILE_TO_ROUTE} is not readable" && exit 1
            shift 2
            ;;
        '-n' )
            readonly NB_FILES_TO_ROUTE=$2
            [[ ${NB_FILES_TO_ROUTE} -le 0 ]] && echo "The number of files to route must be greater than 0" && exit 1
            shift 2
            ;;
        '--back-host' )
            readonly BACK_SERVER=$2
            shift 2
            ;;
        '--db-host' )
            readonly DB_SERVER=$2
            shift 2
            ;;
        '-s' | '--sender' )
            readonly SENDER_PID=$2
            shift 2
            ;;
        * )
            print_usage "$0"
            exit 1
    esac
done

if [[ -z "${FILE_TO_ROUTE}" ]] || \
    [[ -z "${NB_FILES_TO_ROUTE}" ]] || \
    [[ -z "${DB_SERVER}" ]] || \
    [[ -z "${BACK_SERVER}" ]]
then
    print_usage "$0"
    exit 1
fi

# Création du dossier TEMP/ s'il n'existe pas
if ! [[ -d "TEMP" ]]
then
    mkdir TEMP
fi

# Date
readonly TIMESTAMP=$(date +%s)
readonly FULLDATE=$(date --date=@${TIMESTAMP} +%Y%m%d-%H%M%S)
readonly YEAR=$(date --date=@${TIMESTAMP} +%Y)
readonly MONTH=$(date --date=@${TIMESTAMP} +%m)
readonly DAY=$(date --date=@${TIMESTAMP} +%d)
readonly HOUR=$(date --date=@${TIMESTAMP} +%H)
readonly MINUTE=$(date --date=@${TIMESTAMP} +%M)
readonly SECOND=$(date --date=@${TIMESTAMP} +%S)
readonly TIMEZONE=$(date +%:z)

readonly OUTPUT_DIR="TEMP/ROUTAGE_${FULLDATE}_${NB_FILES_TO_ROUTE}"
readonly OUTPUT_FILENAME="${FULLDATE}_$(basename ${FILE_TO_ROUTE})"
readonly HEADER=TEMPLATES/XML/header.xml
readonly ENTRY=TEMPLATES/XML/entry.xml
readonly FOOTER=TEMPLATES/XML/footer.xml
[[ ! -r ${HEADER} || ! -r ${ENTRY} || ! -r ${FOOTER} ]] && echo -e "\nAt least one of the template file is missing or not readable. Please check the installation." && exit 1

echo -n "."

eval $(ssh -q ${BACK_SERVER} "php /data/usr/local/apps/digiposte-php/current/php/legacy/digiposte/batch/batch_config.php _G_SRV_BDD_LOG_ _G_SRV_BDD_PASS_ _G_SRV_BDD_HOST_")
_G_SRV_BDD_NAME_=digiposte
_G_SRV_BDD_PORT_=3306

function mysqlexec()
{
    ssh -q ${DB_SERVER} "mysql -u${_G_SRV_BDD_LOG_} -p${_G_SRV_BDD_PASS_} -h${_G_SRV_BDD_HOST_} -P${_G_SRV_BDD_PORT_} ${_G_SRV_BDD_NAME_} -Bse \"$1\""
}

echo "."


echo -n "Looking for the sender..."

if [[ -n "${SENDER_PID}" ]]
then
    whereSenderPid="AND sender.pid = '${SENDER_PID}'"
else
    whereSenderPid=""
fi

sender=$(mysqlexec "SELECT sender.id, sender.pid, sender.name, sender__manager.certificate
        FROM sender
        JOIN sender__manager ON sender.id = sender__manager.sender_id
        JOIN membership ON sender.id = membership.sender_id
        WHERE sender__manager.has_route_right_right != 0
          AND sender__manager.expires_at > NOW()
          AND membership.status = 4
          AND membership.user_strongbox_id IS NOT NULL
          ${whereSenderPid}
        #ORDER BY Rand()
        LIMIT 1")

[[ "${sender}" == "" ]] && echo -e "\nSender not found" && exit 1

readonly SENDER_ID=$(echo "${sender}" | cut -d $'\t' -f 1)
[[ -z "${SENDER_PID}" ]] && readonly SENDER_PID=$(echo "${sender}" | cut -d $'\t' -f 2)
readonly SENDER_NAME=$(echo "${sender}" | cut -d $'\t' -f 3)
readonly PUBLISHER=$(echo "${sender}" | cut -d $'\t' -f 4)

echo " ${SENDER_NAME} (pid: ${SENDER_PID}) found"

max=0
while [ ${max} -ne ${NB_ARCHIVES} ]; do
	
	max=`expr $max + 1` 
	echo -ne "Creating the archive n°${max}:\t0%"

	[[ -d ${OUTPUT_DIR} ]] && rm -rf ${OUTPUT_DIR}
	mkdir ${OUTPUT_DIR}

	# Header
	sed -e "s@__DAY__@${DAY}@g" \
		-e "s@__MONTH__@${MONTH}@g" \
		-e "s@__YEAR__@${YEAR}@g" \
		-e "s@__YEAR1__@$(( ${YEAR} + 50 ))@g" \
		-e "s@__HOUR__@${HOUR}@g" \
		-e "s@__MINUTE__@${MINUTE}@g" \
		-e "s@__SECOND__@${SECOND}@g" \
		-e "s@__TIMEZONE__@${TIMEZONE}@g" \
		-e "s@__SENDER_NAME__@${SENDER_NAME}@g" \
		-e "s@__SENDER_CODE__@${SENDER_PID}@g" \
		-e "s@__PUBLISHER__@${PUBLISHER}@g" ${HEADER} > ${OUTPUT_DIR}/flux.xml

	# Files
	index=0
	routeCodes=$(mysqlexec "SELECT id
			FROM membership
			WHERE sender_id = '${SENDER_ID}'
			  AND status = 4
			  AND user_strongbox_id IS NOT NULL
			ORDER BY Rand()
			LIMIT ${NB_FILES_TO_ROUTE}")
	while true
	do
		for routeCode in ${routeCodes}
		do
			echo -ne "\rCreating the archive n°${max}:\t$(( $index * 80 / $NB_FILES_TO_ROUTE ))%"

			file="${index}_${OUTPUT_FILENAME}"
			sed -e "s@__DAY__@${DAY}@g" \
				-e "s@__MONTH__@${MONTH}@g" \
				-e "s@__YEAR__@${YEAR}@g" \
				-e "s@__YEAR1__@$(( ${YEAR} + 50 ))@g" \
				-e "s@__HOUR__@${HOUR}@g" \
				-e "s@__MINUTE__@${MINUTE}@g" \
				-e "s@__SECOND__@${SECOND}@g" \
				-e "s@__TIMEZONE__@${TIMEZONE}@g" \
				-e "s@__INDEX__@${index}@g" \
				-e "s@__FILE__@${file}@g" \
				-e "s@__ROUTE_CODE__@${routeCode}@g" ${ENTRY} >> ${OUTPUT_DIR}/flux.xml
			cp ${FILE_TO_ROUTE} ${OUTPUT_DIR}/${file}

			(( index++ ))
			[[ ${index} -ge ${NB_FILES_TO_ROUTE} ]] && break 2
		done
	done

	# Footer
	cat ${FOOTER} >> ${OUTPUT_DIR}/flux.xml
	
	# DATE=`date --date now +%H%M%S`
	LOT_ID=$(uuidgen | tr -d - | cut -c1-16)
	OUTPUT_FILENAME2="$(basename ${OUTPUT_DIR})_$(date +%s)"
	mv ${OUTPUT_DIR}/flux.xml ${OUTPUT_DIR}/flux.xml.ori
	sed -e "s@__UID__@${LOT_ID}@g" \
    -e "s@__TITLE__@${OUTPUT_FILENAME2}@g" \
    ${OUTPUT_DIR}/flux.xml.ori > ${OUTPUT_DIR}/flux.xml
    rm ${OUTPUT_DIR}/flux.xml.ori
    
	# Zip
	nbZippedFiles=0;nbZippedLs=0;head=1000;tail=1;rm -rf copytemp;mkdir copytemp;chmod 755 copytemp
	while [ ${NB_FILES_TO_ROUTE} -ge ${nbZippedLs} ]; do
		liste1K=`ls ${OUTPUT_DIR} | tail -n +${tail} | head -${head}`
		for liste in ${liste1K}; do
			cp ${OUTPUT_DIR}/${liste} copytemp
			(( nbZippedFiles++ ))
			(( nbZippedLs++ ))
		done
		zip -j ${OUTPUT_DIR}_${max}.zip copytemp/* > /dev/null 2>&1
		echo -ne "\rCreating the archive n°${max}:\t$(( 80 + ${nbZippedFiles} * 20 / (${NB_FILES_TO_ROUTE} + 1) ))%"
		rm -rf copytemp/*
		(( tail += head ))
	done
	rm -rf copytemp
	sleep 0.5
	
	echo -e "\rCreating the archive n°${max}:\t100%"

	rm -r ${OUTPUT_DIR}

	echo "You can find the archive here: ${OUTPUT_DIR}_${max}.zip"

done