#! /bin/bash

# Author        Mathieu LEMARCHAND
# Description   Vérifie les retours des nouveaux webservices V7

#Création du dossier de logs si inexistant
if [ ! -d LOGS ]; then
	mkdir LOGS;chmod 755 -R LOGS
fi

# Print the check results on the screen as well as in a log file
NOW=$(date +%Y%m%d-%H%M%S)
exec &> >(tee -a LOGS/volumetrie-WS-V7_$NOW.log)
LOG_FILE="LOGS/volumetrie-WS_$NOW.log"

#Récupération du password root de la BDD
# echo "Merci d'entrer le password root d'accès à la BDD"
# while [ -z ${password} ]; do
	# read -s password
# done
# ssh tma-db "mysql -uroot -p${password} digiposte" >/dev/null 2>&1
# if [ ${?} -eq 1 ]; then
	# echo "ERROR: password incorrect, impossible de se connecter avec la BDD."; exit 1
# fi

# Test en volumétrie du WS préinscription
echo "INFO: début du test en volumétrie du WS préinscription"
fichier_conf="XML_V7_Volumetrie/PreInscription-RECFONC1500.xml";fichier_sortie="Retour_Old_WS/Reppreinscription_RECFONC_${NOW}"
#Appel du WS
curl -s -k -u digiposte:Digi_Pass -E "P12/Emetteur_330.P12:Digiposte" --cert-type p12 --header "content-type: application/soap+xml;charset=UTF-8" --header "SOAPAction:\"\"" --data @${fichier_conf} https://emetteur.recfonc.u-post.fr/emetteur/v5/soap/preinscription -i > ${fichier_sortie}
echo -e "INFO: fin du test en volumétrie du WS préinscription\r\n"

rm -rf XML_V7/*_tmp_*