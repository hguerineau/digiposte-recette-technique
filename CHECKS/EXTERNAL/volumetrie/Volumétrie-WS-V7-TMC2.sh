#! /bin/bash

# Author        Mathieu LEMARCHAND
# Description   Vérifie les retours des nouveaux webservices V7

#Création du dossier de logs si inexistant
if [ ! -d LOGS ]; then
	mkdir LOGS;chmod 755 -R LOGS
fi

# Print the check results on the screen as well as in a log file
NOW=$(date +%Y%m%d-%H%M%S)
exec &> >(tee -a LOGS/volumetrie-WS-V7_$NOW.log)
LOG_FILE="LOGS/volumetrie-WS_$NOW.log"
HOUR=10045

#Récupération du password root de la BDD
# echo "Merci d'entrer le password root d'accès à la BDD"
# while [ -z ${password} ]; do
	# read -s password
# done
# ssh db1-tmc "mysql -uroot -p${password} digiposte" >/dev/null 2>&1
# if [ ${?} -eq 1 ]; then
	# echo "ERROR: password incorrect, impossible de se connecter avec la BDD."; exit 1
# fi

password="e1bd046924"

#Suppression des fichiers temporaires inutiles provenant d'une ancienne exécution (ayant planté en cours)
rm -rf XML_V7_Volumetrie/*_tmp_*

# Test en volumétrie du WS adhésions (POST)
echo -e "\r\nINFO: début du test en volumétrie du WS adhésions (POST)"
fichier_conf="XML_V7_Volumetrie/preadhesions20000.xml";fichier_sortie="Retour_Old_WS/RepAdhesionsPost_${NOW}"
#Appel du WS
curl -s -k -u digiposte:Digi_Pass -E P12/Emetteur_305.P12:Digiposte --cert-type p12 https://emetteur.tmc.u-post.fr/v2/emetteur/atom/adhesions --header "SOAPAction:\"\""   --data-urlencode ressource@${fichier_conf} -i > ${fichier_sortie}
echo -e "INFO: fin du test en volumétrie du WS adhésions (POST)\r\n"

#Mise à jour de la BDD en prévision du test de volumétrie suivant
ssh db1-tmc "mysql -uroot -p${password} digiposte -e \"UPDATE membership SET status='3' WHERE id_client LIKE '${HOUR}%'\"" >/dev/null 2>&1

# Test en volumétrie du WS adhésions (GET)
echo "INFO: début du test en volumétrie du WS adhésions (GET)"
fichier_sortie="Retour_Old_WS/RepAdhesionsGet_${NOW}"
curl -s -k -u digiposte:Digi_Pass -E P12/Emetteur_305.P12:Digiposte --cert-type p12 "https://emetteur.tmc.u-post.fr/v2/emetteur/atom/adhesions?status=3" -i > ${fichier_sortie}
echo -e "INFO: fin du test en volumétrie du WS adhésions (GET)\r\n"

#Récupération de données de la BDD en prévision du test de volumétrie suivant
id_doc="liste_id.tmp";>${id_doc}
ssh db1-tmc "mysql -uroot -p${password} digiposte -e 'select id from membership WHERE id_client LIKE \"${HOUR}%\" LIMIT 1000'" >> ${id_doc}
sed -i -e "s/id//g" ${id_doc}; sed -i '/^$/d' ${id_doc}

# Test en volumétrie du WS adhésions (PUT)
echo "INFO: début du test en volumétrie du WS adhésions (PUT)"
fichier_conf="XML_V7_Volumetrie/modif_adhesions1000.xml";fichier_sortie="Retour_Old_WS/RepAdhesionsPut_${NOW}"
#Appel du WS
curl -s -k -u digiposte:Digi_Pass -E P12/Emetteur_305.P12:Digiposte --cert-type p12 https://emetteur.tmc.u-post.fr/v2/emetteur/atom/adhesions -X PUT -d @${fichier_conf} > ${fichier_sortie}
echo -e "INFO: fin du test en volumétrie du WS adhésions (PUT)\r\n"

# Test en volumétrie du WS préinscription
echo "INFO: début du test en volumétrie du WS préinscription"
fichier_conf="XML_V7_Volumetrie/PreInscription-TMC600.xml";fichier_sortie="Retour_Old_WS/Reppreinscription_TMC_${NOW}"
#Appel du WS
curl -s -k -u digiposte:Digi_Pass -E "P12/Emetteur_330.P12:Digiposte" --cert-type p12 --header "content-type: application/soap+xml;charset=UTF-8" --header "SOAPAction:\"\"" --data @${fichier_conf} https://emetteur.tmc.u-post.fr/emetteur/v5/soap/preinscription -i > ${fichier_sortie}
echo -e "INFO: fin du test en volumétrie du WS préinscription\r\n"

#Suppression des fichiers temporaires inutiles
rm -rf XML_V7_Volumetrie/*_tmp_*