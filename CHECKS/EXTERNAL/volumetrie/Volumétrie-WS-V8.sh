#! /bin/bash

# Author        Mathieu LEMARCHAND
# Description   Vérifie les retours des nouveaux webservices V8

#Vérification que la volumétrie a été passée en paramètre
if [ -z ${1} ]; then
	echo "ERROR: merci de passer en paramètre la volumétrie à utiliser."; exit 1
else
	max=${1}
fi

#Création du dossier de logs si inexistant
if [ ! -d LOGS ]; then
	mkdir LOGS;chmod 755 -R LOGS
fi

#Vérification que le dossier contenant les anciens retours V7 existe
if [ ! -d XML_V8 ]; then
	echo "ERROR: le fichier contenant les retours XML V7 n'a pas été trouvé";exit 1
fi

# Print the check results on the screen as well as in a log file
NOW=$(date +%Y%m%d-%H%M%S)
exec &> >(tee -a LOGS/volumetrie-WS_$NOW.log)
LOG_FILE="LOGS/volumetrie-WS-V8_$NOW.log"
HOUR=$(date +%H%M%S)

#Récupération du password root de la BDD
# echo "Merci d'entrer le password root d'accès à la BDD"
# while [ -z ${password} ]; do
	# read -s password
# done
# ssh recfonc-db "mysql -uroot -p${password} digiposte" >/dev/null 2>&1
# if [ ${?} -eq 1 ]; then
	# echo "ERROR: password incorrect, impossible de se connecter avec la BDD."; exit 1
# fi

password="d54be56caa"

#Suppression des fichiers temporaires inutiles provenant d'une ancienne exécution (ayant planté en cours)
rm -rf XML_V8/*_tmp_*

# Test en volumétrie du WS createRegistration
echo -e "\r\nINFO: début du test en volumétrie du WS createRegistration"
fichier_conf="XML_V8/createRegistration.xml";fichier_sortie_CR="Retour_New_WS/RepcreateRegistration_${NOW}"
i=1; while [ $i -le $max ];do
	echo -ne "$i / $max\r"
	#Remplacement du champ clientId (récupération de la ligne, suppression et insertion)
	ligne=`grep -n clientId ${fichier_conf} | cut -d ':' -f1`
	sed "${ligne}d" ${fichier_conf} >> ${fichier_conf}_tmp_${i}
	sed -i "${ligne}i\            <clientId>${HOUR}${i}</clientId>" ${fichier_conf}_tmp_${i} >/dev/null 2>&1
	#Appel du WS
	curl -s -k -u digiposte:Digi_Pass -E P12/EMETTEUR_306.p12:Digiposte --cert-type p12 --header "content-type: application/soap+xml;charset=UTF-8" --header "SOAPAction:\"\"" --data @${fichier_conf}_tmp_${i}  https://emetteur.recfonc.u-post.fr/ws/v1/createRegistration -i >> ${fichier_sortie_CR} &
	i=$(($i + 1)) 
done
wait;echo -e "INFO: fin du test en volumétrie du WS createRegistration\r\n"

#Mise à jour de la BDD en prévision du test de volumétrie suivant
cat ${fichier_sortie_CR} | grep "routeCode" | cut -d'>' -f9 | cut -d'<' -f1 >> ${fichier_sortie_CR}.tmp
user_id=`ssh recfonc-db "mysql -uroot -p${password} digiposte -e 'select user_id from membership WHERE user_id IS NOT NULL LIMIT 1'"`
user_id=`echo $user_id | cut -d' ' -f2`
#Exécution de l'ensemble des commandes mysql en une seule connexion ssh
cmd="";for routeCode in `cat ${fichier_sortie_CR}.tmp`; do
	cmd="update membership set user_id='${user_id}', status='3' where id='${routeCode}';${cmd}"
done
ssh recfonc-db "mysql -uroot -p${password} digiposte -e \"${cmd}\"" >/dev/null 2>&1

# Test en volumétrie du WS validateMembership
echo "INFO: début du test en volumétrie du WS validateMembership"
fichier_conf="XML_V8/validateMembership.xml";fichier_sortie="Retour_New_WS/RepvalidateMembership_${NOW}"
i=1;for routeCode in `cat ${fichier_sortie_CR}.tmp`; do
	echo -ne "$i / $max\r"
	#Remplacement du champ routeCode (récupération de la ligne, suppression et insertion)
	ligne=`grep -n routeCode ${fichier_conf} | cut -d ':' -f1`
	sed "${ligne}d" ${fichier_conf} >> ${fichier_conf}_tmp_${i}
	sed -i "${ligne}i\         <routeCode>${routeCode}</routeCode>" ${fichier_conf}_tmp_${i} >/dev/null 2>&1
	#Appel du WS
	curl -s -k -u digiposte:Digi_Pass -E P12/EMETTEUR_306.p12:Digiposte --cert-type p12 --header "content-type: application/soap+xml;charset=UTF-8" --header "SOAPAction:\"\"" --data @${fichier_conf}_tmp_${i} https://emetteur.recfonc.u-post.fr/ws/v1/validateMembership -i >> ${fichier_sortie} &
	i=$(($i + 1))
done
wait;echo -e "INFO: fin du test en volumétrie du WS validateMembership\r\n"

# Test en volumétrie du WS searchMembership
echo "INFO: début du test en volumétrie du WS searchMembership"
fichier_conf="XML_V8/searchMembership.xml";fichier_sortie="Retour_New_WS/RepsearchMembership_${NOW}"
i=1;while [ $i -le $max ];do
	echo -ne "$i / $max\r"
	#Appel du WS
	curl -s -k -u digiposte:Digi_Pass -E P12/EMETTEUR_306.p12:Digiposte --cert-type p12 --header "content-type: application/soap+xml;charset=UTF-8" --header "SOAPAction:\"\"" --data @${fichier_conf} https://emetteur.recfonc.u-post.fr/ws/v1/searchMembership -i > ${fichier_sortie} &
	i=$(($i + 1))
done
wait;echo -e "INFO: fin du test en volumétrie du WS searchMembership\r\n"

# Test en volumétrie du WS cancelMembership
echo "INFO: début du test en volumétrie du WS cancelMembership"
fichier_conf="XML_V8/cancelMembership.xml";fichier_sortie="Retour_New_WS/RepcancelMembership_${NOW}"
i=1;for routeCode in `cat ${fichier_sortie_CR}.tmp`; do
	echo -ne "$i / $max\r"
	#Remplacement du champ routeCode (récupération de la ligne, suppression et insertion)
	ligne=`grep -n routeCode ${fichier_conf} | cut -d ':' -f1`
	sed "${ligne}d" ${fichier_conf} >> ${fichier_conf}_tmp_${i}
	sed -i "${ligne}i\		<routeCode>${routeCode}</routeCode>" ${fichier_conf}_tmp_${i} >/dev/null 2>&1
	#Appel du WS
	curl -s -k -u digiposte:Digi_Pass -E P12/EMETTEUR_306.p12:Digiposte --cert-type p12 --header "content-type: application/soap+xml;charset=UTF-8" --header "SOAPAction:\"\"" --data @${fichier_conf}_tmp_${i} https://emetteur.recfonc.u-post.fr/ws/v1/cancelMembership -i >> ${fichier_sortie} &
	i=$(($i + 1))
done
wait;echo -e "INFO: fin du test en volumétrie du WS cancelMembership\r\n"

#Mise à jour de la BDD en prévision du test de volumétrie suivant (remplacer l'id_certif en fonction du certificat utilisé)
id_certif="3441"
pid=`ssh recfonc-db "mysql -uroot -p${password} digiposte -e 'select id from sender WHERE pid=\"${id_certif}\"'"`
pid=`echo $pid | cut -d' ' -f2`
# doc_id=`ssh recfonc-db "mysql -uroot -p${password} digiposte -e 'select id from document WHERE id IS NOT NULL LIMIT 1'"`
# doc_id=`echo $doc_id | cut -d' ' -f2`
#Exécution de l'ensemble des commandes mysql en une seule connexion ssh
# cmd="";i=1;while [ $i -le $max ];do
	# cmd="INSERT INTO notification_queue VALUES (${HOUR}${i},'${pid}','${user_id}','${doc_id}',0,'8999999999999999-00003-0001',NULL,NULL,NULL,'201001.pdf','Document réceptionné','3',0,NULL,'2013-07-02 11:53:19','2013-07-02 11:53:19');${cmd}";
	#cmd="INSERT INTO notification_queue VALUES ('${HOUR}${i}','d0999418f344c6ebd254b17a9fa5da36','45a4a11f72bd1c90c09cc39490813a8b','${doc_id}',0,'9999999999999999-00003-0001',NULL,NULL,NULL,'API16-20130918131247.F1.pdf','Document réceptionné','4',0,NULL,'2014-01-07 16:59:02','2014-01-07 16:59:02');${cmd}";
	i=$(($i + 1))
# done
# echo "-- cmd = $cmd --"
# ssh recfonc-db "mysql -uroot -p${password} digiposte -e \"${cmd}\"" >/dev/null 2>&1

# Test en volumétrie du WS searchActionOnDocument
echo "INFO: début du test en volumétrie du WS searchActionOnDocument"
fichier_conf="XML_V8/searchActionOnDocument.xml";fichier_sortie="Retour_New_WS/RepsearchActionOnDocument_${NOW}"
#Remplacement du champ pid (récupération de la ligne, suppression et insertion)
ligne=`grep -n pid ${fichier_conf} | cut -d ':' -f1`
sed "${ligne}d" ${fichier_conf} >> ${fichier_conf}_tmp_${i}
sed -i "${ligne}i\         <pid>${id_certif}</pid>" ${fichier_conf}_tmp_${i} >/dev/null 2>&1
i=1;while [ $i -le $max ];do
	echo -ne "$i / $max\r"
	#Appel du WS
	curl -s -k -u digiposte:Digi_Pass -E P12/EMETTEUR_306.p12:Digiposte --cert-type p12 --header "content-type: application/soap+xml;charset=UTF-8" --header "SOAPAction:\"\"" --data @${fichier_conf}_tmp_${i}  https://emetteur.recfonc.u-post.fr/ws/v1/searchActionOnDocument -i > ${fichier_sortie} &
	i=$(($i + 1))
done
wait;echo -e "INFO: fin du test en volumétrie du WS searchActionOnDocument\r\n"

#Mise à jour de la BDD en prévision du test de volumétrie suivant
cmd="";i=1;while [ $i -le $max ];do
	cmd="INSERT INTO campaign VALUES ('${HOUR}${i}','${pid}','test2901','${HOUR}flow${i}','\0','2014-01-29 10:39:34','2014-01-29 10:39:34',1,'2020-01-29 10:39:34','2010-01-29 10:39:34',NULL,1,0,0,0);${cmd}"
	i=$(($i + 1))
done
ssh recfonc-db "mysql -uroot -p${password} digiposte -e \"${cmd}\"" #>/dev/null 2>&1

# Test en volumétrie du WS deleteCampaign
echo "INFO: début du test en volumétrie du WS deleteCampaign"
fichier_conf="XML_V8/deleteCampaign.xml";fichier_sortie="Retour_New_WS/RepdeleteCampaign_${NOW}"
#Récupération des numéros des lignes à remplacer, avant exécution du test
ligne_conf=`grep -n pid ${fichier_conf} | cut -d ':' -f1`
ligne=`grep -n campaignId ${fichier_conf} | cut -d ':' -f1`
i=1;while [ $i -le $max ];do
	echo -ne "$i / $max\r"
	#Remplacement du champ pid
	sed "${ligne_conf}d" ${fichier_conf} >> ${fichier_conf}_tmp_${i}
	sed -i "${ligne_conf}i\         <pid>${pid}</pid>" ${fichier_conf}_tmp_${i} >/dev/null 2>&1
	#Remplacement du champ campaignId
	sed "${ligne}d" ${fichier_conf} >> ${fichier_conf}_tmp_${i}
	sed -i "${ligne}i\         <campaignId>${HOUR}flow${i}</campaignId>" ${fichier_conf}_tmp_${i} >/dev/null 2>&1
	#Appel du WS
	curl -s -k -u digiposte:Digi_Pass -E P12/EMETTEUR_306.p12:Digiposte --cert-type p12 --header "content-type: application/soap+xml;charset=UTF-8" --header "SOAPAction:\"\"" --data @${fichier_conf}_tmp_${i}  https://emetteur.recfonc.u-post.fr/ws/v1/deleteCampaign -i >> ${fichier_sortie} &
	i=$(($i + 1))
done
wait;echo -e "INFO: fin du test en volumétrie du WS deleteCampaign\r\n"

#Suppression des fichiers temporaires inutiles
rm -rf XML_V7/*_tmp_*