#! /bin/bash

# Author    Damien PELLISSON

source ../../../LIBS/display.fct
source ../../../LIBS/check.fct

check_init "web-cluster"

SERVICES=( "haproxy_adherer" "haproxy_api" "haproxy_emetteur" "haproxy_secure" "haproxy_ws" )

LB_SERVER=$1
shift 1

LB_WEB_STATUS=$(ssh -n $LB_SERVER "echo 'show stat' | sudo socat stdio /var/run/haproxy.sock | awk -F ',' '{print \$1 \$2 \$18}'")

for server in "$@"
do
    status=$(ssh -n $server "sudo service apache2 status | grep -E 'Apache2 is running' > /dev/null 2>&1 && echo UP ||  echo echo DOWN")
    for service in "${SERVICES[@]}"
    do
        if ! check_test_critical 'echo "$LB_WEB_STATUS"' "${service}${server%%-*}${status}"
        then
            display_error "The service $service is not $status for $server"
        fi
    done
done

check_exit "The status of all the web servers of the cluster are known" ""
