#! /bin/bash

# Author    Damien PELLISSON

source ../../../LIBS/display.fct
source ../../../LIBS/check.fct

check_init "failover-service"

SERVER_VIP=$3

if [[ -n "$(ssh -n $1 "ip addr list | grep $SERVER_VIP")" ]]
then
    PRIMARY_SERVER=$1
    SECONDARY_SERVER=$2
elif check_test_critical "ssh -n $2 'ip addr list'" "$SERVER_VIP"
then
    PRIMARY_SERVER=$2
    SECONDARY_SERVER=$1
else
    check_exit "Cannot find the primary server"
fi
shift 3

for service in "$@"
do
    ssh -n $PRIMARY_SERVER "sudo service $service stop" > /dev/null

    sleep 10
    if ! check_test_critical "ssh -n $PRIMARY_SERVER 'ip addr list'" "!$SERVER_VIP"
    then
        display_error "After stopping $service on $PRIMARY_SERVER, it is still primary"
        ssh -n $PRIMARY_SERVER "sudo service $service start" > /dev/null
        continue
    fi

    if ! check_test_critical "ssh -n $SECONDARY_SERVER 'ip addr list'" "$SERVER_VIP"
    then
        display_error "After stopping $service on $PRIMARY_SERVER, $SECONDARY_SERVER is not become primary"
    fi

    ssh -n $PRIMARY_SERVER "sudo service $service start" > /dev/null

    sleep 10
    if ! check_test_critical "ssh -n $PRIMARY_SERVER 'ip addr list'" "$SERVER_VIP"
    then
        display_error "After restarting $service on $PRIMARY_SERVER, it is still secondary"
    fi
done

check_exit "Failover (with $*) works properly" ""
