#! /bin/bash

# Author    Damien PELLISSON

source ../../../LIBS/display.fct
source ../../../LIBS/check.fct

check_init "failover-dns"

SERVER_VIP=$3
DEPLOY_SERVER=$4

if [[ -n "$(ssh -n $1 'ip addr list' | grep $SERVER_VIP)" ]]
then
    PRIMARY_SERVER=$1
    SECONDARY_SERVER=$2
elif check_test_critical "ssh -n $2 'ip addr list'" "$SERVER_VIP"
then
    PRIMARY_SERVER=$2
    SECONDARY_SERVER=$1
else
    check_exit "Cannot find the primary server"
fi

digPrimary=$(ssh -n $DEPLOY_SERVER 'dig -t any db.digiposte.local')

if ! check_test_critical "ssh -n $PRIMARY_SERVER 'sudo ip addr del $SERVER_VIP/16 dev eth0:0'" ""
then
    check_exit "Cannot remove the VIP from $PRIMARY_SERVER"
fi

if ! check_test_critical "ssh -n $SECONDARY_SERVER 'sudo ip addr add $SERVER_VIP/16 dev eth0:0'" ""
then
    check_exit "Cannot add the VIP to $SECONDARY_SERVER"
fi

digSecondary=$(ssh -n $DEPLOY_SERVER 'dig -t any db.digiposte.local')

if ! check_test_critical "echo '$digSecondary'" "$digPrimary"
then
    display_error "$SECONDARY_SERVER does not resolve db.digiposte.local as $PRIMARY_SERVER"
fi

ssh -n $SECONDARY_SERVER "sudo ip addr del $SERVER_VIP/16 dev eth0:0"
ssh -n $PRIMARY_SERVER "sudo ip addr add $SERVER_VIP/16 dev eth0:0"

check_exit "Failover of DNS servers works properly" ""
