#! /bin/bash

# Author    Damien PELLISSON

source ../../../LIBS/display.fct
source ../../../LIBS/check.fct

check_init "failover-back"

ADMIN_URL=$7
ADMIN_CERT=$8
ADMIN_USER=$9

if ssh -n $1 'sudo crontab -u www-data -l > /dev/null 2>&1'
then
    PRIMARY_SERVER=$1
    PRIMARY_IP=$2
    SECONDARY_SERVER=$3
    SECONDARY_IP=$4
elif check_test_critical "ssh -n $3 'sudo crontab -u www-data -l > /dev/null 2>&1'" ""
then
    PRIMARY_SERVER=$3
    PRIMARY_IP=$4
    SECONDARY_SERVER=$1
    SECONDARY_IP=$2
else
    check_exit "Cannot find the primary server"
fi

if ssh -n $5 'ip addr list | grep .126/ > /dev/null'
then
    LB_SERVER=$5
elif check_test_critical "ssh -n $6 'ip addr list'" ".126/"
then
    LB_SERVER=$6
else
    check_exit "Cannot find the LB primary server"
fi

if ! check_test_critical "ssh -n $LB_SERVER 'cat /etc/haproxy/haproxy.cfg'" "$PRIMARY_IP"
then
    check_exit "The primary server IP address cannot be found HAProxy configuration"
fi

if ! check_test_critical "ssh -n $PRIMARY_SERVER \"sudo test -f /bck/crontab.www-data.bck\"" ""
then
    check_exit "Cannot find the backup of www-data's crontab on $PRIMARY_SERVER"
fi

ssh -n $LB_SERVER "sudo sed -i -e 's/$PRIMARY_IP/$SECONDARY_IP/g' /etc/haproxy/haproxy.cfg; sudo service haproxy reload > /dev/null"

if ! check_test_critical "ssh -n $PRIMARY_SERVER 'sudo crontab -u www-data -r'" ""
then
    display_error "Cannot remove the www-data's crontab on $PRIMARY_SERVER"
fi

if ! check_test_critical "ssh -n $SECONDARY_SERVER 'sudo crontab -u www-data /bck/crontab.www-data.bck'" ""
then
    display_error "Cannot copy the www-data's crontab on $SECONDARY_SERVER"
fi

curl -sil -E $ADMIN_CERT -u $ADMIN_USER $ADMIN_URL > /dev/null
if ! check_test_critical "ssh -n $SECONDARY_SERVER 'find /data/var/log/apache2/ -name admin-access.log -mmin 1'" "/data/var/log/apache2/admin-access.log"
then
    display_error "$SECONDARY_SERVER access logs does not contain the recent access to $ADMIN_URL"
fi

# Restoring default state
ssh -n $SECONDARY_SERVER "sudo crontab -u www-data -r"
ssh -n $PRIMARY_SERVER "sudo crontab -u www-data /bck/crontab.www-data.bck"
ssh -n $LB_SERVER "sudo sed -i -e 's/$SECONDARY_IP/$PRIMARY_IP/g' /etc/haproxy/haproxy.cfg; sudo service haproxy reload > /dev/null"

check_exit "Failover of back servers works properly" ""
