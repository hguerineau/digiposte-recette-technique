#! /bin/bash

# Author    Damien PELLISSON

source ../../../LIBS/display.fct
source ../../../LIBS/check.fct

check_init "failover-drbd"

if [[ -n "$(ssh -n $1 'drbd-overview | grep "Primary/Secondary"')" ]]
then
    PRIMARY_SERVER=$1
    SECONDARY_SERVER=$2
elif  check_test_critical "ssh -n $2 'drbd-overview'" "Primary/Secondary"
then
    PRIMARY_SERVER=$2
    SECONDARY_SERVER=$1
else
    check_exit "Cannot find the primary server"
fi

if ! check_test_critical "ssh -n $PRIMARY_SERVER 'echo 3 | sudo /root/bin/drbd.sh'" "Primary" \\
    ! check_test_critical "ssh -n $SECONDARY_SERVER 'echo 3 | sudo /root/bin/drbd.sh'" "Secondary"
then
    check_exit "drbd-overview and /root/bin/drbd.sh does not give the same status"
fi

if ! check_test_critical "ssh -n $SECONDARY_SERVER 'echo 1 | sudo /root/bin/drbd.sh'" "\[ KO \]"
then
    display_error "$PRIMARY_SERVER and $SECONDARY_SERVER are both primary"
fi

if ! check_test_critical "ssh -n $PRIMARY_SERVER 'echo 2 | sudo /root/bin/drbd.sh'" "!\[ KO \]"
then
    check_exit "$PRIMARY_SERVER cannot become secondary"
fi

if ! check_test_critical "ssh -n $PRIMARY_SERVER 'echo 3 | sudo /root/bin/drbd.sh'" "Secondary"
then
    check_exit "/root/bin/drbd.sh says $PRIMARY_SERVER becomes secondary but its status does not change"
fi

if check_test_critical "ssh -n $SECONDARY_SERVER 'echo 1 | sudo /root/bin/drbd.sh'" "!\[ KO \]"
then
    if check_test_critical "ssh -n $SECONDARY_SERVER 'echo 3 | sudo /root/bin/drbd.sh'" "Primary"
    then
        if ! check_test_critical "ssh -n $SECONDARY_SERVER 'echo 2 | sudo /root/bin/drbd.sh'" "!\[ KO \]"
        then
            check_exit "$SECONDARY_SERVER cannot become secondary again"
        fi
    else
        display_error "/root/bin/drbd.sh says $PRIMARY_SERVER becomes secondary but its status does not change"
    fi
else
    display_error "$PRIMARY_SERVER cannot become primary"
fi

if ! check_test_critical "ssh -n $PRIMARY_SERVER 'echo 1 | sudo /root/bin/drbd.sh'" "Primary"
then
    check_exit "$PRIMARY_SERVER cannot become primary again"
fi


check_exit "failover (with drbd) works properly" ""
