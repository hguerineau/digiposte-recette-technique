# Script de test de la volumétrie des webservices

Le script `volumetrie_webservices.sh` permet de tester la volumétrie des webservices :

- adhésion
	- création d'adhésions en masse (POST)
	- récupération des adhésions au statut 3 (GET)
	- résiliation des adhésions (PUT)
- préinscription
	- avec envoi de mail immédiat (IMM)
	- sans envoi de mail immédiat (DEM)
	- envoi des mails en attente (ENV)

Le script a pour paramètres obligatoires :
	
	-w <WS_TYPE>
        Type du webservice :
            adhesion
            preinscription
    -X <METHOD>
        Si WS_TYPE = adhesion
            POST    Création de <NB> adhésions en masse
            GET     Récupération de <NB> adhésions au statut 3
            PUT     Résiliation de <NB> adhésions (statut => 0)
        Si WS_TYPE = preinscription
            IMM     Création de <NB> preinscriptions IMM
            DEM     Création de <NB> preinscriptions DEM
            ENV     Envoi des mails
    -s <SENDER_PID>, --sender <SENDER_PID>
        PID de l émetteur
    -n <NB>
        Volumétrie à tester. Doit être supérieure à 0

## Commandes
### Adhésion
#### Création d'adhésions en masse (POST)
> **Pré-requis**  
> Aucun

	./volumetrie_webservices.sh -w adhesion -X POST -s 32-WSQTPSOUSEM -n 4

La commande ci-dessus va demander la création de **4** adhésions au webservice pour l'émetteur **32-WSQTPSOUSEM**.


#### Récupération des adhésions au statut 3 (GET)
> **Pré-requis**  
> - Avoir exécuté une "Création d'adhésions en masse"  
> - Avoir récupéré sa date de lancement

	./volumetrie_webservices.sh -w adhesion -X GET -s 32-WSQTPSOUSEM -n 4

La commande ci-dessus va demander à récupérer les **4** adhésions (qui seront passées temporairement au statut 3 par le script) à l'émetteur **32-WSQTPSOUSEM**.


#### Résiliation des adhésions (PUT)
> **Pré-requis**  
> - Avoir exécuté une "Création d'adhésions en masse"  
> - Avoir récupéré sa date de lancement

	./volumetrie_webservices.sh -w adhesion -X PUT -s 32-WSQTPSOUSEM -n 4

La commande ci-dessus va demander la résiliation (passage du statut à 0) des **4** adhésions à l'émetteur **32-WSQTPSOUSEM**.

**/!\ Attention /!\\** : En V7, le webservice supprime physiquement l'adhésion au lieu de passer le statut à 0.

### Préinscription
#### Avec envoi de mail immédiat (IMM)
> **Pré-requis**  
> Renseigner un email (variable `EMAIL`) dans le script `volumetrie_webservices.sh`

	./volumetrie_webservices.sh -w preinscription -X IMM -s 32-WSQTPSOUSEM -n 4

La commande ci-dessus va demander la création de **4** préinscriptions pour l'émetteur **32-WSQTPSOUSEM** au webservice.
Quatre mails doivent être reçus à l'adresse renseignée.

#### Sans envoi de mail immédiat (DEM)
> **Pré-requis**  
> Renseigner un email (variable `EMAIL`) dans le script `volumetrie_webservices.sh`

	./volumetrie_webservices.sh -w preinscription -X DEM -s 32-WSQTPSOUSEM -n 4

La commande ci-dessus va demander la création de **4** préinscriptions pour l'émetteur **32-WSQTPSOUSEM** au webservice.
Aucun mails ne doit être reçu à l'adresse renseignée.

#### Envoi des mails en attente (ENV)
> **Pré-requis**  
> - Avoir exécuté une préinscription DEM  
> - Avoir récupéré le fluxId de cette préinscription DEM

	./volumetrie_webservices.sh -w preinscription -X ENV -s 32-WSQTPSOUSEM -n 4

La commande ci-dessus va demander l'envoi des **4** mails des préinscriptions DEM effectuées auparavant.
Quatre mails doivent être reçus à l'adresse renseignée.