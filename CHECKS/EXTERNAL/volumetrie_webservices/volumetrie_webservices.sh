#! /bin/bash

#***************************************************#
#                     FONCTIONS                     #
#***************************************************#

function print_usage() {
cat << EOF

Usage:
    $(basename $1) [OPTIONS]

Options obligatoires:
    -w <WS_TYPE>
        Type du webservice :
            adhesion
            preinscription
    -X <METHOD>
        Si WS_TYPE = adhesion
            POST    Création de <NB> adhésions en masse
            GET     Récupération de <NB> adhésions au statut 3
            PUT     Résiliation de <NB> adhésions (statut => 0)
        Si WS_TYPE = preinscription
            IMM     Création de <NB> preinscriptions IMM
            DEM     Création de <NB> preinscriptions DEM
            ENV     Envoi des mails
    -s <SENDER_PID>, --sender <SENDER_PID>
        PID de l émetteur
    -n <NB>
        Volumétrie à tester. Doit être supérieure à 0
EOF
}

function mysqlexec() {
    ssh ${DB_SERVER} "mysql -u${_G_SRV_BDD_LOG_} -p${_G_SRV_BDD_PASS_} -h${_G_SRV_BDD_HOST_} -P${_G_SRV_BDD_PORT_} ${_G_SRV_BDD_NAME_} -Bse \"$1\""
}

function mysqlverbose() {
    ssh ${DB_SERVER} "mysql -u${_G_SRV_BDD_LOG_} -p${_G_SRV_BDD_PASS_} -h${_G_SRV_BDD_HOST_} -P${_G_SRV_BDD_PORT_} ${_G_SRV_BDD_NAME_} -vve \"$1\""
}

function adhesion_post () {
    readonly DIRECTORY="TEMP/adhesion_post_${FULLDATE}"
    mkdir ${DIRECTORY}
    readonly XML_FILENAME="${DIRECTORY}/adhesion_post_${FULLDATE}.xml"
    readonly HEADER=TEMPLATES_XML/ADHESION_POST/header.xml
    readonly ENTRY=TEMPLATES_XML/ADHESION_POST/entry.xml
    readonly FOOTER=TEMPLATES_XML/ADHESION_POST/footer.xml
    [[ ! -r ${HEADER} || ! -r ${ENTRY} || ! -r ${FOOTER} ]] && echo -e "\nAt least one of the template file is missing or not readable. Please check the installation." && exit 1

    # Header
    HEADER_ID=$(uuidgen | tr -d - | cut -c1-32)

    sed -e "s@__HEADER_ID__@${HEADER_ID}@g" \
        -e "s@__SENDER_PID__@${SENDER_PID}@g" \
        -e "s@__SENDER_NAME__@${SENDER_NAME}@g" ${HEADER} > ${XML_FILENAME}

    for i in `seq ${NB}`
    do
        ENTRY_ID=$(uuidgen | tr -d - | cut -c1-32)
        sed -e "s@__ENTRY_ID__@${ENTRY_ID}@g" ${ENTRY} >> ${XML_FILENAME}
    done

    # Footer
    cat ${FOOTER} >> ${XML_FILENAME}

    # Appel du webservice
    readonly OUTPUT_FILENAME="TEMP/adhesion_post_${FULLDATE}/Reponse_adhesion_post.txt"
    curl -s -k -u digiposte:Digi_Pass -E Emetteur_305.p12:Digiposte --cert-type P12 https://emetteur.tmc.u-post.fr/v2/emetteur/atom/adhesions  --data-urlencode ressource@${XML_FILENAME} -i -o"${OUTPUT_FILENAME}"

    nb_codeRoutage=$(cat ${OUTPUT_FILENAME} | grep -c -E "<span class=\"codeRoutage\">[0-9a-z]{32}</span>")
    echo -ne "Nombre de codes de routage reçus : ${nb_codeRoutage}\t"
    [[ nb_codeRoutage -eq ${NB} ]] && echo -e "\033[32m[OK]\033[0m" || echo -e "\033[31m[KO]\033[0m"

    echo "Date de lancement du webservice (à utiliser pour GET et PUT) : ${FULLDATE}"
}

function adhesion_get () {
    readonly DIRECTORY="TEMP/adhesion_get_${FULLDATE}"
    mkdir ${DIRECTORY}
    
    valid=0
    while [[ valid -eq 0 ]]
    do
        echo -n "Date de lancement du webservice adhesion POST (YYYYMMDD-hhmmss): "
        read date
        echo $date | grep -E "[0-9]{8}-[0-9]{6}" > /dev/null
        [[ $? -eq 0 ]] && valid=1
    done

    created_at="${date:0:4}-${date:4:2}-${date:6:2} ${date:9:2}:${date:11:2}:${date:13:2}"
    
    echo "Update des enregistrements..."
    result=$(mysqlverbose "UPDATE membership
                           SET status = 3
                           WHERE status = 2
                           AND sender_id = '${SENDER_ID}'
                           AND created_at >= '${created_at}'
                           ORDER BY id
                           LIMIT ${NB}")
    result_count=$(echo $result | grep -o -E "[0-9]+ rows affected" | cut -d" " -f1)
    echo "Nombre d'enregistrements modifiés (statut 2 -> 3) : ${result_count}"

    # Appel du webservice
    beginDate="${date:6:2}/${date:4:2}/${date:0:4}"
    readonly OUTPUT_FILENAME="TEMP/adhesion_get_${FULLDATE}/Reponse_adhesion_get.txt"
    curl -s -k -u digiposte:Digi_Pass -E Emetteur_305.p12:Digiposte --cert-type P12 -X GET "https://emetteur.tmc.u-post.fr/v2/emetteur/atom/adhesions?status=3&beginDate=${beginDate}" -i --trace-ascii "${DIRECTORY}/trace.txt"  -o"${OUTPUT_FILENAME}"
    
    nb_entries=$(cat ${OUTPUT_FILENAME} | grep -c -E "<a:entry>")
    echo -ne "Nombre de <a:entry> dans le XML de retour : ${nb_entries}\t"
    [[ ${nb_entries} -eq ${NB} ]] && echo -e "\033[32m[OK]\033[0m" || echo -e "\033[31m[KO]\033[0m"

    echo "Rétablissement des enregistrements..."
    result=$(mysqlverbose "UPDATE membership
                           SET status = 2
                           WHERE status = 3
                           AND sender_id = '${SENDER_ID}'
                           AND created_at >= '${created_at}'
                           ORDER BY id
                           LIMIT ${NB}")
    result_count=$(echo $result | grep -o -E "[0-9]+ rows affected" | cut -d" " -f1)
    echo "Nombre d'enregistrements modifiés (statut 3 -> 2) : ${result_count}"
}

function adhesion_put () {
    readonly DIRECTORY="TEMP/adhesion_put_${FULLDATE}"
    mkdir ${DIRECTORY}
    readonly XML_FILENAME="${DIRECTORY}/adhesion_put_${FULLDATE}.xml"
    readonly HEADER=TEMPLATES_XML/ADHESION_PUT/header.xml
    readonly ENTRY=TEMPLATES_XML/ADHESION_PUT/entry.xml
    readonly FOOTER=TEMPLATES_XML/ADHESION_PUT/footer.xml
    [[ ! -r ${HEADER} || ! -r ${ENTRY} || ! -r ${FOOTER} ]] && echo -e "\nAt least one of the template file is missing or not readable. Please check the installation." && exit 1

    valid=0
    while [[ valid -eq 0 ]]
    do
        echo -n "Date de lancement du webservice adhesion POST (YYYYMMDD-hhmmss): "
        read date
        echo $date | grep -E "[0-9]{8}-[0-9]{6}" > /dev/null
        [[ $? -eq 0 ]] && valid=1
    done

    created_at="${date:0:4}-${date:4:2}-${date:6:2} ${date:9:2}:${date:11:2}:${date:13:2}"
    
    echo "Récupération des enregistrements..."
    liste_adhesions=$(mysqlexec "SELECT id
                                 FROM membership
                                 WHERE status = 2
                                 AND sender_id = '${SENDER_ID}'
                                 AND created_at >= '${created_at}'
                                 LIMIT ${NB}")

    liste_adhesions_count=$(echo ${liste_adhesions} | wc -w)
    [[ ${liste_adhesions_count} -eq 0 ]] && echo -e "\033[31mError: Aucune adhésion trouvée\033[0m" && exit 1
    [[ ${liste_adhesions_count} -lt ${NB} ]] && echo -e "\033[33mWarning: ${liste_adhesions_count} adhésions trouvées au lieu de ${NB}\033[0m"

    # Header
    HEADER_ID=$(uuidgen | tr -d - | cut -c1-32)

    sed -e "s@__HEADER_ID__@${HEADER_ID}@g" \
        -e "s@__SENDER_PID__@${SENDER_PID}@g" \
        -e "s@__SENDER_NAME__@${SENDER_NAME}@g" ${HEADER} > ${XML_FILENAME}

    # Entries
    for adhesion in ${liste_adhesions}
    do
        sed -e "s@__ADHESION_ID__@${adhesion}@g" ${ENTRY} >> ${XML_FILENAME}
    done

    # Footer
    cat ${FOOTER} >> ${XML_FILENAME}

    # Appel du webservice
    readonly OUTPUT_FILENAME="TEMP/adhesion_put_${FULLDATE}/Reponse_adhesion_put.txt"
    curl -s -k -u digiposte:Digi_Pass -E Emetteur_305.p12:Digiposte --cert-type P12 https://emetteur.tmc.u-post.fr/v2/emetteur/atom/adhesions -X PUT -d @${XML_FILENAME} -i -o"${OUTPUT_FILENAME}"

    nb_entries=$(cat ${OUTPUT_FILENAME} | grep -o -E "<a:entry>" | wc -l)
    echo -ne "Nombre de <a:entry> dans le XML de retour : ${nb_entries}\t"
    [[ ${nb_entries} -eq ${liste_adhesions_count} ]] && echo -e "\033[32m[OK]\033[0m" || echo -e "\033[31m[KO]\033[0m"

    inClause="('"$(echo ${liste_adhesions} | sed -e "s/ /','/g")"')"
    nb_adhesions=$(mysqlexec "SELECT count(*)
                              FROM membership
                              WHERE id in ${inClause}")
    echo -ne "Nombre d'adhésions en BDD suite au PUT : ${nb_adhesions}\t"
    [[ ${nb_adhesions} -eq 0 ]] && echo -e "\033[32m[OK]\033[0m" || echo -e "\033[31m[KO]\033[0m"
}

function preinscription_IMM () {
    readonly DIRECTORY="TEMP/preinscription_IMM_${FULLDATE}"
    mkdir ${DIRECTORY}
    readonly XML_FILENAME="${DIRECTORY}/preinscription_IMM_${FULLDATE}.xml"
    readonly HEADER=TEMPLATES_XML/PREINSCRIPTION_IMM_DEM/header.xml
    readonly ENTRY=TEMPLATES_XML/PREINSCRIPTION_IMM_DEM/entry.xml
    readonly FOOTER=TEMPLATES_XML/PREINSCRIPTION_IMM_DEM/footer.xml
    [[ ! -r ${HEADER} || ! -r ${ENTRY} || ! -r ${FOOTER} ]] && echo -e "\nAt least one of the template file is missing or not readable. Please check the installation." && exit 1

    date=${FULLDATE}
    created_at="${date:0:4}-${date:4:2}-${date:6:2} ${date:9:2}:${date:11:2}:${date:13:2}"
    
    # Header
    FLUX_ID=$(echo ${FULLDATE} | tr -d '-')
    EMAIL_MODE="IMM"
    ACTION_TYPE="C"

    sed -e "s@__FLUX_ID__@${FLUX_ID}@g" \
        -e "s@__SENDER_PID__@${SENDER_PID}@g" \
        -e "s@__EMAIL_MODE__@${EMAIL_MODE}@g" \
        -e "s@__ACTION_TYPE__@${ACTION_TYPE}@g" ${HEADER} > ${XML_FILENAME}

    # Entries
    for i in `seq ${NB}`
    do
        sed -e "s@__EMAIL_MODE__@${EMAIL_MODE}@g" \
            -e "s@__ACTION_TYPE__@${ACTION_TYPE}@g" \
            -e "s/__EMAIL__/${EMAIL}/g" ${ENTRY} >> ${XML_FILENAME}
    done

    # Footer
    cat ${FOOTER} >> ${XML_FILENAME}

    echo "Appel du webservice preinscription IMM avec le fluxId : ${FLUX_ID}"

    # Appel du webservice
    readonly OUTPUT_FILENAME="TEMP/preinscription_IMM_${FULLDATE}/Reponse_preinscription_IMM.txt"
    curl -s -k -u digiposte:Digi_Pass -E Emetteur_305.p12:Digiposte --cert-type P12 https://emetteur.tmc.u-post.fr/emetteur/v5/soap/preinscription --header "content-type: application/soap+xml;charset=UTF-8" --data @${XML_FILENAME} -i -o"${OUTPUT_FILENAME}"

    nb_routeCode=$(cat ${OUTPUT_FILENAME} | grep -o -E "<routeCode xsi:type=\"xsd:string\">[0-9a-z]{32}</routeCode>" | wc -l)
    echo -ne "Nombre de codes de routage reçus : ${nb_routeCode}\t"
    [[ ${nb_routeCode} -eq ${NB} ]] && echo -e "\033[32m[OK]\033[0m" || echo -e "\033[31m[KO]\033[0m"

    nb_mailQueue=$(mysqlexec "SELECT count(*)
                              FROM mail_queue
                              WHERE recipient = '${EMAIL}'
                              AND created_at >= '${created_at}'")
    echo -ne "Nombre de mails dans la table MAIL_QUEUE : ${nb_mailQueue}\t"
    [[ ${nb_mailQueue} -eq ${NB} ]] && echo -e "\033[32m[OK]\033[0m" || echo -e "\033[31m[KO]\033[0m"

    nb_membership=$(mysqlexec "SELECT count(*)
                               FROM membership
                               WHERE status = 2
                               AND sender_id = '${SENDER_ID}'
                               AND created_at >= '${created_at}'")
    echo -ne "Nombre d'entrées créées dans MEMBERSHIP : ${nb_membership}\t"
    [[ ${nb_membership} -eq ${NB} ]] && echo -e "\033[32m[OK]\033[0m" || echo -e "\033[31m[KO]\033[0m"
}

function preinscription_DEM () {
    readonly DIRECTORY="TEMP/preinscription_DEM_${FULLDATE}"
    mkdir ${DIRECTORY}
    readonly XML_FILENAME="${DIRECTORY}/preinscription_DEM_${FULLDATE}.xml"
    readonly HEADER=TEMPLATES_XML/PREINSCRIPTION_IMM_DEM/header.xml
    readonly ENTRY=TEMPLATES_XML/PREINSCRIPTION_IMM_DEM/entry.xml
    readonly FOOTER=TEMPLATES_XML/PREINSCRIPTION_IMM_DEM/footer.xml
    [[ ! -r ${HEADER} || ! -r ${ENTRY} || ! -r ${FOOTER} ]] && echo -e "\nAt least one of the template file is missing or not readable. Please check the installation." && exit 1

    date=${FULLDATE}
    created_at="${date:0:4}-${date:4:2}-${date:6:2} ${date:9:2}:${date:11:2}:${date:13:2}"
    
    # Header
    FLUX_ID=$(echo ${FULLDATE} | tr -d '-')
    EMAIL_MODE="DEM"
    ACTION_TYPE="C"

    sed -e "s@__FLUX_ID__@${FLUX_ID}@g" \
        -e "s@__SENDER_PID__@${SENDER_PID}@g" \
        -e "s@__EMAIL_MODE__@${EMAIL_MODE}@g" \
        -e "s@__ACTION_TYPE__@${ACTION_TYPE}@g" ${HEADER} > ${XML_FILENAME}

    # Entries
    for i in `seq ${NB}`
    do
        sed -e "s@__EMAIL_MODE__@${EMAIL_MODE}@g" \
            -e "s@__ACTION_TYPE__@${ACTION_TYPE}@g" \
            -e "s/__EMAIL__/${EMAIL}/g" ${ENTRY} >> ${XML_FILENAME}
    done

    # Footer
    cat ${FOOTER} >> ${XML_FILENAME}

    echo "Appel du webservice preinscription DEM avec le fluxId : ${FLUX_ID}"

    # Appel du webservice
    readonly OUTPUT_FILENAME="TEMP/preinscription_DEM_${FULLDATE}/Reponse_preinscription_DEM.txt"
    curl -s -k -u digiposte:Digi_Pass -E Emetteur_305.p12:Digiposte --cert-type P12 https://emetteur.tmc.u-post.fr/emetteur/v5/soap/preinscription --header "content-type: application/soap+xml;charset=UTF-8" --data @${XML_FILENAME} -i -o"${OUTPUT_FILENAME}"

    nb_routeCode=$(cat ${OUTPUT_FILENAME} | grep -o -E "<routeCode xsi:type=\"xsd:string\">[0-9a-z]{32}</routeCode>" | wc -l)
    echo -ne "Nombre de codes de routage reçus : ${nb_routeCode}\t"
    [[ ${nb_routeCode} -eq ${NB} ]] && echo -e "\033[32m[OK]\033[0m" || echo -e "\033[31m[KO]\033[0m"

    nb_mailQueue=$(mysqlexec "SELECT count(*)
                              FROM mail_queue
                              WHERE recipient = '${EMAIL}'
                              AND created_at >= '${created_at}'")
    echo -ne "Nombre de mails dans la table MAIL_QUEUE : ${nb_mailQueue}\t"
    [[ ${nb_mailQueue} -eq 0 ]] && echo -e "\033[32m[OK]\033[0m" || echo -e "\033[31m[KO]\033[0m"

    nb_membership=$(mysqlexec "SELECT count(*)
                               FROM membership
                               WHERE status = 2
                               AND sender_id = '${SENDER_ID}'
                               AND created_at >= '${created_at}'")
    echo -ne "Nombre d'entrées créées dans MEMBERSHIP : ${nb_membership}\t"
    [[ ${nb_membership} -eq ${NB} ]] && echo -e "\033[32m[OK]\033[0m" || echo -e "\033[31m[KO]\033[0m"
}

function preinscription_ENV () {
    readonly DIRECTORY="TEMP/preinscription_ENV_${FULLDATE}"
    mkdir ${DIRECTORY}
    readonly XML_FILENAME="${DIRECTORY}/preinscription_ENV_${FULLDATE}.xml"
    readonly HEADER=TEMPLATES_XML/PREINSCRIPTION_ENV/header.xml
    readonly FOOTER=TEMPLATES_XML/PREINSCRIPTION_ENV/footer.xml
    [[ ! -r ${HEADER} || ! -r ${FOOTER} ]] && echo -e "\nAt least one of the template file is missing or not readable. Please check the installation." && exit 1

    date=${FULLDATE}
    created_at="${date:0:4}-${date:4:2}-${date:6:2} ${date:9:2}:${date:11:2}:${date:13:2}"
    
    valid=0
    while [[ valid -eq 0 ]]
    do
        echo -n "fluxId de la preinscription DEM (14 chiffres) : "
        read FLUX_ID
        echo ${FLUX_ID} | grep -E "^[0-9]{14}$" > /dev/null
        [[ $? -eq 0 ]] && valid=1
    done

    # Header
    ACTION_TYPE="E"

    sed -e "s@__FLUX_ID__@${FLUX_ID}@g" \
        -e "s@__SENDER_PID__@${SENDER_PID}@g" \
        -e "s@__ACTION_TYPE__@${ACTION_TYPE}@g" ${HEADER} > ${XML_FILENAME}

    # Footer
    cat ${FOOTER} >> ${XML_FILENAME}

    # Appel du webservice
    readonly OUTPUT_FILENAME="TEMP/preinscription_ENV_${FULLDATE}/Reponse_preinscription_ENV.txt"
    curl -s -k -u digiposte:Digi_Pass -E Emetteur_305.p12:Digiposte --cert-type P12 https://emetteur.tmc.u-post.fr/emetteur/v5/soap/preinscription --header "content-type: application/soap+xml;charset=UTF-8" --data @${XML_FILENAME} -i -o"${OUTPUT_FILENAME}"

    nb_registrationCode=$(cat ${OUTPUT_FILENAME} | grep -o -E "<registrationCode xsi:type=\"xsd:string\">[0-9a-z]{32}</registrationCode>" | wc -l)
    echo -ne "Nombre de registrationCode reçus : ${nb_registrationCode}\t"
    [[ ${nb_registrationCode} -eq ${NB} ]] && echo -e "\033[32m[OK]\033[0m" || echo -e "\033[31m[KO]\033[0m"

    nb_mailQueue=$(mysqlexec "SELECT count(*)
                              FROM mail_queue
                              WHERE recipient = '${EMAIL}'
                              AND created_at >= '${created_at}'")
    echo -ne "Nombre de mails dans la table MAIL_QUEUE : ${nb_mailQueue}\t"
    [[ ${nb_mailQueue} -eq ${nb_registrationCode} ]] && echo -e "\033[32m[OK]\033[0m" || echo -e "\033[31m[KO]\033[0m"
}

#***************************************************#
#                       MAIN                        #
#***************************************************#

while [[ $# -gt 0 ]]
do
    case "$1" in
        '-w' )
            readonly WS_TYPE=$2
            shift 2
            ;;
        '-X' )
            readonly METHOD=$2
            shift 2
            ;;
        '-s' | '--sender' )
            readonly SENDER_PID=$2
            shift 2
            ;;
        '-n' )
            NB=$2
            shift 2
            ;;
        * )
            print_usage "$0"
            exit 1
    esac
done

if  [[ -z "${WS_TYPE}" ]] || \
    [[ -z "${METHOD}" ]] || \
    [[ -z "${SENDER_PID}" ]] || \
    [[ -z "${NB}" ]]
then
    print_usage "$0"
    exit 1
fi

# Vérification des paramètres
if [[ ${WS_TYPE} == "adhesion" ]]
then
    echo ${METHOD} | grep -E "POST|PUT|GET" > /dev/null
    result=$?
    [[ ${result} -eq 1 ]] && print_usage "$0" && exit 1
elif [[ ${WS_TYPE} == "preinscription" ]]
then
    echo ${METHOD} | grep -E "IMM|DEM|ENV" > /dev/null
    result=$?
    [[ ${result} -eq 1 ]] && print_usage "$0" && exit 1
else
    print_usage "$0" && exit 1
fi

[[ ${NB} -le 0 ]] && echo "La volumétrie doit être supérieure à 0" && exit 1

# Récupération des infos de connexion BDD
BACK_SERVER="back1-tmc"
DB_SERVER="db1-tmc"
eval $(ssh ${BACK_SERVER} "php /data/usr/local/apps/digiposte-php/current/php/legacy/digiposte/batch/batch_config.php _G_SRV_BDD_LOG_ _G_SRV_BDD_PASS_ _G_SRV_BDD_HOST_")
_G_SRV_BDD_NAME_=digiposte
_G_SRV_BDD_PORT_=3306

# Recherche de l'émetteur en BDD
echo "Recherche de l'émetteur..."
sender=$(mysqlexec "SELECT sender.name, sender.id
                    FROM sender
                    WHERE sender.pid = '${SENDER_PID}'")
[[ "${sender}" == "" ]] && echo -e "Sender non trouvé" && exit 1

readonly SENDER_NAME=$(echo "${sender}" | cut -d $'\t' -f 1)
readonly SENDER_ID=$(echo "${sender}" | cut -d $'\t' -f 2)
echo -e "Nom émetteur: ${SENDER_NAME}\tPID: ${SENDER_PID}"

# Création du dossier TEMP/ s'il n'existe pas
[[ -d "TEMP" ]] || mkdir TEMP

# Date
readonly TIMESTAMP=$(date +%s)
readonly FULLDATE=$(date --date=@${TIMESTAMP} +%Y%m%d-%H%M%S)
readonly YEAR=$(date --date=@${TIMESTAMP} +%Y)
readonly MONTH=$(date --date=@${TIMESTAMP} +%m)
readonly DAY=$(date --date=@${TIMESTAMP} +%d)
readonly HOUR=$(date --date=@${TIMESTAMP} +%H)
readonly MINUTE=$(date --date=@${TIMESTAMP} +%M)
readonly SECOND=$(date --date=@${TIMESTAMP} +%S)
readonly TIMEZONE=$(date +%:z)
readonly EXPDATE=$(date --date "+60 day" +%Y-%m-%d)

# Adresse utilisée pour l'envoi des emails de préinscription
EMAIL="pcazajous.dgp@gmail.com"

sleep 1

# Appel de la fonction choisie
case ${METHOD} in
    "POST" )
        adhesion_post
        ;;
    "GET" )
        adhesion_get
        ;;
    "PUT" )
        adhesion_put
        ;;
    "IMM" )
        preinscription_IMM
        ;;
    "DEM" )
        preinscription_DEM
        ;;
    "ENV" )
        preinscription_ENV
        ;;
    * )
        print_usage "$0"
        exit 1
esac