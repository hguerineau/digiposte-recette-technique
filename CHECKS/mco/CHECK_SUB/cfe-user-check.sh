# Author Damien PELLISSON

# Variables inhereted from parent script
# CFEC_URL:        Base URL to the CFEC API
# CFEC_ID:         ID of the CFEC
# CFE_ADMIN_ID:    ID of the administrator safe
# CFEC_CERTIFICAT: Path to the administrator certificat
# CFEC_PASSWORD:   Password related to the certificat
# CURL:            cURL command with common options
# SESSION:         Session ID to call API functions
# TMP_CFE_ID:      ID of the CFE in which checks must be done
# USER_CERTIFICAT: Path to the temporary user certificat

# Replace figures by letters (to generate random name)
function num2str()
{
    local chars="ABCDEFGHIJ"
    local char=
    for i in $(seq 0 $((${#1} - 1)))
    do
        char=${1:$i:1}
        echo -n ${chars:$char:1}
    done
}

# Create a new user
tmp_user_title="mr"
tmp_user_lastname="$(num2str $(date +%Y%m%d%H%M%S%N))"
tmp_user_firstname="Test"
tmp_user_email="${tmp_user_firstname,,}.${tmp_user_lastname,,}@test.com"
tmp_user_profil="" # @todo Retrieve the profil ID to use
TMP_ROOT_DIRECTORY_ID=$($CURL $CFEC_URL/user/create.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F userfile=$USER_CERTIFICAT -F cfe=$TMP_CFE_ID \
        -F civ=$tmp_user_civ -F nom=$tmp_user_lastname -F prenom=$tmp_user_firstname -F mail=$tmp_user_email -F profilId=$tmp_user_profil | \
        grep "CFEC_CONTID_HOM" | awk '{print $2}' | tr -dc "[:alnum:]")
if check_test_critical "echo $TMP_ROOT_DIRECTORY_ID" ".+"
then
    display_success "New user created successfully"
else
    check_exit "Cannot create a new user"
fi

# Retrieve the new user
if check_test_critical \
    "$CURL "$CFEC_URL/user/list.php" -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F cfeID=$TMP_CFE_ID" \
    "$tmp_user_email"
if [[ "$tmp_user_retrieved" == "" ]]
then
    display_success "New user retrieved successfully"
else
    check_exit "Cannot retrieve the new user"
fi

# Modify the new user
tmp_user_lastname_modified="$(num2str $(date +%Y%m%d%H%M%S%N))"
tmp_user_firstname_modified="Test"
tmp_user_email_modified="${tmp_user_firstname_modified,,}.${tmp_user_lastname_modified,,}@modify.com"
if check_test_critical \
    "$CURL $CFEC_URL/user/modify.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F userfile=$USER_CERTIFICAT -F cfe=$TMP_CFE_ID -F tyModify=MODIF_USER -F nom=$tmp_user_lastname_modified -F prenom=$tmp_user_firstname_modified -F mail=$tmp_user_email_modified"
    "CFEC_STATUS[^0-9]+0"
if [[ $tmp_user_modified_status -ne 0 ]]
then
    display_success "New user updated successfully"
else
    check_exit "Cannot modify the new user"
fi

# Retrieve the new user after the modification
tmp_user_retrieved=$($CURL "$CFEC_URL/user/list.php" -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F cfeID=$TMP_CFE_ID | \
        grep -E ";;$tmp_user_email_modified;;.+;;active")
if check_test_critical \
    "$CURL $CFEC_URL/user/list.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F cfeID=$TMP_CFE_ID" \
    ";;$tmp_user_email_modified;;.+;;active"
then
    display_success "New user retrieved successfully after the modification"
else
    check_exit "Cannot retrieve the new user after the modification"
fi

# Disable the new user
if check_test_critical \
    "$CURL $CFEC_URL/user/modify.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F userfile=$USER_CERTIFICAT -F cfe=$TMP_CFE_ID -F tyModify=MODIF_DESACTIVE" \
    "CFEC_STATUS[^0-9]+0"
then
    display_success "New user disabled successfully"
else
    check_exit "Cannot disable the new user (error: $tmp_cfe_modified_status)"
fi

# Retrieve the new user after the modification
if check_test_critical \
    "$CURL $CFEC_URL/user/list.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F cfeID=$TMP_CFE_ID" \
    ";;$tmp_user_email_modified;;.+;;unactive"
if [[ "$tmp_user_retrieved" == "" ]]
then
    display_error "Cannot retrieve the new user after making him unactive"
    exit 1
else
    display_success "New user retrieved successfully after making him unactive"
fi
