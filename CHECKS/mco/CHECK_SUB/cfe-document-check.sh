# Author Damien PELLISSON

# Variables inhereted from parent script
# CFEC_URL:              Base URL to the CFEC API
# CFEC_ID:               ID of the CFEC
# CFE_ADMIN_ID:          ID of the administrator safe
# CFEC_CERTIFICAT:       Path to the administrator certificat
# CFEC_PASSWORD:         Password related to the certificat
# CURL:                  cURL command with common options
# SESSION:               Session ID to call API functions
# TMP_CFE_ID:            ID of the CFE in which checks must be done
# TMP_ROOT_DIRECTORY_ID: ID of the root document of the temporary user
# TMP_DIRECTORY_ID:      ID of a document which is in the root document
# DOCUMENT_ARCHIVE:      Path to the archive to upload

# Upload a new document in the root directory
tmp_document_name=$(basename $DOCUMENT_ARCHIVE)
tmp_document_size=$(stat -c%s $DOCUMENT_ARCHIVE)
TMP_DOCUMENT_ID=$($CURL $CFEC_URL/doc/upldxml.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION \
        -F contId=$TMP_ROOT_DIRECTORY_ID -F userfile=$DOCUMENT_ARCHIVE -F algoEmpreinte=MD5 -F empreinte=$(md5sum $DOCUMENT_ARCHIVE) | \
        grep "CFEC_ARCHID" | awk '{print $2}' | tr -dc "[:alnum:]")
if check_test_critical "echo $TMP_DOCUMENT_ID" ".+"
then
    display_success "New document created successfully"
else
    check_exit "Cannot create a new document"
fi

# Find the new document
if check_test_critical \
    "$CURL $CFEC_URL/doc/find.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F contId=$TMP_ROOT_DIRECTORY_ID -F archName=$tmp_document_name" \
    "$TMP_DOCUMENT_ID"
then
    display_success "New document retrieved successfully"
else
    check_exit "Cannot retrieve the new document"
fi

# Download the new document
tmp_document_dnld=$($CURL $CFEC_URL/doc/dnldxml.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F archId=$TMP_DOCUMENT_ID -F mode=org | \
        sed 's/$/\\n/g')
if check_test_critical "echo -e \"$tmp_document_dnld\"" "CFEC_FILENAME.+$tmp_document_name" && \
    check_test_critical "echo -e \"$tmp_document_dnld\"" "CFEC_ARCHVOLM.+$tmp_document_dnld_size"
then
    display_success "New document downloaded successfully"
else
    check_exit "Cannot download the new document"
fi

# Move the new document in the sub directory
tmp_document_moved_status=$($CURL "$CFEC_URL/doc/move.php" -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION \
        -F archId=$TMP_DOCUMENT_ID -F contId=$TMP_DIRECTORY_ID \
        grep "CFEC_STATUS" | awk '{print $2}' | tr -dc "[:alnum:]")
if check_test_critical \
    "$CURL "$CFEC_URL/doc/move.php" -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F archId=$TMP_DOCUMENT_ID -F contId=$TMP_DIRECTORY_ID" \
    "CFEC_STATUS[^0-9]0"
then
    display_success "New document moved successfully"
else
    check_exit "Cannot move the new document"
fi

# Get the information about the new document
tmp_document_stat=$($CURL "$CFEC_URL/doc/stat.php" -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION \
        -F archId=$TMP_DOCUMENT_ID | \
        sed 's/$/\\n/g')
if check_test_critical "echo -e $tmp_document_stat" "CFEC_DOCPARENT.+$TMP_DIRECTORY_ID" && \
    check_test_critical "echo -e $tmp_document_stat" "CFEC_DOCNAME.+$tmp_document_name"
then
    display_success "New document information retrieved successfully"
else
    check_exit "Cannot retrieve the new document information"
fi

# Delete the new document
tmp_document_deleted_status=$($CURL "$CFEC_URL/doc/delete.php" -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION \
        -F archId=$TMP_DOCUMENT_ID \
        grep "CFEC_STATUS" | awk '{print $2}' | tr -dc "[:alnum:]")
if check_test_critical \
    "$CURL $CFEC_URL/doc/delete.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F archId=$TMP_DOCUMENT_ID" \
    "CFEC_STATUS[^0-9]0"
then
    display_error "Cannot delete the new document"
    exit 1
else
    display_success "New document deleted successfully"
fi

# Restaure the new document
tmp_document_restaured_status=$($CURL "$CFEC_URL/doc/remove.php" -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION \
        -F archId=$TMP_DOCUMENT_ID \
        grep "CFEC_STATUS" | awk '{print $2}' | tr -dc "[:alnum:]")
if check_test_critical \
    "$CURL "$CFEC_URL/doc/remove.php" -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F archId=$TMP_DOCUMENT_ID" \
    "CFEC_STATUS[^0-9]0"
then
    display_success "New document restaured successfully"
else
    check_test "Cannot restaure the new document"
fi
