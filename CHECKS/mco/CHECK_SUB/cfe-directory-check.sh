# Author Damien PELLISSON

# Variables inhereted from parent script
# CFEC_URL:              Base URL to the CFEC API
# CFEC_ID:               ID of the CFEC
# CFE_ADMIN_ID:          ID of the administrator safe
# CFEC_CERTIFICAT:       Path to the administrator certificat
# CFEC_PASSWORD:         Password related to the certificat
# CURL:                  cURL command with common options
# SESSION:               Session ID to call API functions
# TMP_CFE_ID:            ID of the CFE in which checks must be done
# TMP_ROOT_DIRECTORY_ID: ID of the root directory of the temporary user

# Go to the user root directory
if check_test_critical \
    "$CURL $CFEC_URL/fld/change.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F contId=$TMP_ROOT_DIRECTORY_ID" \
    "CFEC_STATUS[^0-9]+0"
then
    display_success "Go to the temporary user root directory"
else
    check_exit "Cannot go to the temporary user root directory"
fi

# Create a new directory
tmp_directory_name="FLD_$(date +%Y%m%d%H%M%S%N)"
TMP_DIRECTORY_ID=$($CURL $CFEC_URL/fld/create.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION \
        -F contIdParent=0 -F contName=$tmp_directory_name | \
        grep "CFEC_CONTID" | awk '{print $2}' | tr -dc "[:alnum:]")
if check_test_critical "echo $TMP_DIRECTORY_ID" ".+"
then
    display_success "New directory created successfully"
else
    check_exit "Cannot create a new directory"
fi

# Retrieve the new directory
if check_test_critical \
    "$CURL $CFEC_URL/fld/list.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F contId=0" \
    "$TMP_DIRECTORY_ID"
then
    display_success "New directory retrieved successfully"
else
    check_exit "Cannot retrieve the new directory"
fi

# Rename the new directory
tmp_directory_name_modified="${tmp_directory_name}_renamed"
if check_test_critical \
    "$CURL $CFEC_URL/fld/rename.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F contId=$TMP_DIRECTORY_ID -F contName=$tmp_directory_name_modified" \
    "CFEC_STATUS[^0-9]+0"
then
    display_success "New directory renamed successfully"
else
    check_exit "Cannot rename the new directory"
fi

# Retrieve the new directory after the modification
if check_test_critical \
    "$CURL $CFEC_URL/fld/name.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F contId=$TMP_DIRECTORY_ID" \
    "CFEC_CONTNAME.+$tmp_directory_name_modified"
then
    display_success "New directory retrieved successfully after the modification"
else
    check_exit "Cannot retrieve the new directory after the modification"
fi

# Create other directory for the following checks
tmp_directory2_name="FLD2_$(date +%Y%m%d%H%M%S%N)"
tmp_directory2_id=$($CURL $CFEC_URL/fld/create.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION \
        -F contIdParent=0 -F contName=$tmp_directory2_name)

# Move the 2nd directory in the first one
if check_test_critical \
    "$CURL $CFEC_URL/fld/move.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F contOrigId=$TMP_ROOT_DIRECTORY_ID -F contDestId=$TMP_DIRECTORY_ID" \
    "CFEC_STATUS[^0-9]+0"
then
    display_success "New directory moved successfully"
else
    check_exit "Cannot move the new directory"
fi

# Delete the 2nd directory
if check_test_critical \
    "$CURL $CFEC_URL/fld/delete.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION  -F contId=$tmp_directory2_id" \
    "CFEC_STATUS[^0-9]+0"
then
    display_success "New directory deleted successfully"
else
    check_exit "Cannot move the new directory"
fi

# Check the 2nd directory cannot be found anymore
if check_test_critical \
    "$CURL $CFEC_URL/fld/name.php -E $CFEC_CERTIFICAT:$CFEC_PASSWORD -F CFEC_SESSION=$SESSION -F contId=$tmp_directory2_id" \
    "!CFEC_CONTNAME.+$tmp_directory_name"
then
    display_success "Cannot retrieve the closed directory"
else
    check_exit "The removed directory has been retrieved after its deletion"
fi

# The first directory should be removed in cfe-check.sh (with purge)
