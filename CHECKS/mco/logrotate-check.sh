#! /bin/bash

# Author Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

HOSTNAME=$(hostname -f)
LOG_DIRS=$1

check_init "logrotate"

# Check if logrotate is installed
if ! check_test_critical "test -x /usr/sbin/logrotate" ""
then
    check_exit "logrotate is not installed"
fi

# Check if logrotate is in the crontab
if ! check_test_critical "test -f /etc/cron.daily/logrotate" ""
then
    check_exit "logrotate is not in the daily crontab"
fi

# Check if cron.daily is launched at midnight
if ! check_test_critical "grep '/etc/cron.daily' /etc/crontab" "^0\s+0\s+\*\s+\*\s+\*"
then
    display_error "logrotate is not launched at midnight"
fi

# Browse each log directory which contains logs
for logDir in $LOG_DIRS
do
    # Look for application log files
    for logFile in $(sudo find -P $logDir -name *.log -mtime -12  2> /dev/null)
    do
        logFileName=$(basename $logFile)
        logFileNameWithoutExtension=${logFileName%.*}
        logFileDir=$(dirname $logFile)

	if ! check_test_warning "sudo cat $logFile | wc -l | awk '{ if (\$1 > 20000) { print 1 } else { print 0 } }'" "0"
        then
            display_warning "$logFile: This file contains more than 20,000 lines"
        fi

        # Look for the configuration file
        configLocation=$(sudo grep -rn "$logFile" /etc/logrotate.conf /etc/logrotate.d/ | tail -n 1)
        if [[ -z "$configLocation" ]]
        then
            configLocation=$(sudo grep -worn "$logFileDir[^ ]*log" /etc/logrotate.conf /etc/logrotate.d/)
            echo "$logFile" | grep "$(echo "$configLocation" | cut -d : -f 3 | sed 's/\*/.*/g')" > /dev/null || configLocation=""
        fi

        if [[ -n "$configLocation" ]]
        then
            # Retrieve the configuration for the current log file
            configFile=$(echo $configLocation | cut -d ":" -f 1)
            configLineStart=$(echo $configLocation | cut -d ":" -f 2)
            configLineStart=$(($configLineStart + $(cat $configFile | tail -n +$configLineStart | grep -n "{" | head -n 1 | cut -d ":" -f 1) - 1))
            configLineEnd=$(cat $configFile | tail -n +$configLineStart | grep -n "}" | head -n 1 | cut -d ":" -f 1)
            config=$(cat $configFile | tail -n +$configLineStart | head -n $configLineEnd | sed 's/$/\\n/g')

            if ! check_test_critical "echo -e '$config'" "rotate\s+10"
            then
                display_error "$logFile: Bad number of rotated files"
            fi

            if ! check_test_critical "echo -e '$config'" "daily"
            then
                display_error "$logFile: Bad rotation frequency"
            fi

            if ! check_test_critical "echo -e '$config'" "compress" || ! check_test_critical "echo -e '$config'" "!nocompress"
            then
                display_error "$logFile: Old log files are not compressed"
            fi

            if ! check_test_critical "echo -e '$config'" "dateext" || ! check_test_critical "echo -e '$config'" "!nodateext"
            then
                display_error "$logFile: Old log files do not contain the date in their name"
            fi
        else
            nbCompressedFiles=$(sudo ls $logFile*.gz 2> /dev/null | wc -l)
            if ! check_test_warning "echo $nbCompressedFiles" "^10$"
            then
                if check_test_critical "echo $nbCompressedFiles | awk '{ if (\$1 > 12) { print 1 } else { print 0 } }'" "0"
                then
                    display_warning "$logFile: There are $nbCompressedFiles old files"
                else
                    display_error "$logFile: Too many old files ($nbCompressedFiles)"
                fi
            fi

            if ! check_test_critical "sudo find $logFileDir -name $logFileName*.gz -mtime -12 | wc -l" "^$nbCompressedFiles$"
            then
                display_error "$logFile: Bad rotation frequency"
            fi

            sudo ls -l --time-style=+%Y-%m-%d $logFile*.gz 2> /dev/null | while read fileDesc
            do
                date=$(echo "$fileDesc" | awk '{print $6}')
                year=$(echo "$date" | cut -d - -f 1)
                month=$(echo "$date" | cut -d - -f 2)
                day=$(echo "$date" | cut -d - -f 3)
                compressedFileName=$(echo "$fileDesc" | awk '{print $7}' |
                        sed "s/$year/___YEAR___/" |
                        sed "s/$month/___MONTH____/" |
                        sed "s/$day/___DAY___/")

                if check_test_critical "echo '$compressedFileName'" "___YEAR___"
                then
                    if ! check_test_critical "echo '$compressedFileName'" "___MONTH___" || \
                        ! check_test_critical "echo '$compressedFileName'" "___DAY___"
                    then
                        display_warning "$logFile: Old log files do not contain the last modification date in their name"
                        break
                    fi
                else
                    display_error "$logFile: Old log files do not contain the date in their name"
                    break
                fi
            done
        fi
    done
done

check_exit "logrotate is properly configured" ""
