#!/bin/bash

# Author Ayoub EL MAAROUF (WWSight)
# @Desciption : The script verifies 
#				1- mongo.digiposte.local is an alias of log-web.digiposte.local
#				2- es.digiposte.local is an alias of log-collector.digiposte.local


source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "drbd-status"

mongo_OUT=$(grep -E "^(mongo)" /etc/bind/zones/digiposte.local) 
es_OUT=$(grep -E "^(es)" /etc/bind/zones/digiposte.local) 

if ! check_test_critical "echo $mongo_OUT | cut -d' ' -f4" "log-web"
then
    display_error "incorrect alias for mongo.digiposte.local"
fi
if ! check_test_critical "echo $es_OUT | cut -d' ' -f4" "log-collector"
then
    display_error "incorrect alias for es.digiposte.local"
fi

check_exit "mongo / es status OK : $mongo_OUT\n $es_OUT" ""
