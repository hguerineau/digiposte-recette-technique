#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON, Pierre CAZAJOUS

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "logstash"

isDrbdSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service logstash-agent status" "logstash-agent start/running, process [0-9]+"
    then
        display_error "Logstash is not running"
    elif ! check_test_critical "ps -p \$(cat /var/run/logstash/agent/logstash.pid 2> /dev/null) -o command" "java"
    then
        display_error "Cannot find the pid related to Logstash"
    fi
else
    if ! check_test_critical "sudo service logstash-agent status" "logstash-agent stop/waiting"
    then
        display_error "Logstash is running on the secondary server"
    fi
fi

check_exit "Logstash is working properly" ""
