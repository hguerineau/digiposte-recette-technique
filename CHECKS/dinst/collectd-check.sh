#! /bin/bash

# Author        Damien PELLISSON
# Contributor   Pierre CAZAJOUS

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "collectd"

isDrbdSecondary=$(sudo facter -p drbd_is_secondary)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service collectd status" "collectd is running"
    then
        display_error "Collectd is not running"
    fi
else
    if ! check_test_critical "sudo service collectd status" "collectd is stopped"
    then
        display_error "Collectd is running on the secondary server"
    fi
fi

check_exit "Collectd is working properly" ""
