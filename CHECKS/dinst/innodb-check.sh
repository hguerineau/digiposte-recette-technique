#! /bin/bash

# Author Mathieu LEMARCHAND
# @Desciption : This script checks the server buffer size

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "innodb"

BUFFERSIZE_NEEDED=$1
PSWD=$2
BUFFERSIZE_ACCEPTED="${BUFFERSIZE_NEEDED:0:1}$(echo ${BUFFERSIZE_NEEDED:1} | sed 's/[0-9]/./g')"
SERVER_BUFFERSIZE=`mysql -uroot -p${PSWD} -e 'show variables;' | grep innodb_buffer_pool_size | cut -f2`

if check_test_warning "echo $SERVER_BUFFERSIZE" "^$BUFFERSIZE_NEEDED$"
then
        display_success "Needed buffer: $BUFFERSIZE_NEEDED = Configured buffer: $SERVER_BUFFERSIZE Ok"
elif  check_test_warning "echo $SERVER_BUFFERSIZE" "^$BUFFERSIZE_ACCEPTED$"
then
        display_success "Needed buffer: $BUFFERSIZE_NEEDED approximatly = Configured buffer: $SERVER_BUFFERSIZE Ok"
else
        display_error "Needed buffer: $BUFFERSIZE_NEEDED Ko - Configured buffer: $SERVER_BUFFERSIZE Ko"
fi

check_exit "innoDB is working properly" ""
