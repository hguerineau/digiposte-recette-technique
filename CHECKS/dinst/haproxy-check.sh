#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "haproxy"

isDrbdSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service haproxy status" "haproxy is running"
    then
        display_error "HAProxy is not running"
    fi
else
    if ! check_test_critical "sudo service haproxy status" "haproxy not running"
    then
        display_error "HAProxy is running on the secondary server"
    fi
fi

check_exit "HAProxy is working properly" ""
