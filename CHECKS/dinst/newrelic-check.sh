#!/bin/bash

# Author Pierre CAZAJOUS

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "newrelic"

if ! check_test_critical "curl -v -L http://rpm.newrelic.com" "HTTP/1.1 200 OK"
then
	display_error "Cannot access http://rpm.newrelic.com"
fi

if ! check_test_critical "curl -v https://rpm.newrelic.com" "HTTP/1.1 200 OK"
then
	display_error "Cannot access https://rpm.newrelic.com"
fi

check_exit "Successful requests (http and https) to newrelic" ""
