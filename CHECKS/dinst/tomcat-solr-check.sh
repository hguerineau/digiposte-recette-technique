#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "tomcat-solr"

isDrbdSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service tomcat-solr status" "tomcat-solr.*is running..."
    then
        display_error "Tomcat SolR is not running"
    elif ! check_test_critical "curl -sIL idx.digiposte.local:8414/solr" "HTTP/1.1 200 OK"
    then
        display_error "Cannot access idx.digiposte.local:8414.solr"
    fi
else
    if ! check_test_critical "sudo service tomcat-solr status" "tomcat-solr.*is stopped"
    then
        display_error "Tomcat SolR is running on the secondary server"
    fi
fi

check_exit "Tomcat SolR is working properly" ""
