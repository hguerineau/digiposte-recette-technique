#! /bin/bash

# Author    Pierre CAZAJOUS

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "tomcat-web-wsemetteurs"

check_test_critical "service tomcat-digiposte-java-ws-sender status" "tomcat-digiposte-java-ws-sender .* is running"

check_exit "Tomcat for the Digiposte WS_Emetteurs works properly" "Tomcat for the Digiposte WS_Emetteurs is not running"
