#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "php-fpm"

isDrbdSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service php5-fpm status" "php5-fpm is running"
    then
        display_error "PHP-FPM is not running"
    elif ! check_test_critical "sudo ps aux" "php-fpm: pool www"
    then
        display_error "Cannot find the pid related to PHP-FPM"
    fi
else
    if ! check_test_critical "sudo service php5-fpm status" "php5-fpm is not running"
    then
        display_error "PHP-FPM is running on the secondary server"
    fi
fi

check_exit "PHP-FPM is working properly" ""
