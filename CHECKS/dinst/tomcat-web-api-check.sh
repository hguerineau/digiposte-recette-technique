#! /bin/bash

# Author    Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "tomcat-web-api"

check_test_critical "sudo service tomcat-digiposte-java-api status" "tomcat-digiposte-java-api .* is running"

check_exit "Tomcat for the Digiposte API works properly" "Tomcat for the Digiposte API is not running"
