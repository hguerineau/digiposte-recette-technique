#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "elasticsearch"

isDrbdSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service elasticsearch status" "ElasticSearch Server is running"
    then
        display_error "ElasticSearch is not running"
    fi
    if ! check_test_critical "curl -sil http://es.digiposte.local:9200" "ok.*true"
    then
        display_error "Cannot access http://es.digiposte.local:9200"
    fi
    if ! check_test_critical "sudo ps -p \$(cat /var/run/elasticsearch.pid 2> /dev/null) -o command" "java"
    then
        display_error "Cannot find the pid related to ElasticSearch"
    fi
else
    if ! check_test_critical "sudo service elasticsearch status" "ElasticSearch Server is not running"
    then
        display_error "ElasticSearch is running on the secondary server"
    fi
fi

check_exit "ElasticSearch is working properly" ""
