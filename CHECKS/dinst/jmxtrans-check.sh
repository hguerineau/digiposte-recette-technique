#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "jmxtrans"

isDrbdSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service jmxtrans status" "jmxtrans appears to be running"
    then
        display_error "JMX Transformer is not running"
    elif ! check_test_critical "sudo ps aux" "jmxtrans"
    then
        display_error "Cannot find the pid related to JMX Transformer"
    fi
else
    if ! check_test_critical "sudo service jmxtrans status" "jmxtrans is not running"
    then
        display_error "JMX Transformer is running on the secondary server"
    fi
fi

check_exit "JMX Transformer is working properly" ""
