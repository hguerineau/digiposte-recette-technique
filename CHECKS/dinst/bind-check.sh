#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "bind"

isDrbdSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service bind9 status" "bind9 is running"
    then
        display_error "Bind is not running"
    fi
else
    if ! check_test_critical "sudo service bind9 status" "could not access PID file for bind9"
    then
        display_error "bind is running on the secondary server"
    fi
fi

check_exit "Bind is working properly" ""
