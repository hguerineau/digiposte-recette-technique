#! /bin/bash

# Author	Pierre CAZAJOUS

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "graylog"

isSecondary=$(sudo facter -p drbd_is_secondary)

if ! $isSecondary
then
    if ! check_test_critical "service graylog2-server status" "Graylog2 server is up"
    then
        display_error "Graylog is not running"
    fi
else
    if ! check_test_critical "service graylog2-server status" "Graylog2 server is down"
    then
        display_error "Graylog is running on the secondary server"
    fi
fi

check_exit "Graylog is working properly" ""
