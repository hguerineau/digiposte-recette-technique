#!/bin/bash

# Author Pierre CAZAJOUS

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "batchs-worker"

# The list of worker batchs defined in CONFIG
REFERENCE_BATCHS_LIST=$1

# The list of worker batchs installed on the server
SERVER_BATCHS_LIST=$(ls -1 /etc/init.d/batch-*-worker | tr '\n' ' ')

for batch in $REFERENCE_BATCHS_LIST
do
	if ! check_test_critical "echo $SERVER_BATCHS_LIST" "$batch"
	then
		display_error "The worker batch \"$batch\" is missing"
	fi

	batch_name=${batch:12} 		# supprime "/etc/init.d/"
	if ! check_test_critical "sudo $batch status" "Service ${batch_name} is running with pid [0-9]+"
	then
		display_error "The worker batch \"${batch_name}\" is not running"
	fi
done

check_exit "The worker batchs are properly installed and running" ""
