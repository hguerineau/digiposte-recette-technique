#!/bin/bash

# Author Pierre CAZAJOUS

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "batchs-non-worker"

# The list of non-worker batchs defined in CONFIG
REFERENCE_BATCHS_LIST=$1

# The list of non-worker batchs installed on the server
SERVER_BATCHS_LIST=$(ls -1 /etc/init.d/batch-* | grep -v worker | tr '\n' ' ')

for batch in $REFERENCE_BATCHS_LIST
do
	if ! check_test_critical "echo $SERVER_BATCHS_LIST" "$batch"
	then
		display_error "The non-worker batch \"$batch\" is missing"
	fi
done

check_exit "The non-worker batchs are properly installed" ""
