#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "carbon-cache"

isDrbdSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service carbon-cache status" "carbon-cache is running"
    then
        display_error "Carbon-Cache is not running"
    elif ! check_test_critical "sudo ps -p \$(cat /data/opt/graphite/storage/carbon-cache*.pid 2> /dev/null) -o command" "carbon-cache.py start"
    then
        display_error "Cannot find the pid related to carbon-cache"
    fi
else
    if ! check_test_critical "sudo service carbon-cache status" "carbon-cache is stopped"
    then
        display_error "Carbon-Cache is running on the secondary server"
    fi
fi

check_exit "Carbon-Cache is working properly" ""
