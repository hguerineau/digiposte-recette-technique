#! /bin/bash

# Author Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "crontab"

for user in $(sudo awk -F ':' '{print $1}' /etc/passwd)
do
    crontab="sudo crontab -u $user -l 2>&1"

    # If the user should have tasks in his crontab
    if echo "$*" | grep -E "(^| )$user:" > /dev/null
    then
        nbTasks=0
        for task in "$@"
        do
            # Check if the task belongs to this user
            if echo "$task" | grep -E "^$user:" > /dev/null
            then
                (( nbTasks++ ))
                cmdRegEx=$(echo "$task" | cut -d : -f 2-)
                if ! check_test_critical "$crontab" "^$cmdRegEx"
                then
                    display_error "The $user's crontab does not contain the task \"$cmdRegEx\""
                fi
            fi
        done

        # Check if the crontab contains the right number of tasks
        if ! check_test_critical "$crontab | grep -E '^\s*[^#]' | wc -l" "$nbTasks"
        then
            display_error "The $user's crontab does not contain the right number of tasks"
        fi
    # If the user should not have task in his crontab
    else
        # Check if the crontab is empty
        if ! check_test_warning "$crontab" "no crontab for $user"
        then
            # In case it is not, check if it contains only comment
            if check_test_critical "$crontab" "!^\s*[^#]"
            then
                display_warning "The $user's crontab is not empty but contains only comments"
            else
               display_error "The $user's crontab is not empty"
            fi
        fi
    fi
done

check_exit "All crontabs are set properly" ""
