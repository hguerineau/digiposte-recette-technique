#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "apache"

isDrbdSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service apache2 status" "Apache2 is running"
    then
        display_error "Apache is not running"
    elif ! check_test_critical "sudo apache2ctl -M 2>&1" "Syntax OK"
    then
        display_error "Syntax error in configuration files"
    elif ! check_test_critical "curl -s http://localhost/server-status" "Apache Server Status for localhost"
    then
        display_error "Cannot acccess http://localhost/server-status"
    fi
else
    if ! check_test_critical "sudo service apache2 status" "Apache2 is NOT running"
    then
        display_error "Apache is running on the secondary server"
    fi
fi

check_exit "Apache is working properly" ""
