#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "mongodb"

isDrbdSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service mongodb status" "mongodb start\/running"
    then
        display_error "MongoDB is not running"
    elif ! check_test_critical "curl -isl http://log-web.digiposte.local:28017/serverStatus" "uptime"
    then
        display_error "Cannot access http://log-web.digiposte.local:28017/serverStatus"
    fi
else
    if ! check_test_critical "sudo service mongodb status" "mongodb stop/pause"
    then
        display_error "MongoDB is running on the secondary server"
    fi
fi

check_exit "MongoDB is working properly" ""
