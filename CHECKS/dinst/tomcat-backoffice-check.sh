#! /bin/bash

# Author    Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "tomcat-backoffice"

check_test_critical "sudo service tomcat-digiposte-java-backoffice status" "tomcat-digiposte-java-backoffice .* is running"

check_exit "Tomcat for the Digiposte Backoffice works properly" "Tomcat for the Digiposte Backoffice is not running"
