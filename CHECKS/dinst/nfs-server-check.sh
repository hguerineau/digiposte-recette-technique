#! /bin/bash

# Author        Claude SEGURET
# Contributor   Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "nfs-server"

isDrbdSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service nfs-kernel-server status" "nfsd running"
    then
        display_error "NFS daemon is not running"
    fi
else
    if ! check_test_critical "sudo service nfs-kernel-server status" "nfsd not running"
    then
        display_error "NFS daemon is running on the secondary server"
    fi
fi

check_exit "NFS daemon works properly" ""
