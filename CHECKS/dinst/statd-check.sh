#! /bin/bash

# Author Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "statsd"

isDrbdSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service statsd status" "statsd start/running"
    then
        display_error "statsd is not running"
    elif ! check_test_critical "ps -p \$(cat /var/run/statsd.pid) -o command" ".+"
    then
        display_error "Cannot find the pid related to statsd"
    fi
else
    if ! check_test_critical "sudo service statsd status" "statsd stop/waiting"
    then
        display_error "statsd is running on the secondary server"
    fi
fi

check_exit "statsd works properly" ""
