#! /bin/bash

# Author Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "mysql"

isDrbdSecondary=$([[ "$(drbd-overview | grep 'Secondary/')" == "" ]] && echo false || echo true)

if ! $isDrbdSecondary
then
    if ! check_test_critical "sudo service mysql status" "mysql start/running"
    then
        display_error "MySQL is not running"
    fi
else
    if ! check_test_critical "sudo service mysql status" "mysql stop/pause|mysql stop/waiting"
    then
        display_error "MySQL is running on the secondary server"
    fi
fi

check_exit "MySQL works properly" ""
