#! /bin/sh

# Author Damien PELLISSON

source ../../LIBS/display.fct
source ../../LIBS/check.fct

check_init "users"

for user in $*
do
    echo "checking $user"
    if ! check_test_critical "sudo cat /etc/passwd" "^$user:"
    then
        display_error "The user $user does not exist"
    fi

    if cat /etc/ssh/sshd_config | grep -E "^AllowUsers" > /dev/null
    then
        if ! check_test_critical "cat /etc/ssh/sshd_config" "^AllowUsers.*[^A-Za-z0-9]$user([^A-Za-z0-9]|$)"
        then
            display_error "The user $user is not in the SSH AllowUsers list"
        fi
    fi
done

check_exit "All users are properly defined" ""
